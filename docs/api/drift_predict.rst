.. automodule:: driftutils.drift_predict
    :members:

    Notes
    -----
    Some examples of possible values for the drifter depth parameter:

    * '15.0': Starting depth of 15.0 m
    * '15.0c': Constant depth of 15.0 m
    * 'L15': Starting depth corresponding to 15th depth layer
    * 'L15c': Constant depth corresponding to 15th depth layer
    * 'L15.2c': Constant depth corresponding to the "15.2"th depth layer.
      Note non-integer values are permitted to introduce shift with respect
      to the exact position of the depth level. This value corresponds to
      a depth partway between the 15th and 16th depth level.
