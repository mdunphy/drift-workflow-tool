*******************
Drift Utilities API
*******************

:Author: Clyde Clements
:Created: 2018-03-19

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   assemble_drift_predictions
   assemble_drifter_metadata
   assemble_ocean_metadata
   assemble_drifter_data
   assemble_ocean_data
   drift_predict
   find_nearest_grid_point
   get_skill
   set_initial_positions
   plot_skill_score
