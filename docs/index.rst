******************
Drift Verification
******************

:Author: Clyde Clements
:Created: 2017-11-14
:Date: 2018-03-19

This project contains software (primarily Python scripts) to perform
verification and validation of ocean models using drift trajectories.
Trajectories of drifter buoys are compared with modelled trajectories,
currently calculated using Ariane.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   data_inputs
   data_structures
   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
