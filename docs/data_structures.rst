Data Structures
===============

:Author: Clyde Clements
:Created: 2017-11-14 11:00:03 -0330
:Date: 2017-11-14 11:00:08 -0330


Output from drift-compare run (experiment):

- ``runs.yaml``: contains the following entries

  - ``runs``: a dictionary with keys given by the drift start date and the
    number of drift hours; e.g., ``20170710_0072``. The values are themselves
    dictionaries; for example::

      '20170710_0072':
        ariane_finish_time: '2017-11-03T18:40:39.651357'
        ariane_start_time: '2017-11-03T18:40:17.416055'
        ariane_status: 0
        drift_start_date: '2017-07-10T00:00:00'
        drift_end_date: '2017-07-13T00:00:00'
        run_dir: run_20170710_0003

  - other metadata items; for example::

      updated: '2017-11-03T18:40:39.875397'
      workflow_finish_time: '2017-11-03T18:40:39.875397'
      workflow_start_time: '2017-11-03T18:39:00.609892'

- ``drifters.yaml``: metadata pertaining to all available drifters. Contains
  the following entries:

  - ``drifters``: a list with each item being a dictionary; for example::

      drifters:
      - buoyid: '300234063265890'
        deployment: Hudson AR7W LIne
        description: CONCEPTS Ocean Drifter 300234063265890
        filename: ../data/drifters/300234063265890.nc
        imei: '300234063265890'
        last_data_date: '2017-08-16T10:59:12.000000000'
        start_data_date: '2016-05-17T13:00:48.000000000'
        status: normal
        wmo: '4401601'
      - buoyid: '300234063264890'
        deployment: CCG Terry Fox
        description: CONCEPTS Ocean Drifter 300234063264890
        filename: ../data/drifters/300234063264890.nc
        imei: '300234063264890'
        last_data_date: '2017-08-16T12:01:04.000000000'
        start_data_date: '2016-10-08T11:26:56.000000000'
        status: normal
        wmo: '4401633'

  - ``updated``: string containing the time that the file was last updated
