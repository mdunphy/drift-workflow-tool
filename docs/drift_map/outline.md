# Outline for Drift Map development

* Drift map will be used to generate maps of drift patterns.
* The ocean model domain will be populated with many particles.
* Ariane will compute trajectories.
* We will plot results on a map.
* There are NO evaluation metrics with drift_map.

## Example of drift map product

![Example map](examplemap.png)



## Sketch of drift_map.py

```python
   drift_map(experiment_dir,
             ocean_data_dir,
             ocean_mesh_file,
             first_start_date,
             last_start_date,
             drift_duration,
             initial_bbox = None,
             num_particles_x = 50,
             num_particles_y = 50,
             ...)
```

* All other optional inputs from `drift_predict` will be used, except `drifter_id_attr` and `drifter_metadata_variables`.
* `initial_bbox` - bounding box of initial drift release (LON_MIN, LAT_MIN, LON_MAX, LAT_MAX). If None, the entire model domain is used.
* `num_particles_x`, `num_particles_y` are the number of particles initialized along the x and y dimensions. 

## Pseudo-code for drift_map
`drift_map` will follow a similar structure to `drift_predict` but without dealing with drifter data.

1. Set up run details (like `drift_predict`)
2. Assemble ocean meta data
3. Set up each drifter run (rrule, etc)
4. For each drifter run
    * Create run directory
    * Assemble ocean data
    * Set mapped initial positions ---> will need a new function with arguments `initial_bbox`, `num_particles_x`, `num_particles_y` and other relevant information about the ocean model.
    * Make ariane namelist
    * Run ariane
    * Plotting

## For consideration
* Should we specifiy a number of particles in x/y or a spacing in metres (dx, dy)  between particles.
* How do we handle the case of initial_bbox = None on a global ocean model like giops? 