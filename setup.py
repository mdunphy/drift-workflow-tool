"""
Package Setup
=============
:Author: Clyde Clements
:Contributors: Nancy Soontiens
:Created: 2018-03-08
"""

from setuptools import setup

console_scripts = [
    'add_traj_data_date = driftutils.add_traj_data_date:main',
    'add_traj_label = driftutils.add_traj_label:main',
    'adjust_bbox = driftutils.adjust_bbox:main',
    'assemble_atmos_data = driftutils.assemble_atmos_data:main',
    'assemble_atmos_metadata = driftutils.assemble_atmos_metadata:main',
    'assemble_correction_calculations = driftutils.assemble_correction_calculations:main',
    'assemble_drift_predictions = driftutils.assemble_drift_predictions:main',
    'assemble_drifter_data = driftutils.assemble_drifter_data:main',
    'assemble_drifter_metadata = driftutils.assemble_drifter_metadata:main',
    'assemble_mldp_model_data = driftutils.assemble_mldp_model_data:main',
    'assemble_mldp_model_metadata = driftutils.assemble_mldp_model_metadata:main',
    'assemble_ocean_data = driftutils.assemble_ocean_data:main',
    'assemble_ocean_metadata = driftutils.assemble_ocean_metadata:main',
    'assemble_ocean_predictions = driftutils.assemble_ocean_predictions:main',
    'calculate_correction_factor = driftutils.calculate_correction_factor:main',
    'crop_to_common_comparison_set = plotutils.crop_to_common_comparison_set:main',
    'combine_correction_factors = driftutils.combine_correction_factors:main',
    'combine_track_segments = driftutils.combine_track_segments:main',
    'correction_factors_evaluate = driftutils.correction_factors_evaluate:main',
    'drift_correction_factor = driftutils.drift_correction_factor:main',
    'drift_evaluate = driftutils.drift_evaluate:main',
    'drift_map = driftutils.drift_map:main',
    'drift_map_mldp = driftutils.drift_map_mldp:main',
    'drift_predict = driftutils.drift_predict:main',
    'drift_predict_mldp = driftutils.drift_predict_mldp:main',
    'find_nearest_grid_point = driftutils.find_nearest_grid_point:main',
    'get_drifter_location = driftutils.get_drifter_location:main',
    'get_skill = driftutils.get_skill:main',
    'make_ariane_namelist = driftutils.make_ariane_namelist:main',
    'make_ariane_run_config = driftutils.make_ariane_run_config:main',
    'make_mldp_namelist = driftutils.make_mldp_namelist:main',
    'mldp_preprocess = driftutils.mldp_preprocess:main',
    'ocean_evaluate = driftutils.ocean_evaluate:main',
    'plot_drift_map = plotutils.plot_drift_map:main',
    'plot_correction_factors = plotutils.plot_correction_factors:main',
    'plotting_workflow = plotutils.plotting_workflow:main',
    'mldp_pre_eval = driftutils.mldp_pre_eval:main',
    'rotate_fields = driftutils.rotate_fields:main',
    'run_ariane = driftutils.run_ariane:main',
    'run_opendrift = driftutils.run_opendrift:main',
    'run_mldp = driftutils.run_mldp:main',
    'set_initial_positions = driftutils.set_initial_positions:main',
    'verify_drifter_data = driftutils.verify_drifter_data:main',
    'verify_mesh_mask = driftutils.verify_mesh_mask:main',
    'verify_ocean_data = driftutils.verify_ocean_data:main',
    'yaml2bash = driftutils.yaml2bash:main',
]

setup(
    name='driftutils',
    description='DFO workflow tool for drift analysis',
    version='5.3',
    url='https://gitlab.com/dfo-drift-projection/drift-workflow-tool',
    author='DFO Drift Projection',
    author_email='dfo.drift.projection@gmail.com',
    packages=['driftutils', 'plotutils', 'plotutils.plotting_workflows', 'plotutils.drift_correction_factor', 'plotutils.drift_map'],
    package_dir={'': 'src'},
    install_requires=[],  # requirements are handled elsewhere
    entry_points={
        'console_scripts': console_scripts
    },
    zip_safe=False,
    scripts=['src/settings_ECCC.sh',]
)
