#!/bin/bash

########################################################################
# Author: Nancy Soontiens
# Created: 2019-08-19
#
# Usage:
#   ./opendrift_install.sh -o od_dir -p python_dir -t run_tests
#
# This script installs OpenDrift in a specified Python environment (ex.
# $HOME/miniconda). It copies the OpenDrift source code into od_dir and
# then installs in python_dir/bin. It assumes the Python environment
# python_dir already exists with required OpenDrift packages installed.
########################################################################

# give usage information
usage() {
    u="Usage: $(basename $0) -o od_dir -p pyenv -t run_tests"
    u="$u where od_dir is the name of the directory in which to copy the"
    u="$u OpenDrift source code and python_dir is the name of the directory"
    u="$u of the Python environment in which OpenDrift should be installed."
    u="$u Set run_tests to true to run OpenDrift tests." 
    echo $u
    exit 1;
}

# check for arguments being passed in at the command line. Users are
# required to provide a directory in which the OpenDrift code will be copied 
# and python_dir which is the directory of the Python environment in which
# Opendrift should be installed. Optionally, the user can provide a flag
# that if true runs the opendrift built in unit tests.
while getopts "h?o:p:t:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    o) od_dir=${OPTARG};;
    p) pyenv=${OPTARG};;
    t) run_tests=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

if [ -z "${od_dir}" ]; then
    echo "no path provided to store OpenDrift"
    usage;
    exit 1;
fi
if [ -z "${pyenv}" ]; then
    echo "no path provided for the previously installed python environment"
    usage;
    exit 1;
fi

export PATH=$pyenv/bin:$PATH
if [[ ! -z "$PYTHONPATH" ]]; then
  echo "unsetting PYTHONPATH"
  unset PYTHONPATH
fi

# First install opendrift-landmask-data repo
base=$(dirname $od_dir)
od_landmask=https://github.com/OpenDrift/opendrift-landmask-data.git
if [[ -d "$base/opendrift-landmask-data" ]]; then
    echo "opendrift-landmask-data repository already cloned"
else
    git clone $od_landmask $base/opendrift-landmask-data
fi
cwd=$(pwd)
cd $base/opendrift-landmask-data
python setup.py build install
cd $cwd

# opendrift source repository 
od_src=https://gitlab.com/dfo-drift-projection/dfo-opendrift.git

if [[ -d "$od_dir" ]]; then
    cd $od_dir
    git fetch origin
    git checkout reader_nemo_update
    pip uninstall -y opendrift
else
    # clone opendrift
    git clone --branch reader_nemo_update $od_src $od_dir
    echo "changing directories to install OpenDrift"
    cd $od_dir
fi

yes | pip install -e .
if [ ${run_tests} = true ]; then
    echo -e "\nRunnning OpenDrift tests. This will likely be a long process."
    echo -e "Output log will be saved in ${od_dir}/test_out.txt"
    echo -e "The expected results using the current environment setup should be: 179 passed, 14 skipped, 2 xfailed, 16 warnings"
    tmp_test_dir=${pyenv}/tmp_test_output
    mkdir -p $tmp_test_dir
    export TMPDIR=$tmp_test_dir
    export PYTHONPATH=$pyenv/bin
    pytest tests/ 2>&1 | tee test_out.txt
    echo -e "Check $od_dir/test_out.txt for output from OpenDrift tests."
    echo -e "Deleting temporary OpenDrift test files"
    cd ${pyenv}
    rm -rf tmp_test_output/
fi
echo "Installation of OpenDrift complete."
