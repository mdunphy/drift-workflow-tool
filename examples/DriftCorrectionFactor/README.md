# DriftCorrectionFactor

The DriftCorrectionFactor module can be used to compare ocean model velocities and observed drifter velocities along the observed drifter track. A correction factor is calculated by examining discrepancies between observed and modelled velocities. At present, wind forcing can also be included, in which case two correction factors are provided. The resulting correction factor(s) are completely independent from any drift simulation model. At this time, no analysis or plotting of the output is provided. The correction factors are:

**Ocean correction factor** $`\gamma`$

- Solves for $`\gamma`$ in $`u_{drifter} = \gamma u_{ocean}`$

**Wind correction factor** $`\alpha`$ 

- Solves for $`\alpha`$ in $`u_{drifter} = u_{ocean} + \alpha u_{atmos}`$, where $`u_{drifter}`$, $`u_{ocean}`$ and $`u_{atmos}`$ are the current vectors for the drifter, ocean and atmosphere respectively.

# Running DriftCorrectionFactor

The files included in this directory provide sample applications of the drift correction factor to user grid and c-grid ocean model output. To run the examples: 

1. first update the information in either `nemo_cgrid.yaml` or `user_grid.yaml` if desired. 

2. Next, update `run_drift_correction_factor.sh` by specifying the desired config file and providing a path to the directory where the output will be created. 

3. `run_drift_correction_factor.sh` can then be executed either interactively or by submitting as a job to the queue system. 

To calculate the drift correction factors for your own experiments, a good place to start is by making a copy of the sample scripts provided, then modifying them with your own data. If this is done prior to running either DriftMap or DriftEval, the wind correction factor can optionally be used to provide an estimate for the best windage factor to include.
