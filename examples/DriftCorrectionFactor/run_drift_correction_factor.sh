#!/bin/bash

########################################################################
# This script provides an example of how to run a DriftCorrectionFactor
# calculation  using a configuation file. The configuration file
# contains all of the parameters needed for the calculation. Several
# example configuration files are provided in
# examples/DriftCorrectionFactor.

# To run a new experiment, users should modify the following variables:
# MINICONDA_PATH
# CONFIG_FILE
# EXPERIMENT_DIR
#
# Note: if running on the gpsc, it is recommended that this script is 
# submitted to the job queues.
########################################################################

# User Defined Parameters
# -----------------------
# MINICONDA_PATH: path to drift-tool python environment
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3
# CONFIG_FILE: experiment specific configuation file
CONFIG_FILE=nemo_cgrid.yaml
# EXPERIMENT_DIR: path to user defined output directory
EXPERIMENT_DIR=/home/sdfo000/sitestore7/opp_drift_fa3/share_drift/sample_drift_correction_factor/output/ciopse-cgrid

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Calculate the correction factors
# --------------------------------
drift_correction_factor \
    -c $CONFIG_FILE \
    --experiment-dir $EXPERIMENT_DIR

# Add additional calculated variables
# -----------------------------------
correction_factors_evaluate \
    --experiment-dir $EXPERIMENT_DIR/output/

# Plot the output
# ----------------------------------
plot_correction_factors \
    --data-dir $EXPERIMENT_DIR/output/
