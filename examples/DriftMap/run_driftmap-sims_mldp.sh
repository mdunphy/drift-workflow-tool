#!/bin/bash

########################################################################
# Script to call MLDP
# author: Jen Holden
# runs with environment from ~/.profile_ECCC
########################################################################

# MINICONDA_PATH: path to drift-tool python environment
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3/

# give usage information
usage() {
    u="Usage: run_driftmap_mldpn.sh -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment>"
    u="$u -o <Full path to output directory>"
    echo $u
    exit 1;
}

# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?c:o:m:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    c) CONFIG=${OPTARG};;
    m) MINICONDA_PATH=${OPTARG};;
    o) EXPERIMENT_DIR=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

if [ -z "$CONFIG" ]; then
    usage;
    exit 1;
fi

if [ "$MINICONDA_PATH" == "/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3" ]; then
    n="NOTE: MINICONDA_PATH is set to the default environment for the master branch."
    n="$n This may cause issues if trying to run code from the development branch instead."
    echo $n
fi

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH

# Source the profile script (this loads the ECCC tools).
. settings_ECCC.sh

# Reset PATH and PYTHONPATH so that MINICONDA_PATH is prioritized over
# any settings modified by settings_ECCC.sh. This ensures the correct version
# of python is sourced.
str_match=${MINICONDA_PATH}/bin
export PATH=$MINICONDA_PATH/bin:${PATH//:${str_match}/}
unset PYTHONPATH
export PYTHONPATH=$MINICONDA_PATH/bin

# set the experiment_dir if one was given
if [ -z $EXPERIMENT_DIR ]; then
    echo " "
else
    experiment_dir=$EXPERIMENT_DIR
fi

# Source the config file using yaml2bash.py, which converts it to a form 
# more easily readable by Bash. If the experiment_dir is given in the 
# config file use that one to store the output. 
eval $(yaml2bash --config $CONFIG)

# confirm existance of experiment_dir, otherwise exit
if [ -z "$experiment_dir" ]; then
    u="experiment_dir must exist either as a command line argument (-o)"
    u="$u or as a variable in $CONFIG"
    echo $u
    exit 1;
fi

## run the simulations
drift_map_mldp \
    -c $CONFIG \
    --experiment-dir $experiment_dir

## Call plot_drift_map
plot_drift_map \
  --data-dir $experiment_dir/output/ \
  --bbox $experiment_dir/namelist_bbox \
  --etopo-file '/home/sdfo000/sitestore7/opp_drift_fa3/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot-dir $experiment_dir/plots \
  --style 'default' \
  --interval 24


