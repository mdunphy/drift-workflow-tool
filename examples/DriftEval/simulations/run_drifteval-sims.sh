#!/bin/bash

########################################################################
# This script provides an example of how to run a DriftEval simualtion 
# using a run configuation file and then process the results. The run 
# configuration file contains all of the run parameters needed for the 
# simulation. Several example configuration files are provided in 
# examples/simulations/DriftEval.
#
# Please see examples/plotting for examples of how to plot the results.
#
# To run a new experiment, users should modify the following arguments: 
# -m MINICONDA_PATH optional path to miniconda install
# -c CONFIG required: config file with user defined parameters and paths
# -o EXPERIMENT_DIR optional: directory in which to save the output.
#
# If running on the gpsc, it is recommended that this script is submitted
# to the job queues.
########################################################################

# MINICONDA_PATH: path to drift-tool python environment. For example, the path
# to the master branch environment of the drift-tool is:
MINICONDA_PATH="/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3"

# give usage information
usage() {
    u="Usage: run_drifteval-sims.sh"
    u="$u -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment if other than default>"
    u="$u -o <Optional. Full path to directory in which to save experiment output."
    u="$u Alternately, this path can be added to the config file as experiment-dir>"
    echo $u
    exit 1
}

# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?c:m:o:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    c) CONFIG=${OPTARG};;
    m) MINICONDA_PATH=${OPTARG};;
    o) EXPERIMENT_DIR=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# warn if using default miniconda path
if [ "$MINICONDA_PATH" == "/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3" ]; then
    n="NOTE: MINICONDA_PATH is set to the default environment for the master branch."
    n="$n This may cause issues if trying to run code from the development branch instead."
    echo $n
fi

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# if no config is provided, exit.
if [ -z "$CONFIG" ]; then
    usage
fi

if [ -n $EXPERIMENT_DIR ]; then
    experiment_dir=$EXPERIMENT_DIR
fi

# Source the config file and load the ECCC tools. The config file needs
# to be converted to a form more easily readable by Bash using the
# following python script.
eval $(yaml2bash --config $CONFIG)

# confirm existance of experiment_dir
if [ -z "$experiment_dir" ]; then
    echo "$CONFIG must contain experiment-dir"
    exit 1;
fi

# Run drift prediction
# --------------------
drift_predict \
    -c $CONFIG \
    --experiment-dir $experiment_dir 

# Compute skills scores
# ---------------------
drift_evaluate \
  --data_dir $experiment_dir/output

# Combine the output into per drifter files
# -----------------------------------------
combine_track_segments \
  --data_dir=$experiment_dir/output
