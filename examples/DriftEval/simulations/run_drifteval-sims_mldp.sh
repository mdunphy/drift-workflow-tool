#!/bin/bash

########################################################################
#
# This script provides an example of how to run a DriftEval MLDP simualtion
# using a run configuation file and then process the results. The run
# configuration file contains all of the run parameters needed for the
# simulation. Several example configuration files are provided in
# examples/DriftEval/simulations/*mldp*.yaml.
#
# Please see examples/plotting for examples of how to plot the results.
#
# To run a new experiment, users should modify the following arguments:
# -m MINICONDA_PATH optional path to miniconda install
# -c CONFIG required: config file with user defined parameters and paths
# -o EXPERIMENT_DIR optional: directory in which to save the output.
#
# If running on the gpsc, it is recommended that this script is submitted
# to the job queues.
#
# usage:
# run_drifteval-sims_mldp.sh -c $CONFIG -m $MINICONDA_PATH
#                            -o $experiment_dir
#
########################################################################

# provide a default path the drift-tool miniconda install on the GPSC
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3

# give usage information
usage() {
    u="Usage: run_drifteval-sims_mldp.sh -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment>"
    u="$u -o <path to directory in which to store the experiment output>."
    echo $u
    exit 1;
}

# check for arguments being passed in at the command line. Users are 
# required to provide a CONFIG file and are optionally able to define a 
# path to a miniconda install if they do not wish to use the default 
# provided on the GPSC
while getopts "h?c:m:r:o:p:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    c) CONFIG=${OPTARG};;
    m) MINICONDA_PATH=${OPTARG};;
    o) experiment_dir=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# if no config is provided, exit.
if [ -z "$CONFIG" ]; then 
    usage;
    exit 1; 
fi

if [ "$MINICONDA_PATH" == "/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3" ]; then
    n="NOTE: MINICONDA_PATH is set to the default environment for the master branch."
    n="$n This may cause issues if trying to run code from the development branch instead."
    echo $n
fi

# Set the Python environment
# --------------------------
old_PYTHONPATH=$PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH

# Source the profile script (this loads the ECCC tools).
. settings_ECCC.sh

# Reset PATH and PYTHONPATH so that MINICONDA_PATH is prioritized over
# any settings modified by settings_ECCC.sh. This ensures the correct version
# of python is sourced.
str_match=${MINICONDA_PATH}/bin
export PATH=$MINICONDA_PATH/bin:${PATH//:${str_match}/}
unset PYTHONPATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Source the config file. The config file needs
# to be converted to a form more easily readable by Bash using the
# following python script.
eval $(yaml2bash --config $CONFIG)

mldp_exec=$(which MLDPn)

# Run drift prediction
# --------------------
drift_predict_mldp \
    -c $CONFIG \
    --experiment-dir $experiment_dir \
    --MLDP-executable $mldp_exec

# Compute skills scores
# ---------------------
drift_evaluate \
  --data_dir $experiment_dir/output

# Combine the output into per drifter files
# -----------------------------------------
combine_track_segments \
  --data_dir=$experiment_dir/output

