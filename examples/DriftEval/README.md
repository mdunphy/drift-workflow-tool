# DriftEval

Example run scripts and configuration files for producing and analyzing DriftEval simulations. A DriftEval simulation and analysis compares modelled drifter trajectories to observed drifter trajectories. The resulting analysis and plots are designed to evaluate the accuracy of the modelled drifters using a common set of verification metrics.

## Subdirectories
- simulations - example configuration files and call scripts for generating a DriftEval simulation. Includes examples of drift simulations with OpenDrift, Ariane and MLDP.
- analysis - example configuration files and call scripts for plotting the output of a DriftEval simulation. These examples include instructions on how to analyze output from a single experiment, an example of a comparison between two or more DriftEval simulations with different experimental parameters, and an example of regionally analyzed data.
