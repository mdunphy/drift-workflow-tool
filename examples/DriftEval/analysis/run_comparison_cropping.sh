#!/bin/bash

########################################################################
# This script provides a sample workflow for cropping output from
# multiple drift tool experiments to a common dataset. The raw output
# data is cropped to a common temporal and spatial set of comparison
# output.
#
# Note: The user must have write permissions in the 'savedir' parameter
# of the CROPPING_CONFIG.
#
# The following should be updated before running:
#
# MINICONDA_PATH
# - this is the path to the drift-tool python environment. If no path
#   is given by the user, the scripts will use a default miniconda
#   install provided on the GPSC.
#
# USER_CONFIG
# - This is the experiment specific user provided configuration file.
# - Sample user configuration files are given in the directory
#   /drift-tool/examples/DriftEval/analysis/
# - Sample cropping configuration file
#   config_de-ana_user_comparison_crop.yaml
#
# usage:
#   run_comparison_cropping.sh -c $USER_CONFIG -p $MINICONDA_PATH
#
########################################################################

# provide a default path the drift-tool miniconda install on the GPSC
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3

# give usage information
usage() {
    u="Usage: run_comparison_cropping.sh -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment"
    echo $u
    exit 1;
}
# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?c:m:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    c) USER_CONFIG=${OPTARG};;
    m) MINICONDA_PATH=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# if no config is provided, exit.
if [ -z "$USER_CONFIG" ]; then
    u="Usage: run_comparison_cropping.sh -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment.>"
    echo $u
    exit 1;
fi

if [ "$MINICONDA_PATH" == "/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-miniconda-v5.3" ]; then
    n="NOTE: MINICONDA_PATH is set to the default environment for the master branch"
    n="$n This may cause issues if trying to run code from the development branch instead."
    echo $n
fi

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Crop datasets to a common timeframe and spatial range   
# -----------------------------------------------------
crop_to_common_comparison_set \
  --config $USER_CONFIG
