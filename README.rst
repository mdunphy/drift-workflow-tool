============================
README: DFO Drift Projection
============================

:Authors: Nancy Soontiens, Jennifer Holden, Samuel Babalola, Clyde Clements, Hauke Blanken
:Created: 2018-06-20
:Date: 2018-07-09

.. uid: 7695C2E7-C2C7-45B5-8C96-1205814957BB


Detailed information on installing and running the drift workflow tool can
be found on the project wiki:
https://gitlab.com/dfo-drift-projection/drift-workflow-tool/-/wikis/home

The drift workflow tool is a python command line program that produces and
evaluates modelled drifter trajectories using a choice of three drift models 
(OpenDrift, MLDPn or Ariane). It has three main functions:

DriftEval

This function takes observed drifter data and ocean model data as inputs. 
It generates modelled drifters using starting positions determined from 
the observed drifter tracks. It then produces skill scores and drifter 
track plots based on the comparison between the observed and modelled 
drifter tracks. The purpose of this function is to evaluate modelled drift.

DriftMap

This function takes ocean model data as an input and generates a map of 
the predicted ocean currents. The purpose of this function is to 
characterize modelled drift in a given region.

DriftCorrectionFactor

This function takes observed drifter data, ocean model data and optional 
atmospheric model data as inputs. It determines the modelled currents and 
winds along the observed trajectory and then determines the correction 
factor needed to produce a perfect drift prediction. 
Two correction factors are available (ocean and wind), which are 
independent of any drift model. 
