"""
Assemble Ocean Mesh and Velocity Data
=====================================
:Author: Clyde Clements
:Created: 2017-08-29 10:52:25 -0230

This module assembles the ocean data in preparation for a run with Ariane.

When the data spans multiple files, Ariane requires it to be available in files
that follow a prescribed naming scheme. The name of the files must include
a number that increases one by one according to the time evolution of the data.
"""

import datetime
import os
import os.path
import sys
import yaml

import dateutil.parser
import numpy as np

from . import configargparse
from .ioutils import get_run_ocean_data_dates
from . import utils
from .utils import logger


def assemble_ocean_data(metadata_file, start_date, drift_duration,
                        data_assembly_dir,
                        model_time='time_centered'):
    """Assemble ocean data.

    Parameters
    ----------
    metadata_file : str
        Name of file containing metadata for ocean data.
    start_date : datetime.datetime
        Date of start for drift prediction.
    drift_duration : datetime.timedelta
        Duration for drift prediction.
    data_assembly_dir : str
        Name of directory in which data will be assembled.
    model_time : str
	    Name of variable containing model time stamps, defaults to
        'time_centered'


    Returns
    -------
    info : dict
        A dictionary containing the following entries:

        - ``ocean_data_dir`` (*str*): Name of directory containing assembled
          ocean data. This will be a subdirectory of the data assembly
          directory and relative to the run directory.
        - ``ocean_mesh_file`` (*str*): Name of mesh file relative to the run
          directory.
        - ``run_ocean_data_dates`` (*numpy array*): Sorted array of dates
          for available ocean data for this run.
        - ``ocean_data_variables`` (*list*): List of strings containg names of
          ocean data variables.
        - ``ocean_data_files`` (*dict*): Dictionary containing ocean data files
          for this run. The key is an ocean variable name (such as temperature)
          and the value is the list of data files relative to the run
          directory for that variable.
    """
    logger.info('Assembling ocean data...')

    metadata_dir = os.path.dirname(os.path.abspath(metadata_file))
    with open(metadata_file, 'r') as f:
        metadata = yaml.load(f, Loader=yaml.FullLoader)

    #TODO: This is probably not the best solution!
    if 'ocean_mesh' not in metadata:
        metadata['ocean_mesh'] = 'None'

    ocean_data = metadata['ocean_data']
    ocean_mesh = metadata['ocean_mesh']
    end_date = start_date + drift_duration
    data_vars = set()

    for data in ocean_data.values():
        for var in data:
            data_vars.add(var)

    ocean_data_in_period = {}
    for var in data_vars:
        ocean_data_in_period[var] = {}

    data_dates = find_ocean_data_date_period(
        ocean_data, start_date, end_date,
        time_interval=metadata['ocean_domain']['time_interval'])

    for data_date_str in ocean_data:
        data_date = dateutil.parser.parse(data_date_str)
        if data_date in data_dates:
           for var in ocean_data[data_date_str]:
                ocean_data_in_period[var][data_date] \
                    = ocean_data[data_date_str][var]

    if len(data_dates) == 0:
        msg = ('No ocean data available for drift simulation for the time '
               'period from {} to {}')
        msg = msg.format(start_date, end_date)
        raise ValueError(msg)

    ocean_data_subdir = 'ocean'
    ocean_data_dir = os.path.join(data_assembly_dir, ocean_data_subdir)
    pwd = os.getcwd()

    if not os.path.exists(data_assembly_dir):
        logger.debug('Creating directory for ocean data...')
        os.makedirs(data_assembly_dir)

    os.chdir(data_assembly_dir)

    ocean_mesh_file = ocean_mesh['filename']
    if ocean_mesh_file != 'None':
        base_mesh_file = 'mesh.nc'
        ocean_mesh_file = os.path.join(data_assembly_dir, base_mesh_file)
        mesh_filename = ocean_mesh['filename']
        if not os.path.isabs(mesh_filename):
             mesh_filename = os.path.join(metadata_dir, mesh_filename)
             mesh_filename = os.path.relpath(mesh_filename)
        os.symlink(mesh_filename, base_mesh_file)

    if os.path.exists(ocean_data_subdir):
        msg = ('Assembly directory for ocean data already exists. Please '
               'delete it or specify a different directory.')
        raise RuntimeError(msg)

    os.makedirs(ocean_data_subdir)
    os.chdir(ocean_data_subdir)
    
    data_input_files = {}
    data_symlink_files = {}
    for var in ocean_data_in_period:
        data_input_files[var] = set()
        data_symlink_files[var] = []
        i=0
        for data_date, filename in ocean_data_in_period[var].items():
            if filename in data_input_files[var]:
                continue
            data_input_files[var].add(filename)
            i += 1
            if not os.path.isabs(filename):
                data_filename = os.path.join(metadata_dir, filename)
                data_filename = os.path.relpath(data_filename)
            else:
                data_filename = filename
            symlink_name = '{}_{:05d}.nc'.format(var, i)
            os.symlink(data_filename, symlink_name)
            data_symlink_files[var].append(
                os.path.join(ocean_data_dir, symlink_name)
            )
    # Rename duplicate links
    reverse_dict = {}
    for var, files in data_symlink_files.items():
        for f in files:
            realpath = os.readlink(os.path.basename(f))
            try:
                reverse_dict[realpath][var] = os.path.basename(f)
            except KeyError:
                reverse_dict[realpath] = {}
                reverse_dict[realpath][var] = os.path.basename(f)
            os.remove(os.path.basename(f))
        data_symlink_files[var] = []
    new_links = {}
    for f, var_dict in reverse_dict.items():
        for var in var_dict:
            base = var_dict[var].split('_')[-1]
            try:
                new_links[f] = '{}_{}'.format(var, new_links[f])
            except KeyError:
                new_links[f] = '{}_{}'.format(var, base)
    for data_file, symlink_name in new_links.items():
        variables = symlink_name.split('_')[:-1]
        os.symlink(data_file, symlink_name)
        for var in variables:
            data_symlink_files[var].append(os.path.join(ocean_data_dir,
                                                        symlink_name))

    # List all run ocean data dates in ocean_data_dir
    os.chdir(pwd)
    run_data_dates = get_run_ocean_data_dates(ocean_data_dir, model_time)
    logger.info(
        'Finished assembling ocean data:\n  directory = %s\n  mesh file = %s',
        ocean_data_dir, ocean_mesh_file)

    info = {
        'ocean_data_dir': ocean_data_dir,
        'ocean_mesh_file': ocean_mesh_file,
        'run_ocean_data_dates': run_data_dates,
        'ocean_data_variables': data_vars,
        'ocean_data_files': data_symlink_files
    }
    return info


def find_ocean_data_date_period(ocean_data, start_date, end_date,
                                time_interval='time_centered'):
    """Find the ocean model date range corresponding to drift period.

    Parameters
    ----------
    ocean_data : dict
        Ocean model data diciontary with keys corresponding to all ocean model
         output times.
    start_date : datetime.datetime
        Drift period start date
    end_date : datetime.datetime
        Drift period end date
    time_interval : str
        Type of time interval between model ouput times.
        - time_centered - output times represent midpoint of time period
        - time_instant - output times represent right end points of time period
        - time_left - output times represent left end points of time period

    Returns
    -------
    data_dates : list
        Ocean model output dates associated with drift period.
    """
    all_ocean_dates = ocean_data.keys()
    all_ocean_dates = [dateutil.parser.parse(data_date_str)
                       for data_date_str in ocean_data]
    all_ocean_dates.sort()
    data_dates = []
    diffs = np.diff(all_ocean_dates)
    # Construct endpoints of ocean_dates bounds
    if time_interval == 'time_centered':
        # Ocean dates are midpoints - construct left and right end points.
        ends=[ all_ocean_dates[i] - diffs[i-1]/2
               for i in range(1,len(diffs)+1)]
        # Assuming first endpoint is -diff[0]/2 from first ocean_date
        ends.insert(0, all_ocean_dates[0] - diffs[0]/2 )
        # Assuming last endpoint is diff[-1]/2from last ocean_date
        ends.append(all_ocean_dates[-1] + diffs[-1]/2 )
    elif time_interval == 'time_instant':
        # Assume first endpoint is -diff[0] from first ocean_date
        ends = all_ocean_dates[:]
        ends.insert(0, all_ocean_dates[0] - diffs[0])
    elif time_interval == 'time_left':
        # Assume last endpoint is diff[-1] from last ocean_date
        ends = all_ocean_dates[:]
        ends.append(all_ocean_dates[-1] + diffs[-1])
    else: # Use time_centered approach
        ends=[ all_ocean_dates[i] - diffs[i-1]/2
               for i in range(1,len(diffs)+1)]
        ends.insert(0, all_ocean_dates[0] - diffs[0]/2 )
        ends.append(all_ocean_dates[-1] + diffs[-1]/2 )    
    # If start_date or end_date are outside of endpoint, return empty list
    if (start_date < ends[0]) or (end_date > ends[-1]):
        return data_dates
    else:
        # Check if start_date and end_date lie between endpoint pairs or
        # data_date is between start_date and end_date
        for end1, end2, data_date in zip(ends[:-1], ends[1:], all_ocean_dates):
            if (((end1      <= start_date) and (start_date <= end2     )) or
                ((end1      <= end_date)   and (end_date   <= end2     )) or
                ((data_date >= start_date) and (data_date   <= end_date))):
                data_dates.append(data_date)
        # Remove duplicates
        data_dates=list(set(data_dates))
        data_dates.sort()
    return data_dates


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--ocean_metadata_file', type=str, required=True,
                   help='YAML file containing metadata for ocean data files')
    arg_parser.add('--start_date', type=str, required=True,
                   help='Date and time for start of drift calculations')
    arg_parser.add('--num_drift_hours', type=int,
                   help='Number of hours for drift calculation')
    arg_parser.add('--data_assembly_dir', type=str,
                   help=('Name of directory to create containing symlinks to '
                         'ocean data for subsequent trajectory calculation'))
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    start_date = dateutil.parser.parse(config.start_date, ignoretz=True)
    logger.debug('Parsed user-specified date: %s', start_date)

    drift_duration = datetime.timedelta(hours=config.num_drift_hours)
    assemble_ocean_data(
        config.ocean_metadata_file, start_date, drift_duration,
        config.data_assembly_dir
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
