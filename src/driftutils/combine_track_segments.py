"""
Combine Modelled Track Segments
=============================
:Author: Jennifer Holden
:Created: 2019-02-27
:Modified: 2022-10-23

This module combines drift-tool single output files into
per drifter format files
"""

import os
import glob
import xarray as xr
import numpy as np
import sys
import argparse

from driftutils import utils

logger = utils.logger


def combine_model_tracks(dsets, savefilename):
    """ Combine model drifter track segments from multiple single
    drift-tool output files into one file per unique drifter id.

    Parameters
    ----------
    dsets : list
        list of datasets that will be aggregated
    savefilename : str
        full path and name of the aggregated file that will be created
    """

    datasets = []
    for i, ds in enumerate(dsets):

        # Assign a new variable called timestep that goes from 0 to
        # len(time) - 1
        ds = ds.assign(timestep=xr.DataArray(
            np.arange(0, ds.time.shape[0]), dims=('time')
        ))

        # Any attributes that should change to variables, should be
        # converted here. This makes a new variable in the dataset with
        # a single value, taken from the attribute
        ds = ds.assign(mod_start_date=ds.mod_start_date)
        ds = ds.assign(mod_start_lat=ds.mod_start_lat)
        ds = ds.assign(mod_start_lon=ds.mod_start_lon)

        # This will make a new variable with the original filename
        orig_fname = (str(ds.mod_run_name) + '-' + str(ds.obs_buoyid) + '.nc')
        ds = ds.assign(original_filename=orig_fname)

        # Swap the dimensions so that timestep is now the coordinate and
        # time is a regular variable
        ds = ds.swap_dims({"time": "timestep"})

        # Add this dataset to the datasets list
        datasets.append(ds)

    # Concatenate all the datasets under a new dimension called model_run
    ds = xr.concat(datasets, dim="model_run")

    # This should now handle both the case where there is only one file
    # to aggregate as well as the case where there are multiple files
    # with exactly the same list of binned times since start.
    if (len(ds.time.shape) == 1):
        new_time = np.tile(ds.time.values, (len(ds['model_run']), 1))

        ds.drop('time')
        ds.coords['time'] = (['model_run', 'timestep'], new_time)

    if not os.path.isdir(os.path.dirname(savefilename)):
        os.mkdir(os.path.dirname(savefilename))

    # save out the aggregated file
    ds.to_netcdf(savefilename)


def create_buoy_dictionary(data_dir):
    """ Helper function that creates a dictionary with information about
    each netcdf file in data_dir. The dictionary keys are unique drifter ids
    and the values are dictionaries with the filenames associated with each
    particular drifter id as keys and the associated datasets as values:
    buoydf = {'buoyid1': {'filename1.nc': ds1, 'filename2': ds2},
              'buoyid2': {'filename3.nc': ds3, 'filename4': ds4}, etc}
    """
    buoydict = {}
    files = glob.glob(os.path.join(data_dir, '*.nc'))
    files.sort()
    for file in files:
        with xr.open_dataset(file) as indv_ds:

            bname = os.path.basename(file)
            buoy = indv_ds.obs_buoyid

            if buoy not in buoydict:
                buoydict[buoy] = {}

            buoydict[buoy][bname] = indv_ds

    return buoydict


def combine_track_segments(data_dir):
    """
    Combines all modelled tracks associated with each unique
    drifter in an experiment into a single per drifter file.

    Parameters
    ----------
    data_dir : str
        Path to directory containing drift-tool individual model-run
        output files. These files will be aggregated to create per
        drifter files, which are saved in a new folder in the parent
        directory of data_dir.
     """

    # Determine the directory where the output files will be created
    # based on the original data_dir
    savedir = os.path.join(os.path.dirname(os.path.normpath(data_dir)),
                           'output_per_drifter')

    # Create a dictionary with information from the files in data_dir. The
    # info will be organized into elements according to unique drifter ids
    buoydict = create_buoy_dictionary(data_dir)

    # For each drifter, compile a list of the datasets that will be
    # aggregated, then create the per drifter files
    for drifterid in buoydict.keys():

        logger.info("Aggregating files for " + str(drifterid))

        fnames = list(buoydict[drifterid].keys())
        dsets = [buoydict[drifterid][f] for f in fnames]
        savename = os.path.join(savedir, '{}_aggregated.nc'.format(drifterid))

        combine_model_tracks(dsets, savename)


def main(args=sys.argv[1:]):

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, help='path to output files')
    args = (parser.parse_args())
    data_dir = args.data_dir

    combine_track_segments(data_dir)


'''
# if cli (will also need to update example scripts to replace _ with -):
def main():
    from driftutils import cli
    cli(combine_track_segments)
'''


if __name__ == '__main__':
    main()
