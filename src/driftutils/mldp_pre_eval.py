"""
Convert, Modify, and Assemble results from drift predict.
=============================
:Created: 2019-08-07
"""

import datetime
import glob
import os
import numpy as np
import pandas as pd
import xarray as xr
import yaml

from driftutils import utils

logger = utils.logger


def convert_ocean_predictions(mod_src_dir,
                              duration,
                              wind_coeff,
                              output_dir,
                              output_file='mldp_trajectories.nc'):
    """
    Coverts csv outputs from MLDP runs to netcdf formats when using
    DriftMap. The netcdf files are modified to contain variables and
    attributes required by verification modules.

    Parameters
    ----------
    mod_src_dir : str
         Path to directory containing csv files.
    duration :  datetime.timedelta
        Duration of drift.
    wind_coeff : float
       Percentage of wind velocity to applied to drifter.
    output_dir : str
         Path to save directory.
    output_file : str
        Name of file in which to store trajectories.
    """

    logger.info('Starting file conversion...')
    mod_files = glob.glob(os.path.join(mod_src_dir, '*.csv'))

    drf_yaml_dir = os.path.dirname(mod_src_dir)
    positions_file = os.path.join(drf_yaml_dir, 'drifter_positions.yaml')
    with open(positions_file, 'r') as f:
        posfile = yaml.load(f, Loader=yaml.FullLoader)

    # Loop through all model tracks
    for modf in mod_files:
        datasets = []

        # It is important to choose the right reader function. This can
        # be determined by looking into the csv files and checking the
        # arrangement of the fields.
        mod = read_model_csv(modf)
        grouped = mod.groupby('drifter ID')

        # For each modelled drifter:
        for modelrun in grouped.groups:

            # Prepare model data array
            g = grouped.get_group(modelrun)

            # Drop duplicate rows and convert to a dataset.
            g = g[~g.index.duplicated(keep='first')]

            # Shift the index so that the initial time/pos can be added
            # since it is missing in the original MLDP csv files.
            g.index = g.index + 1

            # Grab the initial times and positions that correspond
            # to this model run from the MLDP namelist dictionary:
            dID = g['drifter ID'].values.tolist()[0]

            # Get the start values
            mslon = posfile['drifter_grid_positions'][dID][0]
            mslat = posfile['drifter_grid_positions'][dID][1]
            time_str = posfile['drifter_grid_positions'][dID][3]
            mstime = datetime.datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%S')

            # Add the starting lat, lon and time to the data since they
            # are not included in the original csv files.
            g.loc[0] = [mstime, dID, mslat, mslon]  # adding a row
            g.sort_index(inplace=True)
            g = g.set_index(['time'])

            # Drop duplicate rows and convert to a dataset
            g.drop_duplicates(subset=None,
                              keep='first',
                              inplace=True,
                              ignore_index=False)

            # Create dataset to save in netcdf
            mod_array = g.to_xarray()
            mod_array['lon'] = utils.wrap_to_180(mod_array.lon)
            ds = xr.Dataset(
                coords={'time': mod_array['time'], 'ntraj': modelrun},
                data_vars={'lat': ('time', mod_array['lat'].data),
                           'lon': ('time', mod_array['lon'].data), })

            # Add attributes
            ds.lat.attrs['long_name'] = 'Latitude of modelled trajectory'
            ds.lat.attrs['units'] = 'degrees_north'
            ds.lat.attrs['_FillValue'] = ds.lat.dtype.type(np.nan)
            ds.lon.attrs['long_name'] = 'Longitude of modelled trajectory'
            ds.lon.attrs['units'] = 'degrees_east'
            ds.lon.attrs['_FillValue'] = ds.lon.dtype.type(np.nan)
            ds.attrs['drifter_id'] = str(modelrun)
            ds.attrs['nb_output'] = '{}'.format(duration.total_seconds()//3600)
            ds.attrs['windage'] = '{}'.format(wind_coeff)

            datasets.append(ds)

        concat_ds = xr.concat(datasets, dim='ntraj')
        concat_ds.to_netcdf(os.path.join(output_dir, output_file))
        logger.info('converted file:')
        logger.info(os.path.join(output_dir, output_file))


def convert_drift_predictions(mod_src_dir,
                              duration,
                              wind_coeff,
                              output_dir,
                              output_file='mldp_trajectories.nc'):
    """
    Coverts csv outputs from MLDP runs to netcdf formats for DriftEval.
    The netcdf files are modified to contain variables and attributes
    required by verification modules.

    Parameters
    ----------
    mod_src_dir : str
         Path to directory containing csv files.
    duration :  datetime.timedelta
        Duration of drift.
    wind_coeff : float
       Percentage of wind velocity to applied to drifter.
    output_dir : str
         Path to save directory.
    output_file : str
        Name of file in which to store trajectories.
    """

    logger.info('Starting mldp file conversion...')
    mod_files = glob.glob(os.path.join(mod_src_dir, '*.csv'))

    positions_file = os.path.join(mod_src_dir, 'drifter_positions.yaml')
    with open(positions_file, 'r') as f:
        posfile = yaml.load(f, Loader=yaml.FullLoader)

    # Loop through all model
    for modf in mod_files:
        datasets = []

        # It is important to choose the right reader function. This can
        # be determined by looking into the csv files and checking the
        # arrangement of the fields.
        mod = read_model_csv(modf)
        grouped = mod.groupby('drifter ID')

        # For each modelled drifter:
        for modelrun in grouped.groups:

            # Prepare model data array
            g = grouped.get_group(modelrun)

            # Drop duplicate rows and convert to a dataset. Note: this is
            # a change from the DriftMap version of this function and should
            # probably be incorporated there as well during a refactor.
            g = g[~g.index.duplicated(keep='first')]
            dID = np.unique(g['drifter ID'].values.tolist())[0]

            # Get the start values
            mslon = posfile['drifter_grid_positions'][dID][0]
            mslat = posfile['drifter_grid_positions'][dID][1]
            mstime_orig = posfile['drifter_grid_positions'][dID][3]
            mstime = mstime_orig.replace("T", " ")

            # Add the starting lat, lon and time to the data since they
            # are not included in the original csv files.
            g.loc[-1] = [mstime, dID, mslat, mslon]  # adding a row
            g.index = g.index + 1  # shifting index
            g.sort_index(inplace=True)
            g = g.set_index(['time'])

            # Convert to xarray for easy writing to netcdf
            mod_array = g.to_xarray()
            mod_array['lon'] = utils.wrap_to_180(mod_array.lon)

            # Converting lats and lons to dtype float so that they can
            # be later cropped with opendrift/ariane output (these write
            # out as floats by default in opendrift/ariane).
            mod_lon = mod_array['lon'].astype(np.single)
            mod_lat = mod_array['lat'].astype(np.single)
            mod_time = pd.to_datetime(mod_array['time'].values)

            # Create dataset to save in netcdf
            ds = xr.Dataset(
                coords={'time': mod_time, 'ntraj': modelrun},
                data_vars={'lat': ('time', mod_lat.data),
                           'lon': ('time', mod_lon.data), })

            # Add attributes
            ds.lat.attrs['long_name'] = 'Latitude of modelled trajectory'
            ds.lat.attrs['units'] = 'degrees_north'
            ds.lat.attrs['_FillValue'] = ds.lat.dtype.type(np.nan)
            ds.lon.attrs['long_name'] = 'Longitude of modelled trajectory'
            ds.lon.attrs['units'] = 'degrees_east'
            ds.lon.attrs['_FillValue'] = ds.lon.dtype.type(np.nan)
            ds.attrs['drifter_id'] = str(modelrun)
            ds.attrs['nb_output'] = '{}'.format(duration.total_seconds()//3600)
            ds.attrs['windage'] = '{}'.format(wind_coeff)
            new_buoyid = mod_array['drifter ID'].item(0)
            ds.attrs['buoyid'] = '{}'.format(new_buoyid)

            datasets.append(ds)

        concat_ds = xr.concat(datasets, dim='ntraj')
        concat_ds.to_netcdf(os.path.join(output_dir, output_file))
        logger.info('converted file:')
        logger.info(os.path.join(output_dir, output_file))


# Helper functions for csv formats
def read_model_csv(fname):
    """ Read model drifter data. fname is filename (str) """
    df = pd.read_csv(fname, parse_dates=[0, ],
                     header=0,
                     names=['time', 'drifter ID', 'lat', 'lon'])
    return df


def main():
    from . import cli
    cli.run(convert_ocean_predictions)


if __name__ == '__main__':
    main()
