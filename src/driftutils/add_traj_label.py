###############################################################################
# Add labels to trajectories
#
# Author: Clyde Clements
# Created: 2017-10-27 15:19:38 -0230
#
# This module adds a coordinate variable to the Ariane netCDF file that
# specifies the labels for the trajectories.
###############################################################################

import os
import shutil
import sys
import tempfile

import xarray as xr

from . import configargparse
from . import utils
from .utils import logger


def add_traj_label_to_datafile(traj_file, labels, drift_model_name='Ariane'):
    dataset = xr.open_dataset(traj_file, decode_times=False, decode_cf=False)
    if drift_model_name == 'OpenDrift':
        num_outputs = dataset['time'].size
    elif drift_model_name == 'Ariane':
        num_outputs = dataset['nb_output'].size
    if num_outputs > 0:
        dataset = add_traj_label_to_dataset(dataset, labels, drift_model_name)
        # We use mkstemp to create a temporary file. It returns a file descriptor
        # and the name of the file. We are only interested in the name. But note
        # that mkstemp opens the file. Below when we call the method to_netcdf(),
        # the file will be opened again. Most Unix platforms will permit that sort
        # of behaviour; however, to be on the safe side we close the file first,
        # allowing to_netcdf() to be the only source opening the file.
        fd, file_name = tempfile.mkstemp(suffix='.nc')
        os.close(fd)
        # Build encoding to avoid _FillValue and missing_value conflicts
        encoding={varname: {'_FillValue': None} for varname in dataset.variables}
        dataset.to_netcdf(file_name, encoding=encoding)
        dataset.close()
        # Now overwrite the current file.
        shutil.copyfile(file_name, traj_file)
        # Remove the temp file so that /tmp. doesn't fill up
        os.remove(file_name)


def add_traj_label_to_dataset(dataset, labels, drift_model_name):
    """Add labels to trajectories in data set.

    This function adds a coordinate variable 'ntraj' to the dataset, which
    is set to the given labels.

    Arguments
        dataset: xarray.Dataset
        labels: list of strings
        drift_model_name: name of drift model (Ariane or OpenDrift)
    """
    if drift_model_name == 'OpenDrift':
        ntraj = dataset.dims['trajectory']
        dataset = dataset.rename({'trajectory': 'ntraj'})
    elif drift_model_name == 'Ariane':
        ntraj = dataset.dims['ntraj']
    else:
        raise ValueError(
            'Drift model name {} not valid'.format(drift_model_name))
    if len(labels) != ntraj:
        msg = ('Number of supplied labels (%d) does not match the number of '
               'trajectories (%d) in the dataset').format(len(labels), ntraj)
        raise ValueError(msg)
    logger.info('Adding labels to dataset...')
    dataset.coords['ntraj'] = labels
    return dataset


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--traj_file', type=str, required=True,
                   help=('Name of NetCDF file containing Ariane-computed '
                         'trajectories'))
    arg_parser.add('--labels', type=str, required=True,
                   help=('String containing comma-separated list of labels to '
                         'add to trajectories'))
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    labels = config.labels.split(',')
    add_traj_label_to_datafile(config.traj_file, labels)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
