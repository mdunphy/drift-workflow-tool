from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import argparse
import sys
import numpy as np

def adjust_bbox(bbox, scale_factor):

    #alter the given bbox, such that the grid of points are defined
    # within the boundary given by the user instead of at the bbox edge
    # itself

    #grab the values from the bbox and convert them to floats.
    user_bbox = bbox.lstrip().rstrip().split(" ")
    user_bbox = [float(i) for i in user_bbox]

    left = user_bbox[0]
    lower = user_bbox[1]
    right = user_bbox[2]
    upper = user_bbox[3]

    llbbox = (left, lower)
    lrbbox = (right, lower)
    ulbbox = (left, upper)
    urbbox = (right, upper)

    #use shapely to shrink the bbox that will be used to define the grid
    #of starting points. The buffer value is just a guess for right now,
    #but it might be good to make this a calculated value in the future.
    #maybe use the fact that a particle can move 864000 m in 2 days
    #(this assumes the particle is moving at 5 m/s and 172800 secs in 2 days)
    polygon = Polygon([llbbox, ulbbox, urbbox, lrbbox])
    new_polygon = polygon.buffer(scale_factor)
    x, y = new_polygon.exterior.coords.xy
    pgb_minlon = np.nanmin(np.unique(x.tolist()))
    pgb_maxlon = np.nanmax(np.unique(x.tolist()))
    pgb_minlat = np.nanmin(np.unique(y.tolist()))
    pgb_maxlat = np.nanmax(np.unique(y.tolist()))

    #convert the new bbox coordinates to a list of strings because that
    #is what is required for the rest of the code to work for now.
    new_bbox_list = [pgb_minlon, pgb_minlat, pgb_maxlon, pgb_maxlat]
    new_bbox_list = [str(i) for i in new_bbox_list]
    new_bbox = " ".join(new_bbox_list)

    return new_bbox
    

def main(args=sys.argv[1:]):

    parser = argparse.ArgumentParser()

    parser.add_argument('--bbox',
                    type=str,
                    help='bbox',
                    )

    helpstr = ('amount to change bbox. can be positive (expand box) ' 
                    + 'or negative (shrink box)')
    parser.add_argument('--scale_factor',
                    type=float,
                    help=helpstr,
                    )

    args = (parser.parse_args())

    bbox = args.bbox
    scale_factor = args.scale_factor 

    bbox = bbox.replace(',',' ')
    new_bbox = adjust_bbox(bbox, scale_factor)

    print(new_bbox)

if __name__ == '__main__':
    main()
