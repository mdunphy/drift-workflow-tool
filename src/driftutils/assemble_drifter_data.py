"""
Assemble Drifter Data
=====================
:Author: Clyde Clements
:Created: 2017-08-30 09:01:09 -0230

This module assembles the drifter data in preparation for a run with Ariane.
It determines all drifters that have data on a specified start date and
extending for the duration of the drift prediction.
It then creates a directory with symbolic links to the drifter data files
needed for this run.
"""

import datetime
import os
import os.path
import sys
import yaml

import dateutil.parser

from . import configargparse
from . import ioutils
from . import utils
from .utils import logger


class NoDrifterDataForTimePeriod(Exception):
    pass


def assemble_drifter_data(metadata_file, start_date, drift_duration,
                          data_assembly_dir='data', mode='predict'):
    """Assemble drifter data.

    Parameters
    ----------
    metadata_file : str
        Name of file containing metadata for drifters.
    start_date : datetime.datetime
        Date of start for drift prediction.
    drift_duration : datetime.timedelta
        Duration for drift prediction.
    data_assembly_dir : str
        Name of directory in which data will be assembled.
    mode : str
        Run mode for assembling drifter data. Options are 'predict'
        for which drifter data is assembled for drift_predict or
        'correction' for which drift predict is assembled for
        drift_correction_factor

    Returns
    -------
    info : dict
        A dictionary containing the following entries:

        - ``drift_data_dir`` (*str*): Name of directory containing assembled
          drifter data. 
        - ``num_drifters`` (*int*): Number of drifters with data in the
          associated period.
        - ``drifter_data_files`` (*dict*): Dictionary containing drifter data
          files for this run. The key is the drifter buoy id and the value is
          the name of the data file relative to the run directory.
    """
    logger.info('Assembling drifter data...')
    if mode not in ['predict', 'correction']:
        raise ValueError("Argument mode={} must take one of 'predict' or "
                          "'correction'".format(mode))
    metadata_dir = os.path.dirname(os.path.abspath(metadata_file))
    with open(metadata_file, 'r') as f:
        metadata = yaml.load(f, Loader=yaml.FullLoader)
    drifters = metadata['drifters']
    # Convert date fields from string to datetime objects.
    for v in drifters:
        v['start_data_date'] = dateutil.parser.parse(v['start_data_date'])
        v['last_data_date'] = dateutil.parser.parse(v['last_data_date'])
    end_date = start_date + drift_duration

    logger.debug('Creating directory for drifter data...')
    drifter_data_subdir = 'drifters'
    if mode == 'predict':
        drifter_data_dir = os.path.join(data_assembly_dir, drifter_data_subdir)
    elif mode == 'correction':
        drifter_data_dir=data_assembly_dir
    if os.path.exists(drifter_data_dir):
        msg = ('Assembly directory for drifter data already exists. Please '
               'delete it or specify a different directory.')
        raise RuntimeError(msg)
    pwd = os.getcwd()
    os.makedirs(drifter_data_dir)
    os.chdir(drifter_data_dir)
    data_files = {}
    for v in drifters:
        if 'qc_status' in v:
            logger.debug('Drifter {} failed quality check \n'
                        'message : {}'.format(v['filename'], v['qc_status']))
            continue
        if mode == 'predict':
            if v['start_data_date'] > start_date or v['last_data_date'] < end_date:
                logger.info(('Ignoring drifter %s because it does not contain data'
                             ' for the entire simulation period.'), v['buoyid'])
                continue
        df = v['filename']
        # Check that file contains more than one data point in time period
        ds = ioutils.load_drifter_dataset(df)
        ds = ds.sel(time=slice(start_date, end_date))
        if len(ds.time.values) < 2:
            logger.info(('Ignoring drifter %s because it contains one or fewer'
                         ' points for the entire period.'), v['buoyid'])
            continue
        ds.close()
        if not os.path.isabs(df):
            df = os.path.join(metadata_dir, df)
            df = os.path.relpath(df)
        symlink_name = os.path.basename(df)
        os.symlink(df, symlink_name)
        data_files[v['buoyid']] = os.path.join(drifter_data_dir, symlink_name)
    os.chdir(pwd)

    num_drifters = len(data_files)
    if num_drifters == 0:
        msg = ('No drifter data available for drift simulation for the time '
               'period from {} to {}').format(start_date, end_date)
        raise NoDrifterDataForTimePeriod(msg)

    logger.info(('Assembled drifter data:\n  directory = %s\n'
                 '  number of drifters = %d'),
                drifter_data_dir, num_drifters)
    info = {'drifter_data_dir': drifter_data_dir,
            'drifter_data_files': data_files, 'num_drifters': num_drifters}
    return info


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--drifter_metadata_file', type=str, required=True,
                   help='YAML file containing metadata for drifters')
    arg_parser.add('--start_date', type=str, required=True,
                   help='Date and time for start of drift calculations')
    arg_parser.add('--num_drift_hours', type=int, required=True,
                   help='Number of hours for drift calculation')
    arg_parser.add('--data_assembly_dir', type=str, default='data',
                   help=('Name of directory to create containing symlinks to '
                         'drifter data for subsequent trajectory calculation'))
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    start_date = dateutil.parser.parse(config.start_date, ignoretz=True)
    logger.debug('Parsed user-specified date: %s', start_date)

    drift_duration = datetime.timedelta(hours=config.num_drift_hours)
    assemble_drifter_data(
        config.drifter_metadata_file, start_date, drift_duration,
        config.data_assembly_dir
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
