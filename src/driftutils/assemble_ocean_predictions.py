"""
Assemble Ocean Predictions
==========================
:Author: Samuel Babalola, Jennifer Holden
:Created: 2019-05-14
:Modified: 2020-01-22

This module gathers the results from a drift-map experiment. For each
modelled drifter, it combines modelled trajectories into a single
Class 4 type drifter file.
"""

import datetime
import glob
import os
from os.path import join as joinpath
import sys
import yaml
import dateutil.parser
import numpy as np
import xarray as xr
from driftutils.get_skill import get_ocean_skill
from driftutils import ioutils
from driftutils import utils
from driftutils import keep_in_domain

logger = utils.logger


def get_drifter_track(dataset, start_date, end_date):
    return dataset.sel(time=slice(start_date, end_date))


def finalize_modelled_track(
    mod_track,
    mod_drifter,
    experiment_metadata,
    run_name,
    drifter_id,
    global_domain=None,
    boundary=None
):
    """Finalize the modelled tracks.

    Parameters
    ----------
    mod_track : xr.Dataset
        observed drifter track
    mod_drifter : xr.Dataset
        modelled drifter track
    experiment_metadata : dictionary
        dictionary with metadata for the experiment
    run_name : str
        run name
    drifter_id : str
        Unique identifier for drifter
    global_domain : boolean
        A value of True means that a global ocean model domain is being used
    boundary : Matplotlib.Path
        Path defining the boundary that will be used to trim the modelled
        tracks. This boundary is calculated from either an ocean mesh file
        or bbox if given. If no boundary is given tracks will not be trimmmed.
    """

    # Trim the tracks if the modelled drifter leaves the domain. Otherwise
    # this function returns the original lats and lons
    trimmed_track_lons, trimmed_track_lats = \
        keep_in_domain.get_trimmed_tracks(mod_track,
                                          drifter_id,
                                          global_domain=global_domain,
                                          boundary=boundary)

    mod_track.lon.values = trimmed_track_lons
    mod_track.lat.values = trimmed_track_lats

    # calculate skill scores on the data
    skill = get_ocean_skill(mod_track)

    # write the results to a dataset
    ds = xr.Dataset(
        coords={'time': mod_drifter.time.values},
        data_vars={'mod_lat': ('time', mod_track.lat.data),
                   'mod_lon': ('time', mod_track.lon.data),
                   'buoyid': ('model_run', [drifter_id, ])}
                    )

    # Add attrs for Latitude and Longitude
    ds.mod_lat.attrs['units'] = 'degrees_north'
    ds.mod_lat.attrs['_FillValue'] = ds.mod_lat.dtype.type(np.nan)
    ds.mod_lat.attrs['long_name'] = 'Latitude of modelled trajectory'
    ds.mod_lon.attrs['units'] = 'degrees_east'
    ds.mod_lon.attrs['_FillValue'] = ds.mod_lon.dtype.type(np.nan)
    ds.mod_lon.attrs['long_name'] = 'Longitude of modelled trajectory'

    # add some extra attributes if possible
    ds.attrs['mod_run_name'] = run_name

    # can these variables be added to the runs.yaml file?
    keylist = ['atmos_model', 'drifter_depth', 'alpha_wind',
               'ocean_model', 'drift_duration']
    for key in keylist:
        if key in list(experiment_metadata.keys()):
            if key == 'drift_duration':
                d_duration = experiment_metadata['drift_duration']
                ds.attrs['mod_nb_output'] = d_duration
            else:
                ds.attrs['mod_' + key] = experiment_metadata[key]

    # add the common attributes
    for a in mod_drifter.attrs:
        ds.attrs['mod_' + a] = mod_drifter.attrs[a]

    # add the variables and attributes for skills
    for v in skill.data_vars:
        ds[v] = skill[v]

    for a in skill.attrs:
        ds.attrs[a] = skill.attrs[a]

    return ds


def assemble_ocean_predictions(
    *,
    experiment_dir,
    lon_var=None,
    lat_var=None,
    ocean_data_file=None,
    bbox=None
):
    """Assemble drift predictions.

    Parameters
    ----------
    experiment_dir : str
        Path to directory containing drift experiment runs.
    ocean_data_file : str
        Filename of the ocean file to use for domain indices. For MLDPN
        this is currently set to None.
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    bbox : str
        list of coordinates defining the outer edge of the user provided
        bounding box. [minlon, minlat, maxlon, maxlat]
    """

    logger.info('\nAssembling ocean predictions...')

    output_dir = joinpath(experiment_dir, 'output')
    experiment_metadata_filename = joinpath(experiment_dir, 'runs.yaml')
    os.makedirs(output_dir, exist_ok=True)

    with open(experiment_metadata_filename, 'r') as f:
        experiment_metadata = yaml.load(f, Loader=yaml.FullLoader)

    # Set up flags to use when deciding whether to check if the domain
    # is global and whether to create a new domain boundary path
    global_domain = False
    boundary = None
    do_domain_check = True

    # If no ocean_data_file or bbox is present, no track
    # trimming will occur so no checks need to be made:
    if not ocean_data_file and not bbox:
        do_domain_check = False

    # For each modelled track
    for run_name in experiment_metadata['runs']:

        run_metadata = experiment_metadata['runs'][run_name]
        start_date = dateutil.parser.parse(run_metadata['drift_start_date'])

        # Old versions of the metadata contained the number of drift days
        # instead of the drift end date.
        if 'num_drift_days' in run_metadata:
            drift_duration = datetime.timedelta(
                days=run_metadata['num_drift_days'])
            end_date = start_date + drift_duration
        else:
            end_date = dateutil.parser.parse(run_metadata['drift_end_date'])

        run_dir = run_metadata['run_dir']

        if run_metadata['drift_calculation_status'] == 'finished':

            drift_calc_method = run_metadata['drift_calculation_method'].lower()

            if drift_calc_method == 'opendrift':
                filestr = 'OpenDrift*.nc'

            elif drift_calc_method == 'ariane':
                filestr = 'ariane_trajectories_qualitative.nc'

            elif drift_calc_method == 'mldp':
                filestr = 'mldp_trajectories.nc'

            else:
                sys.exit('given drift calculation method does not match '
                         + 'either opendrift, ariane or mldp')

            if ocean_data_file:
                ocean_data_file = joinpath(experiment_dir,
                                           run_dir,
                                           ocean_data_file)

            # Assuming either an ocean_data_file or a bbox exists, check
            # whether the domain is global. If it is not global, create 
            # a boundary.
            if do_domain_check:

                # check if a global domain
                global_domain = keep_in_domain.check_if_global(
                    ocean_data_file=ocean_data_file,
                    lon_var=lon_var,
                    lat_var=lat_var,
                    bbox=bbox
                )

                # if not a global domain, attempt to define a boundary.
                # if the ocean_data_file exists, use that. Otherwise,
                # use the bbox if it exists.
                if not global_domain:
                    boundary = keep_in_domain.find_boundary(
                        ocean_data_file=ocean_data_file,
                        bbox=bbox,
                        lon_var=lon_var,
                        lat_var=lat_var
                    )
                do_domain_check = False

            files = glob.glob(os.path.join(experiment_dir, run_dir, filestr))

            for filename in files:
                try:
                    ds = ioutils.load_trajectories(
                                filename,
                                run_metadata['drift_calculation_method']
                                )
                except ValueError:
                    logger.warn(
                            "{} did not open ""successfully".format(filename)
                            )
                    continue

                # Check that file contains successful trajectories
                if len(ds.time.values) == 0:
                    continue

                trajectory_ids = ds['trajectory_id'].values

                logger.info(('\nCreating dataset and appending transport '
                             + 'calculations for ' + str(run_name) + '...'))

                datasets = []
                for drifter_id in trajectory_ids:

                    mod_drifter = ds.sel(trajectory_id=drifter_id)
                    mod_track = get_drifter_track(
                                        mod_drifter,
                                        start_date,
                                        end_date
                                        )
                    mod_drifter['lon'] = utils.wrap_to_180(mod_drifter.lon)

                    # trim the tracks if necessary and write the resulting
                    # track to an output file.
                    indv_track_ds = finalize_modelled_track(
                        mod_track,
                        mod_drifter,
                        experiment_metadata,
                        run_name,
                        drifter_id,
                        global_domain=global_domain,
                        boundary=boundary
                    )

                    # add it all to the common dataset
                    datasets.append(indv_track_ds)

        ###########################################################
        # write the tracks to a single output file
        ###########################################################
        output_file = '{}.nc'.format(indv_track_ds.mod_run_name)
        ds = xr.concat(datasets, dim="model_run")
        output_file = joinpath(output_dir, output_file)
        ds.attrs['dwt_output_type'] = 'drift_map'
        ds.to_netcdf(output_file)


def main():
    from . import cli
    cli.run(assemble_ocean_predictions)


if __name__ == '__main__':
    main()
