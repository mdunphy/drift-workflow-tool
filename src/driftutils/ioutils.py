"""
Input/Output Utilities
======================

:Author: Clyde Clements
:Created: 2018-02-05

This module contains utility routines for doing input and output such as
loading datasets.
"""

import glob
import logging
import os
import sys
from os.path import join as joinpath
import re
import shutil
import tempfile

import glob
import numpy as np
import pandas as pd
import xarray as xr
import yaml


logger = logging.getLogger('drifter')

def mesh_rename_transform(ds):
    renames = None
    dims=ds.dims
    if 'deptht' not in dims:
        renames = {'z': 'deptht'}
    if renames:
        ds = ds.rename(renames)
    return ds


def ocean_model_rename_transform(ds):
    renames = None
    dims=ds.dims
    if 'z' not in dims:
        possible_depths = ['depthu', 'depthv', 'deptht', 'depth']
        for d in possible_depths:
            if d in dims:
                renames = {d: 'z'}
    if renames:
        ds = ds.rename(renames)
    return ds


def load_mesh(filename, transform_func=mesh_rename_transform, **kwargs):
    """Load ocean mesh file.

    Parameters
    ----------
    filename : str
        Path to mesh file.

    Returns
    -------
    ds : xarray.Dataset
        Mesh as an xarray ``Dataset``.
    """
    logger.info('Loading mesh from file %s ...', filename)
    with xr.open_dataset(filename, cache=False, decode_times=False) as ds:
        if transform_func is not None:
            ds = transform_func(ds, **kwargs)
        ds.load()
        return ds

def rearrange_positions(positions):
    lons = [lon[0] for lon in list(positions.values())]
    same_value_index = []
    # Here we are getting indices of the same coordinate values
    # Since number of lats and lons are the same. I used lons
    for lon in range(len(lons)-1):
        try:
            if(lons[lon] == lons[lon+lon]):
                same_value_index.append(lon)
        except IndexError:
            continue

    def indices( mylist, value):
        return [i for i,x in enumerate(mylist) if x==value]
    index_list = list()
    for idx in range(same_value_index[1]):
        each_index = indices(lons, lons[idx])
        index_list.append(each_index)
    # Group the list of indices with same value into 2
    All_values= list()
    rows = 2
    for ix in index_list:
        row_index = [ix[start:start+rows] for start in range(0, len(ix), rows)]
        values = []
        for i in range(len(row_index)):
            for j in row_index[i:i+1]:
                for k in j:
                    kev = positions.get(k)
                    values.append(kev)
        new_row_index = [
            values[start:start+rows] for start in range(0, len(values), rows)
            ]
        # Get values of indices in present list
        All_values.append(new_row_index)
        #print(All_values)

    # Now All_values is a list of positions while the values are group the 
    # list is not grouped
    new_positions = [
            All_values[start:start+rows] for start in range(
                                                0, len(All_values), rows
                                                )
            ]
    final_group = []
    for values in new_positions:
        if (len(values) > 1):
            for value in range(len(values)-1):
                for run in range(len(values[value])):
                    final_group.append(
                                    [values[value][run], values[value+1][run]]
                                    )
        elif (len(values) == 1):
            for value in range(len(values)):
                final_group.append(values[value])
    return final_group

def ariane_rename_transform(ds):
    renames = {
        'ntraj': 'trajectory_id',
        'nb_output': 'time',
        'traj_lon': 'lon',
        'traj_lat': 'lat',
        'traj_depth': 'depth',
    }
    ds = ds.rename(renames)
    return ds


def opendrift_rename_transform(ds):
    renames = {
        'ntraj': 'trajectory_id',
        'z': 'depth',
        'trajectory': 'trajectory_id',
        }
    for k in renames.keys():
        if k in ds:
            ds = ds.rename({k:renames[k]})


def mldp_rename_transform(ds):
    renames = {
        'ntraj': 'trajectory_id',
        }
    for k in renames.keys():
        if k in ds:
            ds = ds.rename({k:renames[k]})

def rename_transforms(ds):
    """ Function to replace the individual transform functions. """

    renames = {
            'ntraj': 'trajectory_id', 
            'z': 'depth', 
            'trajectory': 'trajectory_id', 
            'nb_output': 'time', 
            'traj_lon': 'lon', 
            'traj_lat': 'lat', 
            'traj_depth': 'depth',
            }

    for k in renames.keys():
        if k in ds:
            ds = ds.rename({k: renames[k]})
     
    return ds
       

def update_opendrift_ds(buoy_id, ds):
    ds = ds.assign(ntraj=xr.DataArray(np.array([buoy_id]), dims=('ntraj')))
    return ds


def drifter_rename_transform(ds):

    buoyid_list = ['unique_id']
    for name in buoyid_list:
        if name in ds.attrs:
            ds.attrs['buoyid'] = ds.attrs[name]

    lon_list = ['LONGITUDE', 'longitude']
    lat_list = ['LATITUDE', 'latitude']
    time_list = ['TIME', 'data_date']
    renames = {}
    for name in lon_list:
        if name in ds:
            renames[name] = 'lon'
    for name in lat_list:
        if name in ds:
            renames[name] = 'lat'
    for name in time_list:
        if name in ds:
            renames[name] = 'time'
    if renames:
        ds = ds.rename(renames)

    return ds


def date_filter(
        ds,                      # type: xarray.Dataset
        date_coord='data_date',  # type: str
        start_date=None,         # type: datetime.datetime
        end_date=None            # type: datetime.datetime
):  # type(...) -> xarray.Dataset
    if start_date is None and end_date is None:
        return ds

    # Convert data dates to datetime type.
    data_date = pd.to_datetime(ds[date_coord].values)

    # Determine indices within start-time and end-time range.
    if start_date:
        start_idx = np.where(data_date >= start_date)[0].min()
    else:
        start_idx = 0
    if end_date:
        end_idx = np.where(data_date <= end_date)[0].max()
    else:
        end_idx = len(ds[date_coord])

    # Extract data with indices determined above.
    d_dict = dict()
    d_dict[date_coord] = slice(start_idx, end_idx + 1)
    ds = ds.isel(d_dict)
    return ds


def load_ariane_trajectories(filename, transform_func=ariane_rename_transform,
                             **kwargs):
    logger.info('Loading Ariane trajectories from file %s ...', filename)
    with xr.open_dataset(filename) as ds:
        if transform_func is not None:
            ds = transform_func(ds, **kwargs)
        if ds['time'].size > 0:
            ds.load()
        return ds
    

def load_opendrift_trajectories(filename,
                                transform_func=opendrift_rename_transform,
                                **kwargs):
    #logger.debug('\n')
    logger.debug('Loading OpenDrift trajectories from file %s ...', filename)
    with xr.open_dataset(filename) as ds:
        if transform_func is not None:
            ds = transform_func(ds, **kwargs)
        # Mask the lats and lons to account for missing data
        ds['lon'] = ds['lon'].where(ds['status'] == 0)
        ds['lat'] = ds['lat'].where(ds['status'] == 0)
        if ds['time'].size > 0:
            ds.load()
        return ds
    
def load_trajectories(filename, projection_method, **kwargs):
    """ Function to replace individual traj loading functions """

    logstr = ('Loading trajectories from file ... ' + filename)
    logger.debug(logstr)

    with xr.open_dataset(filename) as ds:
        
        ds = rename_transforms(ds, **kwargs)

        projection_method = projection_method.lower()
        if projection_method == 'opendrift':
            ds['lon'] = ds['lon'].where(ds['status'] == 0)
            ds['lat'] = ds['lat'].where(ds['status'] == 0)

        if ds['time'].size > 0:
            ds.load()

        return ds

    
def load_ariane(drifter_id, filename):
    logger.debug('\n')
    logger.debug('Loading Ariane trajectories from file %s ...', filename)
    ds = xr.open_dataset(filename)
    ds = update_opendrift_ds(drifter_id, ds)
    fd, file_name = tempfile.mkstemp(suffix='.nc')
    os.close(fd)
    ds.to_netcdf(file_name)
    ds.close()
    shutil.copyfile(file_name, filename)
    os.remove(file_name)
    return filename


def load_drifter_dataset(filename, transform_func=drifter_rename_transform,
                         **kwargs):
    """Load drifter dataset.

    Parameters
    ----------
    filename : str
        Name of drifter data file.
    transform_func : callable
        Transform function to apply to dataset. This function can do things
        such as rename variables so that the names match what are expected by
        later processing steps.
    kwargs
        Keyword arguments. These arguments are passed to the transform
        function.
    """

    logger.info(
        '* Loading drifter data from file %s ...', os.path.basename(filename)
        )

    with xr.open_dataset(filename) as ds:
        if transform_func is not None:
            ds = transform_func(ds, **kwargs)
        # Ensure longitude follows -180 to 180 convention
        if 'lon' in ds.variables:
            ds.lon[ds.lon > 180] -= 360
        ds.load()
        return ds


def load_drifter_track(filename, transform_func=drifter_rename_transform,
                       name_prefix=None, **kwargs):
    """Load drifter track.

    This function loads a dataset and then selectively filters out only those
    variables and attributes whose names begin with a specified prefix. All
    other variables and attributes are dropped/removed. The remaining variables
    and attributes are renamed by removing the specified prefix.

    If ``name_prefix`` is ``None``, then this function returns the entire
    dataset and behaves identical to ``load_drifter_dataset``.
    """
    ds = load_drifter_dataset(
        filename, transform_func=transform_func, **kwargs)
    if name_prefix is None:
        return ds

    # Remove variables that do not begin with name_prefix; rename the variables
    # that do begin with name_prefix.
    i = len(name_prefix)
    renames = {}
    vars_to_drop = []
    for v in ds.data_vars:
        if v.startswith(name_prefix):
            renames[v] = v[i:]
        else:
            vars_to_drop.append(v)
    ds = ds.drop(vars_to_drop)
    if renames:
        ds = ds.rename(renames)

    # Remove attributes that do not begin with name_prefix; rename the
    # attributes that do begin with name_prefix.
    renames = {}
    attrs_to_drop = []
    for a in ds.attrs:
        if a.startswith(name_prefix):
            renames[a] = a[i:]
        else:
            attrs_to_drop.append(a)
    for a in attrs_to_drop:
        del ds.attrs[a]
    for old, new in renames.items():
        ds.attrs[new] = ds.attrs[old]
        del ds.attrs[old]
    return ds

def concat_dataset(data_dir, id_list):
    """ This function merges individual grid points after 
        calculating the displacement of each point with time.
        This function will only be used if users run daily_drift_map.py.
        Why? run_days, while it is an integer it breaks down into 24 hour
        periods.
    
    Parameters
    ----------
    data_dir : file
        Directory containing the computed trajecjory longitude and latitude 
        values.
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    """
    logger.info('Concatenate modelled data...')
    files = glob.glob(os.path.join(data_dir,'*.nc'))
    datasets = []
    
    for fname in files:
        ds = xr.open_dataset(fname)
        # Assign values to the new coordinate ntraj
        for buoy_id in id_list:
            if buoy_id in fname:
                ds = ds.assign(
                        ntraj=xr.DataArray(np.array([buoy_id]), dims=('ntraj'))
                        )

        # Add this dataset to the list
        datasets.append(ds)

    # Concatenate all the datasets under a new dimension called model_run
    output_file = 'opendrift_trajectories_qualitative.nc'
    ds = xr.concat(datasets, dim="ntraj")
    output_file=os.path.join(data_dir, output_file)
    ds.to_netcdf(output_file)

def remove_unused_lists(ds_value):
    rave = ds_value.values.tolist().copy()
    dataset = []
    for r in rave:
        for q in r:
            dataset.append(q)
    new_value = np.asarray(dataset)
    return new_value


def load_dataset(data):
    """ This function first determines if the input is a file or folder,
    then tries to open it appropriately. If it is a single file, the function
    tries to determine the type of dataset and call the appropriate load
    function. If the dataset type is not known, the xarray ``Dataset`` is
    returned as is. If the input is a folder, the function determines if
    the input files in the folder are of a known type. If so, a mfdataset 
    is returned that has been concatenated along either the 'buoyid' or 
    'exp_ident' dimensions depending on the dataset type. If the type is 
    not known, an attempt is also made to open the files using the default
    xr.open_mfdataset settings.

    Parameters
    ----------
    data : str
        Name of drifter data file to be loaded as an xarray dataset or 
        folder of files to be loaded as a concatenated dataset.
    """

    # Determine whether the input is a file or folder. If it is a file, 
    # load the dataset: 
    if not os.path.isdir(data):
        ds = load_single_dataset(data)

    # If the input is a folder instead, open the files as an mfdataset. 
    else:
        ds = load_dataset_from_directory(data)
    
    return ds


def load_dataset_from_directory(data):
    """ Helper function that takes all netcdf files in a folder and loads
    them as a mfdataset. Currently works for drift tool output, 
    drift correction factor output, and most observed drifters """

    # determine which dimension will be used to combine the datasets:
    files = glob.glob(os.path.join(data, '*.nc'))
    with xr.open_dataset(files[0]) as ds:

        if 'obs_buoyid' in ds.attrs:
            concat_dim = 'buoyid'
            concat_attr = 'obs_buoyid'

        elif ('dwt_output_type' in ds.attrs 
                and ds.dwt_output_type == 'drift_map'):
            concat_dim = 'exp_ident'
            concat_attr = 'mod_run_name'

        else:
            concat_dim = None
            concat_attr = None

    # If the files do not already contain the correct dimension, add it:
    def preprocess(ds):
        if concat_dim not in ds.attrs:
            ds = ds.assign_coords({concat_dim: ds.attrs[concat_attr]})
            ds = ds.expand_dims(concat_dim)
        return ds

    # if the data is of a recognized type, open the files as a mfdataset
    # using the appropriate dimension to concatenate:
    if concat_dim:
        mfds = xr.open_mfdataset(files, preprocess=preprocess,
                                 combine='nested', concat_dim=concat_dim)
        return mfds

    # If the data type is not recognized, try using the default 
    # xr.open_mfdataset parameters instead. If this doesn't work either,
    # print an error message and exit.
    else:
        try:
            mfds = xr.open_mfdataset(files)
            return mfds
        except:
            raise RuntimeError('Cannot concatenate files of the current '
                               + 'input type')

    return mfds


def load_single_dataset(filename):
    """Load a single netcdf files as an xarray dataset.

    This function tries to determine the type of dataset (observed drifter,
    Ariane trajectory output, etc) and then calls the corresponding load
    function to do the proper transforms. If the dataset type is not known, the
    xarray ``Dataset`` is returned as is.

    Parameters
    ----------
    filename : str
        Name of drifter data file.
    """

    # Determine the dataset type since some files require additional 
    # transformations when opening:

    dataset_type = None
    with xr.open_dataset(filename) as query_ds:

        if 'description' in query_ds.attrs:
            for opt in ['CONCEPTS Ocean Drifter', 
                        'UBC drifter trajectory file',
                        'IOS drifter trajectory file']:
                if re.match(opt, query_ds.description, re.IGNORECASE):
                    dataset_type = 'drifter'

        if 'key_sequential' in query_ds.attrs:
            dataset_type = 'ariane'

    # Observed drifter files and ariane trajectory files need to be opened
    # using parameter specific load methods. Most other files should be 
    # opened as a standard xarray dataset directly:

    if dataset_type == 'drifter':
        return load_drifter_dataset(filename)

    elif dataset_type == 'ariane':
        return load_ariane_trajectories(filename)

    else:
        with xr.open_dataset(filename) as ds:
            ds.load()
            return ds


def get_run_ocean_data_dates(ocean_data_dir, model_time):
    logger.debug('Determining dates for ocean data...')
    data_dates = []
    for dirpath, dirnames, filenames in os.walk(ocean_data_dir):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            logger.debug('Examining ocean data file %s...', filename)
            data_filename = joinpath(dirpath, filename)
            ds = xr.open_dataset(data_filename)
            time_counter = ds.coords[model_time]
            data_dates.extend(time_counter.values)
    return np.array(sorted(set(data_dates)))

def reshape_dataset(ds):
    """ This funciton reshapes the dataset after concatenation
        The arrangement of the vaariables after using xr.concat
        should match format of the ariane_qualitative_trajectory file
    
    """
    logger.info('Finalising dataset format...')
    df = xr.Dataset(
        coords={'time': ds.time.values, 'model_point': ds.model_run.values},
        data_vars={
            'mod_lat': (['time', 'model_point'], ds.mod_lat.values.transpose()),
            'mod_lon': (['time', 'model_point'], ds.mod_lon.values.transpose()),
            'mod_dist': (['time', 'model_point'], ds.mod_dist.values.transpose()),
            'mod_disp': (['time', 'model_point'], ds.mod_disp.values.transpose()),
            'track_dist': (['time', 'model_point'], ds.track_dist.values.transpose()),
            'ratio': (['time', 'model_point'], ds.ratio.values.transpose())})
    #Latitude of modelled trajectory
    df.mod_lat.attrs['units'] = 'degrees_north'
    df.mod_lat.attrs['_FillValue'] = ds.mod_lat.dtype.type(np.nan)
    df.mod_lon.attrs['long_name'] =\
        'Longitude of modelled trajectory'
    df.mod_lon.attrs['units'] = 'degrees_east'
    df.mod_lon.attrs['_FillValue'] = ds.mod_lon.dtype.type(np.nan)
    for a in ds.attrs:
        df.attrs[a] = ds.attrs[a]
    
    return df
    

def merge(data_dir, experiment_dir):
    """ This function merges individual grid points after 
        calculating the displacement of each point with time.
        This function will only be used if users run daily_drift_map.py.
        Why? run_days, while it is an integer it breaks down into 24 hour
        periods.
    
    Parameters
    ----------
    data_dir : file
        Directory containing the computed trajecjory longitude and latitude 
        values.
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    """
    logger.info('Merging modelled data for each grid points...')
    os.chdir(experiment_dir)        
    os.makedirs('output', exist_ok=True)
    output_file_dir = os.getcwd()
    output_file_dir = '{}/output'.format(output_file_dir)
    files = glob.glob(os.path.join(data_dir,'*.nc'))
    datasets = []
    
    for fname in files:
        ds = xr.open_dataset(fname)
            # Add this dataset to the list
        datasets.append(ds)

    # Concatenate all the datasets under a new dimension called model_run
    output_file = '{}.nc'.format(ds.mod_run_name)
    ds = xr.concat(datasets, dim="model_run")
    output_file=os.path.join(output_file_dir, output_file)
    ds.to_netcdf(output_file)
        

def get_ocean_data_variables(ocean_data_dir):
    logger.debug('Determining ocean data variables  ...')
    variables = set()
    for dirpath, dirnames, filenames in os.walk(ocean_data_dir):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            # Extract variable name from file name: strip off everything after
            # the underscore.
            i = filename.index('_')
            var = filename[:i]
            variables.add(var)
    return sorted(variables)


def dump_yaml(data, filename, **kwargs):
    flow_style = kwargs.pop('default_flow_style', False)
    with open(filename, 'w') as f:
        yaml.dump(data, f, default_flow_style=flow_style, **kwargs)
