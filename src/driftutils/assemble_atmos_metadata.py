"""
Assemble Ocean Metadata
=======================
:Author: Clyde Clements, Nancy Soontiens
:Created: 2019-10-31

This module assembles metadata from atmospheric data files. It
recursively walks through a given directory containing atmospheric wind data
files and assembles the metadata of all netCDF files found. It then combines
this with the metadata from a specified ocean mesh netCDF file. The metadata
is then written to a specified file.

The metadata output is in YAML format; sample output follows::

  atmos_data:
    '2017-04-01T00:30:00.000000000':
      xwindvel: data/atmos/20170401/wind_U.nc
      ywindvel: data/atmos/20170401/wind_V.nc
    '2017-04-01T01:30:00.000000000':
      xwindvel: data/ocean/20170401/wind_U.nc
      ywindvel: data/ocean/20170401/wind_V.nc
  updated: '2018-02-15T15:22:20.684280'

Requirements/Limitations:

- The specified data directory must contain data from only one atmos model
  configuration and the data files must all be consistent in dimensions and
  variables defined within.
- The directory of atmos data should not contain netCDF files pertaining to
  other data.
"""

import datetime
import os
import os.path
import sys
import yaml

import numpy as np
import xarray as xr
from . import configargparse
from . import utils


logger = utils.logger

defaults = {
    'xwindvel': {'long_name': 'eastward wind', 'nc_name': 'u_wind'},
    'ywindvel': {'long_name': 'northward wind', 'nc_name': 'v_wind'},
    'longitude': {'long_name': 'longitude', 'nc_name': 'nav_lon'},
    'latitude': {'long_name': 'latitude', 'nc_name': 'nav_lat'},
    'time_var': {'long_name': 'time', 'nc_name': 'time_counter'},
}


def assemble_atmos_metadata(
        data_dir, output_file,
        xwindvel=defaults['xwindvel']['nc_name'],
        ywindvel=defaults['ywindvel']['nc_name'],
        longitude=defaults['longitude']['nc_name'],
        latitude=defaults['latitude']['nc_name'],
        time_var=defaults['time_var']['nc_name']):
    """"Assemble atmos metadata.

    Parameters
    ----------
    data_dir : str
        Name of directory containing ocean-model data files.
    output_file : str
        Name of output file to create containing the metadata.

    Other Parameters
    ----------------
    xwindvel : str, optional
        Name of Eastward wind velocity variable in netCDF files.
    ywindvel : str, optional
        Name of Northward wind velocity variable in netCDF files.
    longitude: str, optional
        Name of longitude variable in netCDF files.
    latitude: str, optional
        Name of latitude variables in netCDF files.
    time_var: str, optional
        Name of time variable in netCDF files.
    """
    logger.info('\nAssembling atmos metadata...')
    data_metadata = {}
    output_dir = os.path.dirname(os.path.abspath(output_file))
    variables = dict(xwindvel=xwindvel, ywindvel=ywindvel)

    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            logger.debug('Examining data file %s...', filename)
            data_filename = os.path.join(dirpath, filename)
            ds = xr.open_dataset(data_filename)
            if not os.path.isabs(data_filename):
                # For the metadata constructed below, we want the path of the
                # data file relative to the output directory.
                data_filename = os.path.relpath(data_filename, output_dir)
            # Determine what variables are in this data file.
            variables_found = {}
            for name, nc_name in variables.items():
                if nc_name and nc_name in ds.variables:
                    variables_found[name] = data_filename
            if not variables_found:
                continue
            time_counter = ds.coords[time_var]
            for t in time_counter.values:
                data_date = str(t)
                if data_date in data_metadata:
                    data_metadata[data_date].update(variables_found)
                else:
                    data_metadata[data_date] = variables_found.copy()
                
    now = datetime.datetime.utcnow()
    metadata = dict(
        updated=now.isoformat(),
        atmos_data=data_metadata,
    )
    logger.info('Dumping atmos metadata to file %s...', output_file)
    with open(output_file, 'w') as f:
        yaml.dump(metadata, f, default_flow_style=False)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add_argument(
        '--data_dir', type=str, required=True,
        help='Path to directory containing ocean data files')
    arg_parser.add_argument(
        '-o', '--output', type=str, default='atmos_data.yaml',
        help='Name of metadata output file to create')

    for var, info in defaults.items():
        arg_parser.add_argument(
            '--%s' % var, type=str, default=info['nc_name'],
            help='Name of variable containing %s' % info['long_name'])

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    assemble_atmos_metadata(
        config.data_dir,  config.output,
        xwindvel=config.xwindvel, ywindvel=config.ywindvel,
        longitude=config.longitude, latitude=config.latitude,
        time_var=config.time_var)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
