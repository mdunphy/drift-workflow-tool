"""
Find Nearest Grid Point
=======================

:Author: Clyde Clements
:Created: 2017-07-19

This module finds the indices of a point (or points) on a lat/lon grid that is
closest to a specified lat/lon location.
"""

import logging
import math
import sys

import numpy as np
from pykdtree.kdtree import KDTree
import xarray as xr

from . import configargparse
from . import utils
from .utils import logger

kd_cache = {}


def find_nearest_grid_point(
        lat, lon, dataset, lat_var_name, lon_var_name, n=1
):
    """Find the nearest grid point to a given lat/lon pair.

    Parameters
    ----------
    lat : float
        Latitude value at which to find the nearest grid point.
    lon : float
        Longitude value at which to find the nearest grid point.
    dataset : xarray.Dataset
        An xarray Dataset containing the mesh variables.
    lat_var_name : str
        Name of the latitude variable in the dataset.
    lon_var_name : str
        Name of the longitude variable in the dataset.
    n : int, optional
        Number of nearest grid points to return. Default is to return the
        single closest grid point.

    Returns
    -------
    dist_sq, iy, ix, lat_near, lon_near
        A tuple of numpy arrays:

        - ``dist_sq``: the squared distance between the given lat/lon location
          and the nearest grid points
        - ``iy``: the y indices of the nearest grid points
        - ``ix``: the x indices of the nearest grid points
        - ``lat_near``: the latitude values of the nearest grid points
        - ``lon_near``: the longitude values of the nearest grid points
    """

    key = hash(frozenset(dataset.attrs))

    # Note the use of the squeeze method: it removes single-dimensional entries
    # from the shape of an array. For example, in the GIOPS mesh file the
    # longitude of the U velocity points is defined as an array with shape
    # (1, 1, 1021, 1442). The squeeze method converts this into the equivalent
    # array with shape (1021, 1442).
    latvar = dataset.variables[lat_var_name].squeeze()
    lonvar = dataset.variables[lon_var_name].squeeze()

    if key not in kd_cache:
        rad_factor = math.pi / 180.0
        latvals = latvar[:] * rad_factor
        lonvals = lonvar[:] * rad_factor
        clat, clon = np.cos(latvals), np.cos(lonvals)
        slat, slon = np.sin(latvals), np.sin(lonvals)
        if latvar.ndim == 1:
            # If latitude and longitude are 1D arrays (as is the case with the
            # GIOPS forecast data currently pulled from datamart), then we need to
            # handle this situation in a special manner. The clat array will be of
            # some size m, say, and the clon array will be of size n. By virtue of
            # being defined with different dimensions, the product of these two
            # arrays will be of size (m, n) because xarray will automatically
            # broadcast the arrays so that the multiplication makes sense to do.
            # Thus, the array calculated from
            #
            #   np.ravel(clat * clon)
            #
            # will be of size mn. However, the array
            #
            #   np.ravel(slat)
            #
            # will be of size m and this will cause the KDTree() call to fail. To
            # resolve this issue, we broadcast slat to the appropriate size and
            # shape.
            shape = (slat.size, slon.size)
            slat = np.broadcast_to(slat.values[:, np.newaxis], shape)
        else:
            shape = latvar.shape
        triples = np.array([np.ravel(clat * clon), np.ravel(clat * slon),
                            np.ravel(slat)]).transpose()
        kdt = KDTree(triples)
        kd_cache[key] = (kdt, shape)
    else:
        kdt, shape = kd_cache[key]
        

    dist_sq, iy, ix = _find_index(lat, lon, kdt, shape, n)
    # The results returned from _find_index are two-dimensional arrays (if
    # n > 1) because it can handle the case of finding indices closest to
    # multiple lat/lon locations (i.e., where lat and lon are arrays, not
    # scalars). Currently, this function is intended only for a single lat/lon,
    # so we redefine the results as one-dimensional arrays.
    if n > 1:
        dist_sq = dist_sq[0, :]
        iy = iy[0, :]
        ix = ix[0, :]

    if latvar.ndim == 1:
        lat_near = latvar.values[iy]
        lon_near = lonvar.values[ix]
    else:
        lat_near = latvar.values[iy, ix]
        lon_near = lonvar.values[iy, ix]

    # Most datasets have longitude defined over the range -180 to +180. The
    # GIOPS forecast data, however, currently uses a 0 to 360 range, so we
    # adjust those values where necessary.
    lon_near[lon_near > 180] -= 360

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug('Nearest grid point%s:', 's' if n > 1 else '')
        for k in range(n):
            msg = ('  (iy, ix, lat, lon) = (%s, %s, %s, %s), '
                   'squared distance = %s')
            logger.debug(msg, iy[k], ix[k], lat_near[k], lon_near[k],dist_sq[k])

    return dist_sq, iy, ix, lat_near, lon_near


def _find_index(lat0, lon0, kdt, shape, n=1):
    """Finds the y, x indicies that are closest to a latitude, longitude pair.

    Arguments:
        lat0 -- the target latitude
        lon0 -- the target longitude
        n -- the number of indicies to return

    Returns:
        squared distance, y, x indicies
    """
    if hasattr(lat0, "__len__"):
        lat0 = np.array(lat0)
        lon0 = np.array(lon0)
        multiple = True
    else:
        multiple = False
    rad_factor = math.pi / 180.0
    lat0_rad = lat0 * rad_factor
    lon0_rad = lon0 * rad_factor
    clat0, clon0 = np.cos(lat0_rad), np.cos(lon0_rad)
    slat0, slon0 = np.sin(lat0_rad), np.sin(lon0_rad)
    q = [clat0 * clon0, clat0 * slon0, slat0]
    if multiple:
        q = np.array(q).transpose()
    else:
        q = np.array(q)
        q = q[np.newaxis, :]
    dist_sq_min, minindex_1d = kdt.query(np.float32(q), k=n)
    iy_min, ix_min = np.unravel_index(minindex_1d, shape)
    return dist_sq_min, iy_min, ix_min


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--lat', type=float, required=True, help='Latitude')
    arg_parser.add('--lon', type=float, required=True, help='Longitude')
    arg_parser.add('--lat_var_name', type=str, default='nav_lat',
                   help='Name of latitude variable in mesh file')
    arg_parser.add('--lon_var_name', type=str, default='nav_lon',
                   help='Name of longitude variable in mesh file')
    arg_parser.add('--mesh_file', type=str, required=True,
                   help='Name of NetCDF file containing lat/lon mesh')
    arg_parser.add('--num_nearest_points', type=int, default=1,
                   help='Number of nearest points to find')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    dataset = xr.open_dataset(config.mesh_file, decode_times=False)
    dist_sq, iy, ix, lat_near, lon_near = find_nearest_grid_point(
        config.lat, config.lon, dataset, config.lat_var_name,
        config.lon_var_name, n=config.num_nearest_points
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
