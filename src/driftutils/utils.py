"""
Utility Routines
================
:Author: Clyde Clements
:Contributors: Jennifer Holden
:Created: 2017-08-04
"""

from collections import namedtuple
import logging
import os
from os import path
import shutil
import yaml
import glob
import numpy as np
import xarray as xr

# For backward compatibility.
from .cli import initialize_logging, log_level, shutdown_logging  # noqa: F401

# Global variables.
logger = logging.getLogger('drifter')

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

# Default variable names.
defaults = {
    'xwatervel': {'long_name': 'X water velocity', 'nc_name': 'vozocrtx'},
    'ywatervel': {'long_name': 'Y water velocity', 'nc_name': 'vomecrty'},
    'zwatervel': {'long_name': 'Z water velocity', 'nc_name': 'vovecrtz'},
    'temperature': {'long_name': 'temperature', 'nc_name': 'votemper'},
    'salinity': {'long_name': 'salinity', 'nc_name': 'vosaline'},
    'density': {'long_name': 'density', 'nc_name': 'density'},
    'xwindvel': {'long_name': 'eastward wind', 'nc_name': 'u_wind'},
    'ywindvel': {'long_name': 'northward wind', 'nc_name': 'v_wind'},
    'start_hrs': ['00', '06', '12', '18'],
}


def interpolate_track(obs, mod, start=0, end=None):
    """Linearly interpolate modelled positions to times when observed
    positions were recorded.

    Parameters
    ----------
    obs : xarray.Dataset
        Observed trajectory data. Required elements:
        * `time`: DataArray containing timestamps
        * `lon`: DataArray containing longitudes
        * `lat`: DataArray containing latitudes
    mod : xarray.Dataset
        Modelled trajectory data. It must contain the same element types as
        `obs`.

    Returns
    -------
    interp_track : xarray.Dataset
        Interpolated track with a `time` coordinate and data variables `lon`
        and `lat`.
    """
    if end is None:
        oTime = obs['time'][start:]
    else:
        oTime = obs['time'][start:end]
    lat = np.interp(
        oTime.astype('float64').values, mod['time'].astype('float64').values,
        np.squeeze(mod['lat'].values))
    lon = np.interp(
        oTime.astype('float64').values, mod['time'].astype('float64').values,
        np.squeeze(mod['lon'].values))
    interp_track = xr.Dataset(
        data_vars={'lon': ('time', lon), 'lat': ('time', lat)},
        coords={'time': oTime})
    return interp_track


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    with np.errstate(invalid='ignore'):
        x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


drift_model_symbols = {'Ariane': 'ar', 
                        'MLDP': 'ml', 
                        'MLDPn': 'ml', 
                        'mldp': 'ml', 
                        'mldpn': 'ml', 
                        'OpenDrift': 'od'
                        }

def set_run_name(ocean_model, drift_model, start, duration):
    """Construct name for drift simulation from given parameters.

    Parameters
    ----------
    ocean_model : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    drift_model : str
        Name of model for drift simulation (e.g. Ariane, MLDP or OpenDrift).
    start : datetime.datetime
        Start date of drift trajectory calculation.
    duration : datetime.timedelta
        Duration of drift trajectory calculation.

    Returns
    -------
    run_name : str
        Name for drift simulation.

    Details
    -------
    The name will have the following format::

       omod_dmod_YYYYmmddHH[MM]_P[<num_days>D][T<num_hours>H]

    The components of the name are:

    - ``omod``: The name of the ocean model converted to lowercase and spaces
      replaced by dashes.

    - ``dmod``: A two-letter code for the drift model, with ``ar`` representing
      Ariane, ``ml` for MLPD and ``od`` for OpenDrift.

    - ``YYYYmmddHH[MM]``: The start date of the drift trajectory. Note start
      dates will normally begin on the hour in which case the 0 minute
      component will not be present. Start dates will not include a component
      for seconds as such a resolution is not expected to be needed.

    - ``P<num_days>D<num_hours>H``: The remaining part of the name corresponds
      to the duration of drift and is represented in the ISO 8601 format for
      durations with the assumption that drift durations will only be specified
      to the nearest hour. If non-zero, the number of hours will be represented
      in a zero-padded field of width 2. The number of days will not be zero
      padded.

    Examples
    --------
    For a start date of 2017-04-12, drift duration of 48 hours and using GIOPS
    model data with Ariane, the run name will be "giops_ar_2017041200_P2D".
    Note that a start date without a time component corresponds to midnight.

    For a start date of 2017-04-12T16:30 and drift duration of 12 hours with
    Salish Sea model data and Ariane particle model, the run name will be
    "salish-sea_ar_201704121630_PT12H".
    """
    run_name = ocean_model.lower()
    run_name = run_name.replace(' ', '-')
    try:
        drift_model_symbol = drift_model_symbols[drift_model]
    except KeyError:
        valid_models = ', '.join(drift_model_symbols.keys())
        msg = 'Unknown drift model "{0}"; valid choices are {1}'.format(
            drift_model, valid_models)
        raise ValueError(msg)
    run_name += '_' + drift_model_symbol + '_'

    if start.second != 0:
        logger.warn(('Non-zero second component for start date will not be '
                     'represented in the run name.'))
    if start.minute == 0:
        run_name += start.strftime('%Y%m%d%H')
    else:
        run_name += start.strftime('%Y%m%d%H%M')

    minutes, seconds = divmod(int(duration.total_seconds()), 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    date = ''
    time = ''
    if days > 0:
        date = '{}D'.format(days)
    if hours > 0:
        time += '{:02}H'.format(hours)
    if minutes > 0:
        # time += '{:02}M'.format(minutes)
        logger.warn(('Non-zero minute component for drift duration will not '
                     'be represented in the run name.'))
    if seconds > 0:
        # time += '{:02}S'.format(seconds)
        logger.warn(('Non-zero second component for drift duration will not '
                     'be represented in the run name.'))
    if time:
        time = 'T' + time
    run_name += '_P' + date + time

    return run_name



def find_subset_indices(lat_min, lat_max, lon_min, lon_max, lats, lons):
    """Finds indices in the arrays lats and lons containing the specified box.
       This function is called in the plot_bathemetry module.
    
    Parameters
    ----------
    lat_min : float
    lat_max : float
    lon_min : float
    lon_max : floatimport matplotlib
    lats : numpy.ndarray
    lons : numpy.ndarray

    Returns
    -------
    LatLonBoundingBox
        The integer indices specified as a dictionary with keys 'lat_min',
        'lat_max', 'lon_min', and 'lon_max'.
    """
    distances1 = []
    distances2 = []
    index = 1

    for point in lats:
        s1 = lat_max - point
        s2 = lat_min - point
        distances1.append((s1 * s1, point, index))
        distances2.append((s2 * s2, point, index - 1))
        index = index + 1

    distances1.sort()
    distances2.sort()
    ilat_max = distances1[0][2]
    ilat_min = distances2[0][2]

    distances1 = []
    distances2 = []
    index = 1

    for point in lons:
        s1 = lon_max - point
        s2 = lon_min - point
        distances1.append((s1 * s1, point, index))
        distances2.append((s2 * s2, point, index - 1))
        index = index + 1

    distances1.sort()
    distances2.sort()
    ilon_max = distances1[0][2]
    ilon_min = distances2[0][2]

    return LatLonBoundingBox(lon_min=ilon_min, lat_min=ilat_min,
                             lon_max=ilon_max, lat_max=ilat_max)

def grab_buoyids_from_filenames(datadir):
    """ make a list of unique ids
    
    Parameters
    ----------
    datadir : str
        path to the drift tool output files
    
    Returns
    -------
    a list of strings representing the unique buoyids
    
    Details
    -------
    filenames in the data dir are expected to have the following format::
       P[<num_days>D]-drifterid_[extra_metadata].nc
    
    The components of the name are:
    
    - ``P<num_days>D<num_hours>H``: The remaining part of the name corresponds
      to the duration of drift and is represented in the ISO 8601 format for
      durations with the assumption that drift durations will only be specified
      to the nearest hour. If non-zero, the number of hours will be represented
      in a zero-padded field of width 2. The number of days will not be zero
      padded.
    
    - ``drifterid``: The unique buoyid for a single observed drifter
    
    - ``[extra_metadata]``: additional filename modifiers specific to drift
        tool output or cropped comparison set output.
    """
    iname = []
    for fpath in glob.glob(os.path.join(datadir, '*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[0].split('-')[1])
    return np.unique(iname)



def grab_buoyids_from_attrs(datadir):
    """ make a list of unique ids based on the obs_buoyid attr

    Parameters
    ----------
    datadir : str
        path to the drift tool output files

    Returns
    -------
    a list of strings representing the unique buoyids

    Details
    -------
    filenames in the data dir are expected to end in .nc
    """
    files = glob.glob(os.path.join(datadir, '*.nc'))
    buoyids = []
    for file in files:
        with xr.open_dataset(file) as ds:
            if 'obs_buoyid' in ds.attrs:
                buoyids.append(ds.obs_buoyid)
            else:
                print('There is no attribute called obs_buoyid in ', str(file),
                      '. No buoyid added to the list for this file')
    return list(np.unique(buoyids))

def load_yaml_config(config_file):
    """ load yaml config file

    Parameters
    ----------
    config_file : str
        path to the yaml configuration file containing input and output
        directories for the files to crop

    Returns
    -------
    Python object
    """
    with open(config_file, "r") as ymlfile:
        try:
            return yaml.safe_load(ymlfile)
        except yaml.YAMLError as exc:
            logger.info(exc)


def clean_savedir(folder):
    """ delete all files and folders in a directory
    
    Parameters
    ----------
    folder : str
        path to folder containing files that will be deleted.
    """

    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)

        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)

        except Exception as e:
            lstr = ('Failed to clean out savedir before starting new '
                'experiment. Cannot delete %s. Reason: %s' % (file_path, e))
            sys.exit(lstr)


def setup_savedir(savedir, subname=None, overwrite_savedir=False, clean_folder=True):
    """ sets up a directory if one is not already present

    Parameters
    ----------
    savedir : str
        Path to the directory where the new directory will be created
    subname : str
        Name for a new sub-directory (optional). Defaults to None if 
        creating a folder instead of a subfolder.
    overwrite_savedir : bool
        Boolean used to determine if the output folder will be 
        overwritten. Default behavior is to give an error and exit if 
        the folder already exists.

    Returns
    ----------
    full path to new directory.
    """

    if subname is None:
        subdir = savedir
    else:
        subdir = os.path.join(savedir,str(subname))

    # if the folder does not exist, create it.
    if os.path.isdir(subdir) is False:
        os.makedirs(subdir)

    else:   
        # if the folder already exists
        if overwrite_savedir is False:  
            # If overwrite flag is False, display an error and exit.
            estr = ('\n' + 'The output folder already exists! Please '
                'define a new one in the user config yaml file, '
                'change overwrite_savedir to True in the user config  '
                'yaml file or delete: \n' + subdir + ' \n ')
            #raise ValueError(estr)
            import sys
            sys.exit(estr)
        else:
            # If the flag is True, delete the files that are already in 
            # the savedir to prepare for the new data that will be written.
            logstr = ('overwriting data in ' + subdir)
            #logger.info(logstr)

            if clean_folder:
                clean_savedir(subdir)

    return subdir


def create_full_path_file_list(indir, outdir, filename):
    """
    Save list of all files in a directory to an output file.

    Parameters
    ----------
    indir : str
        The input directory
    outdir : str
        The directory where the output file is saved
    filename : str
        The name of the output file.
    """
    files = []
    outfile = os.path.join(outdir, filename)    
    for dirpath, dirnames, filenames in os.walk(indir, followlinks=True):
        for filename in filenames:
            files.append(os.path.abspath(os.path.join(dirpath, filename)))
    f =  open(outfile, 'w')
    for item in files:
        f.write(item + '\n')
    f.close()


def debuginfo(val, message=None):
    """ helper script to provide more useful debug info (linenum,filename,etc)

    Parameters
    ----------
    message : str
        identifying string to print. The name of the val that will be 
        printed works well. ex: 'my_list'
    val : val
        the thing to be printed. ex: my_list

    Returns
    -------
    Function prints output in the following form:

    <blank line>
    myfile.py: line ### - 'my_list'
    [elem1 elem2 elem3...]
    <blank line>
    """
    from inspect import getframeinfo, stack

    if message:
        mes_str = ' - ' + message
    else:
        mes_str = ' '

    caller = getframeinfo(stack()[1][0])
    bname = os.path.basename(caller.filename)
    logger.info(' ')
    logger.info('******************************************************')
    logstr = "%s: line %d%s" % (bname, caller.lineno, mes_str) 
    logger.info(logstr)
    logger.info('******************************************************')
    logger.info(val)
    logger.info(' ')


def round_up_base10(number):
    """ Routine for clean rounding up based on powers of 10
        eg. 1475 rounds to 2000
        15 rounds to 20
        0.1545645 rounds to 0.2

        arg number: float
            number to be rounded
    """
    if number != 0:
        power = np.floor(np.log10(np.abs(number)))
        result = 10**power*np.ceil(number/10**power)
        if number < 0:
           result = -result
    else:
        result = 0
    return result

