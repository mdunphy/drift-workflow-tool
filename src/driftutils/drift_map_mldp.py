"""
Run MLDP drift map
=============================
:Author: Samuel T. Babalola
:Created: 2018-09-19

This module runs drift map without preprocess using MLDP.
"""

import collections
import datetime
import dateutil.parser
import dateutil.rrule
import glob
import numpy as np
import os
from os import path
from os.path import abspath, isabs, join as joinpath
import subprocess
import sys
import xarray as xr

from driftutils.adjust_bbox import adjust_bbox
from driftutils.assemble_mldp_model_metadata import assemble_mldp_model_metadata
from driftutils.assemble_mldp_model_data import (assemble_preprocessed_data,
                                                 assemble_raw_data)
from driftutils.assemble_ocean_predictions import assemble_ocean_predictions
from driftutils.ioutils import dump_yaml
from driftutils.make_mldp_namelist import make_mldp_namelist
from driftutils.mldp_preprocess import mldp_preprocess
from driftutils.mldp_pre_eval import convert_ocean_predictions
from driftutils.run_mldp import run_mldp
from driftutils.set_mapped_initial_positions import set_mapped_initial_positions_mldp
from driftutils import utils
from driftutils.utils import (defaults, logger, set_run_name)

LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max',
                                          'lon_min', 'lon_max'))

logger = utils.logger

def drift_map_mldp(*,
        experiment_dir,
        ocean_data_dir,
        atmos_data_dir,
        ocean_model_name,
        drift_duration,
        num_particles_x,
        num_particles_y,
        initial_bbox,
        first_start_date,
        last_start_date,
        run_preprocessing=True,
        preprocess_dir=None,
        res=1000,
        resmask=100,
        drifter_depth=0,
        alpha_wind=3,
        current_uncertainty=0,
        drift_model_name ='MLDP',
        mldp_exec='MLDPn',
        start_date_frequency='daily',
        start_date_interval=1,
        mldp_dt=60):

    """Run drift map workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    atmos_data_dir : str
        Path to directory containing atmos-model data files
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    drift_duration : datetime.timedelta
        Duration of drift.
    num_particles_x : int
        number of x particles
    num_particles_y : int
        number of y  particles
    initial_bbox : str
        A bounding box to specifiy area over which particles are initialized.
        Formatted as 'lon_min lat_min lon_max lat_max'.
    first_start_date : datetime.datetime
        Start date for first drift simulation.
    last_start_date : datetime.datetime
        Start date for last drift simulation.
    run_preprocessing : boolean
        Run pre-processing of rpn files.
    preprocess_dir : str
       Path to pre-processed rpn files. Only needed if
       run_prerocessing=False, otherwise set to None and a
       new directory will be created.
    res : float
        Resolution of output grid for pre-processed rpn files
    resmask : float
        Resolution of pre-preocesed mask.
        Should equal zero if no mask.in is needed.
    drifter_depth : float
        Specifies the depth in metres for drift trajectory simulations.
    alpha_wind : float
        Percentage of wind velocity to be applied to drifter.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*mldp_dt
    drift_model_name : str
        Name of drift model (one of "Ariane", "MLDP" or "OpenDrift").
    mldp_exec : str
        Path to MLDP executable.
    start_date_frequency : str
        Identifies the type of recurrence rule for start dates. The default
        value is "daily", to specify repeating events based on an interval of
        a day or more.
    start_date_interval : int
        A positive integer representing how often the recurrence rule for start
        dates repeats. The default value is 1, meaning every day for a daily
        rule. A value of 2 would mean every two days for a daily rule and so
        on.
    mldp_dt : float
        Integration time step in seconds.
"""
    # Modify None settings
    if preprocess_dir == 'None':
        preprocess_dir = None

    # Track details about each run such as run directory and the workflow start
    # and finish times.
    run_details = {
        'ocean_model': ocean_model_name,
        'runs': {},
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }
    run_details_file = abspath(joinpath(experiment_dir, 'runs.yaml'))
    ocean_metadata_file = abspath(joinpath(experiment_dir, 'ocean_data.yaml'))
    atmos_metadata_file = abspath(joinpath(experiment_dir, 'atmos_data.yaml'))

    logger.info('\nCreating workflow directory...')
    utils.setup_savedir(experiment_dir)
    os.chdir(experiment_dir)

    # Assemble model metadata
    assemble_mldp_model_metadata(
        ocean_data_dir,
        output_file=ocean_metadata_file,
        model_type='ocean'
    )
    
    assemble_mldp_model_metadata(
        atmos_data_dir,
        output_file=atmos_metadata_file,
        model_type='atmos'
    )

    # Expand bbox for mldp
    mldpn_bbox = adjust_bbox(initial_bbox, scale_factor=0.5).split()
    if run_preprocessing:
        preprocess_dir = joinpath(experiment_dir, 'preprocessing_output')
        os.makedirs(preprocess_dir)
        if resmask != 0:
            # Mask generation is required which can be slow over large domains.
            # Check size of bbox and warn user if it is very large.
            # Too large is 0.2deg in longitude or latitude.
            diff_lon = np.abs(float(mldpn_bbox[0]) - float(mldpn_bbox[2]))
            diff_lat = np.abs(float(mldpn_bbox[1]) - float(mldpn_bbox[3]))
            if diff_lon > 0.2 or diff_lat > 0.2:
                logger.warn("\nWARNING: Requested bbox {} is very large."\
                            " Pre-processing may take a long time to"\
                            " complete.".format(initial_bbox))
        else:
            logger.warn("\nWARNING: Running with resmask={} may lead to"\
                        " inaccurate results near coastlines.".format(resmask))
        # Reformat mldpn_bbox as string
        mldpn_bbox_str = " ".join(mldpn_bbox)
        # Look up files required for pre-prcessing
        wetfiles = joinpath(preprocess_dir, 'wetfiles')
        assemble_raw_data(
            first_start_date,
            last_start_date + drift_duration,
            ocean_metadata_file,
            wetfiles,
            model_type='ocean'
        )
        metfiles = joinpath(preprocess_dir, 'metfiles')
        assemble_raw_data(
            first_start_date,
            last_start_date + drift_duration,
            atmos_metadata_file,
            metfiles,
            model_type='atmos'
        )

        mldp_preprocess(
            wetfiles=wetfiles,
            metfiles=metfiles,
            preprocess_dir=preprocess_dir,
            bbox=mldpn_bbox_str,
            depth=drifter_depth,
            res=res,
            resmask=resmask
        )
    else:
        if preprocess_dir is None:
           msg="preprocess_dir is not defined in config file. "\
               "Please define preprocess_dir or set "\
               "run_preprocessing=True in the config file."
           raise RuntimeError(msg)
        if not os.path.exists(preprocess_dir):
            msg="Cannot find preprocessed input data because "\
                "preprocess_dir {} does not exist. "\
                "Please check preprocess_dir in config file or set "\
                "run_preprocessing=True.".format(preprocess_dir)
            raise RuntimeError(msg)
        if os.path.exists(preprocess_dir) and os.path.isdir(preprocess_dir):
            if not os.listdir(preprocess_dir):
                msg="Cannot find preprocessed input data. Please set "\
                    "run_preprocessing=True in the config file, "\
                    "or copy the preprocessed data to {}".format(preprocess_dir)
                raise RuntimeError(msg)
    # Compile metadata on pre-processed data
    preprocess_metadata_file = abspath(joinpath(experiment_dir,
                                                'preprocess_data.yaml'))
    assemble_mldp_model_metadata(
        preprocess_dir,
        output_file=preprocess_metadata_file,
        model_type='preprocess'
    )


    # Interpret current_uncertainty
    if current_uncertainty != 0:
        logger.info("Trajectories will be computed with a horizontal random "
                    "walk with rms ocean velocity {} m/s and equivalent diffusivity "
                    "K={} m^2/s".format(current_uncertainty,
                                        0.5*current_uncertainty**2*mldp_dt))

    rrule_freq_dict = {
        'yearly': dateutil.rrule.YEARLY, 'monthly': dateutil.rrule.MONTHLY,
        'weekly': dateutil.rrule.WEEKLY, 'daily': dateutil.rrule.DAILY,
        'hourly': dateutil.rrule.HOURLY, 'minutely': dateutil.rrule.MINUTELY,
        'secondly': dateutil.rrule.SECONDLY
    }
    rrule = dateutil.rrule.rrule(
        freq=rrule_freq_dict[start_date_frequency],
        interval=start_date_interval, dtstart=first_start_date,
        until=last_start_date)

    if rrule.count() > 1:
        rrule_increment = rrule[1] - rrule[0]
    else:
        rrule_increment = datetime.timedelta(0, 0)
    logger.info(
        ('\nTrajectories will be calculated with starting dates running from %s '
         'to %s of duration %s with an increment of %s between dates'),
        first_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        last_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        str(drift_duration), str(rrule_increment))

    for start_date in rrule:
        logger.info(
            'Beginning trajectory calculations for start date %s...',
            start_date.strftime('%Y-%m-%d %H:%M:%S')
        )
        end_date = start_date + drift_duration
        run_name = set_run_name(
                        ocean_model_name,
                        drift_model_name,
                        start_date,
                        drift_duration)
        run_base_dir = joinpath('runs', run_name)
        mldp_run_dir = joinpath(experiment_dir, run_base_dir)
        utils.setup_savedir(mldp_run_dir)
        cwd = os.getcwd()
        os.chdir(mldp_run_dir)

        run_details['runs'][run_name] = {
            'drift_start_date': start_date.isoformat(),
            'drift_end_date': end_date.isoformat(),
            'num_drifters': 0,
            'drift_calculation_status': 'no_drifters',
            'run_dir': run_base_dir,
            'updated': datetime.datetime.utcnow().isoformat()
        }

        dump_yaml(run_details, run_details_file)
        meteo_dir = joinpath(mldp_run_dir, 'meteo')
        meteo_metadata = assemble_preprocessed_data(
            start_date,
            end_date,
            preprocess_metadata_file,
            meteo_dir
        )
        if len(meteo_metadata) <= 2:
            logger.info(
                "{} does not have data for simulation".format(start_date)
                )
            with open('NOTE', 'w') as f:
                f.write('{} does not contain enough preprocessed data'\
                        ' for entire simulation duration'.format(meteo_dir))
            continue
         
        os.chdir(mldp_run_dir)

        drifter_positions = set_mapped_initial_positions_mldp(
                                        start_date,
                                        drifter_depth,
                                        initial_bbox,
                                        num_particles_x,
                                        num_particles_y,
                                        bbox_file=os.path.join(experiment_dir,
                                                               'namelist_bbox'),
                                        output_file='drifter_positions.yaml'
                                        )
        meteo_files = glob.glob(joinpath(meteo_dir, '*'))
        meteo_files.sort()
        namelist = make_mldp_namelist(
                                    meteo_files,
                                    preprocess_dir,
                                    ocean_model_name,
                                    mldp_run_dir,
                                    start_date,
                                    drifter_positions,
                                    drift_duration,
                                    alpha_wind,
                                    current_uncertainty,
                                    mldp_dt,
                                    'MLDP.in'
                                    )

        # Information about this run.
        run_info = {
            'drift_start_date': start_date.isoformat(),
            'drift_end_date': end_date.isoformat(),
            'drift_calculation_method': 'mldp',
            'drift_calculation_status': 'in_progress',
            'run_dir': run_base_dir,
            'updated': datetime.datetime.utcnow().isoformat()
        }

        start_time = datetime.datetime.utcnow()

        # Run MLDP
        run_dir = os.path.join(mldp_run_dir, 'results')
        results = run_mldp(mldp_exec, namelist, run_dir)
        finish_time = datetime.datetime.utcnow()

        # Covert csv output to NetCDF file formats
        convert_ocean_predictions(results, 
                                  drift_duration, 
                                  alpha_wind, 
                                  mldp_run_dir
        )
        run_info.update({
            'drift_calculation_status': 'finished',
            'drift_calculation_start_time': start_time.isoformat(),
            'drift_calculation_finish_time': finish_time.isoformat(),
            'updated': datetime.datetime.utcnow().isoformat()
        })
        run_details['runs'][run_name] = run_info
        run_details['updated'] = datetime.datetime.utcnow().isoformat()
        # Update run information file. This happens on *every* loop which means
        # the output file is overwritten in every loop with increasingly more
        # detail, rather than writing it once at the end of all of the runs.
        # This method is inefficient and could be improved, but the suspected
        # small overhead is outweighed by the advantage of having the file
        # contain *some* information part way through a long run.
        dump_yaml(run_details, run_details_file)
        os.chdir(cwd)

    now = datetime.datetime.utcnow().isoformat()
    run_details['workflow_finish_time'] = now
    run_details['updated'] = now
    dump_yaml(run_details, run_details_file)
    assemble_ocean_predictions(experiment_dir=experiment_dir,
                               bbox=' '.join(mldpn_bbox))


def main():
    from driftutils import cli
    cli.run(drift_map_mldp)

if __name__ == '__main__':
    main()

