"""
Get mldp model files using date range given by users
=============================
:Author: Samuel T. Babalola, Nancy Soontiens
:Created: 2018-09-27

"""
import dateutil
import os
from os.path import join as joinpath
import sys
import yaml

from driftutils.assemble_atmos_data import find_model_data_date_period
from driftutils import configargparse
from driftutils import utils

logger = utils.logger


def assemble_raw_data(
        start_date,
        end_date,
        model_metadata_file,
        output_file,
        model_type='ocean'
):
    """Collect file paths for raw rpn model data over specified dates.

    Parameters
    ----------
    start_date : datetime.datetime
        Start date for file path collection.
    end_date : datetime.datetime
        End date for file path collection.
    model_metadata_file : str
        Path to model metadata file.
    output_file : str
        Name of output file to save file paths.
    model_type : str
        Type of model data - atmos or ocean
    """
    logger.info(
        'Assembling {} files for pre-processing...'.format(model_type)
    )
    with open(model_metadata_file, 'r') as f:
        model_metadata = yaml.load(f, Loader=yaml.FullLoader)
    model_data = model_metadata['{}_data'.format(model_type)]
    data_dates = find_model_data_date_period(
        model_data, start_date, end_date,
        time_interval='time_instant')
    files = []
    for data_date_str, model_file in model_data.items():
        data_date = dateutil.parser.parse(data_date_str)
        if data_date in data_dates:
            files.append(model_file)
    files.sort()
    logger.info(
        'Writing {} files for pre-processing to {}... '.format(model_type,
                                                               output_file)
    )
    with open(output_file, 'w') as f:
        for my_file in files:
            f.write("%s\n" % my_file )


def assemble_preprocessed_data(
        start_date,
        end_date,
        preprocess_metadata_file,
        output_dir
):
    """Collect file paths to preprocessed data for a run.

    Parameters
    ----------
    start_date : datetime.datetime
        Start date for file collection.
    end_date : datetime.datetime
        End date for file collection.
    preprocess_metadata_file : str
        Path to preprocess metadata file
    output_dir : str
       Path to destination directory for linking run preprocessed files.
    """
    logger.info(
        'Assembling pre-processed files for start date {}...'.format(
            start_date.strftime('%Y-%m-%d %H:%M:%S'))
    )
    os.makedirs(output_dir)
    with open(preprocess_metadata_file, 'r') as f:
        preprocess_metadata = yaml.load(f, Loader=yaml.FullLoader)
    preprocess_data = preprocess_metadata['preprocess_data']
    data_dates = find_model_data_date_period(
        preprocess_data, start_date, end_date,
        time_interval='time_instant')
    files = []
    for data_date_str, model_file in preprocess_data.items():
        data_date = dateutil.parser.parse(data_date_str)
        if data_date in data_dates:
            files.append(model_file)
    files.sort()
    logger.info(
        'Linking files for pre-processing to {}...'.format(output_dir)
    )
    for f in files:
        basename = os.path.basename(f)
        link_name = joinpath(output_dir, basename)
        os.symlink(f, link_name)
    return files


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')
    arg_parser.add('--start_date', type=str, default='2021-10-25',
                   help='Start date for Metfiles or Wetfiles')
    arg_parser.add('--end_date', type=str, default='2021-10-27',
                   help='End date for Metfiles or Wetfiles')
    arg_parser.add('--model_metadata_file', type=str, default='ocean_data.yaml',
                   help='Path to file containing model metadata')
    arg_parser.add('--output_file', type=str, default='wetfiles',
                   help='Wetfile or Metfile file name')
    arg_parser.add('--model_type', type=str, default='ocean',
                   help='Model type - ocean or atmos')

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])
    start_date = dateutil.parser.parse(config.start_date, ignoretz=True)
    end_date = dateutil.parser.parse(config.end_date, ignoretz=True)

    assemble_raw_data(start_date, end_date,
                      config.model_metadata_file,
                      config.output_file,
                      config.model_type)

    utils.shutdown_logging()

if __name__=='__main__':
    main()
