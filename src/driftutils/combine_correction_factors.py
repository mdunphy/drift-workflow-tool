"""
Combine Drift Correction Factor Outputs
=======================================
:Author: Jennifer Holden
:Created: 2022-05-19
"""

import os
import glob
import xarray as xr
import numpy as np
from driftutils import cli
from driftutils import utils

logger = utils.logger

def combine_outputs(*, data_dir, save_dir=None):
    """ combine correction factor output files from multiple time periods
    into single per-drifter netcdf file that covers the full time range

    Parameters
    ----------
    data_dir : str
        Path to the input files that will be combined
    save_dir : str
        Optional path to the directory where the output files will be written.
        If not provided, a new directory will be created at the same level as
        data_dir called aggregated_correction_factor_output/

    Returns
    -------
    ds : Xarray DataSet
        Xarray Dataset containing the combined per drifter data.
    """

    if not save_dir:
        outdir = os.path.normpath(data_dir)
        parentdir = os.path.dirname(outdir)
        save_dir = os.path.join(parentdir, 'aggregated_correction_factor_output')

    # make sure the save_dir location already exists
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)

    # make a list of buoyids
    idnames = []
    for indv_name in glob.glob(os.path.join(data_dir, '*.nc')):

        # output filenames are expected to have the form:
        # correction-factor_<startdate>_<drifterid>.nc
        bname = os.path.basename(indv_name)
        split_name = bname.replace('.', '_').split('_')

        # keep the drifter id in a list for iteration
        idnames.append(split_name[2])

    # aggregate the files
    for drifterid in np.unique(idnames):

        logger.info('Aggregating files for' + str(drifterid))

        fnames = glob.glob(os.path.join(data_dir, '*{}.nc'.format(drifterid)))
        fnames.sort()

        ds = xr.open_mfdataset(fnames)

        # Add the original filenames as a global attribute
        orignames = []
        for f in fnames:
            orignames.append(f.split('/')[-1])

        # overwrite the old start/end dates and geographic extremes so
        # that they represent the newly aggregated data instead.
        ds.attrs['data_lat_min'] = str(np.nanmin(ds.lat.values))
        ds.attrs['data_lat_max'] = str(np.nanmax(ds.lat.values))
        ds.attrs['data_lon_min'] = str(np.nanmin(ds.lon.values))
        ds.attrs['data_lon_max'] = str(np.nanmax(ds.lon.values))
        ds.attrs['data_StartDate'] = str(ds.time.values[0])
        ds.attrs['data_EndDate'] = str(ds.time.values[-1])
        ds.attrs['original_filenames'] = str(', '.join(np.unique(orignames)))

        # write out the aggregated netcdf
        sname = "correction-factor_{}_aggregated.nc".format(drifterid)
        ds.to_netcdf(os.path.join(save_dir, sname))

    return ds


def main():

    cli.run(combine_outputs)


if __name__ == '__main__':

    main()
