"""
Assemble Ocean Metadata
=======================
:Author: Clyde Clements
:Created: 2017-08-25

This module assembles metadata from ocean mesh and velocity data files. It
recursively walks through a given directory containing ocean velocity data
files and assembles the metadata of all netCDF files found. It then combines
this with the metadata from a specified ocean mesh netCDF file. The metadata
is then written to a specified file.

The metadata output is in YAML format; sample output follows::

  ocean_data:
    '2017-04-01T00:30:00.000000000':
      salinity: data/ocean/20170401/grid_T.nc
      temperature: data/ocean/20170401/grid_T.nc
      xwatervel: data/ocean/20170401/grid_U.nc
      ywatervel: data/ocean/20170401/grid_V.nc
      zwatervel: data/ocean/20170401/grid_W.nc
    '2017-04-01T01:30:00.000000000':
      salinity: data/ocean/20170401/grid_T.nc
      temperature: data/ocean/20170401/grid_T.nc
      xwatervel: data/ocean/20170401/grid_U.nc
      ywatervel: data/ocean/20170401/grid_V.nc
      zwatervel: data/ocean/20170401/grid_W.nc
  ocean_mesh:
    dimensions:
      t: 1
      x: 398
      y: 898
      z: 40
    filename: data/mesh.nc
  ocean_domain:
    lon_min: -100
    lon_max: 100
    lat_min: 40
    lat_max: 60
    depth:
      - 0.5
      - 1.0
    time_interval: time_centered
  updated: '2018-02-15T15:22:20.684280'

Requirements/Limitations:

- The specified data directory must contain data from only one ocean model
  configuration and the data files must all be consistent in dimensions and
  variables defined within.
- The directory of ocean data should not contain netCDF files pertaining to
  other data.
"""

import datetime
import glob
import os
import os.path
import re
import subprocess
import sys
import yaml

import numpy as np
import xarray as xr

from . import configargparse
from . import utils
from . import ioutils


logger = utils.logger

defaults = {
    'xwatervel': {'long_name': 'X water velocity', 'nc_name': 'vozocrtx'},
    'ywatervel': {'long_name': 'Y water velocity', 'nc_name': 'vomecrty'},
    'zwatervel': {'long_name': 'Z water velocity', 'nc_name': 'vovecrtz'},
    'temperature': {'long_name': 'temperature', 'nc_name': 'votemper'},
    'salinity': {'long_name': 'salinity', 'nc_name': 'vosaline'},
    'density': {'long_name': 'density', 'nc_name': 'density'},
    'longitude': {'long_name': 'longitude', 'nc_name': 'nav_lon'},
    'latitude': {'long_name': 'latitude', 'nc_name': 'nav_lat'},
    'depth': {'long_name': 'depth', 'nc_name': 'depth'},
    'time_var': {'long_name': 'time', 'nc_name': 'time_counter'},
}


def assemble_ocean_metadata(
        data_dir, 
        mesh_file, 
        output_file,
        xwatervel=defaults['xwatervel']['nc_name'],
        ywatervel=defaults['ywatervel']['nc_name'],
        zwatervel=defaults['zwatervel']['nc_name'],
        temperature=defaults['temperature']['nc_name'],
        salinity=defaults['salinity']['nc_name'],
        density=defaults['density']['nc_name'],
        longitude=defaults['longitude']['nc_name'],
        latitude=defaults['latitude']['nc_name'],
        depth=defaults['depth']['nc_name'],
        time_var=defaults['time_var']['nc_name']
):
    """"Assemble ocean metadata.

    Parameters
    ----------
    data_dir : str
        Name of directory containing ocean-model data files.
    mesh_file : str
        Name of mesh file.
    output_file : str
        Name of output file to create containing the metadata.

    Other Parameters
    ----------------
    xwatervel : str, optional
        Name of X water velocity variable in netCDF files.
    ywatervel : str, optional
        Name of Y water velocity variable in netCDF files.
    zwatervel : str, optional
        Name of Z water velocity variable in netCDF files.
    temperature : str, optional
        Name of temperature variable in netCDF files.
    salinity : str, optional
        Name of salinity variable in netCDF files.
    density : str, optional
        Name of density variable in netCDF files.
    longitude: str, optional
        Name of longitude variable in netCDF files.
    latitude: str, optional
        Name of latitude variables in netCDF files.
    depth: str, optional
        Name of depth variable in netCDF files.
    time_var: str, optional
        Name of time variable in netCDF files.
    """
    logger.info('\nAssembling ocean metadata...')

    data_metadata = {}
    output_dir = os.path.dirname(os.path.abspath(output_file))


    variables = dict(xwatervel=xwatervel, ywatervel=ywatervel,
                     zwatervel=zwatervel, temperature=temperature,
                     salinity=salinity, density=density)

    first_loop=True

    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):

        for filename in filenames:
            if not filename.endswith('.nc'):
                continue

            logger.debug('Examining data file %s...', filename)

            data_filename = os.path.join(dirpath, filename)
            ds = xr.open_dataset(data_filename)

            if not os.path.isabs(data_filename):
                # For the metadata constructed below, we want the path of the
                # data file relative to the output directory.
                data_filename = os.path.relpath(data_filename, output_dir)

            # Determine what variables are in this data file.
            variables_found = {}
            for name, nc_name in variables.items():
                if nc_name and nc_name in ds.variables:
                    variables_found[name] = data_filename

            if not variables_found:
                continue

            time_counter = ds.coords[time_var]
            for t in time_counter.values:
                data_date = str(t)
                if data_date in data_metadata:
                    data_metadata[data_date].update(variables_found)
                else:
                    data_metadata[data_date] = variables_found.copy()

            if first_loop:
                first_loop = False
                # determine method for recording time - averaged vs instant
                if 'time_centered' in ds.variables:
                    time_interval = 'time_centered'
                elif 'time_instant' in ds.variables:
                    time_interval = 'time_instant'
                else:
                    # possibly revise setting by looking at cell_methods
                    # variable attribute in model netcdf files
                    time_interval = 'unknown'

                # Determine domain coverage
                if mesh_file == 'None':
                    lon = ds.variables[longitude].values
                    # Follow convention for lon -180 to 180
                    lon[lon>180] = lon[lon>180] - 360
                    lon_min = np.min(lon[np.nonzero(lon)])
                    lon_max = np.max(lon[np.nonzero(lon)])
                    lat = ds.variables[latitude].values
                    lat_min = np.min(lat[np.nonzero(lat)])
                    lat_max = np.max(lat[np.nonzero(lat)])
                    ndims = len(ds.dims)

                    if ndims == 4:
                        depths = ds.variables[depth].values
                        if depths.ndim == 4: # handle case if 3D depth
                            depths =  depths[0, :, 0, 0]
                        elif depths.ndim == 3:
                            depths = depths[:, 0, 0]
                    else:
                        # Files are 2D (time, y, x)
                        logger.warn(("Ocean data files do not contain a "
                                     "a depth dimension. \n"
                                     "Ariane simulations will not work.\n"
                                     "OpenDrift simulations will ignore "
                                     "the drifter_depth argument."))
                        depths = np.array(['unknown'])
                    depths = np.squeeze(depths).tolist()
                    domain_metadata = dict(lon_min=float(lon_min),
                                           lon_max=float(lon_max),
                                           lat_min=float(lat_min),
                                           lat_max=float(lat_max),
                                           depth=depths,
                                           filename=data_filename,
                                           time_interval=time_interval)
                    mesh_metadata = dict(filename=mesh_file)
            ds.close()

    if first_loop:
        # There was no ocean data so exit
        msg = ("No ocean data for given selected time period and variables in "
                "directory {}. Please check that required ocean data "
                "exists.\n"
                "HINT: ocean data files must have .nc extension.".format(data_dir))
        raise RuntimeError(msg)

    if mesh_file != 'None':
        # if there is no mesh file:
        logger.debug('Examining mesh file %s...', mesh_file)
        mesh_ds = xr.open_dataset(mesh_file, decode_times=False)

        if not os.path.isabs(mesh_file):
            # Determine path of mesh file relative to directory of output file.
            odir = os.path.dirname(os.path.abspath(output_file))
            mesh_file = os.path.relpath(mesh_file, odir)

        mesh_metadata = dict(
            filename=mesh_file,
            dimensions=dict(mesh_ds.dims)
        )

        # Determine domain coverage
        lon = mesh_ds.variables[longitude].values
        # Follow convention for lon -180 to 180
        lon[lon>180] = lon[lon>180] - 360
        lon_min = np.min(lon[np.nonzero(lon)])
        lon_max = np.max(lon[np.nonzero(lon)])
        lat = mesh_ds.variables[latitude].values
        lat_min = np.min(lat[np.nonzero(lat)])
        lat_max = np.max(lat[np.nonzero(lat)])
        depths = mesh_ds.variables[depth]
        if depths.ndim == 4: # handle case if 3D depth (gdepw_0) passed  
            depths = depths.values[0, :, 0, 0]
        elif depths.ndim == 3:
            depths = depths.values[:, 0, 0]
        else:
            depths = depths.values

        depths = np.squeeze(depths).tolist()
        domain_metadata = dict(lon_min=float(lon_min),
                               lon_max=float(lon_max),
                               lat_min=float(lat_min),
                               lat_max=float(lat_max),
                               depth=depths,
                               filename=mesh_file,
                               time_interval=time_interval)
        mesh_ds.close()

    now = datetime.datetime.utcnow()
    metadata = dict(
        updated=now.isoformat(),
        ocean_mesh=mesh_metadata,
        ocean_data=data_metadata,
        ocean_domain=domain_metadata
    )
    logger.info('Dumping ocean metadata to file %s...', output_file)
    with open(output_file, 'w') as f:
        yaml.dump(metadata, f, default_flow_style=False)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add_argument(
        '--data_dir', type=str,
        default='/data/ocean/users/cclements/data/GIOPS/daily',
        help='Path to directory containing ocean data files')
    arg_parser.add_argument(
        '--mesh_file', type=str,
        default='None',
        help='Path to file containing mesh for ocean model')
    arg_parser.add_argument(
        '-o', '--output', type=str, default='ocean_data.yaml',
        help='Name of metadata output file to create')

    for var, info in defaults.items():
        arg_parser.add_argument(
            '--%s' % var, type=str, default=info['nc_name'],
            help='Name of variable containing %s' % info['long_name'])

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    assemble_ocean_metadata(
        config.data_dir, config.mesh_file, config.output,
        xwatervel=config.xwatervel, ywatervel=config.ywatervel,
        zwatervel=config.zwatervel, temperature=config.temperature,
        salinity=config.salinity, density=config.density,
        longitude=config.longitude, latitude=config.latitude,
        time_var=config.time_var, depth=config.depth)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
