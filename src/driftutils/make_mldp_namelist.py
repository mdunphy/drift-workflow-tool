"""
Create MLDP namelist
=============================
:Author: Samuel T. Babalola
:Created: 2018-09-19

This module write MLDP namelist into an input file.
"""
import collections
import xarray as xr
import numpy as np
import glob
import os
import datetime
import time
import sys

from driftutils import configargparse
from driftutils import utils

logger = utils.logger

LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max', 'lon_min', 'lon_max'))


def make_mldp_namelist(
        meteo_file_list,
        data_dir,
        ocean_model_name,
        output_dir,
        start_date, 
        drifter_positions,
        duration,
        windage,
        current_uncertainty,
        mldp_dt,
        namelist,
        output_dt=3600
):
    
    """ Write a namelist for MLDP.

    Parameters
    ----------
        
    meteo_file_list : list[str] 
        List of pre-processed RPN files.
    data_dir : str
        Specifies the directory containing pre-processed files.
    ocean_model_name : str
        Name of ocean model, eg. CIOPSE
    output_dir : str
        Specifies the directory to store created namelist
    start_date : datetime.datetime
        Start date for runs and ocean files
    drifter_positions : dict
        Dictionary of drifter ids and longitude/latitude coordinates.
        e.g. {'1': [lon1, lat1], '2': [lon2, lat2]} 
    duration : datetime.timedelta
        Duration of drift simulation
    windage : float
        Wind coefficient added to ocean models.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*mldp_dt
    mldp_dt : float
        Integration time step in seconds.
    namelist : str
        Name of the MLDP namelist file to be written.
    output_dt : float
        Output frequency of simluation in seconds.

    Returns
    -------
    mldp_namelist_path
    """ 
    
    file_start_date = start_date.strftime('%Y%m%d')
    mask_file = glob.glob(os.path.join(data_dir, 'mask.in'))
    grid_file = glob.glob(os.path.join(data_dir, 'grid.in'))
    
    # Check for emission start date (This could break MLDP run)
    meteo_file_list.sort()
    emission_hour = start_date.strftime('%H')
    
    duration  = duration.total_seconds()
    windage  = windage / 100
    start_date = start_date.strftime('%Y-%m-%d')
    mldp_namelist_path = os.path.join(output_dir, namelist)
    logger.info('Writing namelist to {}'.format(mldp_namelist_path))
    with open(mldp_namelist_path, mode="w", encoding="utf-8") as f:
        f.write('''
   #----- Model parameters

   MDL_DT_INT             = %s        # Internal model time step \[s\].
   MDL_DT_SUR_TL          = 1.0      # Ratio of diffusion time step over Lagrangian time scale \[dimensionless\].
   MDL_DT_BAS             = 1.0      # Diffusion time step minimum value \[s\].
   MDL_SIG2_V             = 2.00     # Horizontal wind velocity variance for mesoscale fluctuations \[m2/s2\].
   MDL_TL_V               = 10800.0  # Lagrangian time scale \[s\].
   MDL_INCLUDEHORIZDIFF   = 0        # isIncludeHorizDiff Flag indicating if including horizontal diffusion in free atmosphere.
   MDL_INCLUDESUV         = 0        # Flag indicating if including horizontal wind speed variances in diffusion calculations.
   MDL_KERNEL             = DRIFTER    # Diffusion kernel selection method (VARIABLE or kernel name)
   MDL_RETRO              = FALSE    # Backward simulation
   MDL_FM_LIM             = 500.0    # FM limit calculations
   MDL_RMS_UV             = %s
   MDL_ADVECT             = RK4

   #----- Oil parameters
   OIL_EMI          = Diesel_2002_1
   OIL_FATE         = FALSE
   OIL_WIND_VAR     = FALSE
'''%(mldp_dt, current_uncertainty))

        for grid_count in drifter_positions:
            start_position = drifter_positions[grid_count]
            # How do we specify a user defined SRC_TIME while being in the meteorological time index (common error)?
            # A user defined wind coefficient and current coefficient. What are the criteria for these?
            f.write('''
   SRC_NAME             = %s      # Source name ( Drifter ID??)
   SRC_COORD            = %s %s
   SRC_TIME             = %s%s00        # Emission date-time [UTC]: YearMonthDayHourMinute
   SRC_DURATION         = %s
   SRC_EMI_GAUSS        = FALSE               # Source distribution type
   SRC_EMI_INTERVAL     = 1 0 0.0 0.0 1.0 1   # Nb Parcel, Duration, Bottom of source m [AGL], Top
   SRC_WIND_COEFF       = %s                # Source wind coefficient(A.K.A. wind drift factor)
   SRC_CURR_COEFF       = 1.0                 # Source current coefficient 
'''%(grid_count, start_position[1], start_position[0], file_start_date, emission_hour, duration, windage))
            
        for grid in grid_file:
            f.write('''
   #----- Output parameters

   OUT_DT         = %s        # Output time step [s]
   OUT_DELTA      = 0         # Output file interval [h]
   OUT_GRID       = %s        # Output grid
   #----- Meteorological parameters
   MET_BOTTOM = 0.9999     # Bottom reflection level of particles in the atmosphere [hybrid|eta|sigma]
'''%(output_dt, grid))
        for mask in mask_file:
            f.write('   MET_MASK   = %s \n'%(mask))     
        f.write('   MET_FILES  =           # Meteorological input files')
        for meter in meteo_file_list:
            f.write('''
   %s'''%(meter))
    return mldp_namelist_path


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add_argument(
        '--meteo_file_list', type=str,
        help='List of pre-processed RPN files separated by white space. '\
             'e.g. "file1 file2"')
    arg_parser.add_argument(
        '--data_dir', type=str,
        help='Specifies the directory containing pre-processed RPN files.')
    arg_parser.add_argument(
        '--ocean_model_name', type=str,
        help='Name of ocean model. e.g. CIOPSE')    
    arg_parser.add_argument(
        '--output_dir', type=str,
        help='Specifies the directory to store created namelist')
    arg_parser.add_argument(
        '--start_date', type=str,
        help='Start date for runs and ocean files')
    arg_parser.add_argument(
        '--drifter_positions', type=str,
        help="Dictionary of drifter ids and longitude/latitude coordinates. "\
             "e.g. '{\"1\": [lon1, lat1], \"2\": [lon2, lat2]}'")
    arg_parser.add_argument(
        '--duration', type=int,
        help='Duration of drift simulation in hours.')
    arg_parser.add_argument(
        '--windage', type=float,
        help='Wind coefficient added to ocean models.')
    arg_parser.add_argument(
        '--current_uncertainty', type=float,
        help='Root-mean-square value for current uncertainty in m/s which is used'\
        ' to add a random walk to particle motion. Relates to horizontal diffusivity,'\
        ' K as K = 0.5*current_uncertainty**2*mldp_dt.')
    arg_parser.add_argument(
        '--mldp_dt', type=float,
        help='Integration time step in seconds.')
    arg_parser.add_argument(
        '--namelist', type=str,
        help='Name of the MLDP namelist file to be written.')

    config = arg_parser.parse(args)
    import json
    import dateutil
    meteo_file_list=config.meteo_file_list.split()
    drifter_positions = json.loads(config.drifter_positions)
    start_date = dateutil.parser.parse(config.start_date, ignoretz=True)
    duration = datetime.timedelta(hours=config.duration)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    make_mldp_namelist(meteo_file_list,
                       config.data_dir,
                       config.ocean_model_name,
                       config.output_dir,
                       start_date,
                       drifter_positions,
                       duration,
                       config.windage,
                       config.current_uncertainty,
                       config.mldp_dt,
                       config.namelist)

    utils.shutdown_logging()
    
if __name__ == '__main__':
    main()

