"""
Assemble Drift Predictions
==========================
:Author: Clyde Clements, Jennifer Holden
:Created: 2018-03-14
:Modified: 2019-01-22

This module gathers the results from a drift-predict experiment. For each
drifter, it takes the observed trajectory and combines it with the associated
modelled trajectory into a single file (a drifter-type Class 4 file).
"""

import datetime
import glob
import os
from os.path import join as joinpath
import sys
import yaml
import dateutil.parser
import numpy as np
import xarray as xr
from . import ioutils
from . import utils
from . import keep_in_domain

logger = utils.logger


def get_drifter_track(dataset, start_date, end_date):
    return dataset.sel(time=slice(start_date, end_date))


def finalize_modelled_track(
    obs_track,
    mod_drifter,
    start_date,
    end_date,
    run_name,
    drifter_id,
    experiment_metadata,
    output_dir,
    global_domain=False,
    boundary=None,
):

    """Finalize the modelled tracks.

    Parameters
    ----------
    obs_track : xr.Dataset
        observed drifter track
    mod_drifter : xr.Dataset
        modelled drifter track
    start_date : datetime
        run start date
    end_date : datetime
        run end date
    run_name : str
        run name
    drifter_id : str
        unique drifter id
    experiment_metadata : dictionary
        dictionary with metadata for the experiment
    output_dir : str
        Directory in which to create output files.
    global_domain : boolean
        A value of True means that a global ocean model domain is being used
    boundary : matplotlib.path.Path
        path created either from the edges of an ocean mesh file or from
        an initial bounding box.

    Notes
    -----
    Writes the observed and modelled tracks to a netcdf file.

    """
    mod_start_lon = mod_drifter.lon.values[0]
    mod_start_lat = mod_drifter.lat.values[0]

    mod_track = utils.interpolate_track(
        obs_track, get_drifter_track(mod_drifter, start_date, end_date))

    mod_track['trajectory_id'] = mod_drifter.trajectory_id

    obs_track['lon'] = utils.wrap_to_180(obs_track.lon)
    mod_track['lon'] = utils.wrap_to_180(mod_track.lon)

    # Trim the tracks if the modelled drifter leaves the domain. Otherwise
    # this function returns the original lats and lons
    trimmed_track_lons, trimmed_track_lats = \
        keep_in_domain.get_trimmed_tracks(
            mod_track, run_name, global_domain=global_domain, boundary=boundary
        )

    # replace the lat and lon values in the ds with the trimmed values
    mod_track.lon.values = trimmed_track_lons
    mod_track.lat.values = trimmed_track_lats

    # common operations
    ds = xr.Dataset(
        coords={'time': obs_track.time},
        data_vars={'obs_lat': ('time', obs_track.lat.data),
                   'obs_lon': ('time', obs_track.lon.data),
                   'mod_lat': ('time', mod_track.lat.data),
                   'mod_lon': ('time', mod_track.lon.data)})

    ds.obs_lat.attrs['long_name'] = 'Latitude of observed trajectory'
    ds.obs_lat.attrs['units'] = 'degrees_north'
    ds.obs_lat.attrs['_FillValue'] = ds.obs_lat.dtype.type(np.nan)
    ds.obs_lon.attrs['long_name'] = 'Longitude of observed trajectory'
    ds.obs_lon.attrs['units'] = 'degrees_east'
    ds.obs_lon.attrs['_FillValue'] = ds.obs_lon.dtype.type(np.nan)
    ds.mod_lat.attrs['long_name'] = 'Latitude of modelled trajectory'
    ds.mod_lat.attrs['units'] = 'degrees_north'
    ds.mod_lat.attrs['_FillValue'] = ds.mod_lat.dtype.type(np.nan)
    ds.mod_lon.attrs['long_name'] = 'Longitude of modelled trajectory'
    ds.mod_lon.attrs['units'] = 'degrees_east'
    ds.mod_lon.attrs['_FillValue'] = ds.mod_lon.dtype.type(np.nan)

    for v in obs_track.data_vars:
        if v in ('lat', 'lon'):
            continue
        ds['obs_' + v] = obs_track[v]

    for a in obs_track.attrs:
        ds.attrs['obs_' + a] = obs_track.attrs[a]

    for a in mod_track.attrs:
        ds.attrs['mod_' + a] = mod_track.attrs[a]

    ds.attrs['mod_run_name'] = run_name
    ds.attrs['drift_model'] = experiment_metadata['drift_model']
    ds.attrs['ocean_model'] = experiment_metadata['ocean_model']
    ds.attrs['atmos_model'] = experiment_metadata['atmos_model']
    ds.attrs['alpha_wind'] = experiment_metadata['alpha_wind']
    ds.attrs['mod_start_date'] = start_date.strftime('%Y-%m-%d %H:%M:%S')
    ds.attrs['mod_start_lat'] = mod_start_lat
    ds.attrs['mod_start_lon'] = mod_start_lon
    ds.attrs['dwt_output_type'] = 'drift_eval'

    # Write out the file
    output_file = '{}-{}.nc'.format(run_name, drifter_id)
    output_file = joinpath(output_dir, output_file)
    logger.info('Creating file %s...', os.path.basename(output_file))
    ds.to_netcdf(output_file)


def assemble_drift_predictions(
    *,
    experiment_dir,
    ocean_data_file=None,
    lon_var=None,
    lat_var=None,
    bbox=None
):
    """Assemble drift predictions.

    Parameters
    ----------
    experiment_dir : str
        Path to directory containing drift experiment runs.
    ocean_data_file : str
        Filename of the ocean file to use for domain indices.
    output_dir : str
        Directory in which to create output files.
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    bbox : str
        Bounding bbox domain in which particles should be considered valid
        e.g. "min_lon min_lat max_lon max_lat"
    """

    output_dir = joinpath(experiment_dir, 'output')

    logger.info('Assembling drift predictions...')
    experiment_metadata_filename = joinpath(experiment_dir, 'runs.yaml')
    with open(experiment_metadata_filename, 'r') as f:
        experiment_metadata = yaml.load(f, Loader=yaml.FullLoader)

    # Set up flags to use when deciding whether to check if the domain
    # is global and whether to create a new domain boundary path
    global_domain = False
    boundary = None
    do_domain_check = True

    # If no ocean_data_file or bbox is present, no track
    # trimming will occur so no checks need to be made:
    if not ocean_data_file and not bbox:
        do_domain_check = False

    # For each modelled track
    for run_name in experiment_metadata['runs']:

        run_metadata = experiment_metadata['runs'][run_name]
        start_date = dateutil.parser.parse(run_metadata['drift_start_date'])

        # Old versions of the metadata contained the number of drift days
        # instead of the drift end date.
        if 'num_drift_days' in run_metadata:
            drift_duration = datetime.timedelta(
                days=run_metadata['num_drift_days'])
            end_date = start_date + drift_duration
        else:
            end_date = dateutil.parser.parse(run_metadata['drift_end_date'])

        run_dir = run_metadata['run_dir']

        if run_metadata['drift_calculation_status'] == 'finished':

            ###################################################################
            # read the files
            ##################################################################
            drifter_ids = sorted(run_metadata['drifter_data_files'].keys())

            drift_calc_method = run_metadata['drift_calculation_method'].lower()

            if drift_calc_method == 'opendrift':
                filestr = '*.nc'

            elif drift_calc_method == 'ariane':
                filestr = 'ariane_trajectories_qualitative.nc'

            elif drift_calc_method == 'mldp':
                filestr = 'mldp_trajectories.nc'

            else:
                sys.exit("the given drift calculation method is not one"
                         + " of OpenDrift, Ariane or mldp. Exiting. ")

            if ocean_data_file:
                ocean_data_file = joinpath(experiment_dir,
                                           run_dir,
                                           ocean_data_file)

            # Assuming either an ocean_data_file or a bbox exists, check
            # whether the domain is global. If it is not global, create 
            # a boundary.
            if do_domain_check:

                # check if a global domain
                global_domain = keep_in_domain.check_if_global(
                    ocean_data_file=ocean_data_file,
                    lon_var=lon_var,
                    lat_var=lat_var,
                    bbox=bbox
                )

                # if not a global domain, attempt to define a boundary.
                # if the ocean_data_file exists, use that. Otherwise,
                # use the bbox if it exists.
                if not global_domain:
                    boundary = keep_in_domain.find_boundary(
                        ocean_data_file=ocean_data_file,
                        bbox=bbox,
                        lon_var=lon_var,
                        lat_var=lat_var
                    )
                do_domain_check = False

            files = glob.glob(os.path.join(experiment_dir, run_dir, filestr))

            if len(files) == 0:
                logger.warn("Drift simulation not successful")
                continue

            try:
                traj_filename = files[0]
                ds = ioutils.load_trajectories(
                            traj_filename,
                            run_metadata['drift_calculation_method']
                            )

            except ValueError:
                warnstr = ("Drifter file {} not accessible. It is likley "
                           + "that Open Drift computation did not "
                           + "complete successfully")
                logger.warn(warnstr.format(traj_filename))
                continue

            trajectory_ids = ds['trajectory_id'].values

            os.makedirs(output_dir, exist_ok=True)

            for drifter_id in drifter_ids:

                if drifter_id not in trajectory_ids:
                    logger.warn('Drifter {} not computed'.format(drifter_id))
                    continue

                drifter_filename = joinpath(
                                experiment_dir,
                                run_dir,
                                run_metadata['drifter_data_files'][drifter_id])
                obs_drifter = ioutils.load_drifter_dataset(drifter_filename)
                obs_track = get_drifter_track(obs_drifter,
                                              start_date,
                                              end_date)
                if obs_track['time'].size <= 0:
                    st = ('Drifter {} has no data in date range {} to {}')
                    logger.warn((st.format(drifter_id, start_date, end_date)))
                    continue

                mod_drifter = ds.sel(trajectory_id=drifter_id)

                if 'x_sea_water_velocity' in ds:
                    md_x = mod_drifter.x_sea_water_velocity.values
                    md_y = mod_drifter.y_sea_water_velocity.values
                    if (((md_x == 0).all() and (md_y == 0).all())):
                        logstr = ('OpenDrift computation results in 0 '
                                  + 'velocities for drifter {}. Excluding '
                                  + 'from analysis')
                        logger.warn((logstr.format(drifter_id)))
                        continue

                # Trim the tracks if necessary and write the resulting
                # track to an output file
                finalize_modelled_track(
                    obs_track,
                    mod_drifter,
                    start_date,
                    end_date,
                    run_name,
                    drifter_id,
                    experiment_metadata,
                    output_dir,
                    global_domain=global_domain,
                    boundary=boundary
                )


def main():
    from . import cli
    cli.run(assemble_drift_predictions)


if __name__ == '__main__':
    main()
