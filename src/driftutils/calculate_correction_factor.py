"""
Calculate Drift Correction Factor
=================================
:Author: Nancy Soontiens
:Created: 2020-11-13

Two correction factors are calculated:

1. ocean correction factor - solves for \gamma in
.. math::
   :nowrap:

   \begin{equation*}
     u_{drifter} = \gamma u_{ocean}
   \end{equation*}

2. wind correction factor - solves for :math:`\alpha` in 
.. math::
   :nowrap:

   \begin{equation*}
     u_{drifter} = u_{ocean} + \alpha u_{atmos}
   \end{equation*}

"""

import os

import dateutil.parser
from datetime import datetime
import numpy as np
import pickle
import pyproj
import scipy.interpolate as interp
import xarray as xr

from .find_nearest_grid_point import find_nearest_grid_point
from .ioutils import (date_filter,
                                load_drifter_dataset,
                                ocean_model_rename_transform)
from .rotate_fields import (opa_angle_2016_p, rot_rep_2017_p)
from .utils import (defaults, load_yaml_config, logger)


class DrifterNotInDomainError(Exception):
    def __init__(self, message):
        self.message = message

def calculate_correction(*,
    drifter_details_file,
    drifter_subdir,
    drifter_depth='L1',
    xwatervel=defaults['xwatervel']['nc_name'],
    ywatervel=defaults['ywatervel']['nc_name'],
    lon_var_ocean='nav_lon',
    lat_var_ocean='nav_lat',
    ulon_var_ocean='glamu',
    ulat_var_ocean='gphiu',
    vlon_var_ocean='glamv',
    vlat_var_ocean='gphiv',
    model_time_ocean='time_counter',
    grid_type='cgrid',
    rotation_data_file='None',
    orca_grid=False,
    xwindvel=defaults['xwindvel']['nc_name'],
    ywindvel=defaults['ywindvel']['nc_name'],
    lon_var_atmos='nav_lon',
    lat_var_atmos='nav_lat',
    model_time_atmos='time_counter'
):
    """Calculate drift correction factors.
    
    Parameters
    ----------
    drifter_details_file : str
        Name of file containing drifter information, including list of 
        ocean and atmos files needed for interpolation over a time range.
    drifter_subdir : str
        Name of subdirectory to save calculations.
    drifter_depth : str
        Specifies the depth for drift correction calculations. If the string
        begins with 'L', it indicates the corresponding depth layer for the
        vertical gridpoints; otherwise, it indicates the depth in meters. 
    xwatervel : str
        Name of variable containing X water velocity.
    ywatervel : str
        Name of variable containing Y water velocity.
    lon_var_ocean : str
        Name of longitude variable in mesh file.
    lat_var_ocean : str
        Name of latitude variable in mesh file.
    ulon_var_ocean : str
        Name of U longitude variable in mesh file.
    ulat_var_ocean : str
        Name of U latitude variable in mesh file.
    vlon_var_ocean : str
        Name of V longitude variable in mesh file.
    vlat_var_ocean : str
        Name of V latitude variable in mesh file.
    model_time_ocean : str
        Name of variable containing timestamp of ocean model output.
    grid_type : str
        Indicates the type of ocean model grid. Options are 'cgrid', 'agrid',
        or 'user_grid'.
    rotation_data_file : str
        Path to data file containing coefficients for rotating velocity
        components on desired grid to true east/north. This file will be 
        created if it does not exists, i.e. if a grid is being run for the
        first time. If set to  'None' no rotation is performed.
    orca_grid : boolean
        Indicates an orca grid which requires specials treatment for
        rotation of velocity fields for OpenDrift. True means the grid is an
        orca grid.
    xwindvel : str
        Name of variable containing eastward wind.
    ywindvel : str
        Name of variable containing northward wind.
    lon_var_atmos : str
        Name of longitude variable in atmos model.
    lat_var_atmos : str
        Name of latitude variable in atmos model.
    model_time_atmos: str
        Name of variable containing timestamp of atmos model output.
    """
    cwd = os.getcwd()
    os.chdir(drifter_subdir)

    # Load  details file
    drifter_details = load_yaml_config(drifter_details_file)
    drifter_file = drifter_details['drifter_file']
    start_calc_date = dateutil.parser.parse(drifter_details['start_calc_date'])
    end_calc_date = dateutil.parser.parse(drifter_details['end_calc_date'])
    atmos_model_name = drifter_details['atmos_model_name']
    ocean_model_name = drifter_details['ocean_model_name']

    # Gather ocean data
    logger.info('Gathering ocean data')
    ocean_mesh_file = drifter_details['ocean_mesh_file']
    has_mesh = True
    if ocean_mesh_file == 'None':
        has_mesh = False

    # If agrid or user grid, ensure that latitude/longitude coordinates are
    # the same for u and v
    if (grid_type == 'agrid') or (grid_type == 'user_grid'):
        ulon_var_ocean = lon_var_ocean
        ulat_var_ocean = lat_var_ocean
        vlon_var_ocean = lon_var_ocean
        vlat_var_ocean = lat_var_ocean
    fous = drifter_details['ocean_data_files']['xwatervel']
    fous.sort()
    fovs = drifter_details['ocean_data_files']['ywatervel']
    fovs.sort()
    dou = xr.open_mfdataset(fous, combine='by_coords')
    uocean = dou[xwatervel]
    if fous == fovs:
        dov = dou
        vocean = dou[ywatervel]
    else:
        dov = xr.open_mfdataset(fovs, combine='by_coords')
        vocean = dov[ywatervel]

    # Setting up grid file to handle cases where no rotation is needed and
    # no mesh (e.g. GIOPS latlon user grid)
    if has_mesh:
        mesh = xr.open_dataset(ocean_mesh_file)
        ugrid = mesh
        vgrid = mesh
    else:
        grid_dict=dict()
        grid_dict[model_time_ocean] = 0
        ugrid = dou.isel(grid_dict)
        vgrid = dov.isel(grid_dict)

    # Rename depth dimension of u/v if needed
    uocean = ocean_model_rename_transform(uocean)
    vocean = ocean_model_rename_transform(vocean)

    # Rotation of ocean currents if needed
    if rotation_data_file == 'None':
        rotation_data_file = None
    if rotation_data_file is not None:
        logger.info("Rotating ocean data to east/north currents")
        if os.path.exists(rotation_data_file):
            with open(rotation_data_file,'rb') as f:
                coeffs = pickle.load(f)
        else:
            coeffs = opa_angle_2016_p(ocean_mesh_file, orca_grid=orca_grid)
            with open(rotation_data_file,'wb',-1) as f:
                pickle.dump(coeffs,f)
        if grid_type == 'cgrid':
            uocean_east = rot_rep_2017_p(uocean, vocean,'U','ij->e', coeffs)
            vocean_north = rot_rep_2017_p(uocean, vocean, 'V','ij->n', coeffs)
        elif grid_type == 'agrid':
            uocean_east = rot_rep_2017_p(uocean, vocean,'T','ij->e', coeffs)
            vocean_north = rot_rep_2017_p(uocean, vocean, 'T','ij->n', coeffs)
        else:
            raise ValueError(
                ("Velocity rotation is not supported for grid_type={}. "
                 "Review choices for grid_type and "
                 "rotation_data_file.".format(grid_type))
            )
    else:
        uocean_east = uocean
        vocean_north = vocean

    # Parse drifter_depth and interpolate vertically if needed.
    if drifter_depth.startswith('L'):
        depth_option = drifter_depth[1:]
        depth_in_layer = True
    else:
        depth_option = drifter_depth
        depth_in_layer = False
    if depth_in_layer:
        if uocean_east.ndim == 4: # 3D arrays so index depth accordingly
            depth_option = int(depth_option) -1 #python indexing
            uocean_east = uocean_east.isel(z=depth_option)
            vocean_north = vocean_north.isel(z=depth_option)
        # No depth indexing needed for 2D arrays
    else:
        # Interpolate vertically
        depth_option = float(depth_option)
        uocean_east = uocean_east.interp(z=depth_option,
                                         fill_value='extrapolate',
                                         assume_sorted=True)
        vocean_north = vocean_north.interp(z=depth_option,
                                           fill_value='extrapolate',
                                           assume_sorted=True)

    # Gather atmos data if needed
    calculate_alpha=False
    if 'atmos_data_files' in drifter_details:
        logger.info('Gathering atmos data')
        calculate_alpha=True
        faus = drifter_details['atmos_data_files']['xwindvel']
        faus.sort()
        favs = drifter_details['atmos_data_files']['ywindvel']
        favs.sort()
        dau = xr.open_mfdataset(faus, combine='by_coords')
        u_wind = dau[xwindvel]
        if faus == favs:
            dav = dau
            v_wind = dau[ywindvel]
        else:
            dav = xr.open_mfdataset(favs, combine='by_coords')
            v_wind = dav[ywindvel]

    # Gather drifter data
    drifter = load_drifter_dataset(drifter_file)

    setname = (str(ocean_model_name) 
               + '_' + str(atmos_model_name)) 

    drifter = date_filter(drifter, date_coord='time',
                          start_date=start_calc_date, end_date=end_calc_date)

    obs_FilteredData_StartDate = drifter.time.values[0]
    obs_FilteredData_EndDate = drifter.time.values[-1]

    basename = os.path.basename(drifter_file)
    sdate_timeobj = datetime.strptime(
        str(obs_FilteredData_StartDate).split('.')[0], 
        '%Y-%m-%dT%H:%M:%S'
    )
    dlab_format_str = '{sdto.year}{sdto.month:02}{sdto.day:02}{sdto.hour:02}'
    datelab = dlab_format_str.format(sdto=sdate_timeobj)
    savestr = 'correction-factor_{}_{}'.format(datelab, basename)
    outfile = os.path.join(drifter_subdir, savestr)

    # Calculate drifter velocity and resample to hourly
    dvel = drifter_velocity(drifter)
    dvel = dvel.resample({'time': '1H'}).interpolate('linear').dropna('time')

    # Interpolate ocean data
    logger.info('Interpolating ocean data to drifter positions')
    dou_interp = interpolate_variable_to_obs_track(
        dvel,
        uocean_east,
        model_time_ocean,
        ulon_var_ocean,
        ulat_var_ocean,
        ugrid)
    dou_interp = dou_interp.rename({'vari_ts': 'ueast_ocean'})
    dou_interp['ueast_ocean'].attrs =\
        {'standard_name': 'eastward_sea_water_velocity',
         'long_name': 'Eastward ocean velocity',
         'units': 'm/s'}
    dov_interp = interpolate_variable_to_obs_track(
        dvel,
        vocean_north,
        model_time_ocean,
        vlon_var_ocean,
        vlat_var_ocean,
        vgrid)
    dov_interp = dov_interp.rename({'vari_ts': 'vnorth_ocean'})
    dov_interp['vnorth_ocean'].attrs =\
        {'standard_name': 'northward_sea_water_velocity',
         'long_name': 'Northward ocean velocity',
         'units': 'm/s'}
    if (np.isnan(dou_interp.ueast_ocean.values).all()) or \
       (np.isnan(dov_interp.vnorth_ocean.values).all()):
        os.chdir(cwd)
        msg = ('Drifter not in ocean domain: {}'.format(drifter_file))
        raise DrifterNotInDomainError(msg)

    if calculate_alpha:
        logger.info('Interpolating atmos data to drifter positions')
        atmos_dict = dict()
        atmos_dict[model_time_atmos] = 0
        dau_interp = interpolate_variable_to_obs_track(
            dvel,
            u_wind,
            model_time_atmos,
            lon_var_atmos,
            lat_var_atmos,
            dau.isel(atmos_dict))
        dau_interp = dau_interp.rename({'vari_ts': 'ueast_atmos'})
        dau_interp['ueast_atmos'].attrs =\
            {'standard_name': 'eastward_wind',
             'long_name': 'Eastward wind velocity',
             'units': 'm/s'}
        dav_interp = interpolate_variable_to_obs_track(
            dvel,
            v_wind,
            model_time_atmos,
            lon_var_atmos,
            lat_var_atmos,
            dav.isel(atmos_dict))
        dav_interp = dav_interp.rename({'vari_ts': 'vnorth_atmos'})
        dav_interp['vnorth_atmos'].attrs =\
            {'standard_name': 'northward_wind',
             'long_name': 'Northward wind velocity',
             'units': 'm/s'}

        # Check for all nan's - drifter not in domain
        if (np.isnan(dau_interp.ueast_atmos.values).all()) or \
           (np.isnan(dav_interp.vnorth_atmos.values).all()):
            os.chdir(cwd)
            msg = ('Drifter not in atmos domain: {}'.format(drifter_file))
            raise DrifterNotInDomainError(msg)

    # Calculate correction coefficients
    n = len(dov_interp.vnorth_ocean.values)
    ocean = np.array([
        complex(dou_interp.ueast_ocean.values[i],
                dov_interp.vnorth_ocean.values[i]) for i in range(n)])
    drift = np.array([
        complex(dvel.u.values[i],
                dvel.v.values[i]) for i in range(n)])
    gamma = drift/ocean
    if calculate_alpha:
        atmos = np.array(
            [complex(dau_interp.ueast_atmos.values[i],
                     dav_interp.vnorth_atmos.values[i]) for i in range(n)])
        alpha = (drift - ocean)/atmos

    # Save the output
    dnew = xr.merge([dou_interp, dov_interp, dvel])
    gamma_real = xr.DataArray(
        gamma.real, dims=['time',],
        coords={'time': dvel.time.values},
        attrs={'long_name': 'Real part of ocean correction factor',
               'units': '1'}
    )
    gamma_imag = xr.DataArray(
        gamma.imag, dims=['time',],
        coords={'time': dvel.time.values},
        attrs={'long_name': 'Imaginary part of ocean correction factor',
               'units': '1'}
    )
    dnew['gamma_real'] = gamma_real
    dnew['gamma_imag'] = gamma_imag

    if calculate_alpha:
        dnew = xr.merge([dnew, dau_interp, dav_interp])
        alpha_real = xr.DataArray(
            alpha.real, dims=['time',],
            coords={'time': dvel.time.values},
            attrs={'long_name': 'Real part of wind correction factor',
                   'units': '1'}
        )
        alpha_imag = xr.DataArray(
            alpha.imag, dims=['time',],
            coords={'time': dvel.time.values},
            attrs={'long_name': 'Imaginary part of wind correction factor',
                   'units': '1'}
        )
        dnew['alpha_real'] = alpha_real
        dnew['alpha_imag'] = alpha_imag

    # Adding attributes and renaming some variables
    dnew = dnew.rename({'u': 'ueast_drifter', 'v': 'vnorth_drifter'})

    dnew['ueast_drifter'].attrs = {
        'long_name': 'Easterward drifter velocity',
        'units': 'm/s'}
    dnew['vnorth_drifter'].attrs = {
        'long_name': 'Northward drifter velocity',
        'units': 'm/s'}
    dnew['lon'].attrs = {
        'standard_name': 'longitude',
        'long_name': 'Drifter longitude',
        'units': 'degree_east'}
    dnew['lat'].attrs = {
        'standard_name': 'latitude',
        'long_name': 'Drifter latitude',
        'units': 'degree_north'}

    for a in drifter.attrs:
        dnew.attrs['obs_' + a] = drifter.attrs[a]

    if 'comment' in list(dnew.attrs):
        dnew.attrs['drifter_comment'] = dnew.attrs['comment']

    source_str = ('File created using the DriftCorrectionFactor function of '
        + 'https://gitlab.com/dfo-drift-projection/drift-workflow-tool/. '
        + 'The calculations used to generate the correction factors are '
        + 'explained in detail in: Sutherland, G., et al. (2020) '
        + '\"Evaluating the Leeway Coefficient of Ocean Drifters Using '
        + 'Operational Marine Environmental Prediction Systems\" Journal of '
        + 'Atmospheric and Oceanic Technology, 37(11), 1-36. '
        + 'https://doi.org/10.1175/JTECH-D-20-0013.1')

    comment_str = ('This file provides a drifter model independent comparison '
        + 'between ocean model velocities and observed drifter velocities '
        + 'along the observed drifter track. Modelled currents and winds '
        + 'along observed trajectories are determined and the ocean '
        + 'correction factor needed to produce a perfect drift prediction '
        + 'is generated (gamma). In addition, if wind forcing is included, '
        + 'a wind correction factor (alpha) is also provided.')

    calc_comment_str = 'Drifter positions resampled to an hourly frequency'

    dnew.attrs['drifter_filename'] = str(basename)
    dnew.attrs['ocean_model_name'] = drifter_details['ocean_model_name']
    dnew.attrs['atmos_model_name'] = drifter_details['atmos_model_name']
    dnew.attrs['drifter_depth'] = drifter_depth
    dnew.attrs['calc_comment'] = calc_comment_str
    dnew.attrs['source'] = source_str
    dnew.attrs['comment'] = comment_str
    dnew.attrs['data_lat_min'] = str(np.nanmin(drifter.lat.values))
    dnew.attrs['data_lat_max'] = str(np.nanmax(drifter.lat.values))
    dnew.attrs['data_lon_min'] = str(np.nanmin(drifter.lon.values))
    dnew.attrs['data_lon_max'] = str(np.nanmax(drifter.lon.values))
    dnew.attrs['data_StartDate'] = str(obs_FilteredData_StartDate)
    dnew.attrs['data_EndDate'] = str(obs_FilteredData_EndDate)
    dnew.attrs['setname'] = setname
    dnew.attrs['dwt_output_type'] = 'drift_correction_factor'

    logger.info("\nSaving calculations in {}".format(outfile))
    dnew.to_netcdf(outfile)
    os.chdir(cwd)
    
                                      
def drifter_velocity(d):
    """Calculate the time series of drifter velocity given a drifter dataset.
    Velocity is calculated using a first-order, forward difference 
    discretization:
    U_i = (x_{i+1} - x_i)/(t_{i+1} - t_i)

    Parameters
    ----------
    d : xarray.Dataset
        Drfiter dataset with lon, lat and time variables

    Returns:
    ds : xarray.Dataset
        The drifter dataset with added variables u (eastward velocity) and 
        v (northward velocity).  
    """
    lon1 = d.lon.values[0:-1]
    lon2 = d.lon.values[1:]
    lat1 = d.lat.values[0:-1]
    lat2 = d.lat.values[1:]
    times = d.time.values[:]
    
    # Define a projection
    g = pyproj.Geod(ellps='WGS84')
    
    azimuth, backazimuth, dist = g.inv(lon1, lat1, lon2, lat2, radians=False)
    
    speed = []
    for i, d in enumerate(dist):
        speed.append(dist[i] /((times[i+1] - times[i])/np.timedelta64(1, 's')))
    speed = np.array(speed)
    
    u = speed*np.sin(np.radians(azimuth))
    v = speed*np.cos(np.radians(azimuth)) 
    
    ds = xr.Dataset(coords={'time': times[0:-1]},
                    data_vars={'lon': ('time', lon1),
                               'lat': ('time', lat1),
                               'u': ('time', u),
                               'v': ('time', v)})
    return ds


def interpolate_time(var, t, ti):
    """Interpolate var(t) to time ti. Linear interpolation in time. 
    var is an array of data with first index time (eg. [time, y, x])
    ti is a np.datetime64 object
    t is an array of np.datetime64 objects
    """
    
    # Define a reference time
    tr = t[0] 

    # t and ti with repsect to epoc in seconds
    ti_tr = (ti-tr)/np.timedelta64(1, 's')
    t_tr = (t-tr)/np.timedelta64(1, 's')
    
    # Identify neighbouring time indices
    diff = ti_tr - t_tr
    i1 = np.argmin(np.abs(diff))
    sign = np.sign(diff[i1])
    i2 = int(i1 + sign*1)
    
    # Ensure i2 is in a valid range.
    if i1 == 0:
        if i2 < 0:
            logger.warn(
                "Interpolation time {} is not in model time range {} to {}. "\
                "Extrapolation will occur.".format(ti.isoformat(),
                                                   t[0].isoformat(),
                                                   t[-1].isoformat())
            )
        i2 = i1 + 1
    elif i1 == len(t_tr) -1:
        if i2 > len(t_tr) -1:
            logger.warn(
                "Interpolation time {} is not in model time range {} to {}. "\
                "Extrapolation will occur.".format(ti.isoformat(),
                                                   t[0].isoformat(),
                                                   t[-1].isoformat())
            )
        i2 = i1 - 1
        
    # Edge case when i1 = i2 (sign is 0)
    if i1 == i2:
        vari = var[i1]
    else:
        # Interpolate
        vari = var[i1] +\
            (var[i2] - var[i1])/(t_tr[i2] - t_tr[i1])*(ti_tr-t_tr[i1])
    return vari


def interpolate_space(var, lon, lat, loni, lati):
    """ Interpolate field var(lon, lat) to (loni, lati)

    Parameters
    ----------
    var : array-like
       The data to be interpolated
    lon : array-like
       The longitudes of the data
    lat : array-like
        The latitudes of the data
    loni : float
        The longitude to interpolate to
    lati : float
        The latitude to interpolate to

    Returns
    -------
    vari : float
       The interpolated value of the data
    """
    points = np.column_stack([lon.ravel(),lat.ravel()])
    f = interp.LinearNDInterpolator(points, var.ravel())
    vari = f(loni, lati)
    return vari


def interpolate_variable_to_obs_track(
        dobs, dmodel, time_var, lon_var, lat_var, grid_dataset):
    """Interpolate a variable in space annd time to an observed drifter
    trajectory

    Paramaters
    ----------
    dobs : xarray.Dataset
        Observed drifter dataset with lon, lat and time variables
    dmodel : xarray.DataArray
        Model data array of variable to be interpolated
    time_var : str
        Name of variable containing timestamp of model output
    lon_var : str
        Name of variable containing longitude of model output
    lat_var : str
        Name of variable containing latitude of model output
    grid_dataset : xarray.Dataset
        Dataset containing model grid information

    Returns
    -------
    ds_vari_ts : xarray.Dataset
       A dataset containing the model variable interpolated to drifter 
       positions and times. The dataset contains the following variables:

       - ``time``: The timestamps of the interpolated data
       - ``lon``: The longitudes of the interpolated data
       - ``lat``: The latitudes of the interpolated data
       - ``vari_ts``: The interpolated variable 
    """

    vari_ts_array = []
    lon_mod = np.squeeze(grid_dataset[lon_var].values)
    lon_mod_adjust = lon_mod.copy()
    lon_mod_adjust[lon_mod > 180] -= 360
    lat_mod = np.squeeze(grid_dataset[lat_var].values)

    if lat_mod.ndim == 1:
        lon_mod_adjust, lat_mod = np.meshgrid(lon_mod_adjust, lat_mod)

    for t in range(len(dobs.time.values)):
        loni = dobs.lon.values[t]
        lati = dobs.lat.values[t]
        ti = dobs.time.values[t]

        # Interpolate in time
        vari_t = interpolate_time(dmodel, dmodel[time_var].values, ti)

        # Identify closest grid cell and interpolate in space
        dist, j, i, lat_near, lon_near =\
            find_nearest_grid_point(lati, loni,
                                    grid_dataset,
                                    lat_var,lon_var,
                                    n=10)
        Ny = dmodel.shape[-2]
        Nx = dmodel.shape[-1]
        if i[0] <=1 or i[0] >= Nx-2 or j[0] <=1 or j[0] >= Ny-2:
            logger.debug(
                "Drifter position ({},{}) outside of " \
                "domain".format(loni, lati))
            vari_ts = np.nan
        else:
            vari_ts = interpolate_space(vari_t.values[j,i],
                                        lon_mod_adjust[j,i],
                                        lat_mod[j,i],
                                        loni,
                                        lati)
        vari_ts_array.append(vari_ts)

    ds_vari_ts = xr.Dataset(
        coords={'time': dobs.time.values},
        data_vars={'lon': ('time', dobs.lon.values),
                   'lat': ('time', dobs.lat.values),
                   'vari_ts': ('time', np.array(vari_ts_array))}
    )

    return ds_vari_ts


def main():
    from . import cli
    cli.run(calculate_correction)


if __name__=='__main__':
    main()
