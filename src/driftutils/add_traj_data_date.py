###############################################################################
# Add trajectory data date
#
# Author: Clyde Clements
# Created: 2017-10-19 16:33:50 -0230
#
# This module adds a coordinate variable to the Ariane netCDF file that
# specifies the dates for the trajectory points. It assumes all trajectories
# start from the same date.
###############################################################################

import collections
import os
import shutil
import sys
import tempfile

import dateutil.parser
import pandas as pd
import xarray as xr

from . import configargparse
from . import utils
from .utils import logger


LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

def add_traj_data_date_to_datafile(traj_file, start_date):
    dataset = xr.open_dataset(traj_file, decode_times=False)
    if dataset['nb_output'].size > 0:
        add_traj_data_date_to_dataset(dataset, start_date)
        # We use mkstemp to create a temporary file. It returns a file descriptor
        # and the name of the file. We are only interested in the name.
        fd, file_name = tempfile.mkstemp(suffix='.nc')
        # But note that mkstemp opens the file. Below when we call the to_netcdf()
        # method, the file will be opened again. Most Unix platforms will permit
        # that sort of behaviour; however, to be on the safe side we close the file
        # here, allowing the to_netcdf() to be the only source opening the file.
        # Build encoding to avoid missing_value and _FillValue conlficts
        encoding = {varname: {'_FillValue': None} for varname in \
                    dataset.variables}
        dataset.to_netcdf(file_name, encoding=encoding)
        dataset.close()
        # Overwrite the current file.
        shutil.copyfile(file_name, traj_file)
        # Remove the temp file so that /tmp. doesn't fill up
        os.remove(file_name)

def add_attributes_to_dataset(traj_file,run_name,drift_duration, time_taken):
    dataset = xr.open_dataset(traj_file, decode_times=False)
    ocean_run_name = '{}'.format(run_name)
    duration = '{}'.format(int(drift_duration.total_seconds()//3600))
    time_taken = '{}'.format(time_taken)
    dataset.attrs['run_name'] = ocean_run_name
    dataset.attrs['drift_duration'] = duration
    dataset.attrs['total_time_taken'] = time_taken
    fd, file_name = tempfile.mkstemp(suffix='.nc')
    # Build encoding to avoid missing_value and _FillValue conlficts
    encoding = {varname: {'_FillValue': None} for varname in \
                dataset.variables}
    dataset.to_netcdf(file_name, encoding=encoding)
    dataset.close()
    # Overwrite the current file.
    shutil.copyfile(file_name, traj_file)


def add_traj_data_date_to_dataset(dataset, start_date):
    """Add data date to trajectory data set.

    This function adds a coordinate variable 'nb_output' to the dataset, which
    is set to the dates corresponding to the trajectory points. Note the
    dataset is updated in-place.

    Arguments
        dataset: xarray.Dataset
        start_date: datetime.datetime
    """
    delta_t = dataset.attrs['delta_t']
    output_frequency = dataset.attrs['frequency']
    output_sampling_time = delta_t * output_frequency
    date_delta = pd.tseries.offsets.DateOffset(seconds=output_sampling_time)
    nperiods = dataset.dims['nb_output']
    data_date = pd.date_range(start=start_date, periods=nperiods,
                              freq=date_delta)
    logger.info(('Adding data date: start date = %s, end date = %s, '
                 'frequency = %s seconds'), data_date[0].isoformat(),
                data_date[-1].isoformat(), output_sampling_time)
    dataset.coords['nb_output'] = data_date


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--traj_file', type=str, required=True,
                   help=('Name of NetCDF file containing Ariane-computed '
                         'trajectories'))
    arg_parser.add('--date', type=str, required=True,
                   help='Date and time of start of trajectories')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    start_date = dateutil.parser.parse(config.date, ignoretz=True)
    add_traj_data_date_to_datafile(config.traj_file, start_date)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
