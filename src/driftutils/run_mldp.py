"""
Create MLDP namelist
=============================
:Author: Samuel T. Babalola
:Created: 2018-09-19

This module write MLDP namelist into an input file.
"""

import os
import subprocess
import sys

from driftutils import configargparse
from driftutils import utils
from driftutils.utils import logger


def run_mldp(
        mldp_exec, 
        namelist=None, 
        run_dir=None,
        overwrite_run_dir=False):
    
    run_dir = os.path.abspath(run_dir)
    logger.info('\nPreparing to run MLDP in directory %s...', run_dir)

    if not os.path.exists(run_dir):
        logger.info('Creating run directory...')
        os.makedirs(run_dir)

    logger.info('Running MLDP...')
    mldp_out = os.path.join(run_dir, 'MLDPn.out')
    with open(mldp_out, 'w') as out:
        process = subprocess.run([mldp_exec, '-i', namelist, '-o', run_dir, '-t', '1'],
                                 stdout=out,
                                 stderr=subprocess.STDOUT)
    return_status = process.returncode
    if return_status != 0:
        logger.warn('MLDP finished with an error status of %s',
                    return_status)
    logger.info('MLDP run information written to {}'.format(mldp_out))
    logger.info('Finished running MLDP.')
    return run_dir


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')
    arg_parser.add('--mldp_exec', type=str,
                   default=None,
                   help='Path to  MLDP executable')
    arg_parser.add('-i','--namelist', type=str, default=None,
                   help='Name of namelist configuration file')
    arg_parser.add('-o', '--run_dir', type=str, default=None,
                   help=('Name of directory in which to run MLDP. This is '
                         'where its output files will be generated.'))
    arg_parser.add('--overwrite_run_dir', type=bool, default=True,
                   action='store',
                   help='Overwrite contents of run directory if it exists')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    run_mldp(config.mldp_exec, config.namelist, 
             config.run_dir, overwrite_run_dir=config.overwrite_run_dir)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
