"""
Assemble Drifter Metadata
=========================
:Author: Clyde Clements
:Created: 2017-08-16

This module assembles metadata about drifter data files. It recursively walks
through a given directory and examines all NetCDF files with the assumption
that they all represent drifter data. Information gathered includes the start
and end data dates for each drifter. The metadata is then written to
a specified file.
"""

import datetime
import os
import os.path
import sys
import yaml

from . import configargparse
from . import ioutils
from . import utils


logger = utils.logger


def assemble_drifter_metadata(
        data_dir, output_file, buoy_id_attr_name='buoyid',
        meta_variables=None):
    """Assemble drifter metadata.

    Parameters
    ----------
    data_dir : str
        Name of directory containing drifter data files.
    output_file : str
        Name of output file to create containing the metadata.

    Other Parameters
    ----------------
    buoy_id_attr_name : str, optional
        Name of attribute containing buoy id.
    meta_variables : None or iterable, optional
        Names of variables that contain metadata. Such variables, if they
        exist, must have a single dimension and only the first value is
        recorded in the metadata output file.
    """
    logger.info('\n' + 'Assembling drifter metadata...')
    drifters = []
    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            logger.debug('Examining file %s...', filename)
            drifter_filename = os.path.join(dirpath, filename)
            ds = ioutils.load_drifter_dataset(drifter_filename)
            if not os.path.isabs(drifter_filename):
                # Determine path of drifter file relative to directory of
                # output file.
                odir = os.path.dirname(os.path.abspath(output_file))
                drifter_filename = os.path.relpath(drifter_filename, odir)

            if buoy_id_attr_name not in ds.attrs:
                raise ValueError(
                    ('Drifter data file "{filename}" does not contain a buoy '
                     'id attribute named "{attrname}"').format(
                         filename=drifter_filename,
                         attrname=buoy_id_attr_name))
            # Basic metadata.
            drifter_metadata = dict(
                buoyid=ds.attrs[buoy_id_attr_name],
                filename=drifter_filename,
                start_data_date=str(ds['time'].values[0]),
                last_data_date=str(ds['time'].values[-1])
            )

            # Add other global attributes to metadata.
            for attr in ds.attrs.keys():
                drifter_metadata[attr] = str(ds.attrs[attr])

            # Add "meta" variables to metadata.
            if meta_variables:
                for mvar in meta_variables:
                    if mvar in ds.variables:
                        drifter_metadata[mvar] = ds[mvar].values[0]

            drifters.append(drifter_metadata)

    now = datetime.datetime.utcnow()
    metadata = dict(updated=now.isoformat(), drifters=drifters)
    logger.info('Dumping drifter metadata to file %s...', output_file)
    with open(output_file, 'w') as f:
        yaml.dump(metadata, f, default_flow_style=False)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser,
        args_for_writing_out_config_file=['-w', '--write-out-config-file']
    )
    arg_parser.add(
        '-c', '--config', is_config_file=True,
        help='Name of configuration file')
    arg_parser.add(
        '--log_level', default='info', choices=utils.log_level.keys(),
        help='Set level for log messages')

    arg_parser.add(
        '--data_dir', type=str,
        default='/data/ocean/users/cclements/data/drifter',
        help='Path to directory containing drifter data files')
    arg_parser.add(
        '-o', '--output', type=str, default='drifters.yaml',
        help='Name of metadata output file to create')
    arg_parser.add(
        '--buoy_id_attr', type=str, default='buoyid',
        help='Name of attribute containing buoy id')
    arg_parser.add(
        '--meta_variables', nargs='+', type=str, action='store',
        metavar='VAR_NAME', help='Names of variables that contain metadata')

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    assemble_drifter_metadata(
        config.data_dir, config.output,
        buoy_id_attr_name=config.buoy_id_attr,
        meta_variables=config.meta_variables)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
