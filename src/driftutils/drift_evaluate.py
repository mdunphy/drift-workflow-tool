"""
Run Drift Evaluation Workflow
=============================
:Author: Clyde Clements
:Created: 2017-10-27
"""

from collections import namedtuple
import os
from os.path import join as joinpath
import sys

from . import configargparse
from .get_skill import get_skill
from . import ioutils
from . import utils
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import glob

logger = utils.logger

def drift_evaluate(data_dir):
    """Run drift evaluation workflow."""
    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            
            ds_filename = joinpath(dirpath, filename)
            obs_track = ioutils.load_drifter_track(ds_filename,
                                                   name_prefix='obs_')
            mod_track = ioutils.load_drifter_track(ds_filename,
                                                   name_prefix='mod_')
            drifter_id = obs_track.buoyid
            run_name = mod_track.run_name

            skill = get_skill(obs_track, mod_track)
            liu_score = skill['liu']
            molcard_score = skill['molcard']
            logger.debug('Drifter %s:', drifter_id)
            if liu_score.size > 1:
                logger.debug('  liu: [%s, %s]',
                            liu_score[1:].min().values,
                            liu_score[1:].max().values)
            else:
                logger.warn('  liu size <= 1')
            if molcard_score.size > 1:
                logger.debug('  molcard: [%s, %s]',
                            molcard_score[1:].min().values,
                            molcard_score[1:].max().values)
            else:
                logger.warn('  molcard size <= 1')
            ds = ioutils.load_dataset(ds_filename)
            for v in skill.data_vars:
                ds[v] = skill[v]
            for a in skill.attrs:
                ds.attrs[a] = skill.attrs[a]
            ds.to_netcdf(ds_filename, mode='w')


def main(args=sys.argv[1:]):
    for i, arg in enumerate(args):
        if (arg[0] == '-') and arg[1].isdigit():
            args[i] = ' ' + arg
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser)
    arg_parser.add(
        '-c', '--config', is_config_file=True,
        help='Name of configuration file')
    arg_parser.add(
        '--log_level', default='info', choices=utils.log_level.keys(),
        help='Set level for log messages')
    arg_parser.add(
        '--data_dir', type=str, required=True,
        help=('Path to directory containing drifter class-4 type netCDF '
              'files'))

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])
                
        
    drift_evaluate(config.data_dir)
  
   
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
