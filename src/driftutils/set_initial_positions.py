"""
Set Initial Trajectory Positions
================================
:Author: Clyde Clements
:Created: 2017-08-28

This module determines the initial positions to use for trajectory simulations.
For a user-specificed date and time, the (interpolated) location of each
drifter buoy is found and the initial position for a corresponding trajectory
calculation is determined based on that drifter location and the given ocean
mesh.
"""

import datetime
from functools import wraps
import inspect
import logging
import os
import os.path
import collections
import numpy as np
from scipy import interpolate
from scipy import optimize
import xarray as xr
import yaml
from .find_nearest_grid_point import find_nearest_grid_point
from .get_drifter_location import get_drifter_location
from . import ioutils
from . import utils

class NoDrifterDataInModelDomain(Exception):
    pass

LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

logger = logging.getLogger('drifter')


def inquad(x, y, qx, qy):
    """Determine if a point lies within or on the boundary of a quadrilateral.

    Parameters
    ----------
    x : float
        X coordinate of point in question.
    y : float
        Y coordinate of point in question.
    qx : np.ndarray of size 4
        X coordinates of the four points of the quadrilateral.
    qy : np.ndarray of size 4
        Y coordinates of the four points of the quadrilateral.

    Returns
    -------
    inquad : bool
        True if a point lies within or on the boundary of a quadrilateral;
        false otherwise.

    Notes
    -----
    This function returns true if a point lies within or on the boundary of a
    quadrilateral of any shape on a plane. The coordinates ``qx``, ``qy`` of
    the points of the quadrilateral must be ordered; that is, it is assumed
    that point (``qx[0]``, ``qy[0]``) connects to point (``qx[1]``, ``qy[1]``),
    and (``qx[1]``, ``qy[1]``) connects to (``qx[2], ``qy[2]``), and so on.

    Method: For the quadrilateral ABCD (with the points ordered) and given
    point P, check if the cross (vector) products PA x PD, PB x PA, PD x PC,
    and PC x PB are all negative.
    """
    inquad = False
    zst1 = (x - qx[0]) * (y - qy[3]) - (y - qy[0]) * (x - qx[3])
    if zst1 <= 0.0:
        zst2 = (x - qx[3]) * (y - qy[2]) - (y - qy[3]) * (x - qx[2])
        if zst2 <= 0.0:
            zst3 = (x - qx[2]) * (y - qy[1]) - (y - qy[2]) * (x - qx[1])
            if zst3 <= 0.0:
                zst4 = (x - qx[1]) * (y - qy[0]) - (y - qy[1]) * (x - qx[0])
                if zst4 <= 0.0:
                    inquad = True
    return inquad


def find_containing_grid_cell(
        lon,           # type: float
        lat,           # type: float
        ocean_dataset,  # type: xr.Dataset
        lon_var,       # type: str
        lat_var        # type: str
):  # type: (...) -> (nd.array, nd.array, nd.array, nd.array)
    """Find the grid cell containing a location.

    Parameters
    ----------
    lon : float
        Longitude of location in question.
    lat : float
        Latitude of location in question.
    ocean_dataset : xarray.Dataset
        Ocean model dataset in which to find the grid cell.
    lon_var : str
        Name of longitude variable in ocean dataset.
    lat_var : str
        Name of latitude variable ocean dataset.

    Returns
    -------
    tuple
        A four-element tuple containing ``ix``, ``iy``, ``lon`` and ``lat``.
        Each element is a *numpy.ndarray* containing four values corresponding
        to the x indices, y indices, longitudes, and latitudes, respectively,
        of the containing grid cell.
    """
    # First find the closest grid point.
    dist_sq, j, i, clat, clon = find_nearest_grid_point(
        lat, lon, ocean_dataset, lat_var, lon_var, n=1
    )
    # Values i and j are arrays with only a single value; change them to
    # scalars.
    i = i[0]
    j = j[0]
    # The longitude and latitude variables should be two-dimensional arrays.
    # In some mesh files, these are four-dimensional arrays but the extra
    # dimensions are of size 1. The squeeze() removes the single-dimensional
    # entries.
    lons = ocean_dataset[lon_var].values.squeeze()
    lats = ocean_dataset[lat_var].values.squeeze()

    # Most datasets have longitude defined over the range -180 to +180. The
    # GIOPS forecast data, however, currently uses a 0 to 360 range, so we
    # adjust those values where necessary.
    lons[lons > 180] -= 360

    if lats.ndim == 1:
        # If latitude and longitude are 1D arrays (as is the case with the
        # GIOPS forecast data currently pulled from datamart), then we need to
        # handle this situation in a special manner. If lats is shape m and
        # lons is shape n, we want to reshpe as (m, n) -> use meshgrid
        lons, lats = np.meshgrid(lons, lats)


    # Now check each box that the closest grid point is a member of. We have
    # four boxes to check:
    #
    # * one with lower-left corner point (i, j);
    # * one with lower-left corner point (i - 1, j);
    # * one with lower-left corner point (i, j - 1); and
    # * one with lower-left corner point (i - 1, j - 1).
    #
    # As soon as we find a containing box, we immediately return the details of
    # that box.
    for jshift in [0, -1]:
        for ishift in [0, -1]:
            ix = [i + ishift, i + ishift + 1, i + ishift + 1, i + ishift]
            iy = [j + jshift, j + jshift, j + jshift + 1, j + jshift + 1]
            try:
                lon_box = lons[tuple([iy, ix])].squeeze()
                lat_box = lats[tuple([iy, ix])].squeeze()
            except IndexError:
                continue
            if inquad(lon, lat, lon_box, lat_box):
                ix = np.array(ix)
                iy = np.array(iy)
                return ix, iy, lon_box, lat_box

    msg = 'Could not determine box containing lat/lon point ({}, {})'.format(
        lat, lon
    )
    raise NoDrifterDataInModelDomain(msg)


def set_ariane_interpolated_positions(
        lat, lon, mesh_dataset, ulon_var='glamu', ulat_var='gphiu',
        vlon_var='glamv', vlat_var='gphiv',
):
    ix_u, iy_u, lon_u, lat_u = find_containing_grid_cell(
        lon, lat, mesh_dataset, ulon_var, ulat_var
    )
    ix_v, iy_v, lon_v, lat_v = find_containing_grid_cell(
        lon, lat, mesh_dataset, vlon_var, vlat_var
    )

    lon_u_box = np.zeros((2, 2), dtype=np.float64)
    for i, j, u in zip(ix_u - ix_u.min(), iy_u - iy_u.min(), lon_u):
        lon_u_box[i][j] = u

    lat_v_box = np.zeros((2, 2), dtype=np.float64)
    for i, j, v in zip(ix_v - ix_v.min(), iy_v - iy_v.min(), lat_v):
        lat_v_box[i][j] = v

    # Determine initial guess for grid point indices.
    ix_interp = ix_u.mean()
    iy_interp = iy_v.mean()

    def residual(g, ulon, vlat, lon, lat):
        gi = g[0]
        gj = g[1]
        a = gi - int(gi)
        b = (gj + 0.5) - int(gj + 0.5)
        residual1 = (1 - a) * (1 - b) * ulon[0][0] \
            + (1 - a) * b * ulon[0][1] \
            + a * (1 - b) * ulon[1][0] \
            + a * b * ulon[1][1] - lon
        a = (gi + 0.5) - int(gi + 0.5)
        b = gj - int(gj)
        residual2 = (1 - a) * (1 - b) * vlat[0][0] \
            + (1 - a) * b * vlat[0][1] \
            + a * (1 - b) * vlat[1][0] \
            + a * b * vlat[1][1] - lat
        return [residual1, residual2]

    # Note that solutions to the above residual are not unique; if [gi, gj] is
    # a solution, then [gi, gj] + n * [1, 1] where n is an integer will also be
    # a solution. A root-finding method such as optimize.root() will find a
    # solution, but is not guaranteed to find the right solution. Therefore, we
    # resort to a least squares procedure with bound constraints.
    bounds = ((ix_u.min(), iy_v.min()), (ix_u.max(), iy_v.max()))
    sol = optimize.least_squares(residual, [ix_interp, iy_interp],
                                 args=(lon_u_box, lat_v_box, lon, lat),
                                 bounds=bounds)
    if sol.success:
        ix_interp = sol.x[0]
        iy_interp = sol.x[1]
        # Compute interpolated lat/lon positions.
        lon_interp, lat_interp = residual([ix_interp, iy_interp],
                                          lon_u_box, lat_v_box, 0, 0)
    else:
        # Fallback for when root method fails to converge.
        ix_interp = ix_u[0]
        iy_interp = iy_v[0]
        lon_interp = lon_u_box[0]
        lat_interp = lat_v_box[0]

    return ix_interp, iy_interp, lon_interp, lat_interp


def set_general_interpolated_positions(
        lat, lon, ocean_dataset, interp_method, lon_var='glamu',
        lat_var='gphiu'
):
    # NOTE: This function has not been heavily tested.
    # As Ariane is the only trajectory module currently in use and requires its
    # own interpolation routine, this function is not used at present. Testing
    # should be done once it finds a use!
    # Clyde Clements, 2017-09-19

    n_interp_points = {'linear': 2 * 2, 'cubic': 4 * 4, 'quintic': 6 * 6}
    # Find the "box" of grid points containing the lat/lon point.
    dist_sq, iy, ix, lats, lons = find_nearest_grid_point(
        lat, lon, ocean_dataset, lat_var, lon_var,
        n=n_interp_points[interp_method]
    )
    f = interpolate.interp2d(lons, lats, ix, kind=interp_method)
    ix_interp = f(lon, lat)[0]
    f = interpolate.interp2d(lons, lats, iy, kind=interp_method)
    iy_interp = f(lon, lat)[0]

    # Compute interpolated lat/lon positions.
    f = interpolate.interp2d(iy, ix, lons, kind=interp_method)
    lon_interp = f(iy_interp, ix_interp)[0]
    f = interpolate.interp2d(iy, ix, lats, kind=interp_method)
    lat_interp = f(iy_interp, ix_interp)[0]

    return ix_interp, iy_interp, lon_interp, lat_interp


def set_vertical_position(depth, ocean_dataset, wdep_var):
    depths = ocean_dataset.variables[wdep_var].squeeze().values[:]
    iz = np.arange(1, len(depths) + 1, dtype=float)
    f = interpolate.interp1d(depths, iz, kind='linear')
    iz_interp = f(depth)
    return iz_interp


def set_time_index(run_ocean_data_dates, start_date,
                   time_interval='time_centered'):
    """Find index in run_ocean data dates corresponding to start date.

    The time values in the ocean data files can represent the
    center of the period for which the data is valid
    (time_interval='time_centered') or instantaneous output
    (time_interval='time_instant').

    As an example for time_ceneter data, if ocean current data is available
    hourly, the data dates, averaging period and corresponding Ariane time
    index would look like the following:

    ================   ========================   =====
    Timestamp          Averaging Period           Index
    ================   ========================   =====
    2017-04-01 00:30   2017-04-01 00:00 - 01:00   1
    2017-04-01 01:30   2017-04-01 01:00 - 02:00   2
    2017-04-01 02:30   2017-04-01 02:00 - 03:00   3
    2017-04-01 03:30   2017-04-01 03:00 - 04:00   4
    ================   ========================   =====

    The following are some sample start dates and what index value this
    function will return for the above data dates:

    ================   =====
    Start date         Index
    ================   =====
    2017-04-01 00:00   0.5
    2017-04-01 00:30   1
    2017-04-01 01:00   1.5
    2017-04-01 01:30   2
    ================   =====

    For time_instant data, if the ocean data is available hourly, the data
    dates, saving period and corresponding Ariane time index would look
    like the following:

    ================  ========================  =====
    Timestamp         Saving period             Index
    ================  ========================  =====
    2017-04-01 01:00  2017-04-01 00:00 - 01:00  1.5
    2017-04-01 02:00  2017-04-01 01:00 - 02:00  2.5
    2017-04-01 03:00  2017-04-01 02:00 - 03:00  3.5
    ================  ========================  =====

    The following are some sample start dates and what index values this
    function will return for the above dates:

    ================  =====
    Start date        Index
    ================  =====
    2017-04-01 00:30  1
    2017-04-01 01:00  1.5
    2017-04-01 01:30  2
    ================  =====
    """
    if time_interval == 'time_centered':
        indices = np.arange(1, len(run_ocean_data_dates) + 1, dtype=float)
    elif time_interval == 'time_instant':
        indices = np.arange(1.5, len(run_ocean_data_dates) + 1.5, dtype=float)
    else: # use time_cenetered convention
        indices = np.arange(1, len(run_ocean_data_dates) + 1, dtype=float)
    ocean_dd = run_ocean_data_dates.astype('datetime64[s]').astype('float64')
    start_dd = np.array([start_date], dtype='datetime64[s]').astype('float64')
    f = interpolate.interp1d(
        ocean_dd, indices, kind='linear', fill_value='extrapolate')
    idx = float(f(start_dd[0]))
    return idx


def set_initial_positions(
        *, start_date, drifter_data_dir, ocean_data_file,
        ocean_metadata_file,
        run_ocean_data_dates,
        drift_model_name = 'Ariane',
        drifter_depth='1c',
        lon_var='nav_lon', lat_var='nav_lat',
        ulon_var='glamu', ulat_var='gphiu',
        vlon_var='glamv', vlat_var='gphiv', dep_var='gdepw_1d',
        tmask_var='tmask', interp_method='ariane', output_file=None,
        has_mesh=True
):
    """Set initial trajectory positions.

    The format for specifying the drifter depth is detailed below; some
    examples of possible values are:

    - '15.0': Starting depth of 15.0 m

    - '15.0c': Constant depth of 15.0 m

    - 'L15': Starting depth corresponding to 15th depth layer

    - 'L15c': Constant depth corresponding to 15th depth layer

    - 'L15.2c': Constant depth corresponding to the "15.2"th depth layer.
      Note non-integer values are permitted to introduce shift with respect
      to the exact position of the depth level. This value corresponds to
      a depth partway between the 15th and 16th depth level.

    If specified, an output file will be in created in YAML format and will
    contain the following entries:

    - ``start_date`` (*str*): Date/time in ISO format for which positions
      of drifters was determined.

    - ``ocean_data_file`` (*str*): Name of ocean model file which contains
      grid information.

    - ``drifter_grid_positions`` (*dict*): Dictionary containing initial
      drifter positions, the same as the returned result; see below.

    - ``drifter_data_files`` (*dict*): Dictionary containing drifter data
      files for this run. The key is the drifter buoy id and the value is
      the name of the data file. Note the file paths will be relative to
      the output file.

    - ``updated`` (*str*): Date/time in ISO format when this information
      was constructed.

    If ``output_file`` is set to ``None``, no file will be created.

    Parameters
    ----------
    start_date : datetime.datetime
        Date for which to determine drifter positions.
    drifter_data_dir : str
        Name of directory containing drifter data files.
        All NetCDF files in this directory and any subdirectories will be
        processed.
    ocean_data_file : str
        Name of NetCDF file containing ocean model grid. Must be mesh file
        for ariane simulations.
    ocean_metadata_file: str
        Name of yaml file containing ocean metadata
    run_ocean_data_dates: np.ndarray
        Dates, in ascending order, corresponding to ocean current data for
        all ocean data linked in run dir.
    drift_model_name : str
        Name of drift trajectory model to be used ('Ariane' or 'OpenDrift')
        defaults to 'Ariane'
    drifter_depth : str
        Drifter depth in the format "[L]n[c]". An optional prefix of "L" means
        the number "n" indicates the depth layer; otherwise, the number "n"
        indicates the actual depth in meters. An optional suffix of "c"
        indicates a constant-layer (or fixed depth) trajectory calculation.
        Specifies the depth for drift trajectory simulations; otherwise, the
        particle is free to move in the vertical direction.
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    ulon_var : str
        Name of variable in mesh file defining longitude for U velocity
        points.
    ulat_var : str
        Name of variable in mesh file defining latitude for U velocity
        points.
    vlon_var : str
        Name of variable in mesh file defining longitude for V velocity
        points.
    vlat_var : str
        Name of variable in mesh file defining latitude for V velocity
        points.
    dep_var : str
        Name of variable in mesh file defining depth for W velocity points.
        Name of depth variable in model files for OpenDrift.
        This must be a one dimensional array.
    tmask_var : str
        Name of variable in mesh file containing land/ocean mask for T grid
        points.
    interp_method : str
        Name of interpolation method to use for determining drifter horizontal
        position. Must be one of 'linear', 'nearest', 'ariane', 'cubic' or
        'quintic'.
    output_file : str
        Name of file to create containing details of determined drifter
        positions.
        If not specified, no file is created.
    has_mesh: boolean
        True if ocean_data_file is a mesh file, False otherwise.

    Returns
    -------
    grid_positions : dict
        A dictionary containing initial drifter positions. The key is the
        drifter buoy id and the value is a two-element list containing the x
        and y indices in terms of the given mesh.
    """
    logger.info('\n' + 'Determining initial positions for drifters...')
    if interp_method not in ['nearest', 'ariane', 'linear', 'cubic', 'quintic']:
        msg = ('Unknown interpolation method "{}"; valid options are nearest, '
                'ariane, linear, cubic and quintic').format(interp_method)
        raise ValueError(msg)

    # Parse drifter depth option.
    if drifter_depth.startswith('L'):
        depth_in_layers = True
        depth_option = drifter_depth[1:]
    else:
        depth_in_layers = False
        depth_option = drifter_depth
    if depth_option.endswith('c'):
        constant_depth = True
        depth_option = depth_option[:-1]
    else:
        constant_depth = False
    depth = float(depth_option)

    # Convert data dates to an array if necessary.
    if not isinstance(run_ocean_data_dates, np.ndarray):
        run_ocean_data_dates = np.array(run_ocean_data_dates)

    
    ocean_dataset = xr.open_dataset(ocean_data_file, decode_times=False)

    # open ocean_metadata_file
    with open(ocean_metadata_file) as f:
        meta = yaml.load(f, Loader=yaml.FullLoader)

    grid_positions = {}
    if output_file:
        # Details of the drifter files are only created for the output file.
        drifter_files = {}

    for dirpath, dirnames, filenames in os.walk(drifter_data_dir):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue

            logger.debug('Examining drifter data file %s...', filename)
            drifter_dataset = ioutils.load_drifter_dataset(
                                                os.path.join(dirpath, filename)
                                                )
          
            buoyid = drifter_dataset.attrs['buoyid']
            lon, lat = get_drifter_location(
                            date=start_date, 
                            dataset=drifter_dataset, 
                            method='linear'
                            )

            #if a mesh file is given:
            if has_mesh:
                try:
                    if interp_method == 'nearest':
                        dist_sq, iy, ix, lats, lons = find_nearest_grid_point(
                            lat, lon, ocean_dataset, lat_var, lon_var, n=1
                            )
                        ix_interp = ix[0]
                        iy_interp = iy[0]
                        lon_interp = lons[0]
                        lat_interp = lats[0]

                    elif interp_method == 'ariane':
                        ix_interp, iy_interp, lon_interp, lat_interp \
                            = set_ariane_interpolated_positions(
                                lat, lon, ocean_dataset, ulon_var=ulon_var,
                                ulat_var=ulat_var, vlon_var=vlon_var,
                                vlat_var=vlat_var)

                    else:
                        ix_interp, iy_interp, lon_interp, lat_interp \
                            = set_general_interpolated_positions(
                                lat, lon, ocean_dataset, interp_method,
                                lon_var=lon_var, lat_var=lat_var)

                    logger.info(
                        'Drifter location (lat, lon) = (%s, %s)', lat, lon)
                    logger.info(
                        'Interpolated location (lat, lon) = (%s, %s)', 
                        lat_interp, lon_interp)

                except NoDrifterDataInModelDomain as e:
                    logger.info(
                        ('Drifter trajectory not computed: {}'.format(e)))

                    continue

                # ix_interp and iy_interp are calculated based on indices
                # starting from 0; Ariane uses indices starting from 1,
                # so adjust these values.
                ix_interp = ix_interp + 1
                iy_interp = iy_interp + 1

                # If using Ariane and iy_interp is calculated from U points,
                # then use the following statement in place of the one above:
                # iy_interp = iy_interp + 1 - 0.5
                if depth_in_layers:
                    iz = depth

                else:
                    # Convert deoth value to corresponding depth layer/level
                    iz = set_vertical_position(depth, ocean_dataset, dep_var)

                if constant_depth:
                    # The minus signe requests a constant-layer trajetory
                    # calculation from Ariane.
                    iz = -iz

                it = set_time_index(
                    run_ocean_data_dates,
                    start_date,
                    time_interval=meta['ocean_domain']['time_interval'])

                # Ariane checks the mask value of a neighbouring grid point
                # and if the mask value indicates that it is a land point,
                # Ariane simply stops. Therefore, we will perform the
                # same check here and exclude the drifter if the land
                # condition is met.
                ix_neighbour = (int(ix_interp) + 1) - 1
                iy_neighbour = (int(iy_interp) + 1) - 1
                iz_neighbour = (int(abs(-iz))) - 1

                # It is possible that the neighbouring point is outside domain
                # Check here and exclude if drifter is on domain edge
                if (ix_neighbour == ocean_dataset.variables[tmask_var].shape[-1]) \
                   or (iy_neighbour == ocean_dataset.variables[tmask_var].shape[-2] ):
                    logger.info(
                        ('Excluding drifter %s because the grid point is at domain edge'), 
                        buoyid
                        )
                    continue

                # Note the subtraction of 1 in each of the above statements to
                # switch to 0-based indexing.
                if ocean_dataset.variables[tmask_var].ndim == 4:
                    # tmask assumed to have dimensions (t, z, y, x)
                    tmask = ocean_dataset.variables[tmask_var][
                        0, iz_neighbour, iy_neighbour, ix_neighbour
                    ]
                else:
                    # tmask assumed to have dimensions (z, y, x)
                    tmask = ocean_dataset.variables[tmask_var][
                        iz_neighbour, iy_neighbour, ix_neighbour
                    ]
                if tmask == 0:
                    logger.info(('Excluding drifter %s because the'
                                 ' neighbouring mesh point is a land point'), 
                                buoyid)
                    continue

                # Convert indices to standard Python float objects instead of
                # retaining the numpy types; otherwise, dumping to YAML yields
                # a complicated output that tries to preserve the numpy type.
                if drift_model_name == 'Ariane':
                     grid_positions[buoyid] = [float(ix_interp), 
                                               float(iy_interp), 
                                               float(iz), 
                                               float(it)]

                # If meshfile is available for an OpenDrift run
                # then the above logic will exclude points outside of the
                # domain
                elif drift_model_name == 'OpenDrift':
                    dep = depth
                    # Look up depth from model level
                    if depth_in_layers:
                        model_depths = meta['ocean_domain']['depth']
                        if 'unknown' in model_depths:
                            # dataset did not contain depth dimension
                            dep = 0
                        else:
                            dep = set_opendrift_depth_from_level(depth, 
                                                                 model_depths)

                    # Depth must be negative scale in OpenDrift
                    if dep > 0:
                        dep = -dep
                    grid_positions[buoyid] = [float(lon), float(lat), 
                                              float(dep), 
                                              start_date.isoformat()]
            else:
                # No mesh file available - check inside/outside of domain
                logger.info('No mesh file available - check inside/outside of domain')
                try:
                    ix, iy, lon_near, lat_near = find_containing_grid_cell(
                        lon, lat, ocean_dataset, lon_var, lat_var)
                except NoDrifterDataInModelDomain as e:
                    logger.info(('Drifter trajectory not computed: '
                                 '{}'.format(e)))
                    continue

                if drift_model_name == 'OpenDrift':
                    dep = depth

                    # Look up depth from model level
                    if depth_in_layers:
                        model_depths = meta['ocean_domain']['depth']
                        if 'unknown' in model_depths:
                            # dataset did not contain dept dimension
                            dep = 0
                        else:
                            dep = set_opendrift_depth_from_level(depth, 
                                                                 model_depths)
                    # Depth must be negative scale in OpenDrift
                    if dep > 0:
                        dep = -dep

                    grid_positions[buoyid] = [float(lon), float(lat), 
                                              float(dep), 
                                              start_date.isoformat()]


            if output_file:
                # Record drifter file relative to path of output file.
                rel_drifter_dir = os.path.relpath(
                    dirpath, os.path.dirname(output_file)
                )
                drifter_file = os.path.join(rel_drifter_dir, filename)
                drifter_files[buoyid] = drifter_file

            drifter_dataset.close()

    ocean_dataset.close()
    if output_file:
        # Record mesh file relative to path of output file.
        if has_mesh:
            mesh_file = os.path.relpath(ocean_data_file, 
                                        os.path.dirname(output_file))
            data = dict(
                updated=datetime.datetime.utcnow().isoformat(),
                start_date=start_date.isoformat(),
                mesh_file=mesh_file,
                drifter_grid_positions=grid_positions,
                drifter_data_files=drifter_files
               )
        else:
            data = dict(
                updated=datetime.datetime.utcnow().isoformat(),
                start_date=start_date.isoformat(),
                drifter_grid_positions=grid_positions,
                drifter_data_files=drifter_files
                )

        logger.info('Writing drifter positions to file %s...', output_file)

        with open(output_file, 'w') as f:
            yaml.dump(data, f, default_flow_style=False)

    return grid_positions


def set_initial_positions_mldp(*,
                               start_date,
                               drifter_data_dir,
                               bbox,
                               drifter_depth=0,
                               output_file=None):
    """Set initial trajectory positions for mldp.

       If specified, an output file will be in created in YAML format and will
    contain the following entries:

    - ``start_date`` (*str*): Date/time in ISO format for which positions
      of drifters was determined.

    - ``drifter_grid_positions`` (*dict*): Dictionary containing initial
      drifter positions, the same as the returned result; see below.

    - ``drifter_data_files`` (*dict*): Dictionary containing drifter data
      files for this run. The key is the drifter buoy id and the value is
      the name of the data file. Note the file paths will be relative to
      the output file.

    - ``updated`` (*str*): Date/time in ISO format when this information
      was constructed.

    If ``output_file`` is set to ``None``, no file will be created.

    Parameters
    ----------
    start_date : datetime.datetime
        Date for which to determine drifter positions.
    drifter_data_dir : str
        Name of directory containing drifter data files.
        All NetCDF files in this directory and any subdirectories will be
        processed.
    bbox : str
        outer corners of bounding box domain in which to perform evaluations
        e.g. "min_lon min_lat max_lon max_lat"
    drifter_depth : float
        Depth of the drifter in metres.
    output_file : str
        Name of file to create containing details of determined drifter
        positions.
        If not specified, no file is created.

    Returns
    -------
    grid_positions : dict
        A dictionary containing initial drifter positions. The key is the
        drifter buoy id and the value is a two-element list containing the
        lon and lat of the drifter start position.
    """
    logger.info('\n' + 'Determining initial positions for drifters...')
    grid_positions = {}
    if output_file:
        # Details of the drifter files are only created for the output file.
        drifter_files = {}
    # Format bbox quadrilateral for domain checking with inquad
    bbox_list = bbox.split()
    lon_bbox = [float(bbox_list[0]), float(bbox_list[2]),
                float(bbox_list[2]), float(bbox_list[0])]
    lat_bbox = [float(bbox_list[1]), float(bbox_list[1]),
                float(bbox_list[3]), float(bbox_list[3])]
    for dirpath, dirnames, filenames in os.walk(drifter_data_dir):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue

            logger.debug('Examining drifter data file %s...', filename)
            drifter_dataset = ioutils.load_drifter_dataset(
                                                os.path.join(dirpath, filename)
                                                )
          
            buoyid = drifter_dataset.attrs['buoyid']
            lon, lat = get_drifter_location(
                            date=start_date, 
                            dataset=drifter_dataset, 
                            method='linear'
                            )
            # Check that lon/lat in are in bbox
            if not inquad(lon, lat, lon_bbox, lat_bbox):
                logger.info(
                    "Drifter not computed: trajectory not in bbox {}".format(bbox))
                continue
            grid_positions[buoyid] = [float(lon), float(lat),
                                      float(drifter_depth),
                                      start_date.isoformat()]
            
            if output_file:
                # Record drifter file relative to path of output file.
                rel_drifter_dir = os.path.relpath(
                    dirpath, os.path.dirname(output_file)
                )
                drifter_file = os.path.join(rel_drifter_dir, filename)
                drifter_files[buoyid] = drifter_file
    
    if output_file:
        data = dict(
            updated=datetime.datetime.utcnow().isoformat(),
            start_date=start_date.isoformat(),
            drifter_grid_positions=grid_positions,
            drifter_data_files=drifter_files
        )

        logger.info('Writing drifter positions to file %s...', output_file)

        with open(output_file, 'w') as f:
            yaml.dump(data, f, default_flow_style=False)

    return grid_positions




def set_opendrift_depth_from_level(iz, model_depths):
    """
    For open drift simulations, return the corresponding depth of 
    a given model level iz.
    0 is returned if iz < 1 or iz > len(model_depths)
    iz=1 maps to model_depths[0] (cell centre)
    Parameters
    ----------
    iz : float
        model level of desired drifter start
    model_depths : numpy array
        array of model depth values at cell centre
    """
    levels = np.arange(1, len(model_depths)+1, dtype=float)
    fz = interpolate.interp1d(levels, model_depths, bounds_error=False,
                              kind='linear', fill_value=0)
    depth = fz(iz)
    return depth


def adjust_func_for_cli(func):
    """Update function for command-line usage.
    """
    @wraps(func)
    def wrapper(*args, ocean_data_dir, **kwargs):
        run_ocean_data_dates = ioutils.get_run_ocean_data_dates(ocean_data_dir)
        return func(*args, run_ocean_data_dates=run_ocean_data_dates, **kwargs)
    doc = func.__doc__
    try:
        i = doc.index('Parameters\n')
        i += len('Parameters\n')
        n = doc[i - 1:].index('----------\n')
        i += n + len('----------\n') - 1
        indent = ' ' * max(n - 1, 0)
        newdoc = indent + 'ocean_data_dir : str\n' + \
            indent + '    Path to directory containing ocean data files.\n'
        wrapper.__doc__ = doc[:i] + newdoc + doc[i:]
    except ValueError:
        pass
    # Modify function signature: replace 'run_ocean_data_dates' parameter with
    # 'ocean_data_dir'.
    sig = inspect.signature(func)
    params = list(sig.parameters.values())
    new_param = inspect.Parameter(
        'ocean_data_dir', inspect.Parameter.KEYWORD_ONLY)
    for i, p in enumerate(params):
        if p.name == 'run_ocean_data_dates':
            break
    params[i] = new_param
    wrapper.__signature__ = sig.replace(parameters=params)
    return wrapper


def main():
    from . import cli
    cli.run(adjust_func_for_cli(set_initial_positions))


if __name__ == '__main__':
    main()
