"""
Run Ocean Evaluation Workflow
=============================
:Author: Samuel Babalola
:Created: 2019-03-27
"""

from collections import namedtuple
import os
from os.path import join as joinpath
import sys

from . import configargparse
from .get_skill import get_skill
from .get_skill import get_ocean_skill
from . import ioutils
from . import utils
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import glob

logger = utils.logger

def ocean_evaluate(data_dir, experiment_dir):
    """Run ocean evaluation workflow.
    
    Parameters
    ----------
    data_dir : file
        Directory containing the computed trajecjory longitude and latitude 
        values.
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    run_days : int 
        Number of days to run a simulation from a given start date
    """

    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            
            ds_filename = joinpath(dirpath, filename)
            mod_track = ioutils.load_drifter_track(ds_filename,
                                                   name_prefix='mod_')
            run_name = mod_track.run_name
            skill = get_ocean_skill(mod_track)
            ds = ioutils.load_dataset(ds_filename)
            for v in skill.data_vars:
                ds[v] = skill[v]
            for a in skill.attrs:
                ds.attrs[a] = skill.attrs[a]
            ds.to_netcdf(ds_filename, mode='w')
        ioutils.merge(data_dir, experiment_dir)
        

def main(args=sys.argv[1:]):
    for i, arg in enumerate(args):
        if (arg[0] == '-') and arg[1].isdigit():
            args[i] = ' ' + arg
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser)
    arg_parser.add(
        '-c', '--config', is_config_file=True,
        help='Name of configuration file')
    arg_parser.add(
        '--log_level', default='info', choices=utils.log_level.keys(),
        help='Set level for log messages')
    arg_parser.add(
        '--data_dir', type=str, required=True,
        help=('Path to directory containing drifter class-4 type netCDF '
              'files'))
    arg_parser.add(
        '--experiment_dir', type=str, required=True,
        help=('Path to directory containing drifter class-4 type netCDF '
              'files'))


    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])
                
        
    ocean_evaluate(config.data_dir, config.experiment_dir)
   
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
