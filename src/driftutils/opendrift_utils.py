"""
Opendrift Utility Routines
==========================
:Author: Nancy Soontiens
:Created: 2019-11-15
"""
import os

import numpy as np
import xarray as xr

from driftutils.utils import round_up_base10

def create_land_binary_mask(ocean_file, land_mask_file,
                            varname='vozocrtx',
                            lon_var='nav_lon',
                            lat_var='nav_lat'):
    """Function to create a land_binary_mask file from the ocean model.
    The land_binary_mask is used in Open Drift to determine if particles are
    on land.

    Parameters
    ----------
    ocean_file : str
        Name of the ocean NetCDF file that contains masked data. Must
        contain variables for varname, lon_var and lat_var
    land_mask_file : str
        Name of the file where the land mask will be stored. This file will be
        created if it doesn't exist.
    varname : str
        Name of the variable in the ocean NetCDF file to be used for mask
        creation
    lon_var : str
        Name of the longitude variable in the ocean NetCDF file
    lat_var : str
        Name of the latitude variable in the ocean NetCDF file
    """
    if not os.path.exists(land_mask_file):
        ds = xr.open_dataset(ocean_file)
        # Catch case where lat/lon are coordindate variables
        try:
            ds = ds.reset_coords([lon_var, lat_var])
        except ValueError:
            pass
        # Different treatment if variable is the tmask
        if varname == 'tmask':
            mask = ds[varname].values
            mask = np.ones(mask.shape) - mask
        else:
            v = np.ma.masked_invalid(ds[varname].values)
            mask = v.mask
        lon = ds[lon_var]
        lon.values[lon.values>180] = lon.values[lon.values>180]-360
        lat = ds[lat_var]
        if lon.values.ndim == 1:
            # coords are 1D - arrays could be flattened or a reg lat/lon grid
            if len(lon.values) != len(lat.values):
                # lat/lon are likely flattened but its possible we still
                # have a regular grid of equal size
                # store data_size for checking later
                data_size = len(lon.values)*len(lat.values)
            else:
                data_size = len(lon.values)
        else:
            data_size = lon.values.size
        while mask.size != data_size:
            mask = mask[0, :]
        mask = np.squeeze(mask)
        mask_int = np.multiply(mask, 1.0)
        if lon.dims != lat.dims:
            dims = (lat.dims[0], lon.dims[0])
        else:
            dims = lon.dims
        dnew = xr.Dataset()
        dnew[lat_var] = lat
        dnew[lon_var] = lon
        dnew['land_binary_mask'] = (dims, mask_int)
        dnew['land_binary_mask'].attrs['standard_name'] = 'land_binary_mask'
        dnew[lon_var].attrs['valid_min'] = -180.
        dnew[lon_var].attrs['valid_max'] = 180.
        # Add projection information if it exists
        for var_name in ds.variables:
            var = ds.variables[var_name]
            attributes = var.attrs
            if 'grid_mapping_name' in attributes:
                dnew[var_name] = var
        dnew.to_netcdf(land_mask_file)
        ds.close()
        dnew.close()


def determine_buffer(ocean_file, lon_var, lat_var,
                     drift_duration, lat_min, lat_max,
                     max_speed=1):
    """Determine the buffer and grid spacing for the land_binary_mask
       interpolation.  Open Drift interpolates the unstrcutred grid
       to a regular grid. This function determines the extent and grid spacing
       of the interpolated grid based the expected speed of the particles and
       model grid spacing.

    Parameters
    ----------
    ocean_file: str
        Name of the ocean file that contains grid information.
    lon_var : str
        Name of the longitude variable.
    lat_var : str
        Name of the latitude variable.
    drift_duration : datetime.timedelta
        Duration of drift.
    lat_min : float
        Minimum value of particle latitudes
    lat_max : float
        Maximum value of partical latitudes
    max_speed : float
        Maximum expected speed of particles (m/s)

    Returns
    -------

    geobuffer : maximum expected distance (degrees) travelled in given
        max_speed.
    delta_lon : ocean model average longitude grid spacing (degrees)
    delta_lat : ocean model average latitude grid spacing (degrees)
    """
    # Ocean model grid spacing
    ds = xr.open_dataset(ocean_file)
    lons = ds.variables[lon_var].values
    lats = ds.variables[lat_var].values
    if lons.ndim == 1:
        delta_lon = np.diff(lons)
        delta_lat = np.diff(lats)
    else:
        delta_lon = np.diff(lons, axis=-1)
        delta_lat = np.diff(lats, axis=0)
    delta_lon = np.abs(np.mean(delta_lon))
    delta_lat = np.abs(np.mean(delta_lat))
    
    # Distance travelled by particles
    max_distance = max_speed * drift_duration.total_seconds()
    dlat = max_distance/111000.
    dlon = dlat/np.cos(np.radians(np.mean([lat_min, lat_max])))
    geobuffer = np.max([dlat, dlon])
    ds.close()

    return geobuffer, round_up_base10(delta_lon), round_up_base10(delta_lat) 
