"""
Assemble Atmosphweic Wind Data
=====================================
:Author: Clyde Clements, Nancy Soontiens
:Created: 2017-08-29 10:52:25 -0230

This module assembles the atmopsheric data in preparation for a drift run.

"""

import datetime
import os
import os.path
import sys
import yaml
from pyproj import Proj

import dateutil.parser
import numpy as np

from . import configargparse
from . import utils
from .utils import logger


def assemble_atmos_data(metadata_file, start_date, drift_duration,
                        data_assembly_dir,
                        xwindvel='u_wind', ywindvel='v_wind',
                        model_time='time_counter'):
    """Assemble atmos data.

    Parameters
    ----------
    metadata_file : str
        Name of file containing metadata for ocean data.
    start_date : datetime.datetime
        Date of start for drift prediction.
    drift_duration : datetime.timedelta
        Duration for drift prediction.
    data_assembly_dir : str
        Name of directory in which data will be assembled.
    xwindvel : str
	Name of variable containing model eastward wind, defaults to 'u_wind'
    ywindvel : str
	Name of variable containing model northward, defaults to 'v_wind'
    model_time : str
	Name of variable containing model time stamps, defaults to
        'time_counter'


    Returns
    -------
    info : dict
        A dictionary containing the following entries:

        - ``atmos_data_dir`` (*str*): Name of directory containing assembled
          atmos data. This will be a subdirectory of the data assembly
          directory and relative to the run directory.
        - ``run_atmos_data_dates`` (*numpy array*): Sorted array of dates
          for available atmos data for this run.
        - ``atmos_data_variables`` (*list*): List of strings containg names of
          atmos data variables.
        - ``atmos_data_files`` (*dict*): Dictionary containing atmos data files
          for this run. The key is an atmos variable name (such as u_wind)
          and the value is the list of data files relative to the run
          directory for that variable.
    """
    logger.info('Assembling atmos data...')
    metadata_dir = os.path.dirname(os.path.abspath(metadata_file))
    with open(metadata_file, 'r') as f:
        metadata = yaml.load(f, Loader=yaml.FullLoader)
    atmos_data = metadata['atmos_data']
    end_date = start_date + drift_duration
    data_vars = set()
    for data in atmos_data.values():
        for var in data:
            data_vars.add(var)
    atmos_data_in_period = {}
    for var in data_vars:
        atmos_data_in_period[var] = {}
    data_dates = find_model_data_date_period(
        atmos_data, start_date, end_date,
        time_interval='time_instant') ## Assuming atm data is instantaneous!
        # It is probably better to add some kind of check/namelist setting
    for data_date_str in atmos_data:
        data_date = dateutil.parser.parse(data_date_str)
        if data_date in data_dates:
           for var in atmos_data[data_date_str]:
                atmos_data_in_period[var][data_date] \
                    = atmos_data[data_date_str][var]
    if len(data_dates) == 0:
        msg = ('No atmos data available for drift simulation for the time '
               'period from {} to {}')
        msg = msg.format(start_date, end_date)
        raise ValueError(msg)

    atmos_data_subdir = 'atmos'
    atmos_data_dir = os.path.join(data_assembly_dir, atmos_data_subdir)
    pwd = os.getcwd()
    if not os.path.exists(data_assembly_dir):
        logger.debug('Creating directory for atmos data...')
        os.makedirs(data_assembly_dir)
    os.chdir(data_assembly_dir)
    if os.path.exists(atmos_data_subdir):
        msg = ('Assembly directory for atmos data already exists. Please '
               'delete it or specify a different directory.')
        raise RuntimeError(msg)
    os.makedirs(atmos_data_subdir)
    os.chdir(atmos_data_subdir)

    data_input_files = {}
    data_symlink_files = {}
    for var in atmos_data_in_period:
        data_input_files[var] = set()
        data_symlink_files[var] = []
        i = 0
        for data_date, filename in atmos_data_in_period[var].items():
            if filename in data_input_files[var]:
                continue
            data_input_files[var].add(filename)
            i += 1
            if not os.path.isabs(filename):
                data_filename = os.path.join(metadata_dir, filename)
                data_filename = os.path.relpath(data_filename)
            else:
                data_filename = filename
            symlink_name = '{}_{:05d}.nc'.format(var, i)
            os.symlink(data_filename, symlink_name)
            data_symlink_files[var].append(
                os.path.join(atmos_data_dir, symlink_name)
            )

    # Rename duplicate links
    reverse_dict = {}
    for var, files in data_symlink_files.items():
        for f in files:
            realpath = os.readlink(os.path.basename(f))
            try:
                reverse_dict[realpath][var] = os.path.basename(f)
            except KeyError:
                reverse_dict[realpath] = {}
                reverse_dict[realpath][var] = os.path.basename(f)
            os.remove(os.path.basename(f))
        data_symlink_files[var] = []
    new_links = {}
    for f, var_dict in reverse_dict.items():
        for var in var_dict:
            base = var_dict[var].split('_')[-1]
            try:
                new_links[f] = '{}_{}'.format(var, new_links[f])
            except KeyError:
                new_links[f] = '{}_{}'.format(var, base)
    for data_file, symlink_name in new_links.items():
        variables = symlink_name.split('_')[:-1]
        os.symlink(data_file, symlink_name)
        for var in variables:
            data_symlink_files[var].append(os.path.join(atmos_data_dir,
                                                        symlink_name))
            
    os.chdir(pwd)
    logger.info(
        'Finished assembling atmos data:\n  directory = %s\n', atmos_data_dir)

    info = {
        'atmos_data_dir': atmos_data_dir,
        'atmos_data_variables': data_vars,
        'atmos_data_files': data_symlink_files
    }
    return info


def find_model_data_date_period(model_data, start_date, end_date,
                                time_interval='time_centered'):
    """Find the model date range corresponding to drift period.

    Parameters
    ----------
    model_data : dict
        Model data diciontary with keys corresponding to all atmos model
         output times.
    start_date : datetime.datetime
        Drift period start date
    end_date : datetime.datetime
        Drift period end date
    time_interval : str
        Type of time interval between model ouput times.
        - time_centered - output times represent midpoint of time period
        - time_instant - output times represent right end points of time period
        - time_left - output times represent left end points of time period

    Returns
    -------
    data_dates : list
        Model output dates associated with drift period.
    """
    all_model_dates = model_data.keys()
    all_model_dates = [dateutil.parser.parse(data_date_str)
                       for data_date_str in model_data]
    all_model_dates.sort()
    data_dates = []
    diffs = np.diff(all_model_dates)
    # Construct endpoints of model_dates bounds
    if time_interval == 'time_centered':
        # model dates are midpoints - construct left and right end points.
        ends=[ all_model_dates[i] - diffs[i-1]/2
               for i in range(1,len(diffs)+1)]
        # Assuming first endpoint is -diff[0]/2 from first model_date
        ends.insert(0, all_model_dates[0] - diffs[0]/2 )
        # Assuming last endpoint is diff[-1]/2from last model_date
        ends.append(all_model_dates[-1] + diffs[-1]/2 )
    elif time_interval == 'time_instant':
        # Assume first endpoint is -diff[0] from first model_date
        ends = all_model_dates[:]
        ends.insert(0, all_model_dates[0] - diffs[0])
    elif time_interval == 'time_left':
        # Assume last endpoint is diff[-1] from last model_date
        ends = all_model_dates[:]
        ends.append(all_model_dates[-1] + diffs[-1])
    else: # Use time_centered approach
        ends=[ all_model_dates[i] - diffs[i-1]/2
               for i in range(1,len(diffs)+1)]
        ends.insert(0, all_model_dates[0] - diffs[0]/2 )
        ends.append(all_model_dates[-1] + diffs[-1]/2 )    
    # If start_date or end_date are outside of endpoint, return empty list
    if (start_date < ends[0]) or (end_date > ends[-1]):
        return data_dates
    else:
        # Check if start_date and end_date lie between endpoint pairs or
        # data_date is between start_date and end_date
        for end1, end2, data_date in zip(ends[:-1], ends[1:], all_model_dates):
            if (((end1      <= start_date) and (start_date <= end2     )) or
                ((end1      <= end_date)   and (end_date   <= end2     )) or
                ((data_date >= start_date) and (data_date   <= end_date))):
                data_dates.append(data_date)
        # Remove duplicates
        data_dates=list(set(data_dates))
        data_dates.sort()
    return data_dates


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--atmos_metadata_file', type=str, required=True,
                   help='YAML file containing metadata for atmos data files')
    arg_parser.add('--start_date', type=str, required=True,
                   help='Date and time for start of drift calculations')
    arg_parser.add('--num_drift_hours', type=int, default=24,
                   help='Number of hours for drift calculation')
    arg_parser.add('--data_assembly_dir', type=str, required=True,
                   help=('Name of directory to create containing symlinks to '
                         'ocean data for subsequent trajectory calculation'))
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    start_date = dateutil.parser.parse(config.start_date, ignoretz=True)
    logger.debug('Parsed user-specified date: %s', start_date)

    drift_duration = datetime.timedelta(hours=config.num_drift_hours)
    assemble_atmos_data(
        config.atmos_metadata_file, start_date, drift_duration,
        config.data_assembly_dir
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
