###############################################################################
# Construct configuration for Ariane run
#
# Author: Clyde Clements
# Created: 2017-09-01 07:43:29 -0230
###############################################################################

import sys

import yaml

from . import configargparse
from . import utils
from .utils import logger


def make_ariane_run_config(
        namelist_file,           # type: str
        run_dir,                 # type: str
        drifter_positions_file,  # type: str
        ariane_run_config_file   # type: str
):  # type: (...) -> None
    logger.debug('Constructing configuration for Ariane run...')

    with open(drifter_positions_file, 'r') as f:
        drifters = yaml.load(f)

    ariane_run_config = {
        'namelist': namelist_file,
        'run_dir': run_dir
    }

    positions = drifters['drifter_positions'].copy()
    for position in positions.values():
        # At this point the position variable is a list with two elements, the
        # x and y grid-point indices. The list needs to be extended to include
        # the z and time indices.
        # We set the z index to -11.0; grid point 11 corresponds to a depth of
        # about 14.6m, while the minus sign requests a constant-layer
        # calculation for trajectories (no vertical movement).
        position.append(-11.0)
        # We set the time index to 1.0; according to the Ariane tutorial manual
        # this corresponds to the center of the period.
        position.append(1.0)
        # TODO: Allow z and time indices to be specified by user options.
    ariane_run_config['initial_position'] = list(positions.values())

    logger.debug('Writing Ariane run configuration to file %s...',
                 ariane_run_config_file)
    with open(ariane_run_config_file, 'w') as f:
        yaml.dump(ariane_run_config, f)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--namelist_file', type=str, required=True,
                   help='Name of Ariane namelist file')
    arg_parser.add('--run_dir', type=str, default='run',
                   help=('Name of directory in which to run Ariane. This is '
                         'where its output files will be generated.'))
    arg_parser.add('--drifter_positions_file', type=str, required=True,
                   help='Number of YAML file containing drifter positions')
    arg_parser.add('--ariane_run_config_file', type=str, required=True,
                   help=('Name of YAML file to create containing Ariane run '
                         'configuration'))
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    make_ariane_run_config(
        config.namelist_file, config.run_dir, config.drifter_positions_file,
        config.ariane_run_config_file
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
