###############################################################################
# Run OpenDrift trajectory simulation
#
# Author: Nancy Soontiens
# Created: 2022-02-17
###############################################################################

import dateutil.parser
import glob
import os

import numpy as np
import yaml

from opendrift.models import oceandrift
from opendrift.readers import (reader_netCDF_CF_generic,
                               reader_netCDF_CF_irregular2D,
                               reader_NEMO_native)

from .add_traj_label import add_traj_label_to_datafile
from .opendrift_utils import (create_land_binary_mask,
                              determine_buffer)
from .utils import (defaults, logger)


def run_opendrift(*,
    run_dir,
    data_assembly_dir,
    drifter_positions_file,
    start_date,
    drift_duration,
    ocean_mesh_file,
    ocean_metadata_file,
    xwatervel=defaults['xwatervel']['nc_name'],
    ywatervel=defaults['ywatervel']['nc_name'],
    zwatervel=None,
    model_time_ocean='time_counter',
    lon_var_ocean='nav_lon',
    lat_var_ocean='nav_lat',
    alpha_wind=0.,
    atmos_forcing=False,
    atmos_metadata_file=None,
    xwindvel=defaults['xwindvel']['nc_name'],
    ywindvel=defaults['ywindvel']['nc_name'],
    model_time_atmos='time_counter',
    lon_var_atmos='nav_lon',
    lat_var_atmos='nav_lat',
    rotation_data_file='None',
    opendrift_dt=600,
    current_uncertainty=0,
    land_mask_type='ocean_model',
    grid_type='cgrid',
    orca_grid=False,
    output_dt=3600              
):
    """Prepare and run OpenDrift.
   
    Parameters
    ----------
    run_dir : str
        Name of directory in which to run OpenDrift.
        This is where its output files will be generated.
    data_assembly_dir : str
        Name of directory where simulation data is stored.
    drifter_positions_file : str
        Name of yaml file which stores drifter initial positions.
    start_date : datetime.datetime
        The start date of the simulation.
    drift_duration : datetime.timedelta
        The duration of the simulation.
    ocean_mesh_file : str
        Name of the ocean mesh file.
        Could be None for user grids.
    ocean_metadata_file : str
        Name of the file which stores ocean model metadate.
    xwatervel : str
        Name of variable containing X water velocity.
    ywatervel : str
        Name of variable containing Y water velocity.
    zwatervel : str
        Name of variable containing vertical water velocity.
    model_time_ocean : str
        Name of the ocean model time variable.
    lon_var_ocean : str
        Name of the ocean model longitude variable.
    lat_var_ocean : str
        Name of the ocean model latitude variable.
    alpha_wind : float
        Percentage of wind velocity to be applied to drifter.
    atmos_forcing : boolean
        True means include atmospheric forcing, False do not include.
    atmos_metadata_file : str
        Name of the file which stores atmos model metadate.
    xwindvel : str
        Name of variable containing X wind velocity.
    ywindvel : str
        Name of variable containing Y wind velocity.
    model_time_atmos : str
        Name of the atmosphere model time variable.
    lon_var_atmos : str
        Name of the atmosphere model longitude variable.
    lat_var_atmos : str
        Name of the atmosphere model latitude variable.
    rotation_data_file : str
        Path to data file containing coefficients for rotating velocity
        components on desired grid to true east/north. This file will be
        created if it does not exists, i.e. if a grid is being run for
        the first time. If set to  'None' no rotation is performed.
    opendrift_dt : float
        Integration timestep for OpenDrift in seconds.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*opendrift_dt
    land_mask_type : str
         Option for how OpenDrift determines land/ocean points. Choices are:
        'ocean_model' - a land mask is created from the ocean model data
        'GSHHS' - a land mask is created from a GSHHS coastline dataset
        (Opendrift's default)
    grid_type : str
        Indicates the type of ocean model grid. Options are 'cgrid',
        'agrid', or 'user_grid'.
    orca_grid : boolean
        Indicates an orca grid which requires specials treatment for
        rotation of velocity fields for OpenDrift. True means the grid is an
        orca grid. False means the grid is not orca.
    output_dt : float
        Output frequency of simluation in seconds.

    Returns
    -------
    status of OpenDrift simulation - 0 mean successfully completed.
    """
    logger.info('Setting up OpenDrift simulation.')
    # Interpret inputs
    if rotation_data_file == 'None':
        rotation_data_file = None
    ocean_data_dir = os.path.join(data_assembly_dir, 'ocean')
    if atmos_forcing:
        atmos_data_dir = os.path.join(data_assembly_dir, 'atmos')

    # Domain extent
    with open(ocean_metadata_file) as f:
        ocean_meta = yaml.load(f, Loader=yaml.FullLoader)
    domain_lon_min = ocean_meta['ocean_domain']['lon_min']
    domain_lon_max = ocean_meta['ocean_domain']['lon_max']
    domain_lat_min = ocean_meta['ocean_domain']['lat_min']
    domain_lat_max = ocean_meta['ocean_domain']['lat_max']

    max_speed = 5 # maximum expected speed of particles (m/s)

    # Prepare ocean model parameters
    model_land_mask=False
    if land_mask_type=='ocean_model':
        model_land_mask=True
    ocean_variable_mapping = {xwatervel: 'x_sea_water_velocity',
                              ywatervel: 'y_sea_water_velocity',
                              model_time_ocean: 'time',
                              lon_var_ocean: 'longitude',
                              lat_var_ocean: 'latitude'}
    if zwatervel:
        ocean_variable_mapping[zwatervel] = 'upward_sea_water_velocity'
        
    # Gather ocean files and create ocean reader
    ocean_files = glob.glob(os.path.join(ocean_data_dir, '*.nc'))
    ocean_files.sort()
    if rotation_data_file is None:
        logger.info('Preparing OpenDrift netCDF_CF_generic reader for'
                    ' ocean model.')
        reader_ocean = reader_netCDF_CF_generic.Reader(ocean_files)
        reader_list = [reader_ocean,]
        if model_land_mask:
            logger.info('Determining OpenDrift land mask from ocean model.')
            opendrift_map_file = os.path.join(data_assembly_dir,
                                              'land_binary_mask.nc')
            create_land_binary_mask(ocean_files[0],
                                    opendrift_map_file,
                                    varname=xwatervel,
                                    lon_var=lon_var_ocean,
                                    lat_var=lat_var_ocean)
            reader_map = reader_netCDF_CF_generic.Reader(opendrift_map_file)
        else:
            logger.info('Using default GSHHS coastline data for OpenDrift'
                        ' land mask.')
            reader_map = None
    else:
        # Determine geobuffer
        ocean_data=ocean_meta['ocean_data']
        dates=[*ocean_data]
        ocean_dt = dateutil.parser.parse(dates[1]) - \
                   dateutil.parser.parse(dates[0]) 
        geobuffer, _, _ = determine_buffer(ocean_files[0],
                                           lon_var_ocean,
                                           lat_var_ocean,
                                           ocean_dt,
                                           domain_lat_min,
                                           domain_lat_max,
                                           max_speed=max_speed)
        logger.info(
            'Ocean model geobuffer is {} degrees based on max speed {} m/s.'.format(
            geobuffer, max_speed))
        logger.info('Preparing OpenDrift NEMO_native reader for'
                    ' ocean model.')
        reader_ocean = reader_NEMO_native.Reader(ocean_files,
                                                 meshfile=ocean_mesh_file,
                                                 grid_type=grid_type,
                                                 model_land_mask=model_land_mask,
                                                 variable_mapping=ocean_variable_mapping,
                                                 geobuffer=geobuffer,
                                                 time_dim=model_time_ocean,
                                                 orca_grid=orca_grid,
                                                 rotation_pickle_file=rotation_data_file)
        reader_list = [reader_ocean, ]
        reader_map = None

    # Prepare atmospheric forcing, if needed
    if atmos_forcing:
        # Prepare atmos parameter
        atmos_variable_mapping = {xwindvel: 'x_wind',
                                  ywindvel: 'y_wind',
                                  model_time_atmos: 'time',
                                  lon_var_atmos: 'longitude',
                                  lat_var_atmos: 'latitude'}
        # Gather atmos files and create atmos reader
        atmos_files = glob.glob(os.path.join(atmos_data_dir, '*.nc'))
        atmos_files.sort()
        # Determine geobuffer
        with open(atmos_metadata_file) as f:
            atmos_meta = yaml.load(f, Loader=yaml.FullLoader)
        atmos_data=atmos_meta['atmos_data']
        dates=[*atmos_data]
        atmos_dt = dateutil.parser.parse(dates[1]) - \
                   dateutil.parser.parse(dates[0])

        geobuffer, _, _ = determine_buffer(atmos_files[0],
                                           lon_var_atmos,
                                           lat_var_atmos,
                                           atmos_dt,
                                           domain_lat_min,
                                           domain_lat_max,
                                           max_speed=max_speed)
        logger.info(
            'Atmos model geobuffer is {} degrees based on max speed {} m/s.'.format(
            geobuffer, max_speed))
        logger.info('Preparing OpenDrift netCDF_CF_irregular_2D reader for'
                    ' atmos model.')
        reader_atmos = reader_netCDF_CF_irregular2D.Reader(
            atmos_files,
            time_dim=model_time_atmos,
            lon_var=lon_var_atmos,
            lat_var=lat_var_atmos,
            variable_mapping=atmos_variable_mapping,
            geobuffer=geobuffer
        )
        reader_list.append(reader_atmos)

    # Prepare starting positions
    with open(drifter_positions_file) as f:
        drifter_data=yaml.load(f, Loader=yaml.FullLoader)
    drifter_positions = drifter_data['drifter_grid_positions']
    labels = sorted(drifter_positions.keys())
    start_positions = np.zeros((3, len(labels)))
    for c, buoyid in enumerate(labels):
        start_positions[0, c] = drifter_positions[buoyid][0]
        start_positions[1, c] = drifter_positions[buoyid][1]
        start_positions[2, c] = drifter_positions[buoyid][2]

    # Prepare output files
    opendrift_traj_file = ('OpenDrift_raw_trajectory_' +\
                           str(start_date)[:10] + '_' +\
                           str(start_date)[11:13] +'h.nc')
    opendrift_traj_file = os.path.join(run_dir,
                                       opendrift_traj_file)
    logfile = os.path.join(run_dir,
                           'opendrift_log.txt')
    run_opendrift_simulation(
        opendrift_traj_file,
        logfile,
        reader_list,
        reader_map,
        start_positions,
        start_date,
        drift_duration,
        opendrift_dt,
        current_uncertainty,
        alpha_wind,
        model_land_mask,
        max_speed=max_speed,
        output_dt=output_dt)
    if os.path.exists(opendrift_traj_file):
        logger.info('OpenDrift simulation complete.')
        add_traj_label_to_datafile(opendrift_traj_file,
                                   labels,
                                   drift_model_name='OpenDrift')
        status=0
    else:
        status="OpenDrift did not run."
    return status


def run_opendrift_simulation(
        output_file,
        logfile,
        reader_data,
        reader_land,
        start_position,
        start_date,
        drift_duration,
        opendrift_dt,
        current_uncertainty,
        alpha_wind,
        model_land_mask,
        max_speed=5,
        output_dt=3600
):
    """Run a single OpenDrift simulation.

    Parameters
    ----------
    output_file : str
        Name of the file in which to store OpenDrift output.
    logfile : str
        Name of the OpenDrift logfile.
    reader_data : list
        List of OpenDrift readers contatining ocean and model data
    reader_land : opendrift.reader
        The landmask reader to be used in OpenDrift.
        None if reader_NEMO_native is used or model_land_mask=False.
    start_position : np.array
        Array of starting positions [[lons], [lats], [depths]].
    start_date : datetime.datetime
        The start time of the simultion.
    drift_duration : datetime.timedelta
        The duration of the simulation.
    opendrift_dt : float
        The OpenDrift timestep in seconds.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*opendrift_dt
    alpha_wind : float
        Percentage of wind velocity to be applied to drifter.
    model_land_mask : boolean
        A boolean that indicates to use the model's land mask as opposed
        to OpenDrift's default land ask from GSHHS coastline data. If True,
        a land mask is calculated based on where the ocean model inputs are
        masked. If False, the GSHHS coastline is used to define land which 
        is OpenDrift's default setting.
    max_speed : float
        The maximum speed particles are expected to travel (in m/s).
    output_dt : float
        Output frequency of simluation in seconds.
    """
    logger.info('Running OpenDrift.')
    o = oceandrift.OceanDrift(loglevel=20, logfile=logfile, seed=None)
    if reader_land is not None:
        if model_land_mask:
            # Set a high max speed for land_mask reader because
            # it is a static variable and OpenDrift does not update
            # its buffer as particles move.
            # s - static reader time step (hard coded in OpenDrift)
            default_reader_dt = 3600
            mask_maxspeed = max_speed*drift_duration.total_seconds()/default_reader_dt
            o.max_speed = mask_maxspeed
            o.add_reader(reader_land)     
    o.max_speed = max_speed # back to reasonable max speed
    o.add_reader(reader_data)
    o.set_config('drift:advection_scheme','runge-kutta4')
    o.set_config('general:coastline_action','previous')
    o.set_config('seed:ocean_only', False)
    o.set_config('drift:current_uncertainty', current_uncertainty)
    if model_land_mask:
        o.set_config('general:use_auto_landmask', False)
    o.seed_elements(lon=start_position[0],
                    lat=start_position[1],
                    z=start_position[2],
                    time=start_date,
                    wind_drift_factor=alpha_wind/100.)
    try:
        o.run(
            time_step=opendrift_dt,
            time_step_output=output_dt,
            steps=int(drift_duration.total_seconds()/opendrift_dt),
            outfile=output_file)
    except ValueError:
        logger.warn(
            ("Initial positions {} rejected by OpenDrift.  "
             "OpenDrift simulation failed".format(start_position)))

       
def main():
    from . import cli
    cli.run(run_opendrift)


if __name__ == '__main__':
    main()
