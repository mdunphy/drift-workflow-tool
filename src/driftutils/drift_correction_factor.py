"""
Run Drift Correction Factor Workflow
=============================
:Author: Nancy Soontiens
:Created: 2020-11-12

Two correction factors are calculated:

1. ocean correction factor - solves for \gamma in
.. math::
   :nowrap:

   \begin{equation*}
     u_{drifter} = \gamma u_{ocean}
   \end{equation*}

2. wind correction factor - solves for :math:`\alpha` in 
.. math::
   :nowrap:

   \begin{equation*}
     u_{drifter} = u_{ocean} + \alpha u_{atmos}
   \end{equation*}

"""

import datetime
import os

import dateutil.parser

from .assemble_atmos_data import assemble_atmos_data
from .assemble_atmos_metadata import assemble_atmos_metadata
from .assemble_correction_calculations import \
    assemble_correction_calculations
from .assemble_drifter_data import (assemble_drifter_data,
                                    NoDrifterDataForTimePeriod)
from .assemble_drifter_metadata import assemble_drifter_metadata
from .assemble_ocean_data import (assemble_ocean_data,
                                  find_ocean_data_date_period)
from .assemble_ocean_metadata import assemble_ocean_metadata
from .calculate_correction_factor import (calculate_correction,
                                          DrifterNotInDomainError)
from .ioutils import (dump_yaml,
                      load_drifter_dataset,
                      get_run_ocean_data_dates)
from .rotate_fields import rot_rep_2017_p
from .utils import (defaults, load_yaml_config,
                              logger, setup_savedir)


def drift_correction_factor(*,
        experiment_dir,
        drifter_data_dir,
        ocean_data_dir,
        ocean_mesh_file,
        ocean_model_name,
        start_date,
        end_date,
        drifter_depth='L1',
        drifter_id_attr='buoyid',
        drifter_meta_variables=None,
        rotation_data_file='None',
        xwatervel=defaults['xwatervel']['nc_name'],
        ywatervel=defaults['ywatervel']['nc_name'],
        zwatervel=None,
        temperature=None,
        salinity=None,
        density=None,
        lon_var_ocean='nav_lon',
        lat_var_ocean='nav_lat',
        ulon_var_ocean='glamu',
        ulat_var_ocean='gphiu',
        vlon_var_ocean='glamv',
        vlat_var_ocean='gphiv',
        dep_var='gdepw_1d',
        model_time_ocean='time_counter',
        grid_type='cgrid',
        orca_grid=False,
        xwindvel=defaults['xwindvel']['nc_name'],
        ywindvel=defaults['ywindvel']['nc_name'],
        atmos_data_dir='None',
        atmos_model_name='None',
        lon_var_atmos='nav_lon',
        lat_var_atmos='nav_lat',
        model_time_atmos='time_counter',
        overwrite_savedir=False
):
    """Run drift correction factor workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the calculation. This is where the
        output files will be generated.
    drifter_data_dir : str
        Path to directory containing drifter data files.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    ocean_mesh_file : str
        Path to mesh file for ocean model.
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    start_date: datetime.datetime
        Beginning date of time interval over which correction is to be 
        calculated.
    end_date: datetime.datetime
        End date of time interval over which the correction is to be 
        calculated.
    drifter_depth : str
        Specifies the depth for drift correction calculations. If the string
        begins with 'L', it indicates the corresponding depth layer for the
        vertical gridpoints; otherwise, it indicates the depth in meters.
    drifter_id_attr : str
        Name of attribute in drifter data files containing buoy id.
    drifter_meta_variables : str
        Names of variables in drifter data files that contain metadata.
    rotation_data_file : str
        Path to data file containing coefficients for rotating velocity
        components on desired grid to true east/north. This file will be
        created if it does not exists, i.e. if a grid is being run for the
        first time. If set to  'None' no rotation is performed.
    xwatervel : str
        Name of variable containing X water velocity.
    ywatervel : str
        Name of variable containing Y water velocity.
    zwatervel : str
        Name of variable containing Z water velocity.
    temperature : str
        Name of variable containing temperature.
    salinity : str
        Name of variable containing salinity.
    density : str
        Name of variable containing density.
    lon_var_ocean : str
        Name of longitude variable in mesh file.
    lat_var_ocean : str
        Name of latitude variable in mesh file.
    ulon_var_ocean : str
        Name of U longitude variable in mesh file.
    ulat_var_ocean : str
        Name of U latitude variable in mesh file.
    vlon_var_ocean : str
        Name of V longitude variable in mesh file.
    vlat_var_ocean : str
        Name of V latitude variable in mesh file.
    dep_var : str
        Name of depth (1D) variable.
    model_time_ocean : str
        Name of variable containing timestamp of ocean model output.
    grid_type : str
        Indicates the type of ocean model grid. Options are 'cgrid', 'agrid',
        or 'user_grid'.
    orca_grid : boolean
        Indicates an orca grid which requires specials treatment for
        rotation of velocity fields for OpenDrift. True means the grid is an
        orca grid.
    xwindvel : str
        Name of variable containing eastward wind.
    ywindvel : str
        Name of variable containing northward wind.
    atmos_data_dir : str
        Path to directory containing atmos-model data files. Can be 'None' if
        no atmospheric forcing is desired.
    atmos_model_name : str
        Name of atmos model (e.g. HRDPS). Can be 'None' if no atmospheric
        forcing is desired.
    lon_var_atmos : str
        Name of longitude variable in atmos model.
    lat_var_atmos : str
        Name of latitude variable in atmos model.
    model_time_atmos: str
        Name of variable containing timestamp of atmos model output.
    overwrite_savedir : boolean
        boolean value specifying whether to overwrite the user defined
        savedir if it already exists.
    """

    experiment_details = {
        'ocean_model': ocean_model_name,
        'atmos_model': atmos_model_name,
        'drifter_depth': drifter_depth,
        'experiment_start_date':  start_date.isoformat(),
        'experiment_end_date':  end_date.isoformat(),
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }
    experiment_details_file = os.path.abspath(
        os.path.join(experiment_dir, 'experiment_details.yaml'))
    drifter_metadata_file = os.path.abspath(
        os.path.join(experiment_dir, 'drifters.yaml'))
    ocean_metadata_file = os.path.abspath(
        os.path.join(experiment_dir, 'ocean_data.yaml'))

    logger.info('Creating workflow directory...')

    # Set up the output directory. If the user has not chosen to overwrite
    # the savedir and it exists, provide an error message and exit the
    # program.  Otherwise create a new folder or remove files from an
    # already exisiting folder.
    setup_savedir(experiment_dir, overwrite_savedir=overwrite_savedir)

    assemble_drifter_metadata(
        drifter_data_dir, drifter_metadata_file,
        buoy_id_attr_name=drifter_id_attr,
        meta_variables=drifter_meta_variables)
    assemble_ocean_metadata(
        ocean_data_dir, ocean_mesh_file, ocean_metadata_file,
        xwatervel=xwatervel, ywatervel=ywatervel, zwatervel=zwatervel,
        temperature=temperature, salinity=salinity, density=density,
        longitude=lon_var_ocean, latitude=lat_var_ocean, depth=dep_var,
        time_var=model_time_ocean)

    # Check that atmos settings make sense
    if (atmos_data_dir == 'None') and (atmos_model_name != 'None'):
        logger.warn(
            ("atmos_model_name=%s but no atmospheric data provided: "
             "atmos_data_dir=%s \n. Setting atmos_model_name='None'"),
             atmos_model_name, atmos_data_dir)
        atmos_model_name='None'

    calculate_alpha = True
    if atmos_data_dir == 'None':
        calculate_alpha = False
    if calculate_alpha:
        atmos_metadata_file = os.path.abspath(
            os.path.join(experiment_dir, 'atmos_data.yaml'))
        assemble_atmos_metadata(atmos_data_dir, atmos_metadata_file,
                                xwindvel=xwindvel, ywindvel=ywindvel,
                                longitude=lon_var_atmos,
                                latitude=lat_var_atmos,
                                time_var=model_time_atmos)
    # Applying rotation and grid settings
    if rotation_data_file == 'None':
        rotation_data_file = None
    if ocean_mesh_file == 'None':
        has_mesh = False
    else:
        has_mesh = True
    # Check that rotation and grid settings make sense
    if (rotation_data_file is not None) and (not has_mesh):
        raise ValueError(
            ("Velocity rotation requires an ocean_mesh_file. "
             "Review choices for ocean_mesh_file and rotation_data_file.")
        )

    # Identify drifters in time period of interest (start_date to end_date)
    # Drifters that have a least some data between start_date and end_date
    # will be selected but drifters do not necessarily need to span all of
    # start_date to end_date.
    cwd = os.getcwd()
    os.chdir(experiment_dir)
    data_assembly_dir = 'drifters'
    duration = end_date - start_date
    try:
        drifter_data = assemble_drifter_data(
            drifter_metadata_file, start_date, duration,
            data_assembly_dir, mode='correction')
    except NoDrifterDataForTimePeriod as e:
        with open('NOTE', 'w') as f:
             f.write('Correction calculation not performed: {}'.format(e))
        experiment_details['num_drifters'] = 0
        experiment_details['workflow_end_time'] =\
            datetime.datetime.utcnow().isoformat()
        experiment_details['updated'] = datetime.datetime.utcnow().isoformat()
        dump_yaml(experiment_details, experiment_details_file)
        raise RuntimeError(e)

    # Loop through every drifter file and assemble data
    drifter_files = list(drifter_data['drifter_data_files'].keys())
    drifter_files.sort()
    for drifter in drifter_files:
        drifter_file =\
            os.path.realpath(drifter_data['drifter_data_files'][drifter])
        logger.info('\nBeginning data assembly for drifter: {}'.format(drifter))
        # Create a subdirectory for this drifter.
        drifter_subdir = '{}/calc_{}'.format(data_assembly_dir, drifter)
        os.makedirs(drifter_subdir)
        drifter_subdir = os.path.abspath(drifter_subdir)
        cwd = os.getcwd()
        os.chdir(drifter_subdir)
        # Load drifter data and identify time range needed for calculations
        drifter_dataset = load_drifter_dataset(drifter_file)
        start_drift_date = dateutil.parser.parse(
            str(drifter_dataset['time'].values[0]))
        end_drift_date = dateutil.parser.parse(
            str(drifter_dataset['time'].values[-1]))
        # Round start_drift_date/end_drift_date down/up to nearest hour
        start_drift_date_r = datetime.datetime(start_drift_date.year,
                                               start_drift_date.month,
                                               start_drift_date.day,
                                               start_drift_date.hour)
        end_drift_date_r = end_drift_date + datetime.timedelta(hours=1)
        end_drift_date_r = datetime.datetime(end_drift_date_r.year,
                                            end_drift_date_r.month,
                                            end_drift_date_r.day,
                                            end_drift_date_r.hour)
        start_calc_date = max(start_drift_date_r, start_date)
        # Determine upper and lower time bounds for correction calculation
        end_calc_date = min(end_drift_date_r, end_date)
        duration = end_calc_date - start_calc_date
        # Generate drifter details
        drifter_details = {'drifter_file': drifter_file,
                           'start_calc_date': start_calc_date.isoformat(),
                           'end_calc_date': end_calc_date.isoformat(),
                           'start_drift_date': start_drift_date.isoformat(),
                           'end_drift_date': end_drift_date.isoformat(),
                           'ocean_model_name': ocean_model_name,
                           'atmos_model_name': atmos_model_name
                           }
        drifter_details_file = 'drifter_details.yaml'
        # Assemble drifter data
        drifter_data_dir = 'data'
        os.makedirs(drifter_data_dir)
        os.symlink(drifter_file,
                   os.path.join(drifter_data_dir,
                                os.path.basename(drifter_file)))
        # Assemble ocean data for this drifter
        ocean_data = assemble_ocean_data(
            ocean_metadata_file, start_calc_date,
            duration, drifter_data_dir,
            model_time=model_time_ocean)
        drifter_details['ocean_data_files'] = ocean_data['ocean_data_files']
        drifter_details['ocean_mesh_file'] = ocean_data['ocean_mesh_file']
        if calculate_alpha:
            # assemble atmos data for this drifter
            atmos_data = assemble_atmos_data(
                atmos_metadata_file, start_calc_date,
                duration, drifter_data_dir,
                xwindvel=xwindvel, ywindvel=ywindvel,
                model_time=model_time_atmos)
            drifter_details['atmos_data_files'] =\
                atmos_data['atmos_data_files']
        drifter_details['updated'] = datetime.datetime.now().isoformat()
        dump_yaml(drifter_details, drifter_details_file)
        os.chdir(cwd)
    # Now that data is assembled, loop through for calculations
    for drifter in drifter_files:
        logger.info('\nBeginning calculations for drifter: {}'.format(drifter))
        drifter_subdir = os.path.abspath('{}/calc_{}'.format(
            data_assembly_dir, drifter))
        drifter_details_file = os.path.join(drifter_subdir,
                                            'drifter_details.yaml')
        drifter_details = load_yaml_config(drifter_details_file)
        drifter_details['calculation_start_time'] =\
            datetime.datetime.now().isoformat()
        # Perform correction calculation
        try:
            calculate_correction(
                drifter_details_file=drifter_details_file,
                drifter_subdir=drifter_subdir,
                drifter_depth=drifter_depth,
                xwatervel=xwatervel,
                ywatervel=ywatervel,
                lon_var_ocean=lon_var_ocean,
                lat_var_ocean=lat_var_ocean,
                ulon_var_ocean=ulon_var_ocean,
                ulat_var_ocean=ulat_var_ocean,
                vlon_var_ocean=vlon_var_ocean,
                vlat_var_ocean=vlat_var_ocean,
                grid_type=grid_type,
                orca_grid=orca_grid,
                rotation_data_file=rotation_data_file,
                xwindvel=xwindvel,
                ywindvel=ywindvel,
                model_time_ocean=model_time_ocean,
                lon_var_atmos=lon_var_atmos,
                lat_var_atmos=lat_var_atmos,
                model_time_atmos=model_time_atmos)
            drifter_details['calc_status'] = 0
        except DrifterNotInDomainError as e:
            logger.warn(e)
            drifter_details['calc_status'] = e.message
            drifter_details['updated'] = datetime.datetime.now().isoformat()
            dump_yaml(drifter_details, drifter_details_file)
            continue
        drifter_details['calculation_end_time'] =\
            datetime.datetime.now().isoformat()
        drifter_details['updated'] = datetime.datetime.now().isoformat()
        dump_yaml(drifter_details, drifter_details_file)
    experiment_details['workflow_end_time'] =\
        datetime.datetime.now().isoformat()
    # Clean up output
    assemble_correction_calculations(experiment_dir=experiment_dir,
                                     drifters_dir=data_assembly_dir)
    experiment_details['updated'] = datetime.datetime.now().isoformat()
    dump_yaml(experiment_details, experiment_details_file)

    logger.info('\nDrift correction factor calculations complete! \n\nThe output '
                + 'can be found in: ' + str(experiment_dir))
    
def main():
    from . import cli
    cli.run(drift_correction_factor)


if __name__=='__main__':
    main()
