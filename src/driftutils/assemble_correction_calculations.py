"""
Assemble Correction Calculations
================================
:Author: Nancy Soontiens
:Created: 2020-11-25

This module assembles the correction factor calculation outputs
for an experiment into a single directory. 
"""

import os
import glob
import shutil

from .utils import logger


def assemble_correction_calculations(*, experiment_dir, drifters_dir):
    """Clean up outpot from correction calualtions and assemble the 
    calculations into a single output directory. All outputs in
    experiment_dir/drifters_dir will be copied into experiment_dir/output. 

    Parameters
    ----------
    experiment_dir : str
        Path to directory containing experiment results
    drifters_dir : str
        Subdirectory within experiment_dir where per drifter correction results
        are stored
    """
    logger.info('\nAssembling drift corrections')
    # Clean up unnecessary drifter file links
    cwd = os.getcwd()
    os.chdir(os.path.join(experiment_dir, drifters_dir))
    for f in glob.glob('*.nc'):
        os.unlink(f)
    os.chdir(cwd)
    # Copy all of the output files to common directory
    output_dir = os.path.join(experiment_dir, 'output')
    os.makedirs(output_dir, exist_ok=True)
    for f in glob.glob(os.path.join(experiment_dir,
                                    drifters_dir,
                                    '*/*.nc')):
        base = os.path.basename(f)
        shutil.copyfile(f, os.path.join(output_dir, base))


def main():    
    from . import cli
    cli.run(assemble_correction_calculations)


if __name__ == '__main__':
    main()
