###############################################################################
# Verify mesh mask
#
# This script does some QA on the input mesh mask file to ensure it was
# not created with Land Processor Elimination activated. Two checks are used:
# 1. All values in the scale factor arrays e1t, e2t are > 0
# 2. All values in the mbathy array are > 0
#
# Author: Nancy Soontiens
# Created: 2022-06-30
###############################################################################

import sys

import xarray as xr

from driftutils import configargparse


def verify_mesh_mask(
        ocean_mesh_file): 
    """Verifies ocean data

    Parameters
    ----------
    ocean_mesh_file : str
        Path to mesh file for ocean model.
    Raises:
        ValueError: ocean_mesh_file contains invalid data
    """
    mesh = xr.open_dataset(ocean_mesh_file)

    e2t = mesh.e2t.values
    e1t = mesh.e1t.values
    mbathy = mesh.mbathy.values

    if (e2t <= 0).any() or \
       (e1t <= 0).any() or \
       (mbathy < 0).any() :
        msg = f'{ocean_mesh_file} contains invalid entries for'\
              ' scale factors and mbathy. It is suspected that the'\
              ' mesh was generated with Land Processor Elimination '\
              ' activated. Please check the mesh file.'
        raise ValueError(msg)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')

    arg_parser.add('--ocean_mesh_file', type=str,
                   help='Path to mesh file for ocean model')

    config = arg_parser.parse(args)

    verify_mesh_mask(config.ocean_mesh_file)


if __name__ == '__main__':
    main()
