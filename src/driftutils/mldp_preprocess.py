
"""
Run MLDP preprocess
=============================
:Author: Samuel T. Babalola, Nancy Soontiens
:Created: 2018-09-19

This module creates and runs the preprocess bash script.
"""

import os
from os.path import abspath, isabs, join as joinpath
import subprocess

from driftutils import utils

logger = utils.logger


def mldp_preprocess(
        *,
        wetfiles,
        metfiles,
        preprocess_dir,
        bbox,
        depth=0,
        res=1000,
        resmask=100,
):
    """Run MLDP preprocessing.

    Parameters
    ----------
    wetfiles : str
        Path to file containing list of ocean files to pre-process.
    metfiles : str
        Path to file containing list of atmos files to pre-process.
    preprocess_dir : str
        Path to store output from pre-preprocessing.
    bbox : str
        Specify coordinates for pre-process.
        min_lon min_lat max_lon max_lat.
    depth : float
        depth of ocean currents in m.
    res : float
        resolution of output grid
    resmask : float
        Resolution of mask. Should equal zero if no mask.in is needed.
    """
    cwd = os.getcwd()
    os.chdir(preprocess_dir)
    script_name = 'preprocess_script.sh'
    # Write the executable preprocess script
    call_script = joinpath(preprocess_dir, script_name)
    logger.info('Writing run script to {} '.format(call_script))

    # Reformat bbox in mldpn order - min_lat min_lon max_lat max_lon
    preproc_bbox = bbox.split()
    preproc_bbox = "{} {} {} {}".format(preproc_bbox[1], preproc_bbox[0],
                                        preproc_bbox[3], preproc_bbox[2])

    with open(os.open(call_script, os.O_CREAT | os.O_WRONLY, 0o777), 'w') as f:
        f.write('''\
#!/bin/bash
# Script to call EEWetDB for processing rpn data for us in MLDP
# run with environment from ~/.profile

# Constants
script=EERWetDB.tcl
out=%s # output dir
res=%s # resolution of output grid
resmask=%s # resolution of mask
depth=%s # depth in NEMO in m
bbox=\'%s\' #bounding box
metfiles=%s   #list of meteo files
wetfiles=%s   # list of riops files

# Run script

$script \\
-metfiles $metfiles \\
-wetfiles $wetfiles \\
-depth $depth \\
-res $res \\
-resmask $resmask \\
-bbox $bbox \\
-out $out \\
-waves false \\
-interp
'''%( preprocess_dir, res, resmask, depth,
    preproc_bbox, metfiles, wetfiles))

    logger.info('\nRunning preprocess...')
    preprocess_out = joinpath(preprocess_dir, 'preprocess.out')
    logger.info('preprocess run information will be written to {}'.format(preprocess_out))
    with open(preprocess_out, 'w') as out:
        process = subprocess.run([call_script],
                                 cwd=preprocess_dir,
                                 stdout=out,
                                 stderr=subprocess.STDOUT)
    return_status = process.returncode
    if return_status != 0:
        logger.warn('preprocess  with an error status of %s',
                return_status)
    logger.info('Finished running preprocess.')
    os.chdir(cwd)
        

def main():
    from . import cli
    cli.run(mldp_preprocess)

    
if __name__ == '__main__':
    main()
