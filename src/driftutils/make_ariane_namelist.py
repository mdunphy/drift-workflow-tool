"""
Construct Ariane namelist
=========================
:Author: Clyde Clements
:Created: 2017-08-30

This module constructs a namelist file for a single run of Ariane. The namelist
file is the basis of how configuration parameters are passed to Ariane.
"""

import datetime
import os.path
import sys
import yaml

import f90nml
import numpy as np

from . import configargparse
from .ioutils import get_run_ocean_data_dates, get_ocean_data_variables, load_mesh
from . import utils


logger = utils.logger


def make_ariane_namelist(
        ariane_config_file, ariane_run_dir, ocean_data_dir,
        run_ocean_data_dates,
        ocean_data_variables, ocean_mesh_file, num_drifters, drift_duration,
        namelist_file, output_dt=3600):
    """Construct Ariane namelist file.

    Parameters
    ----------
    ariane_config_file : str
        Name of Ariane configuration file containing parameters such as
        frequency of requested output and many others.
    ariane_run_dir : str
        Name of directory where Ariane will be run.
    ocean_data_dir : str
        Name of directory containing ocean data for this run.
    run_ocean_data_dates : list or array of datetime.datetime objects
        Dates for which ocean data is available. Complete list.
    ocean_data_variables : list of str objects
        List containing names of available ocean data variables
    ocean_mesh_file : str
        Path to ocean mesh file.
    num_drifters : int
        Number of drifters in this run.
    drift_duration : datetime.timedelta
        Duration of drift trajectory calculation.
    namelist_file : str
        Path to namelist file to create.
    output_dt : float
        Output frequency of simluation in seconds.
    """
    logger.info('Constructing Ariane namelist...')

    with open(ariane_config_file, 'r') as f:
        ariane_user_config = yaml.load(f, Loader=yaml.FullLoader)

    seconds_per_hour = 3600.
    hours_per_day = 24.
    seconds_per_day = seconds_per_hour * hours_per_day

    # Set output frequency
    delta_t = output_dt
    freq = 1

    seconds_between_outputs = delta_t * freq
    num_outputs = int(drift_duration.total_seconds() / seconds_between_outputs)

    mesh_ds = load_mesh(ocean_mesh_file)
    imt = mesh_ds.dims['x']
    jmt = mesh_ds.dims['y']
    kmt = mesh_ds.dims['deptht']
    lmt = len(run_ocean_data_dates)

    tunit = determine_ocean_model_output_frequency(run_ocean_data_dates)
    # By default, set tcyc = 86400 so that traj_time in ariane output files
    # is reocrded in days. tcyc can be overwritten in ariane_user_config
    # ariane's default is to record traj_time as a fraction of the simulation
    # length so that traj_time takes values 0 to 1 but setting to days
    # provides more consistency between different simulations.
    # Note: traj_time and tcyc aren't used anywhere else. Date time stamps
    # for ariane output is handled in add_traj_data_dates by using delta_t,
    # freq and the run start date
    tcyc = 86400. 

    # Determine relative paths from Ariane run directory to data directory and
    # mesh file.
    rel_ocean_data_dir = os.path.relpath(ocean_data_dir, ariane_run_dir)
    rel_ocean_mesh_file = os.path.relpath(ocean_mesh_file, ariane_run_dir)
    # Determine ind0, inde for each velocity variable
    ind0_zo, indn_zo = namelist_start_end_indices(ocean_data_dir, 'xwatervel')
    ind0_me, indn_me = namelist_start_end_indices(ocean_data_dir, 'ywatervel')
    ariane_default_config = {
        'ariane': {
            'bin': 'nobin',
            'mode': 'qualitative',
            'nmax': num_drifters,
            'ntfic': 1,
            'tunit': tunit.astype('float'),
            'tcyc': tcyc
        },
        'sequential': {
            'maxcycles': 1
        },
        'qualitative': {
            'delta_t': delta_t,
            'frequency': freq,
            'nb_output': num_outputs
        },
        'opaparam': {
            'imt': imt,
            'jmt': jmt,
            'kmt': kmt,
            'lmt': lmt
        },
        'zonalcrt': {
            'c_dir_zo': rel_ocean_data_dir,
            'c_prefix_zo': 'xwatervel_',
            'c_suffix_zo': '.nc',
            'ind0_zo': ind0_zo,
            'indn_zo': indn_zo,
            'maxsize_zo': 5
        },
        'meridcrt': {
            'c_dir_me': rel_ocean_data_dir,
            'c_prefix_me': 'ywatervel_',
            'c_suffix_me': '.nc',
            'ind0_me': ind0_me,
            'indn_me': indn_me,
            'maxsize_me': 5
        },
        'mesh': {
            'dir_mesh': os.path.dirname(rel_ocean_mesh_file),
            'fn_mesh': os.path.basename(rel_ocean_mesh_file)
        }
    }
    for var in ocean_data_variables:
        if var in ['xwatervel', 'ywatervel']:
            # These variables must always be present and handled in the default
            # config above.
            continue
        if var == 'temperature':
            ind0_te, indn_te = \
                namelist_start_end_indices(ocean_data_dir, 'temperat')
            ariane_default_config['temperat'] = {
                'c_dir_te': rel_ocean_data_dir,
                'c_prefix_te': 'temperature_',
                'c_suffix_te': '.nc',
                'ind0_te': ind0_te,
                'indn_te': indn_te,
                'maxsize_te': 5
            }
        elif var == 'salinity':
            ind0_sa, indn_sa = \
                namelist_start_end_indices(ocean_data_dir, 'salinity')
            ariane_default_config['salinity'] = {
                'c_dir_sa': rel_ocean_data_dir,
                'c_prefix_sa': 'salinity_',
                'c_suffix_sa': '.nc',
                'ind0_sa': ind0_sa,
                'indn_sa': indn_sa,
                'maxsize_sa': 5
            }
        else:
            msg = ('Unknown variable "{}": do not know Ariane namelist entry '
                   'to create. Ignoring this variable; please update the code '
                   'if you require different behaviour.').format(var)
            logger.warn(msg)

    # We need to merge the dictionary ariane_user_config with
    # ariane_default_config. This is somewhat tricky to do since they are
    # actually dictionaries of dictionaries. The f90nml module has a built-in
    # patch capability and so my first thought was to make use of that.
    # Unfortunately, the patching feature only works with files (not with
    # Namelist objects), and therefore requires the use of temporary files. The
    # biggest problem, though, is that f90nml has a limitation whereby only
    # existing sections in the current namelist are updated from the patch
    # namelist; in other words, sections that exist only in the patch namelist
    # are completely ignored. Thus here is a simple implementation in Python to
    # merge the dictionaries.
    ariane_config = ariane_default_config.copy()
    for k in ariane_user_config:
        if k in ariane_config:
            ariane_config[k].update(ariane_user_config[k])
        else:
            ariane_config[k] = ariane_user_config[k]

    logger.info('Writing Ariane namelist to file %s...', namelist_file)
    f90nml.write(ariane_config, namelist_file)


def determine_ocean_model_output_frequency(run_ocean_data_dates):
    """Determing the ocean model output frequency which is used to set
    tunit in ariane namelist

    Parameters
    ----------

    run_ocean_data_dates : list or array of datetime.datetime objects
        Dates for which ocean data is available. Complete list.

    returns: tunit, the time spacing between ocean model outputs in seconds
    """
    time_diffs = np.diff(run_ocean_data_dates)
    tunit = time_diffs[0].astype('timedelta64[s]')
    return tunit


def namelist_start_end_indices(ocean_data_dir, prefix):
    """Search indices of first and last file in ocean_data_dir with prefix.
    e.g. Files like this: prefix_0001.nc, prefix_0002.nc, ... prefix_0100.nc
    would return ind0=1, indn=100.

    Parameters
    ----------
    ocean_data_dir : str
        Name of directory containing ocean data for this run.
    prefix: str
        file name prefix

    returns: ind0, indn both integers
    """
    files = []
    # List all files with prefix in ocean_data_dir
    for dirpath, dirnames, filenames in os.walk(ocean_data_dir):
        for filename in filenames:
            if not filename.startswith(prefix):
                continue
            files.append(filename)
    files.sort()
    f = files[0]
    ind0 = f[(f.index(prefix)+len(prefix)):f.index('.nc')].replace('_','')
    f=files[-1]
    indn = f[(f.index(prefix)+len(prefix)):f.index('.nc')].replace('_','')
    return int(ind0), int(indn)


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--ariane_config_file', type=str, required=True,
                   help='Name of YAML file containing Ariane configuration')
    arg_parser.add('--ariane_run_dir', type=str, required=True,
                   help='Path to run directory for Ariane')
    arg_parser.add('--num_drifters', type=int, required=True,
                   help='Number of drifters')
    arg_parser.add('--num_drift_hours', type=int, required=True,
                   help='Number of hours for drift calculation')
    arg_parser.add('--ocean_data_dir', type=str, required=True,
                   help=('Path to directory containing data files for ocean '
                         'currents'))
    arg_parser.add('--ocean_mesh_file', type=str, required=True,
                   help='Name of file containing ocean mesh')
    arg_parser.add('--namelist_file', type=str, required=True,
                   help='Name of Ariane namelist file to create')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    run_ocean_data_dates = run_get_ocean_data_dates(config.ocean_data_dir)
    ocean_data_variables = get_ocean_data_variables(config.ocean_data_dir)
    drift_duration = datetime.timedelta(hours=config.num_drift_hours)
    make_ariane_namelist(
        config.ariane_config_file, config.ariane_run_dir,
        config.ocean_data_dir, run_ocean_data_dates, ocean_data_variables,
        config.ocean_mesh_file, config.num_drifters, drift_duration,
        config.namelist_file)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
