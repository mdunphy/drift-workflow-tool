# Load the compiler
. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199

# OpenMPI stuff
. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.2

# Load RPN stuff
. ssmuse-sh -x eccc/mrd/rpn/vgrid/20220216/
. ssmuse-sh -x eccc/mrd/rpn/libs/20220216/
. ssmuse-sh -d eccc/mrd/rpn/utils/20220509/


# Actual stuff we want
. ssmuse-sh -d /fs/ssm/dfo/dpnm/exp/MLDP_20220902

# Env variables
export GEO_PATH=/home/sdfo000/sitestore7/hnas/geo
