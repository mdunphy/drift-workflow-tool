"""
Plot drift correction factor workflow
=============================
:Author: Jennifer Holden
:Created: 2022-05-20

This script creates a series of plots that aid in the analysis and
visualization of drift correction factor output.
"""

import os
import sys
import glob
import xarray as xr
import pandas as pd

from driftutils import cli
from driftutils import utils
from driftutils import ioutils

from plotutils.drift_correction_factor import dcf_timeseries
from plotutils.drift_correction_factor import dcf_histogram
from plotutils.drift_correction_factor import dcf_drifter_map
from plotutils.drift_correction_factor import dcf_error_hexes
from plotutils.drift_correction_factor import dcf_velocities_histograms
from plotutils.drift_correction_factor import dcf_create_stats_table
from plotutils.drift_correction_factor import dcf_rotary_spectra

utils.initialize_logging(level=utils.log_level['info'])
logger = utils.logger


def plot_correction_factors(
    *,
    data_dir,
    plot_dir=None,
    overwrite_savedir=False,
    plot_timeseries=True,
    plot_histogram=True,
    plot_map=True,
    plot_error_hexes=True,
    plot_velocities_histograms=True,
    plot_rotary_spectra=True,
    create_velcity_error_stats=True,
    style="default"
):
    """ Plotting workflow for drift correction factor output

    Parameters
    ----------
    data_dir : str
        full path to directory containing the drift correction factor
        output to be plotted.
    plot_dir : str
        full path to the directory where the plots will be generated
    overwrite_savedir : boolean
        If False, the script will exit rather than allowing save_dir to
        be overwritten.
    plot_timeseries : boolean
        If True, a timeseries plot will be created for each drifter. The
        plot will show drifter track, as well as east and north velocities,
        and speeds for ocean, drifter and atmos (if present).
    plot_histogram : boolean
        a value of True means that the histogram will be created
    plot_map : boolean
        a value of True means that a map of all included drifter tracks
        will be created
    plot_error_hexes : boolean
        a value of True means that hexbin map plots showing error in
        ocean model speed and counts per bin for the comparable data
        will be created.
    plot_velocities_histograms : boolean
        a value of True means that 2d histograms of eastward and northward
        velocities will be created.
    plot_rotary_spectra : boolean
        a value of True calculates and plots a rotary spectral estimate.
    create_velcity_error_stats : boolean
        If true, creates files in plot_dir named velocity_error_stats.csv and
        and velocity_error_stats.nc. These files contain statistics compiled
        for drift correction factor output.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom .mplstyle style sheet created by the user.
    """
    ####################################################################
    # check to be sure that the data given by the user is sufficient
    # to create the plots
    ####################################################################

    # check to be sure that the data_dir exists and contains .nc files
    if not os.path.isdir(data_dir):
        sys.exit('data_dir does not exist: ' + str(data_dir))

    files = glob.glob(os.path.join(data_dir, '*.nc'))
    if not len(files):
        sys.exit('there are no .nc files to plot in ' + (data_dir))

    # If plot_dir wasn't given, write the plots to the parent directory
    # of data_dir. If it does not already exist create it. Otherwise
    # clear it out.
    if not plot_dir:
        dname = os.path.dirname(os.path.normpath(data_dir))
        plot_dir = os.path.join(dname, 'drift_correction_factor_plots')
    utils.setup_savedir(plot_dir, overwrite_savedir=overwrite_savedir)
    logger.info('\nplots will be saved in ' + str(plot_dir))

    ####################################################################
    # create the plots
    ####################################################################

    # Cycle though the data files to plot. At the same time, add the
    # opened datasets to a list so that they can potentially be reused
    # later for summary plots without having to re-open the files.
    files.sort()
    dslist = []
    for filename in files:

        ds = ioutils.load_dataset(filename)
        dslist.append(ds)

        if plot_timeseries or plot_histogram or plot_rotary_spectra:
            logger.info('\ncreating plots for ' + str(ds.obs_buoyid))

        if plot_timeseries:
            dcf_timeseries.plot_timeseries(ds=ds,
                                           plot_dir=plot_dir,
                                           style=style)

        if plot_histogram:
            dcf_histogram.plot_histogram(ds=ds,
                                         plot_dir=plot_dir,
                                         style=style)

        if plot_rotary_spectra:
            dcf_rotary_spectra.plot_rotary_spectral_estimate(ds=ds,
                                                             plot_dir=plot_dir,
                                                             style=style)

    # Concatenate all the datasets in the list into one dataset
    ds_all = xr.concat(dslist, pd.Index([ds.obs_buoyid for ds in dslist],
                                        name='buoyid'))

    # create drifter track plot with all the drifters from
    # the entire set contained in data_dir.
    if plot_map:
        dcf_drifter_map.plot_drifter_tracks(ds=ds_all,
                                            plot_dir=plot_dir,
                                            style=style)

    # create hexbin plots of error in ocean model speed
    if plot_error_hexes:
        dcf_error_hexes.dcf_error_hexes_workflow(ds=ds_all,
                                                 plot_dir=plot_dir,
                                                 style=style)

    # Plot showing 2d histograms of eastward and northward velocities
    if plot_velocities_histograms:
        dcf_velocities_histograms.plot_velocities_histograms(ds=ds_all,
                                                             plot_dir=plot_dir,
                                                             style=style)

    # compile statistics for the dcf output and write to csv and nc files.
    if create_velcity_error_stats:
        dcf_create_stats_table.create_dcf_stats_tables(
            ds=ds_all,
            dir=plot_dir
        )


def main():

    cli.run(plot_correction_factors)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
