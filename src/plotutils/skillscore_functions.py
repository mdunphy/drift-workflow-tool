"""
skill score plotting functions
================
:Author: Jennifer Holden
:Contributors: Nancy Soontiens
:Created: 2020-07-02
"""

import matplotlib
import os 
import sys
import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import matplotlib.colors as colors
import math
import collections

from driftutils import utils
from plotutils import plotting_utils as putils

logger = utils.logger

############################################################
# Plot skill scores by drifter
############################################################
def plot_skills(
    drifterid, 
    ds, 
    setname, 
    skill, 
    plotcolors, 
    skill_label_params, 
    lwidth=1, 
    lstyle='-',
    opacity=1, 
    labs=True, 
    leg=False, 
    add_markers=True, 
    ax=None):
    """ Plot skill scores by drifter 

    Parameters
    ----------
    drifterid : str
        unique id representing the buoy to be plotted
    ds : xarray.Dataset
        xarray dataset containing the data to be plotted
    setname : str
        label representing the experiment to be plotted
    skill : str
        string value for type of skill score that will be plotted
    plotcolors : dict
        dictionary with setnames as keys and unique colors as values
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : int
        value for the line width
    lstyle : str
        string representing the linestyle to plot
    opacity : float
        alpha value allowing users to change the line opacity
    labs : boolean
        boolean value determining if plot labels will be included
    leg : boolean
        boolean value determining if a legend will be included
    add_markers : boolean
        boolean value determining if plot markers will be added
    ax : Axes
        Axis to use for plotting

    Returns
    -------
    a plot object for an individual drifter and an individual skill. 
    Currently works for liu, molcard, sep, sutherland, obs_dist, 
    obs_disp, mod_dist, and mod_disp

    """
    #In the future, can leverage ds attributes for labels: 
    # skill:long_name, skill:units (ex, liu:units)

    if skill == 'logobsratio':
        plotlog=True
        skill = 'obsratio'
    elif skill == 'logmodratio':
        plotlog=True
        skill = 'modratio'
    else:
        plotlog=False

    #call the binning function
    df_res, all_tracks = putils.bin_skills_individual_drifter(ds, skill)
    df_res = df_res[df_res['hours since start'] > 0]

    #if using sep, dists or disps, convert the units to km
    if any(
        skill in x for x in [
            'sep','obs_disp','obs_dist','mod_disp','mod_dist'
            ]
        ):
        df_res[skill]=df_res[skill]/1000.

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    if isinstance(plotcolors,dict):
        if setname in plotcolors.keys():
            plotcolor = plotcolors[setname]
        else:
            plotcolor = 'b'
    else:
        plotcolor = plotcolors

    #add the plot
    if plotlog:
        ylab = 'log(' + ylab + ')'
        ax.semilogy(
            df_res['hours since start'], 
            df_res[skill], 
            color=plotcolor, 
            linewidth=lwidth,
            linestyle=lstyle, 
            alpha=opacity, 
            label=yleg
        )
        
        # Disables the log-formatting that comes with semilogy
        from matplotlib.ticker import LogFormatter, ScalarFormatter
        ax.yaxis.set_major_formatter(ScalarFormatter())

    else:
        ax.plot(
            df_res['hours since start'], 
            df_res[skill], 
            color=plotcolor, 
            linewidth=lwidth, 
            linestyle=lstyle,
            alpha=opacity, 
            label=yleg
        ) 
        if add_markers:
            ax.plot(
                df_res['hours since start'], 
                df_res[skill], 
                'o', color=plotcolor, 
                markersize=2,  
                alpha=opacity
            )
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)

    if labs:
        ax.set_ylabel(ylab, fontsize=12)
        ax.set_xlabel('hours since start', fontsize=12)

    if leg:
        ax.legend(fontsize=12)

############################################################
# Plot binned skill score for entire analysis set
############################################################
def plot_average_skill(
    df_res_combined, 
    setname, 
    skill, 
    plotcolors, 
    skill_label_params, 
    lwidth=1, 
    leg=False, 
    ax=None
    ):

    """ Plot average skill scores for experiment

    Parameters
    ----------
    df_res_combined : pandas.DataFrame
        Pandas Dataframe containing all the data for all the drifters
        from an experiment concatonated together.
    setname : str
        label representing the experiment to be plotted
    skill : str
        string value for type of skill score that will be plotted
    plotcolors : dict
        dictionary with setnames as keys and unique colors as values
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : int
        value for the line width
    leg : boolean
        boolean value determining if a legend will be included
    ax : Axes
        Axis to use for plotting

    Returns
    -------
    a plot object showing the average of an individual skill for all
    the drifters in an experiment. Currently works for liu, molcard, 
    sep, sutherland, obs_dist, obs_disp, mod_dist, and mod_disp

    """

    if skill == 'logobsratio':
        plotlog = True
        skill = 'obsratio'
    elif skill == 'logmodratio':
        plotlog = True
        skill = 'modratio'
    else:
        plotlog = False


    # if using sep, dists or disps, convert the units to km
    if any(
        skill in x for x in [
            'sep','obs_disp','obs_dist','mod_disp','mod_dist'
            ]
        ):

        df_res_combined[skill] = df_res_combined[skill] / 1000.

    df_res_combined = df_res_combined[df_res_combined['hours since start'] > 0]

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    if isinstance(plotcolors,dict):
        if setname in plotcolors.keys():
            plotcolor = plotcolors[setname]
        else:
            plotcolor = 'b'
    else:
        plotcolor = plotcolors

    # make the plot
    if plotlog:
        ylab = 'log(' + ylab + ')'
        ax.semilogy(
            df_res_combined['hours since start'], 
            df_res_combined[skill], 
            color=plotcolor,
            linewidth=lwidth, 
            label=setname
            )

    else:
        ax.plot(
            df_res_combined['hours since start'], 
            df_res_combined[skill], 
            color=plotcolor,
            linewidth=lwidth, 
            label=setname
            )

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)

    if leg:
        ax.legend(fontsize=12)

############################################################
# Plot the average skill for the entire analysis set with
# the individual skills for each drifter (individual
# scores are un-binned)
############################################################
def plot_skill_with_indv(
    buoyids,
    datasets_dict, 
    skill, 
    plotcolors, 
    skill_label_params, 
    lwidth=1, 
    labs=True, 
    leg=False, 
    ax=None,
    pickledir=None
    ):

    """ Plot average skill scores for an entire experiment as well as 
        the individual means for each drifter.

    Parameters
    ----------
    buoyids : list
        list of strings representing unique drifter ids for the drifters
        to be plotted
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill : str
        name of the skill score to be plotted
    plotcolors : dict
        dictionary with setnames as keys and unique colors as values
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : float
        value for the line width
    lab : boolean
        boolean determining if plot labels will be added
    leg : boolean
        boolean determining if a legend will be added
    ax : Axes
        Axis to use for plotting
    pickledir : str
        path to an optional pickle file containing binned skill score data

    Returns
    -------
    a plot object showing the average skill scores for an entire 
    experiment as well as the individual means for each drifter

    """

    for drifterid in buoyids:
        logger.info('......' + drifterid)

        for setname in datasets_dict.keys():
            subdict = [
                d for d in datasets_dict[setname] 
                    if d.obs_buoyid == drifterid
                ][0]

            plot_skills(
                drifterid, 
                subdict, 
                setname, 
                skill, 
                plotcolors,
                skill_label_params,
                lstyle=':',
                lwidth=1, 
                opacity=0.8, 
                labs=True, 
                leg=False, 
                add_markers=False, 
                ax=ax
                )

    #plot average
    for setname in datasets_dict.keys():
        all_mean, all_drifters_all_tracks =\
            putils.bin_skills_all_drifters(
                datasets_dict[setname],     
                skill, 
                pickledir
                )

        plot_average_skill(
            all_mean, 
            setname, 
            skill, 
            plotcolors, 
            skill_label_params, 
            lwidth=2, 
            leg=True, 
            ax=ax
            )
    # Use only line handles/labels from plot_average_skill
    handles, labels = ax.get_legend_handles_labels()
    numsets = len(datasets_dict.keys())
    handles = handles[-numsets:]
    labels = labels[-numsets:]
    ax.legend(handles, labels, loc='best', fontsize=10)

############################################################
# Plot the average skill plus filled range
############################################################
def plot_skill_with_filled(
    datasets_dict, 
    skill, 
    plotcolors, 
    skill_label_params, 
    lwidth=1, 
    filledrangetype=None, 
    labs=True, 
    leg=False, 
    ax=None,
    bounding_bars_only=None,
    pickledir=None
    ):

    """ Plot average skill scores for an entire experiment as well as
        a filled range.

    Parameters
    ----------

    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill : str
        name of the skill score to be plotted
    plotcolors : dict
        dictionary with setnames as keys and unique colors as values
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : float
        value for the line width
    filledrangetype : str
        string representing the method to use when creating the filled
        range. Currently works for Interquartile Range (IQR), bootstrapped
        95% confidence intervals (boot95),  min and max values for the
        entire experiment at each time (extremes), and the standard 
        devation (1std). If None, default value of IQR is used.
    lab : boolean
        boolean determining if plot labels will be added
    leg : boolean
        boolean determining if a legend will be added
    ax : Axes
        Axis to use for plotting
    pickledir : str
        path to a directory containing a pickled version of the binned
        skill score data

    Returns
    -------
    a plot object showing the average skill scores for an entire
    experiment as well as a filled range.

    """


    for setname in datasets_dict.keys():
        all_mean, all_drifters_all_tracks =\
            putils.bin_skills_all_drifters(
                datasets_dict[setname], 
                skill, 
                pickledir
                )

        plot_filled_skill(
            all_drifters_all_tracks,
            setname,
            skill,
            plotcolors[setname],
            skill_label_params,
            filledrangetype,
            lwidth=1, #0.5,
            leg=True,
            ax=ax,
            bounding_bars_only=bounding_bars_only
            )
        # If plotting bootstrap, we want the average to be from
        # the bootstrap samples which is handled in plot_filled_skill
        if filledrangetype != 'boot95':
            plot_average_skill(
                all_mean,
                setname,
                skill,
                plotcolors[setname],
                skill_label_params,
                lwidth=lwidth,
                leg=leg,
                ax=ax
                )


############################################################
# plot only a filled range calculated from the average skill
############################################################
def plot_filled_skill(
    all_drifters_all_tracks, 
    setname, 
    skill, 
    plotcolor, 
    skill_label_params, 
    filledrangetype, 
    lwidth=0.5, 
    leg=False, 
    ax=None,
    bounding_bars_only=False
    ):

    """ Plot filled range for an entire experiment. Usually called from
        plot_skill_with_filled 

    Parameters
    ----------

    all_drifters_all_tracks : pandas.DataFrame
        A data frame that contains the average score for each track for
        all drifters organized into hourly bins.
    setname : str
        unique identifier for the experiment. For example 'RIOPS'
    skill : str
        name of the skill score to be plotted
    plotcolor : str
        color to use for line plot
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : float
        value for the line width
    filledrangetype : str
        string representing the method to use when creating the filled
        range. Currently works for Interquartile Range (IQR), min and
        max values for the entire experiment at each time (extremes),
        and the standard devation (1std). If None, it is set by default
        to IQR.
    leg : boolean
        boolean determining if a legend will be added
    ax : Axes
        Axis to use for plotting
    bounding_bars_only : bool
        optional boolean argument used to remove the fill from the 
        filled region leaving only the upper and lower bounds. Default
        value of False plots the filled region.

    Returns
    -------
    a plot object.

    """

    if filledrangetype is None:
        filledrangetype = 'IQR'

    if filledrangetype == 'IQR':
        leg_str = 'Interquartile Range'
        df_grouped = all_drifters_all_tracks.groupby('time_since_start')
        df_filledrange = df_grouped.apply(
            lambda x: pd.Series(
                {
                    '25th percentile': x[skill].quantile(0.25),
                    '75th percentile': x[skill].quantile(0.75)
                }
            )
        )

        df_filledrange = df_filledrange.reset_index()
        df_filledrange.rename(
            columns={
                '25th percentile': 'lower', 
                '75th percentile': 'upper'
                }, inplace=True
            )

    elif filledrangetype == 'boot95':
        leg_str = 'Bootstrapped 95% CI'
        bootstraps = putils.bootstrap_skills(all_drifters_all_tracks,
                                             skill).groupby('time_since_start')
        df_filledrange = bootstraps.apply(
            lambda x: pd.Series(
                {
                    '2.5th percentile': x[skill].quantile(.025),
                    '97.5th percentile': x[skill].quantile(.975),
                    'mean': x[skill].mean()
                }
            )
        )
        df_filledrange = df_filledrange.reset_index()

        df_filledrange.rename(
            columns={
                '2.5th percentile': 'lower',
                '97.5th percentile': 'upper'
                }, inplace=True
            )

    elif filledrangetype == '1std':
        leg_str = '1std'
        df_grouped = all_drifters_all_tracks.groupby('time_since_start')
        df_filledrange = df_grouped.apply(
            lambda x: pd.Series(
                {
                    'std': x[skill].std(),
                    '1std_upper': (x[skill]+x[skill].std()),
                    '1std_lower': (x[skill]-x[skill].std())
                }
            )
        )

        df_filledrange = df_filledrange.reset_index()
        df_filledrange['upper'] = df_filledrange['1std_upper']
        df_filledrange['lower'] = df_filledrange['1std_lower']

    elif filledrangetype == 'extremes':
        leg_str = 'extremes'

        df_grouped = all_drifters_all_tracks.dropna().groupby(
            'time_since_start'
        )

        df_filledrange = df_grouped.apply(
            lambda x: pd.Series(
                {
                    'min_values': x[skill].min(),
                    'max_values': x[skill].max()
                }
            )
        )

        df_filledrange = df_filledrange.reset_index()
        df_filledrange['upper'] = df_filledrange['max_values'] 
        df_filledrange['lower'] = df_filledrange['min_values'] 
        leg_str = 'extremes'
    else:
        sys.exit(
            'This is unexpected behavior! filledrangetype should have ' 
            'defaulted to IQR if no other option was given')
    
    if any(
        skill in x for x in [
            'sep','obs_disp','obs_dist','mod_disp','mod_dist'
            ]
        ):

        df_filledrange = df_filledrange.apply(
            lambda x: x/1000. if x.name != 'time_since_start' else x
            )

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    #convert from time delta to float in hours
    df_filledrange['hours_since_start'] =\
        df_filledrange['time_since_start'].apply(
            lambda x: (x.total_seconds() / 3600)
            )

    df_filledrange = df_filledrange[
        df_filledrange['hours_since_start'] > 0
        ].copy()

    #make the plot
    if not bounding_bars_only:
        ax.fill_between(
            df_filledrange['hours_since_start'].values,
            df_filledrange['lower'].values,
            df_filledrange['upper'].values,
            alpha=0.2,
            facecolor=plotcolor,
            label=leg_str
            )

    if bounding_bars_only:  
        bblinestyle = '--'
        bblinecolor = plotcolor
        label=leg_str
    else:
        bblinestyle = '-'
        bblinecolor = 'k'
        label='_nolegend_'

    ax.plot(
        df_filledrange['hours_since_start'].values,
        df_filledrange['upper'].values,
        linestyle=bblinestyle,
        color=bblinecolor,
        linewidth=lwidth, 
        label=label
        )

    ax.plot(
        df_filledrange['hours_since_start'].values,
        df_filledrange['lower'].values,
        linestyle=bblinestyle,
        color=bblinecolor,
        linewidth=lwidth, 
        label='_nolegend_'
        )
    # For boot95, also plot the mean of bootstraps
    if filledrangetype == 'boot95':
        ax.plot(
            df_filledrange['hours_since_start'].values,
            df_filledrange['mean'].values,
            color=plotcolor,
            linewidth=2,
            label=setname
        )


    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('time since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)

    if leg:
        ax.legend(fontsize=12)


def plot_subtracted_skill(
    df1, 
    df2, 
    skill, 
    leg_label=None, 
    skill_labels=None, 
    lwidth=1, 
    zorder=1, 
    lcolor=None, 
    ax=None
    ):
    """ create line plot of average subtracted skill for two experiments. 

    Parameters
    ----------
    df1 : pandas.DataFrame
        pandas dataframe containing the average data for a single 
        experiment binned by hour.
    df2 : pandas.DataFrame
        pandas dataframe containing the average data for a second 
        single experiment binned by hour.
    skill : str
        string representing the skill score that will be plotted
    leg_label : str
        string representing the label that will be used for a legend
    skill_labels : dict
        dictionary containing label information for common variables
        included in the drift tool output
    lwidth : float
        value for the line width
    zorder : int
        represents the order in which to display the lines. (ie, shows
        how to overlay the various lines on the plot)
    lcolor : str
        string representing the linecolor
    ax : Axes
        Axis on which to create the plot.

    Returns
    -------
    plim : float
        maximum value of subtracted skill in the data to be plotted.
    tlim : datetime
        maximum time value in the data to be plotted.
    """

    if skill_labels is None:
        ylab_start = skill 
    else:
        ylab_start = str(skill_labels[skill]['ylab'])

    if leg_label is None:
        leglabstr = "_nolegend_" 
        ylab = ylab_start
    else:
        ylab = ylab_start + '\n' + leg_label
        leglabstr = leg_label

    subdf = df1[['time_since_start', 'hours since start']].copy()
    subdf[skill] = df1[skill].sub(df2[skill])
    subdf = subdf[subdf['hours since start'] > 0].copy()

    # if using sep, dists or disps, convert the units to km
    if (
        (skill == 'sep') or 
        (skill == 'obs_disp') or 
        (skill == 'obs_dist') or 
        (skill == 'mod_disp') or 
        (skill == 'mod_dist')
        ):
        subdf[skill] = subdf[skill] / 1000.

    diff = subdf[skill].values

    if ax is None:
        fig, ax = plt.subplots(figsize=(11, 8.5))

    lineplot, = ax.plot(
        subdf['hours since start'].values, 
        diff, 
        color=lcolor, 
        linewidth=lwidth, 
        zorder=zorder
        )

    plim = max(abs(diff))
    tlim = max(subdf['hours since start'].values)

    # plot parameters
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    # ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)

    if leg_label is not None:
        ax.legend([lineplot],[leg_label],fontsize=12)

    #plot a horizonal line at 0 to make the comparison more obvious
    plt.axhline(y=0, color='k', linewidth=1, linestyle=':', zorder=1)

    return plim, tlim

