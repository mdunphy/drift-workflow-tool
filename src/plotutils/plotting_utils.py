"""
Plotting Utility Routines
================
:Author: Jennifer Holden
:Contributors: Nancy Soontiens
:Created: 2020-07-02
"""

import os
import sys
import glob
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import cm
import datetime
from datetime import timedelta
import pickle

from driftutils import utils
import plotutils.regional_analysis_functions as rafuncs

logger = utils.logger


# create a list of dataset labels
def get_label_list(datadir, buoyids=None):
    """ create label list

    Parameters
    ----------
    datadir : str
        path to the folder containing drift tool netcdf output files.
    buoyids : list
        list of unique drifter ids

    Returns
    -------
    Unique list of setname labels. 

    Details
    -------
    If the input files are from comparison sets, the list
    is created using the setname dimension. If not, the 
    ocean_model attribute is used.
    """

    files = create_file_list(datadir, buoyids=buoyids)
    label_list = []
    for file in files:
        with xr.open_dataset(file) as ds:

            #if no label_list has been created yet, add the info from
            #the first file.
            if not label_list:
                if 'setname' in ds.dims:
                    label_list = list(ds.setname.values)
                else:
                    label_list = [ds.ocean_model]

            else:
                #if we're on the second file in the folder or later, 
                #check to be sure that the new list matches the list 
                #from the first file.
                if 'setname' in ds.dims:
                    new_list = list(ds.setname.values)
                else:
                    new_list = [ds.ocean_model]

                #check to be sure the new list matches the current label
                #list. All the files in the folder should have the same
                #set of labels
                if new_list != label_list:
                    errorstr = ('This is unexpected behavior. All files in ' +
                        'datadir should contain matching setnames.')
                    sys.exit(errorstr)

    return label_list


# convert the new format files into a list of datasets
def drifter2datasets(datadir, drifterid):
    """ convert a drifter file to a dataset

    Parameters
    ----------
    datadir : str
        path to the folder containing drift tool netcdf output files.
    drifterid : str
        unique id for an observed drifter
        
    Returns
    -------
    a dictionary with setnames as keys and associated datasets as values

    Details
    -------
    This assumes that there is only one filename containing drifterid 
    in the datadir
    """

    for fpath in glob.glob(os.path.join(datadir, '*' + drifterid + "*.nc")):
        drifterdict = {}

        with xr.open_dataset(fpath) as ds:
            vars_to_keep = [
                var for var in ds.data_vars 
                if 'DEPTH' not in ds[var].dims
            ]
            if 'setname' in ds.dims:
                for setnameplace in range(0, len(ds.setname.values)):
                    s = ds.setname.values[setnameplace]
                    dsnew = ds[vars_to_keep].sel(setname=s)
                    # add the data frame to a list of dataframes
                    drifterdict[s] = dsnew
            else:
                s = ds.ocean_model
                drifterdict[s] = ds[vars_to_keep]

    # return a dict with setnames as keys and datasets as values
    return drifterdict


def create_file_list(datadir, buoyids=None):
    """ create a list of filenames

    Parameters
    ----------
    datadir : str
        path to the folder containing drift tool netcdf output files.
    buoyids : list
        list of one or more unique observed drifter ids.

    Returns
    -------
    a sorted list containing full paths to all files meeting criteria. 

    """

    if buoyids is None:
        files = glob.glob(os.path.join(datadir, '*.nc'))

    else:
        potential_files = [
            "{}{}{}".format('*', i, '*.nc') for i in buoyids]
        files = []

        for potential_file in potential_files:
            file = glob.glob(os.path.join(datadir, potential_file))
            files.extend(file)
    files.sort()

    return files


def create_datasets_list(files):
    """ create datasets list

    Parameters
    ----------
    files : list
        list containing full paths to input data files

    Returns
    -------
    a list of datasets with one element per element in files. 
    If files represents comparsion output files, each element 
    will still contain all the information for all setnames 
    contained in each comparison output file. 
    """

    datasets=[]
    for f in files:
        with xr.open_dataset(f) as ds:
            vars_to_keep = [
                var for var in ds.data_vars 
                if 'DEPTH' not in ds[var].dims
            ]
            datasets.append(ds[vars_to_keep])

    #return a list of datasets
    return datasets


def create_datasets_dict(datasets_list, dataset_labels):
    """ create datasets dictionary

    Parameters
    ----------
    datasets_list : list
        List of xarray datasets. 
        Each element is the dataset belonging to an individual setname.
    datasets_labels : list
        List of setname labels. Each element corresponds to the labels 
        associated with the elements in the dataset_list. 

    Returns
    -------
    A dictionary. The dictionary has setnames as keys and associated 
    datasets as values. Ex, {CIOPSW: [drifter1, drifter2, etc]}
    """

    drifterdict = {k: [] for k in dataset_labels}
    for d in datasets_list:

        if 'setname' in d.dims:

            for s in d.setname.values:
                dssel = d.sel(setname=s)

                if s in drifterdict.keys():
                    drifterdict[s].append(dssel)
        else:
            s = d.ocean_model

            if s in drifterdict.keys():
                drifterdict[s].append(d)

    # return: a dictionary with setnames as keys and associated 
    # datasets as values. ie {CIOPSW: [drifter1, drifter2, etc]}
    return drifterdict


def prep_files(datadir, buoyids=None):
    """ prep files

    Parameters
    ----------
    datadir : str
        directory containing the input data
    buoyids : list
        list of one or more unique observed drifter ids.

    Returns
    -------
    dataset_labels : list 
        a list of setnames for the data sets 
    datasets_list : list 
        a list of datasets, one element for each file in the datadir,
        usually corresponds to one element per each drifter. 
        ie, [drifter1, drifter2, etc]
    datasets_dict : dict 
        a dictionary with setnames as keys and associated datasets as 
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    buoyids : list 
        the list of unique observed drifter ids. This is either passed 
        into the funtion or determined from all files in the datadir
    """

    #if no buoy ids are given, find all the file names in the datadir
    if buoyids is None:
        buoyids = utils.grab_buoyids_from_attrs(datadir)

    #get a list of labels for the setnames
    dataset_labels = get_label_list(datadir, buoyids=buoyids)

    #create a list of the files to use
    files = create_file_list(datadir, buoyids=buoyids)

    #create a list of datasets
    datasets_list = create_datasets_list(files)

    #create a dictionary with setnames as keys and lists of datasets 
    #as values
    datasets_dict = create_datasets_dict(datasets_list, dataset_labels)
    
    return dataset_labels, datasets_list, datasets_dict, buoyids


def drifter_to_binned_dataframes_list(datasets_dict, skill, pickledir=None):
    """convert drifter files to a binned list of dataframes

    Parameters
    ---------_
    datasets_dict : dict
        Dictionary containing setnames as keys and lists of datasets as 
        values. Each list element contains a dataset representing output
        from a single drifter.
    skill : str
        Skill score to bin
    pickledir : str
        Optional path to a directory containing pickled file of binned
        skill score data.

    Returns
    -------
    masterdict : dict
        Dictionary containing setnames as keys and binned dataframes as 
        values. Each binned dataframe is created by concatonating the 
        output from all observed drifters included in an experiment.

    """

    masterdict = {}
    for setname in datasets_dict.keys():

        all_mean, all_drifters_all_tracks =\
            bin_skills_all_drifters(datasets_dict[setname], skill, pickledir)
        masterdict[setname] = [all_drifters_all_tracks]

    # need result to be per model. combine all the lists of dataframes 
    # into one dataframe
    for key, value in masterdict.items():
        masterdict[key] = pd.concat(value)

    return masterdict


def assign_colors(dataset_labels, lcolors=None):
    """assign unique colors to a list of labels 

    Parameters
    ----------
    dataset_labels : list
        list of unique names representing the data sets. 
        Ex, ['RIOPS','GIOPS']   
    lcolors : dict
        optional dictionary containing setnames as keys and color names 
        as values

    Returns
    -------
    plotcolors : dict
        a dictionary with setnames as keys and color names as values. 
        The color names are taken from lcolors provided, or otherwise 
        calculated from cm.viridis. A check is performed to prevent 
        duplicated colors. 
    """

    #define a set of auto colors for each of the lines
    num_of_sets = len(dataset_labels)
    pclrs = cm.viridis(np.linspace(0, 1, num_of_sets))
    autoplotcolors = list(zip(dataset_labels, pclrs))
    pclrs_rounded = []
    for p in pclrs:
        pclrs_rounded.append([round(num, 1) for num in p.tolist()])

    #if you haven't been given a dictionary of colors
    if lcolors is None:
        plotcolors = autoplotcolors

    elif not any(i in dataset_labels for i in lcolors.keys()):
        #if lcolors exists, but none of the labels match the setname
        plotcolors = autoplotcolors

    else:
        #if lcolors exists
        plotcolors = []
        temp_setname_list = []

        #if lcolors is a dictionary
        if isinstance(lcolors,dict):
            for setname in dataset_labels:
                if setname in lcolors.keys():
                    pclr = matplotlib.colors.to_rgba_array(
                        lcolors[setname], 
                        alpha=None
                    )
                    plotcolors.append((setname, pclr[0]))

                else:
                    #if the setname isn't in the list, put it aside for 
                    #now by adding it to a temporary setname list
                    temp_setname_list.append(setname)

            #check to be sure that two identical values have not been 
            #passed in via lcolors
            for p in plotcolors:
                plotcolors_rounded = []
                #get a list of rounded color values

                for rcol in plotcolors:
                    plotcolors_rounded.append(
                        [round(num, 0) for num in rcol[1]]
                    )

                #check if the current value exists and remove it from 
                #the plotcolors list if it does
                if plotcolors_rounded.count(
                    [round(num, 0) for num in p[1]]
                ) > 1:
                    #add the setname to the temp list to re-assign a 
                    #color later
                    temp_setname_list.append(p[0])
                    #remove the element from plotcolors
                    plotcolors = [i for i in plotcolors if i[0] != p[0]]

            if len(temp_setname_list) != 0:
                #if there are setnames not in the color list, grab them 
                #from the auto colors instead
                colorcount = 0
                
                for setname in temp_setname_list:
                    newcolor_rounded = pclrs_rounded[colorcount]
                    colorcount += 1

                    plotcolors_rounded = []
                    for color in plotcolors_rounded:

                        if (len(color) == len(newcolor_rounded) 
                            and len(color) == sum(
                                [1 for i, j in zip(
                                    color, newcolor_rounded
                                ) if i == j]
                            )
                        ):
                            #The color for this set is identical to the 
                            #color used by another set. Try again
                            continue

                        else:
                            #colors do not match
                            newcolor = newcolor_rounded
                            plotcolors.append((setname,np.array(newcolor)))
                            break

        else:
            #lcolors is not a dictionary, which is unexpected. Taking
            #the plot colors from autocolors instead.
            plotcolors = autoplotcolors

    #convert the list of tuples to a dataframe with the setnames as keys
    plotcolorsdict = dict()
    for key, value in plotcolors:
        plotcolorsdict[key] = value

    return plotcolorsdict


def find_plot_pairs(dataset_labels, reference_set=None):
    """ find plot pairs 

    Parameters
    ----------
    dataset_labels : list
        list of unique names representing the data sets. 
        Ex, ['RIOPS','GIOPS']
    reference_set : str
        optional dataset name to use as a reference set.

    Returns
    -------
    list of zipped pairs of setnames to use for plotting. 
    Ex [('CIOPS-W','RIOPS'),('CIOPS-W','GIOPS')]
    """

    #Check if the user has given the option for a reference set in the 
    #config file. If there is a reference set chosen, then create a 
    #zipped list of pairs:
    plot_pairs = []
    if reference_set is not None:
        #use reference set
        if reference_set not in dataset_labels:
            errstr = ('\nunrecongnized reference_set given in user config file.'
                ' The reference_set input must match experiment identifier '
                'exactly (setname dimension in comparison files and '
                'ocean_model attribute in single output file). The reference '
                'set given in the config file is: [' + reference_set + 
                '] and the experiment identifiers are: ' + str(dataset_labels))
            sys.exit(errstr)
        #create pairs with referense set
        for l in dataset_labels:
            if l != reference_set:
                plot_pairs.extend(zip([l], [reference_set]))
    else:
        #no reference set
        for first, second in zip(dataset_labels, dataset_labels[1:]):
            if first != second:
                plot_pairs.extend(zip([first], [second]))
        plot_pairs.extend(zip([dataset_labels[0]], [dataset_labels[-1]]))

    return plot_pairs


def weighted_average(data, weights):
    """Caclulate a weighted average. Ignore NaN elements. Sets average 
    to NaN if all weights are zero.
    
    Parameters
    ----------
    data: pandas.DataFrame
        The data to be averaged.
        
    weights: pandas.DataFrame
        The weights for averaging.
        
    Returns
    -------
    avg: float
        The weighted average or np.nan if all elements are NaN."""
    try:
        avg = np.average(data[data.notnull()], weights=weights[data.notnull()])
    except ZeroDivisionError:
        avg = np.nan
    return avg


def grouped_mean(df, skill):
    """Helper function to group a dataframe and then calculate the 
    weighted average of a skill. The dataframe is grouped by 
    time_since_start.
    
    Parameters
    ----------
    df : pandas.DataFrame
        The dataframe must have a time_since_start column which are 
        timedeltas.
    skill : str
        Represents the skill to be averaged. (e.g. sep, molcard etc)
        
    Returns
    -------
    df_mean : pandas.DataFrame
        A data frame with the weighted average grouped by hours since 
        start.
    """
    df_grouped = df.groupby('time_since_start')

    df_mean = df_grouped.apply(
        lambda x: pd.Series({skill: weighted_average(x[skill], x['count']),
                             'count': x[x[skill].notnull()]['count'].sum()}))
    df_mean = df_mean.reset_index()

    df_mean['hours since start'] =\
            df_mean.time_since_start.astype('timedelta64[h]')

    return df_mean


def bin_skills_individual_drifter(ds, skill):
    """Calculate the average skill score for an individual drifter in 
    hourly bins.
    
    Parameters
    ----------
    ds : xarray.Dataset
        The dataset which contains data to be averaged.
        
    skill : str
        The skill to be averaged (e.g. sep, molcard, etc).
        
    Returns
    -------
    drifter_mean : pandas.DataFrame
        A data frame that contains the weighted average of all the 
        tracks for this drifter organized into hourly bins.
    all_tracks : pandas.DataFrame
        A data frame that contains the average score for each track 
    """
    all_tracks = pd.DataFrame()

    for m in ds.model_run.values:

        start = datetime.datetime.strptime(
            ds.sel(model_run=m)['mod_start_date'].values.item(0), 
            '%Y-%m-%d %H:%M:%S'
            )

        df_track = ds.sel(model_run=m).to_dataframe()
        df_track = df_track.set_index('time')

        initial_lat = np.unique(df_track['mod_start_lat'].values)
        initial_lon = np.unique(df_track['mod_start_lon'].values)
        mod_start_date = np.unique(df_track['mod_start_date'].values)

        # Need to keep track of the counts in order to do the drifter 
        # average properly.
        df_count = df_track[skill].dropna().sort_index().resample(
            '1H', 
            closed='right', 
            label='right', 
            origin=start
        ).count()

        df_track = df_track[skill].dropna().sort_index().resample(
            '1H', 
            closed='right', 
            label='right', 
            origin=start
        ).mean()

        df_track = df_track.reset_index()
        dftracktime64 = df_track['time'].astype('datetime64[s]')
        df_track['time_since_start'] = dftracktime64 - start
        df_track['model_run'] = m
        df_track['count'] = df_count.values
        df_track['initial_lat'] = [initial_lat[0]] * df_track.index.size
        df_track['initial_lon'] = [initial_lon[0]] * df_track.index.size 
        df_track['mod_start_date'] = [mod_start_date[0]] * df_track.index.size 

        all_tracks = pd.concat([all_tracks, df_track])

    all_tracks = all_tracks.reset_index()
    all_tracks['hours since start'] = \
        all_tracks.time_since_start.astype('timedelta64[h]')

    # Now find mean of all tracks
    drifter_mean = grouped_mean(all_tracks, skill)

    # Remove NaNs
    drifter_mean.dropna(how='any', inplace=True)
    all_tracks.dropna(how='any', inplace=True)

    return drifter_mean, all_tracks


def bin_skills_all_drifters(ds_list, skill, pickledir=None):
    """Calculate the average skill score for an all drifters a model run.
    Average scores are organized into hourly bins.
    
    Parameters
    ----------
    ds_list : list of xarray.Dataset
        The list datasets which should be averaged.
    skill : str
        The skill to be averaged (e.g. sep, molcard, etc).
    pickledir : str
        Optional path to a directory containing a pickled file of binned
        skill score data.
        
    Returns
    -------
    all_mean : pandas.DataFrame
        A data frame that contains the weighted average of all the 
        tracks of all the drifters organized into hourly bins.
    all_drifters_all_tracks : pandas.DataFrame
        A data frame that contains the average score for each track for 
        all drifters organized into hourly bins.
    """

    if pickledir:  
        zone = os.path.basename(pickledir)
    else:
        zone = 'unknown'
        pickledir = ''

    if 'setname' in ds_list[0].dims:
        setname = ds_list[0].setname.values
    else:
        setname = ds_list[0].ocean_model

    #check here for pre-existing yaml files
    picklename = ('pickled_data/binned_skill_scores_' + str(zone) 
                    + '_' + str(setname) + '_' + str(skill) + '.pickle')
    pickled_file = os.path.join(pickledir, picklename)
    if os.path.isfile(pickled_file):
        with open(pickled_file, 'rb') as handle:
            pickled_data = pickle.load(handle)
            all_mean = pickled_data['all_mean']
            all_drifters_all_tracks = pickled_data['all_drifters_all_tracks']

    else:
        all_drifters_all_tracks = pd.DataFrame()
        for ds in ds_list:
            drifter_mean, drifter_tracks = \
                        bin_skills_individual_drifter(ds, skill)
            drifter_tracks['buoyid'] = ds.obs_buoyid
            all_drifters_all_tracks = pd.concat(
                [all_drifters_all_tracks, drifter_tracks]
            )

        all_drifter_all_tracks = all_drifters_all_tracks.reset_index()
        all_drifter_all_tracks['hours since start'] = \
            all_drifter_all_tracks.time_since_start.astype('timedelta64[h]')

        # Now find mean of all drifters and all tracks
        all_mean = grouped_mean(all_drifters_all_tracks, skill)

    return all_mean, all_drifters_all_tracks


def create_savename(
    savedir, 
    savestring, 
    drifterid=None, 
    zone=None, 
    extension='png'
    ):

    """ helper script used to create a savename for plotting. 

    Parameters
    ----------
    savedir : str
        path to the directory where the plot will be saved
    savestring : str
        a string describing the plot type
    drifterid : str
        unique identifier for the drifter to be used with the per 
        drifter plots
    zone : str
        name of the polygon area if one has been defined by the user
    extension : str
        type of plot to save (default is png)

    Returns
    -------
    savepath : str
        full path including savename to use when saving figure.
    """

    if drifterid is None:
        drifterid = ''
    else:
        drifterid = str(drifterid) + '_'

    if savestring is None:
        savestring = ''
    else: 
        savestring = savestring + '_'

    if zone is None:
        zone = ''
    elif zone == 'all':
        zone = 'all'
    else:
        zone = str(zone) 

    savename = drifterid + savestring + zone + '.' + extension
    savepath = os.path.join(savedir, savename)

    return savepath


def bootstrap_skills(all_drifters_all_tracks, skill, num_boots=1000):
    """Calculate a skill's bootstrapped means for a dataframe with drifter
    tracks binned hourly

    Parameters
    ==========

    all_drifters_all_tracks : pd.DataFrame
       Dataframe containinged drifter skills

    skill : str
        The skill score to bootstrap (eg. sep, molcard, liu, etc)

    num_boots : int
       The number of bootstrap interations

    Returns
    =======
    bootstraps : pd.DataFrame
        Dataframe of binned boostrapped means.
        Each column has num_boots entries.
    """
    rg = all_drifters_all_tracks.groupby('time_since_start')
    bootstraps = pd.DataFrame()
    for h, rgh in rg:
        mean_boots = []
        for _ in range(num_boots):
            mean_boot = np.mean(np.random.choice(rgh[skill].values,
                                                 size=len(rgh[skill].values)))
            mean_boots.append(mean_boot)
        df = pd.DataFrame({'time_since_start': h,
                           skill: mean_boots})
        bootstraps = pd.concat([bootstraps, df])
    return bootstraps
