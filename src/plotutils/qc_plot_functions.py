"""
Quality control plotting functions
================
:Author: Jennifer Holden
:Contributors: Nancy Soontiens
:Created: 2020-09-24
"""

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

from driftutils import utils
from plotutils import map_functions as mapfuncs
from plotutils import skillscore_functions as ssfuncs

logger = utils.logger


def do_plot_skills_plus_map(
    setname, 
    drifterid, 
    datasets_dict, 
    plotcolors, 
    skill_label_params, 
    etopo_file, 
    all_drifter_tracks_dict,
    plot_parameters,
    dataproj=ccrs.PlateCarree(),
    proj=ccrs.PlateCarree(),
    ):

    """ function to create a qc style plot for examining sep, molcard,
    and liu skill scores plus drifter tracks for an individual drifter.

    Parameters
    ----------
    setname : str
        identifier for the eiperiment
    drifterid : str
        identifier for the particular drifter
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry or None.
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    proj : cartopy.crs.Projection
        Cartopy projection to use when plotting the data.
    """

    logger.info('.... ' + drifterid)

    #set up the figure
    fig = plt.figure(figsize=(
                        plot_parameters['figure_height'], 
                        plot_parameters['figure_width'])
                    )
    gs = fig.add_gridspec(2, 2)

    #plot the skills
    sub_dict = [
        d for d in datasets_dict[setname]
            if d.obs_buoyid == drifterid
        ][0]

    ax1 = fig.add_subplot(gs[0, 0])
    ssfuncs.plot_skills(
        drifterid,
        sub_dict,
        setname,
        'sep',
        plotcolors[setname],
        skill_label_params,
        lwidth=1,
        opacity=1,
        labs=True,
        leg=False,
        add_markers=True,
        ax=ax1
        )

    ax2 = fig.add_subplot(gs[0, 1])
    ssfuncs.plot_skills(
        drifterid,
        sub_dict,
        setname,
        'molcard',
        plotcolors[setname],
        skill_label_params,
        lwidth=1,
        opacity=1,
        labs=True,
        leg=False,
        add_markers=True,
        ax=ax2
        )

    ax3 = fig.add_subplot(gs[1, 0])
    ssfuncs.plot_skills(
        drifterid,
        sub_dict,
        setname,
        'liu',
        plotcolors[setname],
        skill_label_params,
        lwidth=1,
        opacity=1,
        labs=True,
        leg=False,
        add_markers=True,
        ax=ax3
        )

    # make the drifter track plot
    ax4 = fig.add_subplot(gs[1, 1], projection=proj)
    drifterdf = all_drifter_tracks_dict[setname].loc[
                    all_drifter_tracks_dict[setname]['buoyid'] == drifterid
                ]

    map_extremes = mapfuncs.finalize_plot_extremes(
        data=drifterdf,
        proj=proj,
        dataproj=dataproj,
        min_aspect_ratio=1, 
        lon_buff_percent=None,
        lat_buff_percent=None,
        polygons=None,
        zone=None,
    )

    # set the bbox to be equal to the 'padded_extremes', which will give the map
    # an aspect ratio of 1 in this case. Alternately, the 'buffered extremes'
    # will also work: map_extremes['buffered_extremes']['buffered_extremes']
    bbox = map_extremes['padded_extremes']

    #plot the map
    savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[setname],
        plot_parameters,
        obs_tracks=True,
        mod_tracks=True,
        start_dot=True,
        mod_initial_positions=True,
        ax=ax4,
        map_extremes=bbox, 
        obs_color=None,
        mod_color=plotcolors[setname],
        dot_color=None,
        mod_dot_color='orange',
        fig=fig,
        add_landmask=True,
        etopo_file=None,
        add_gridlines=True,
        drifterid=drifterid,
        restrict_extent=True,
        )

    titlestr = (str(drifterid) + '\n' + setname)
    fig.suptitle(titlestr, fontsize=plot_parameters['title_fontsize'])
    gs.tight_layout(fig)


def do_plot_ratios_plus_map(
    setname,
    drifterid,
    datasets_dict,
    plotcolors,
    skill_label_params,
    all_drifter_tracks_dict,
    plot_parameters,
    dataproj=ccrs.PlateCarree(),
    proj=ccrs.PlateCarree(),
    ):

    """ function to create a qc style plot for examining obsratio, 
    modratio and drifter tracks for an individual drifter

    Parameters
    ----------
    setname : str
        identifier for the experiment
    drifterid : str
        identifier for the particular drifter
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    proj : cartopy.crs.Projection
        Cartopy projection to use when plotting the data.
    """

    logger.info('\n.... ' + drifterid)

    #organize the data
    sub_dict = [
        d for d in datasets_dict[setname]
            if d.obs_buoyid == drifterid
        ][0]

    plot_dict = {'all_drifter_tracks': sub_dict.to_dataframe()}

    #set up the figure and subplots
    fig = plt.figure(figsize=(11, 5.5))
    gs = fig.add_gridspec(1, 2)
    skillhandle = fig.add_subplot(gs[0, 1])
    maphandle = fig.add_subplot(gs[0, 0], projection=proj)

    # plot the ratios
    ssfuncs.plot_skills(
        drifterid,
        sub_dict,
        setname,
        'obsratio',
        plotcolors[setname],
        skill_label_params,
        lwidth=1,
        opacity=1,
        labs=True,
        leg=True,
        add_markers=True,
        ax=skillhandle
        )

    ssfuncs.plot_skills(
        drifterid,
        sub_dict,
        setname,
        'modratio',
        plotcolors[setname],
        skill_label_params,
        lwidth=1,
        lstyle='--',
        opacity=1,
        labs=True,
        leg=True,
        add_markers=True,
        ax=skillhandle
        )

    #find the geographical extremes across all the experiments
    map_extremes = mapfuncs.finalize_plot_extremes(
        data=plot_dict,
        proj=proj,
        dataproj=dataproj,
        min_aspect_ratio=1,
    )

    #create the drifter track map plot
    savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[setname],
        plot_parameters,
        obs_tracks=True,
        mod_tracks=True,
        start_dot=True,
        mod_initial_positions=True,
        ax=maphandle,
        map_extremes=map_extremes['padded_extremes'], #sets aspect ratio to 1
        obs_color=None,
        mod_color=plotcolors[setname],
        dot_color=None,
        mod_dot_color='orange',
        fig=fig,
        add_landmask=True,
        etopo_file=None,
        add_gridlines=True,
        drifterid=drifterid,
        restrict_extent=True,
        )

    #finishing touches
    titlestr = (str(drifterid) + '\n' + setname)
    fig.suptitle(titlestr, fontsize=14)
    gs.tight_layout(fig)
    #add space to the top of the plot to accommodate the suptitle
    gs.update(top=0.85)


def do_plot_persistence(
    all_drifter_tracks_dict,
    datasets_dict,
    setname,
    etopo_file,
    plotcolors,
    plot_parameters,
    drifterid,
    zone='all'
    ):
    """ Plot all observed drifter tracks, all modelled tracks and all
    calculated persistence tracks on the same axis for each experiment.

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
     setname : str
        identifier for the experiment.   
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    drifterid : str
        unique buoyid that will be plotted. 
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plot showing observed, modelled and persistence tracks.
    """

    #organize the data
    sub_dict = [
        d for d in datasets_dict[setname]
            if d.obs_buoyid == drifterid
        ][0]

    plot_dict = {'all_drifter_tracks': sub_dict.to_dataframe()}

    # Create a figure object (right now, it's best to use PlateCarree()
    # for both the data projection and data transform functions. We
    # should revisit this at a later date. 
    fig = plt.figure(figsize=(
                        plot_parameters['figure_height'],
                        plot_parameters['figure_width']))
    dataproj = ccrs.PlateCarree()
    proj = ccrs.PlateCarree()

    # Generate axes using Cartopy
    ax = fig.add_subplot(1,1,1, projection=proj)

    # find the geographical extremes across all the experiments. This 
    # currently choses plot extents based on the obs and mod tracks only.
    # This can lead to some persistence tracks falling outside the figure
    # window. To include the persistence tracks when calculating the plot
    # extent, add the flag include_persistence=True to the following
    # mapfuncs.finalize_plot_extremes() function.
    map_extremes = mapfuncs.finalize_plot_extremes(
        data=plot_dict,
        proj=proj,
        dataproj=dataproj,
        min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
    )

    #create the drifter track map plot
    savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[setname],
        plot_parameters,
        obs_tracks=True,
        mod_tracks=True,
        pers_tracks=True,
        start_dot=True,
        mod_initial_positions=True,
        ax=ax,
        map_extremes=map_extremes['padded_extremes'], 
        obs_color=None,
        mod_color=plotcolors[setname],
        dot_color='green',
        mod_dot_color='orange',
        pers_color='teal',
        fig=fig,
        add_landmask=True,
        etopo_file=etopo_file,
        add_gridlines=True,
        drifterid=drifterid,
        restrict_extent=True,
        )

    # add a title

    titlestr = str(drifterid)
    if zone != 'all':
        titlestr = titlestr + '\n' + str(zone)
    ax.set_title(titlestr)
    fig.tight_layout()

    # Adjust legend to show the dataset name instead of "modelled tracks"
    handles, labels = ax.get_legend_handles_labels()
    newlabels = (['observed tracks','modelled tracks','persistence', 
                        'mod deployment points', 'obs deployment point'])
    mapfuncs.shift_legend(
                ax, 
                newlabels, 
                handles, 
                plot_parameters['legend_label_fontsize']
                )

