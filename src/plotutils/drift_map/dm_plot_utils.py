"""
Functions for drift data visualization
=============================
:Author: Clyde Clements
:Contributors: Samuel Babalola, Frederic Cyr
"""

from collections import namedtuple
import hashlib
import os
from os.path import join as joinpath, exists as pathexists
from os import path

import glob
import imageio
import matplotlib
from matplotlib.pylab import cm
import matplotlib.pyplot as plt
import math
from numpy import ma
import numpy as np
import pylab as pl
from scipy.interpolate import griddata
import xarray as xr

from driftutils import utils


logger = utils.logger

# provided matplotlib styles that can be applied to plots 
style_dict = {
    'talk': 'seaborn-talk',
    'notebook': 'seaborn-notebook',
    'poster': 'seaborn-poster',
    'paper': 'seaborn-paper'
    }

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

"""A set of utility functions for matplotlib"""

# ------------------
# Plot land mask
# ------------------

def landmask(M, color='0.8'):

   # Make a constant colormap, default = grey
   constmap = pl.matplotlib.colors.ListedColormap([color])

   jmax, imax = M.shape
   # X and Y give the grid cell boundaries,
   # one more than number of grid cells + 1
   # half integers (grid cell centers are integers)
   X = -0.5 + pl.arange(imax+1)
   Y = -0.5 + pl.arange(jmax+1)

   # Draw the mask by pcolor
   M = ma.masked_where(M > 0, M)
   pl.pcolor(X, Y, M, shading='flat', cmap=constmap)

# -------------
# Colormap
# -------------

def LevelColormap(levels, cmap=None):
    """Make a colormap based on an increasing sequence of levels"""
    
    # Start with an existing colormap
    if cmap == None:
        cmap = pl.get_cmap()

    # Spread the colours maximally
    nlev = len(levels)
    S = pl.arange(nlev, dtype='float')/(nlev-1)
    A = cmap(S)

    # Normalize the levels to interval [0,1]
    levels = pl.array(levels, dtype='float')
    L = (levels-levels[0])/(levels[-1]-levels[0])

    # Make the colour dictionary
    R = [(L[i], A[i,0], A[i,0]) for i in range(nlev)]
    G = [(L[i], A[i,1], A[i,1]) for i in range(nlev)]
    B = [(L[i], A[i,2], A[i,2]) for i in range(nlev)]
    cdict = dict(red=tuple(R),green=tuple(G),blue=tuple(B))

    return matplotlib.colors.LinearSegmentedColormap(
        '%s_levels' % cmap.name, cdict, 256)

def handle_duplicate_legends(axi):
    """
    Create legend for each plots and handle duplicated legends due to 
    multipe unique observed id for all maps
    """

    # handle duplicated legends 
    handles, labels = axi.get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    pl.legend(handle_list, label_list,loc='upper right')


def plot_ratio_contour(filename, bbox_file, plot_style, ratio_file=None):
    """Plot the displacement and average displacement for modelled ocean data

    Parameters
    ----------
    filename    : str
        name of file containing the computed trajecjory longitude and
        latitude values.
    output_file : str or file-like object, optional
        File to save ocean data plots to. If not provided, no plot file is
        saved.
    bbox_file   : str
        Path to namelist bounding box to be used in plots.
    plot_style  : str 
        mpl style to be applied to plots
                  
    """
    logger.info('Plotting contour for ocean trajectories...')
    ds = xr.open_dataset(filename)
    fig, ax = plt.subplots(1,1,figsize=(20,15))
    #plt.style.use(style_dict[plot_style])
    ax.set_title('Contour plot of {}: duration = {} days'.format
              (ds.mod_run_name, ds.mod_nb_output//24))
    adj_olen = ds.track_dist.values.copy()
    adj_olen[adj_olen == 0.] = 1.
    pre_z = ds.mod_disp.values/adj_olen
    z = pre_z
    # get variables
    lat = ds.mod_lat.values
    lon = ds.mod_lon.values

    # reshape data to vector
    lats = lat.reshape(lat.size)
    lons = lon.reshape(lon.size)
    z = z.reshape(z.size)

    # define a regular grid
    # resolution preference. Here, quarter of a degree
    dc = 0.1
    with open(bbox_file) as f:
        lines = f.readlines()
        initial_bbox = lines[0]
    initial_bbox = initial_bbox.split(" ")
    bbox_lon_min=float(initial_bbox[0])
    bbox_lat_min=float(initial_bbox[1])
    bbox_lon_max=float(initial_bbox[2])
    bbox_lat_max=float(initial_bbox[3])

    lonLims = [bbox_lon_min, bbox_lon_max] 
    latLims = [bbox_lat_min, bbox_lat_max]
    lon_reg = np.arange(lonLims[0]+dc/2, lonLims[1]-dc/2, dc)
    lat_reg = np.arange(latLims[0]+dc/2, latLims[1]-dc/2, dc)
    
    # Aggregate the data on a regular grid
    V = np.full((lat_reg.size, lon_reg.size), np.nan)
    for i, xx in enumerate(lon_reg):
        for j, yy in enumerate(lat_reg):    
            idx = np.where((lons>=xx-dc/2) &
                           (lons<xx+dc/2) &
                           (lats>=yy-dc/2) &
                           (lats<yy+dc/2))
            if np.size(idx)>=1:
                V[j,i] = np.array(z[idx].mean())

    ## Horizontal interpolation to remove NaNs
    #lon_grid, lat_grid = np.meshgrid(lon_reg,lat_reg)
    #lon_vec = np.reshape(lon_grid, lon_grid.size)
    #lat_vec = np.reshape(lat_grid, lat_grid.size)
    # Meshgrid 1D data (after removing NaNs)
    #tmp_vec = np.reshape(V, V.size)
    # griddata (after removing nans)
    #idx_good = np.argwhere(~np.isnan(tmp_vec))
    #LN = np.squeeze(lon_vec[idx_good])
    #LT = np.squeeze(lat_vec[idx_good])
    #TT = np.squeeze(tmp_vec[idx_good])
    #Vi = griddata((LN, LT), TT, (lon_grid, lat_grid), method='linear')
    
    m = Basemap(projection='merc',lon_0=-50,lat_0=50,
                llcrnrlon=lonLims[0],llcrnrlat=latLims[0],
                urcrnrlon=lonLims[1],urcrnrlat=latLims[1], resolution='h')
    x,y = m(*np.meshgrid(lon_reg,lat_reg))
    c = m.contourf(x, y, V, levels=np.linspace(0,1200,25))
    #levels=np.linspace(0,1,25)
    m.drawmapboundary(fill_color='#A6CAE0', linewidth=0)
    
    # draw coastlines, country boundaries, fill continents.
    ax.set_facecolor('steelblue')
    #m.fillcontinents(color='peru')
    m.fillcontinents(color='lightgrey', alpha=0.7)
    m.drawcoastlines(linewidth=0.8, color="black")
    m.drawparallels(np.arange(10,70,10), labels=[1,0,0,0],
                    fontsize=12, fontweight='bold')

    m.drawmeridians(np.arange(-80, 5, 10), labels=[0,0,0,1],
                    fontsize=12, fontweight='bold')
    c_bar = m.colorbar(c,location='bottom',pad="8%")
    c_bar.set_label('Ratio of displacement to distance (km)')
    ds.close()
    disp_fig.savefig(output_file, bbox_inches='tight', dpi=300, 
                papertype='letter', orientation='portrait')
    plt.close(disp_fig)


def plot_contour(filename, bbox_file, plot_style,
                 disp_file=None, dist_file=None):
    """Plot the displacement and average displacement for modelled ocean data

    Parameters
    ----------
    filename    : str
        name of file containing the computed trajecjory longitude and
        latitude values.
    output_file : str or file-like object, optional
        File to save ocean data plots to. If not provided, no plot file is
        saved.
    bbox_file   : str
        Path to namelist bounding box to be used in plots.
    plot_style  : str 
         mpl style to be applied to plots
    """
    
    ds = xr.open_dataset(filename)
    # Calculate maximum value of color bar
    dispmax = np.nanmax(ds.mod_disp.values)
    # divide by 1000 to convert to km
    if dispmax/1000. > 500:
        level_max = 1000
    else:
        level_max = utils.round_up_base10(dispmax/1000)

    # open file containing namelist bounding box
    try:
        with open(bbox_file) as f:
            lines = f.readlines()
            initial_bbox = lines[0]
    except FileNotFoundError:
        logger.info('Plots will use user-defined bounding box...')
        initial_bbox = bbox_file
        initial_bbox = initial_bbox.lstrip()
    initial_bbox = initial_bbox.split(" ")

    variable_plot = [disp_file, dist_file]
    for v in variable_plot:
        if v == disp_file:
            z = ds.mod_disp.values/1000
            output_file = disp_file
            label = 'Displacement [km]'
        if v == dist_file:
            z = ds.track_dist.values/1000
            output_file = dist_file
            label = 'Cummulative Distance [km]'
        logger.info('Plotting contour for ocean trajectories...')
        fig, ax = plt.subplots(1,1,figsize=(20,15))
        plt.style.use(style_dict[plot_style])
        plt.title('Contour plot of {}: duration = {} days'.format
                (ds.mod_run_name, ds.mod_nb_output//24))
        
        # get variables
        lat = ds.mod_lat.values
        lon = ds.mod_lon.values
        #only = []
        #for lister in ds.mod_dist.values:
        #    only.append(np.cumsum(lister).tolist())
        #z = ds.mod_disp.values/np.asarray(only)
        # reshape data to vector
        lats = lat.reshape(lat.size)
        lons = lon.reshape(lon.size)
        z = z.reshape(z.size)

        # define a regular grid
        # resolution preference. Here, quarter of a degree
        dc = 0.1
        bbox_lon_min=float(initial_bbox[0])
        bbox_lat_min=float(initial_bbox[1])
        bbox_lon_max=float(initial_bbox[2])
        bbox_lat_max=float(initial_bbox[3])

        lonLims = [bbox_lon_min, bbox_lon_max] 
        latLims = [bbox_lat_min, bbox_lat_max]
        lon_reg = np.arange(lonLims[0]+dc/2, lonLims[1]-dc/2, dc)
        lat_reg = np.arange(latLims[0]+dc/2, latLims[1]-dc/2, dc)
        
        # Aggregate the data on a regular grid
        V = np.full((lat_reg.size, lon_reg.size), np.nan)
        for i, xx in enumerate(lon_reg):
            for j, yy in enumerate(lat_reg):    
                idx = np.where((lons>=xx-dc/2) &
                               (lons<xx+dc/2) & (lats>=yy-dc/2) &
                               (lats<yy+dc/2))
                if np.size(idx)>=1:
                    V[j,i] = np.array(z[idx].mean())

        # Horizontal interpolation to remove NaNs
        lon_grid, lat_grid = np.meshgrid(lon_reg,lat_reg)
        lon_vec = np.reshape(lon_grid, lon_grid.size)
        lat_vec = np.reshape(lat_grid, lat_grid.size)
        # Meshgrid 1D data (after removing NaNs)
        tmp_vec = np.reshape(V, V.size)
        # griddata (after removing nans)
        idx_good = np.argwhere(~np.isnan(tmp_vec))
        LN = np.squeeze(lon_vec[idx_good])
        LT = np.squeeze(lat_vec[idx_good])
        TT = np.squeeze(tmp_vec[idx_good])
        Vi = griddata((LN, LT), TT, (lon_grid, lat_grid), method='linear')
        
        m = Basemap(projection='merc',lon_0=-50,lat_0=50,
                    llcrnrlon=lonLims[0],llcrnrlat=latLims[0],
                    urcrnrlon=lonLims[1],urcrnrlat=latLims[1], resolution='h')
        x,y = m(*np.meshgrid(lon_reg,lat_reg))
        c = m.contourf(x, y, Vi, levels=np.linspace(0,level_max,25));
        m.drawmapboundary(fill_color='#A6CAE0', linewidth=0)
        
        # draw coastlines, country boundaries, fill continents.
        m.fillcontinents(color='lightgrey', alpha=0.7, lake_color='grey')
        m.drawcoastlines(linewidth=0.8, color="black")
        m.drawparallels(np.arange(10,70,10), labels=[1,0,0,0],
                        fontsize=12, fontweight='bold')
        m.drawmeridians(np.arange(-80, 5, 10), labels=[0,0,0,1],
                        fontsize=12, fontweight='bold')

        c_bar = m.colorbar(c,location='bottom',pad="8%")
        c_bar.set_label(label)
       
        fig.savefig(output_file, bbox_inches='tight', dpi=300, 
                    papertype='letter', orientation='portrait')
    ds.close()
    plt.close(fig)


def make_gif(run_date_dir, name):
    """ Creates gif for drift map projection"""
    
    logger.info('\nCreating gif for drift map projection')

    ocean_model_name = name.split("_")
    filenames = glob.glob(
                    os.path.join(
                        run_date_dir, '{}*'.format(ocean_model_name[0])
                        )
                    )
    filenames.sort()

    if len(filenames) <= 1:
       logger.info('One or no plots produced, no gif needed')

    else:    
        images = []
        for filename in filenames:
            images.append(imageio.imread(filename))
        imageio.mimsave(
            '{}/drift_map.gif'.format(run_date_dir), images, duration = 0.4
            )

    logger.info('finished creating gif')


def determine_latlon_bbox_for_drifters(drifters, bbox_ratio):
    """Determine the latitude and longitude bounding box for drifter tracks.

    Parameters
    ----------
    drifters : list of xarray.Dataset
        List of drifter tracks. Each drifter must be ``an xarray.Dataset``
        with variables ``lat`` and ``lon``.
        
    bbox_ratio : float
        Ratio between area of covered by the plot bounding box and area 
        covered by all drifters


    Returns
    -------
    bbox : LatLonBoundingBox
    """
    # These initial values are outside the valid ranges for latitudes and
    # longitudes, and are guaranteed to change on the first loop below.

   
    lat_min = 1000.
    lon_min = 1000.
    lat_max = -1000.
    lon_max = -1000.
    for drifter in drifters:
        drifter_lat_min = drifter['lat'].values.min()
        drifter_lat_max = drifter['lat'].values.max()
        drifter_lon_min = drifter['lon'].values.min()
        drifter_lon_max = drifter['lon'].values.max()
        lat_min = min(drifter_lat_min, lat_min)
        lat_max = max(drifter_lat_max, lat_max)
        lon_min = min(drifter_lon_min, lon_min)
        lon_max = max(drifter_lon_max, lon_max)

    lat_diff = lat_max - lat_min
    lon_diff = lon_max - lon_min
            
    buffer_lon = lon_diff * (math.sqrt(bbox_ratio)-1)/2
    buffer_lat = lat_diff * (math.sqrt(bbox_ratio)-1)/2
    
    lon_min=lon_min - buffer_lon
    lat_min=lat_min - buffer_lat
    lon_max=lon_max + buffer_lon
    lat_max=lat_max + buffer_lat
    
    return LatLonBoundingBox(lon_min=np.around(lon_min,decimals =2),
                             lat_min=np.around(lat_min,decimals=2),
                             lon_max=np.around(lon_max,decimals=2),
                             lat_max=np.around(lat_max,decimals=2))
