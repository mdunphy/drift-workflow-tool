"""
Plot DriftMap percentage histograms
=============================
:Author: Samuel T. Babalola
:Contributors: Nancy Soontiens, Jennifer Holden
:Created: 2019-05-15

Plot the percentage displacement and percentage cumulative distance
histograms for DriftMap output.
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os

from driftutils import utils
from driftutils import ioutils
from driftutils import cli

matplotlib.use('agg')
logger = utils.logger


def get_maximums(ds, val):
    m = np.delete(ds[val].values, 0, axis=1)
    masked = np.ma.masked_invalid(m)
    final = masked.max(axis=1)
    final = [f / 1000 for f in final
             if not isinstance(f, np.ma.core.MaskedConstant)]
    return final


def plot_percentage_histogram(*, filename, savedir, style='default'):
    """Plot the percentage displacement and percentage cumulative distance
    histograms.

    Parameters
    ----------
    filename : str
        name of file containing the computed trajectory lon and lat values.
    savedir : str
        name of folder in which to save the plotted output.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    ds = ioutils.load_dataset(filename)
    logger.info('\nplotting the percentage histogram for ' + ds.mod_run_name)

    with plt.style.context(style):

        fig, axs = plt.subplots(1, 2, figsize=(16, 7))

        xlabs = ['Maximum Displacement Per Model Drifter (km)',
                 'Maximum Cumulative Distance Per Model Drifter (km)']
        vals = [get_maximums(ds, 'mod_disp'), get_maximums(ds, 'track_dist')]
        for ax, xlab, val in zip(axs, xlabs, vals):
            logger.info('... plotting ' + xlab.lower().replace(' (km)', ''))
            weights = np.ones_like(val)/float(len(val))
            ax.hist(val, rwidth=0.85, weights=weights, bins=20)
            ax.set_ylabel('Frequency')
            ax.set_xlabel(xlab)
            statstr = '$\mu={} $, $\sigma={} $'.format(round(np.mean(val), 4),
                                                       round(np.std(val), 4))
            titlestr = (str(ds.mod_run_name) + '\n' + statstr)
            ax.set_title(titlestr)

        # use a common set of xlims and ylims for both histograms
        xmax = max([axs[0].get_xlim()[1], axs[1].get_xlim()[1]])
        ymax = max([axs[0].get_ylim()[1], axs[1].get_ylim()[1]])
        for ax in [axs[0], axs[1]]:
            ax.set_xlim(0, xmax)
            ax.set_ylim(0, ymax)

        # save and close
        histogram_basename = 'percentage_disp_dist_{}.png'.format(ds.mod_run_name)
        hist_plot_filename = os.path.join(savedir, histogram_basename)
        fig.savefig(hist_plot_filename, bbox_inches='tight',
                    dpi=300, orientation='portrait')
        plt.close(fig)


def main():

    cli.run(plot_percentage_histogram)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
