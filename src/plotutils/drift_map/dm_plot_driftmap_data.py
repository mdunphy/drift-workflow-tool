"""
Plot DriftMap Trajectories
=============================
:Author: Samuel T. Babalola
:Contributors: Nancy Soontiens, Jennifer Holden
:Created: 2018-07-25

Creates model trajectory plots at each user defined time interval
"""

import matplotlib
import matplotlib.pyplot as plt
import geopy.distance
import numpy as np
import os
import imageio
import glob
import xarray as xr
import cartopy.crs as ccrs
from mpl_toolkits.axes_grid1 import make_axes_locatable
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import utils
from driftutils import cli
from plotutils import map_functions as mapfuncs
from plotutils import scalebar as sbar
from plotutils.drift_map import dm_plot_utils as dmp_utils

matplotlib.use('agg')
logger = utils.logger


def make_gif(savedir, gif_name, mod_run_name):
    """ Creates gif for drift map projection"""

    filenames = glob.glob(os.path.join(savedir, '{}*H*'.format(mod_run_name)))
    filenames.sort()

    if len(filenames) <= 1:
        logger.info('One or no plots produced, no gif needed')

    else:
        images = []
        for filename in filenames:
            images.append(imageio.imread(filename))

        imageio.mimsave(gif_name, images, duration=0.4)


def plot_model_trajectories(
    *, file_name,
    savedir,
    bbox,
    etopo_file=None,
    interval=None,
    show_bbox=False,
    add_scale=True,
    lock_extremes=True,
    plot_gif=True,
    style='default'
):
    """ Create model trajectory plots at each user defined time interval.

    Parameters
    ----------
    file_name : str
        File containing the computed trajecjory longitudes and latitudes
    savedir : str
        Directory in which the plots will be created.
    bbox : str
        Path to namelist bounding box to be used in plots.
    etopo_file : str
        Path to bathymetry file.
    interval : int
        User specified input to determine frequency of plots that will
        be created. For example, plots every 12 hours, 6 hours etc. If
        interval is None or unspecified, one plot will be created at the
        total experiment duration time.
    show_bbox : boolean
        When True, the bbox is added to the plot
    add_scale : boolean
        When True, adds a distance scale to plot
    lock_extremes : boolean
        When True, calculates the plot extent from the entire dataset
        (all timesteps). This is particularly important when plotting
        a gif since in that case all maps should cover the same extent.
    plot_gif : boolean
        When True, plots the 000H time interval then creates a gif from
        the trajectory plots of all the user defined time intervals.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    ds = xr.open_dataset(file_name)

    logger.info('\nplotting the trajectories for ' + str(ds.mod_run_name))

    # determine the experiment duration and set it to an int (this is a
    # string in mldpn output files)
    mod_nb_output_int = int(round(float(ds.mod_nb_output)))

    # define a range of times for plot creations.
    if interval:
        time_list = list(range(0, mod_nb_output_int+1, interval))
    else:
        time_list = [mod_nb_output_int]

    if not plot_gif:
        # no need to include plot at 0h (though this does provide an easy
        # means of plotting the initial starting positions)
        time_list.remove(0)

    # Open file containing namelist bounding box if given
    try:
        with open(bbox) as f:
            lines = f.readlines()
            bbox = lines[0]
            bbox = bbox.lstrip().rstrip().split(" ")
            bbox = [float(i) for i in bbox]
    except FileNotFoundError:
        # otherwise, the bbox was passed in as a string
        bbox = bbox.lstrip().rstrip().split(" ")
        bbox = [float(i) for i in bbox]

    # Produce plots using user-defined interval
    for hour in time_list:

        ds_truncate = ds.isel(time=slice(None, hour+1))
        ds_truncate.attrs['mod_nb_output'] = hour
        name = ds_truncate.mod_run_name.split('_')
        run_name = '{}_{}_{}_{}H'.format(
                                    name[0], name[1], name[2],
                                    str(hour).zfill(3)
                                    )
        ds_truncate.attrs['mod_run_name'] = run_name

        logger.info('...' + str(ds_truncate.mod_run_name))

        if lock_extremes:
            plot_minlon = np.nanmin(ds.mod_lon.values.flatten())
            plot_minlat = np.nanmin(ds.mod_lat.values.flatten())
            plot_maxlon = np.nanmax(ds.mod_lon.values.flatten())
            plot_maxlat = np.nanmax(ds.mod_lat.values.flatten())
        else:
            plot_minlon = np.nanmin(ds_truncate.mod_lon.values)
            plot_minlat = np.nanmin(ds_truncate.mod_lat.values)
            plot_maxlon = np.nanmax(ds_truncate.mod_lon.values)
            plot_maxlat = np.nanmax(ds_truncate.mod_lat.values)

        # add an extra bit of buffer if looking at a small area
        lon_buff_perc = 0.05
        lat_buff_perc = 0.05
        if (plot_maxlon - plot_minlon) < 3:
            lon_buff_perc = 0.15
        if (plot_maxlat - plot_minlat) < 2:
            lat_buff_perc = 0.15

        extremes_with_buffer = mapfuncs.add_map_extent_buffer(
            plot_minlon, plot_maxlon, plot_minlat, plot_maxlat,
            lon_buff_percent=lon_buff_perc,
            lat_buff_percent=lat_buff_perc
        )
        map_extremes = extremes_with_buffer['buffered_extremes']

        with plt.style.context(style):

            cmap = plt.cm.get_cmap('copper')

            # Create a figure object (right now, it's best to use
            # PlateCarree() for both the data projection and data
            # transform functions.
            fig = plt.figure(figsize=(20, 15))
            dataproj = ccrs.PlateCarree()
            proj = ccrs.PlateCarree()

            # Generate axes using Cartopy
            ax = fig.add_subplot(1, 1, 1, projection=proj)

            # use the calculated extents to restrict the plot domain
            ax.set_extent(map_extremes)

            minlat = map_extremes[2]
            maxlat = map_extremes[3]
            minlon = map_extremes[0]
            maxlon = map_extremes[1]

            # Calculate maximum value of color bar
            dispmax = np.nanmax(ds.mod_disp.values)
            if dispmax/1000 > 400:
                vmin = 0
                vmax = 400
            else:
                vmax = utils.round_up_base10(dispmax/1000.)
                vmin = 0

            # add bathymetery if specified
            if etopo_file is not None:
                mapfuncs.plot_bathymetry(
                    ax, bbox,
                    bathymetry_file=etopo_file,
                    dataproj=dataproj,
                    add_contourf=True,
                    colorbar_visible=False,
                    add_contours=False,
                    fig=fig
                )

            for md in range(0, len(ds_truncate.model_run)):

                traj_value_x = ds_truncate.mod_lon[md].values
                traj_value_y = ds_truncate.mod_lat[md].values

                ax.plot(
                    traj_value_x,
                    traj_value_y,
                    color='k',
                    linewidth=0.4,
                    transform=dataproj)

                mesh = ax.scatter(
                    traj_value_x,
                    traj_value_y,
                    c=ds_truncate.mod_disp[md].values/1000,
                    cmap=cmap,
                    marker='o', s=6,
                    vmin=vmin, vmax=vmax,
                    transform=dataproj)

                ax.plot(
                    traj_value_x[0],
                    traj_value_y[0],
                    '.', color='k',
                    markersize=3,
                    transform=dataproj)

                ax.plot(
                    traj_value_x[-1],
                    traj_value_y[-1],
                    '.', color='sandybrown',
                    markersize=3,
                    transform=dataproj)

                cbarval_list = np.linspace(vmin, vmax, 6, endpoint=True)
                colorbarstr = [f"{np.round(x):0.0f} km" for x in cbarval_list]
                divider = make_axes_locatable(ax)
                cbar_ax = divider.new_horizontal(
                                    size="5%", pad=0.1, axes_class=plt.Axes
                                    )
                fig.add_axes(cbar_ax)
                cbar = plt.colorbar(mesh, cax=cbar_ax)
                cbar.set_label('Displacement [km]')
                cbar.set_ticks(cbarval_list)
                cbar.ax.set_yticklabels(colorbarstr)

            # to draw a bounding box around the area containing the
            # starting locations:
            if show_bbox:

                lonlft = bbox[0]
                latbot = bbox[1]
                lonrgt = bbox[2]
                lattop = bbox[3]

                botx, boty = (np.linspace(lonlft, lonrgt),
                              np.linspace(latbot, latbot))
                topx, topy = (np.linspace(lonlft, lonrgt),
                              np.linspace(lattop, lattop))
                lftx, lfty = (np.linspace(lonlft, lonlft),
                              np.linspace(latbot, lattop))
                rgtx, rgty = (np.linspace(lonrgt, lonrgt),
                              np.linspace(latbot, lattop))

                plotparams = dict(linewidth=0.5, color='red',
                                  linestyle='--', zorder=100)
                ax.plot(botx, boty, **plotparams, transform=dataproj)
                ax.plot(topx, topy, **plotparams, transform=dataproj)
                ax.plot(lftx, lfty, **plotparams, transform=dataproj)
                ax.plot(rgtx, rgty, **plotparams, transform=dataproj)

            # add a scale bar to the plot. This feature is currently missing
            # from Cartopy, so the most likely candidate for the funtion that
            # will be included has been added to the drift-tool repo as a
            # temporary measure (scalebar.py, which is found in the carotpy
            # pull request: https://github.com/SciTools/cartopy/pull/1728).
            # Once a scalebar feature is included in Cartopy, we should switch
            # to that version instead.
            if add_scale:

                # first, define the place where the bar will appear on the plot
                lat_pos = minlat+(maxlat-minlat)/20

                # determine the width of the plot and use it to calculate the
                # length of the scale. This way the scale bar will be
                # approximately 1/10 of the width of the plot area.
                lftpt = (lat_pos, minlon)
                rgtpt = (lat_pos, maxlon)
                plot_width = geopy.distance.distance(lftpt, rgtpt).m
                scale_width = plot_width/10

                # the scalebar funtion looks nicest when dividing the bar
                # into 3 segments. Therefore divide the data as well.
                sbar_len = scale_width / 3
                if sbar_len > 10000:
                    sbar_len = round(sbar_len, -4)

                # scale location choices:
                # sbar_loc_center = (0.43, 0.05)
                # sbar_loc_right = (0.85, 0.05)
                sbar_loc_left = (0.0005, 0.05)

                # add the scalebar
                sbar.add_scalebar(ax,
                                  bbox_to_anchor=sbar_loc_left,
                                  length=sbar_len,
                                  ruler_unit='km',
                                  max_stripes=3,
                                  fontsize=10,
                                  frameon=True,
                                  ruler_unit_fontsize=13,
                                  ruler_fontweight='light',
                                  tick_fontweight='light',
                                  dy=0.085)

            # add gridlines
            ax.gridlines(
                crs=dataproj,
                draw_labels=['bottom', 'left'],
                xformatter=LongitudeFormatter(),
                xlabel_style={'rotation': 45, 'ha': 'center'},
                yformatter=LatitudeFormatter(),
                ylabel_style={'rotation': 45, 'ha': 'center'}
            )

            # add the landmask
            add_landmask = True
            if add_landmask:
                mapfuncs.plot_land_mask(
                    ax, bbox,
                    dataproj=dataproj,
                    landmask_type=None,
                    landmask_files=None,
                    )

            # add title
            textstr = 'Drift map of {}\nduration = {} hours'
            titlestr = (textstr.format(ds_truncate.mod_run_name, hour))
            ax.set_title(titlestr)

            # save the figure
            basename = '{}'.format(ds_truncate.mod_run_name)
            output_file = os.path.join(savedir, basename)
            fig.savefig(
                    output_file,
                    bbox_inches='tight',
                    dpi=300,
                    orientation='portrait'
                )
            plt.close(fig)
            ds_truncate.close()

    # make the gif if required
    if plot_gif:
        logger.debug('making the gif...')
        model_run_name = '_'.join(ds_truncate.mod_run_name.split('_')[0:-1])
        gifname = 'dm_{}.gif'.format(model_run_name)
        make_gif(savedir,
                 os.path.join(savedir, gifname),
                 model_run_name)

    ds.close()


def main():

    cli.run(plot_model_trajectories)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
