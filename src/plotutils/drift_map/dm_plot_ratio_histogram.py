"""
Plot DriftMap ratio histogram
=============================
:Author: Samuel T. Babalola
:Contributors: Nancy Soontiens, Jennifer Holden
:Created: 2019-05-15

Plot histogram of the ratio of displacement to cumulative distance.
"""

import matplotlib
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from driftutils import utils
from driftutils import ioutils
from driftutils import cli

matplotlib.use('agg')
logger = utils.logger


def plot_ratio_histogram(*, filename, savedir, style='default'):
    """Plot the ratio of displacement to cumulative distance for DriftMap
    output data.

    Parameters
    ----------
    filename : str
        name of file containing the computed trajectory lon and lat values
    savedir : str
        name of folder in which to save the plotted output.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    ds = ioutils.load_dataset(filename)

    logger.info('\nplotting the ratio histogram for ' + str(ds.mod_run_name))

    # For ratio of diplacement to cumulative distance
    logger.info(('...plotting ratio of diplacement to cumulative distance'))

    # Currently finding the last common timestep where at least one of the
    # tracks in the ratio array have a non-masked value. To instead find
    # the last valid (non-masked) value for each track:
    # valid_ind = [pd.DataFrame(r.T).apply(pd.Series.last_valid_index)[0]
    #              for r in ratio]
    # last_valid_ratio = [r[ind] for r, ind in zip(ratio, valid_ind)]
    ratio = np.ma.masked_invalid(ds.ratio.values)
    take_last = np.take(ratio, -1, axis=1)

    if take_last.count() > 0:
        # the last timestep contains non-masked values
        timelab = pd.to_datetime(str(ds.time.values[-1]))
        tlab = timelab.strftime('%Y-%m-%d %H:%M:%S')

    else:
        # remove time steps from the end of the track until we find a
        # time step that contains at least one non-masked value. Start
        # the time count at 2 because we will count back from the
        # end of the list of times
        tcount = 2
        while take_last.count() == 0:
            ratio = np.ma.masked_invalid(np.delete(ratio, -1, axis=1))
            take_last = np.take(ratio, -1, axis=1)
            timelab = pd.to_datetime(str(ds.time.values[-1 * tcount]))
            tlab = timelab.strftime('%Y-%m-%d %H:%M:%S')
            tcount += 1

    # taking the histogram values from the last timestep that
    # contains at least one non-masked value
    end_ratio = [ts[-1] for ts in ratio if ts[-1]]

    with plt.style.context(style):

        # create the figure
        ratio_fig = Figure(figsize=(10, 8))
        ax = ratio_fig.add_subplot(1, 1, 1)
        ax.hist(end_ratio, rwidth=0.85, bins=20)
        ax.set_xlim(0, 1)

        # add labels and titles
        # the ratio is properly "Displacement along modelled trajectory
        # divided by the total distance travelled by modelled trajectory"
        ax.set_xlabel('Displacement along track / Total distance travelled')
        ax.set_ylabel('Frequency')
        statstr = '$\mu={} $, $\sigma={} $'.format(
            round(np.mean(end_ratio), 4), round(np.std(end_ratio), 4)
        )
        titlestr = ('{} at {}'.format(ds.mod_run_name, tlab) + '\n' + statstr)
        ax.set_title(titlestr)

        # save and close
        ratio_basename = 'ratio_disp2dist_{}.png'.format(ds.mod_run_name)
        ratio_plot_filename = os.path.join(savedir, ratio_basename)
        ratio_fig.savefig(ratio_plot_filename, bbox_inches='tight',
                          dpi=300, orientation='portrait')
        plt.close(ratio_fig)


def main():

    cli.run(plot_ratio_histogram)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
