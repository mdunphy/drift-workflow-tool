"""
Explore and plot the rotary spectra for drift correction factor output.
This work heavily references the following excellent tutorial:
www.jmlilly.net/course/labs/html/SpectralAnalysis-Python.html
==============================================
:Author: Jennifer Holden, Nancy Soontiens
:Created: 2022-08, 2022
"""

import spectrum
import os
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sg
from scipy.stats import chi2
from scipy.special import digamma

# ignoring warnings for a couple of specific future code changes
import warnings

from driftutils import utils
from driftutils import cli

logger = utils.logger


# define functions to return the Coriolis and tidal frequencies
def corfreq_range(d, lats=['mean', 'min', 'max']):
    """
    The Coriolis frequency in rad / day at a given latitude.

    Parameters
    ----------
    d : xr.DataSet
        dataset containing latitude values
    lats : list
        list of strings defining which frequencies will be
        returned in the array

    Returns
    -------
    An np array containg Coriolis frequecy at chosen latitudes
    """

    def corfreq(lat):
        omega = 7.2921159e-5
        return 2 * np.sin(lat * 2 * np.pi / 360) * omega * (3600) * 24

    freqdict = {'mean': corfreq(d.lat.values.mean()) / 2 / np.pi,
                'min': corfreq(d.lat.values.min()) / 2 / np.pi,
                'max': corfreq(d.lat.values.max()) / 2 / np.pi}

    return np.array([freqdict[latitude] for latitude in lats])


def tidefreq():
    """Eight major tidal frequencies in rad / day (See Gill 1982, page 335)."""
    return 24 * 2 * np.pi / np.array([327.85, 25.8194, 24.0659, 23.9344,
                                      12.6584, 12.4206, 12.0000, 11.9673])


def mconf(K, gamma, str):
    """ Compute symmetric confidence intervals for multitaper
    spectral estimation.

    Parameters
    ----------
    K : float
        Number of tapers used in the spectral estimate, normally 2*P-1
    gamma : float
        confidence level, e.g., 0.95
    str : str
        'lin' to return confidence intervals for linear axis
        'log' to return confidence intervals for log10 axis

    Returns
    -------
    ra, rb:  Ratio factors for confidence interval

    If S0 is the true value of the spectrum and S is the spectral estimate,
    then the confindence interval is defined such that

        Probability that ra < S/S0 < ra = gamma     (linear case)
        Probability that ra < log10(S)/log10(S0) < ra = gamma (log10 case)

    The confidence interval will be S*ra to S*rb for the spectral values
    in linear space, or S*10^ra to S*10^rb in log10 space.  If log10(S)
    is plotted rather than S with a logarithmic axis, the latter would
    become log10(S) + ra to log10(S) * rb.
    """

    dx = 0.0001

    # Compute pdf symmetrically about unity
    if str == 'lin':

        # from one to minus one
        x1 = np.arange(1 - dx / 2, -1, step=-dx) * 2 * K
        # from 1 to three
        x2 = np.arange(1 + dx / 2, 3, step=dx) * 2 * K
        fx = (chi2.pdf(x1, 2 * K) + chi2.pdf(x2, 2 * K)) * 2 * K
        sumfx = np.cumsum(fx) * dx

        ii = np.where(sumfx >= gamma)[0][0]
        ra = x1[ii] / 2 / K
        rb = x1[ii] / 2 / K

    elif str == 'log':

        xo = np.log(2) + np.log(1 / 2 / K) + digamma(K)  # see pav15-arxiv
        c = np.log(10)
        xo = xo / c  # change of base rule

        x1 = np.power(10, np.arange(xo - dx/2, xo - 2, step=-dx))
        x2 = np.power(10, np.arange(xo + dx/2, xo + 2, step=dx))

        fx1 = c * 2 * K * np.multiply(x1, chi2.pdf(2 * K * x1, 2 * K))
        fx2 = c * 2 * K * np.multiply(x2, chi2.pdf(2 * K * x2, 2 * K))

        sumfx = np.cumsum(fx1 + fx2) * dx

        ii = np.where(sumfx >= gamma)[0][0]
        ra = np.log10(x1[ii])
        rb = np.log10(x2[ii])

    return ra, rb


def create_cvdict(d):
    """ Helper function that returns the complex velocities """

    cv_obs = (d.isel(setname=0).ueast_drifter.values
              + 1j * d.isel(setname=0).vnorth_drifter.values)
    cvdict = {'Observations': cv_obs}
    setnames = list(d.setname.values)

    for sind in range(0, len(setnames)):
        dsel = d.isel(setname=sind)
        cvdict[str(setnames[sind])] = \
            dsel.ueast_ocean.values + 1j * dsel.vnorth_ocean.values
    return cvdict


def add_reference_lines(
    ax, d,
    tidal=True,
    coriolis=True,
    cori_lats=['mean', 'min', 'max']
):
    """ helper function that plots reference lines for the main tidal
    frequencies and the coriolis frequency at defined latitudes """

    if tidal:
        # add reference lines for major tidal frequencies
        tcount = 0
        for tidal_freq in tidefreq() / 2 / np.pi:
            tlab = 'Tidal Frequencies'
            if tcount > 0:
                tlab = '__nolabel'
            tcount += 1
            ax.axvline(tidal_freq,
                       linestyle="--",
                       color="gray",
                       linewidth=1,
                       label=tlab)

    if coriolis:
        # add reference lines for the Coriolas freq
        # at the chosen latitudes
        ccount = 0
        for coriolis_freq in corfreq_range(d, cori_lats):
            clab = 'Coriolis Frequency'
            if ccount > 0:
                clab = '__nolabel'
            ccount += 1
            ax.axvline(coriolis_freq,
                       color="black",
                       linewidth=1,
                       label=clab)


def calc_rotary_spectrum(cv, dt, P):
    """Calculate rotary spectrum using mulit taper method with P tapers
    following method found on
    www.jmlilly.net/course/labs/html/SpectralAnalysis-Python.html"""

    psi, eigs = spectrum.mtm.dpss(np.size(cv), NW=P, k=2 * P - 1)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        f, _ = sg.periodogram(cv - np.mean(cv), fs=1 / dt)

    Zk, weights, eigenvalues = spectrum.mtm.pmtm(cv - np.mean(cv),
                                                 k=2 * P - 1,
                                                 NFFT=np.size(cv),
                                                 v=psi,
                                                 e=eigs,
                                                 method="unity")

    S = np.mean(np.transpose(np.abs(Zk) ** 2), axis=1) * dt

    return f, S


def plot_confidence_intervals(
    ax, f, S, P,
    c=None,
    filled=True,
    cilines=False
):
    """ Helper function that plots the confidence intervals.

    Parameters
    ----------
    ax : axes
        axis on which to draw the plot
    f : list
        frequencies (cycles / day)
    S : list
        power spectra density [m ** 2 / s ** 2 days]
    P : int
        The time-bandwith product, which sets the degree of smoothing across
        frequencies. A larger P value leads to smoother spectra.
    c : str
        color that will be used to draw the plot
    filled : boolean
        A value of True fills the area between the upper and lower 95%
        confidence intervals using the same color as the corresponding line.
    cilines : boolean
        A value of True plots upper and lower 95% confidence intervals
    """

    # define the confidence interval
    r = mconf(2 * P - 1, 0.95, 'log')

    # if plotting the confidence intervals (either via lines, a filled range
    # or both):
    if (filled or cilines):

        lstr = ('95% confidence interval')

        # set the labels for the cilines here. If the filled range is plotted
        # these labels will be removed and labels will be added to the filled
        # range instead.
        cilab = ['__nolabel', lstr]

        # if using a filled range, set the cis line labels to None and
        # label the filled range instead
        if filled:
            # reset the cilab variable so that the labels only plot for the
            # filled range and not for the lines.
            cilab = ['__nolabel', '__nolabel']
            fkwargs = {'color': c, 'alpha': 0.1, 'zorder': -10, 'label': lstr}
            ax.fill_between(f, (10 ** r[0]) * S, (10 ** r[1]) * S, **fkwargs)

        # plot lines for the confidence intervals as well
        if cilines:
            for ci, lab in zip([r[0], r[1]], cilab):
                cikwargs = {'color': c, 'linewidth': 0.75, 'linestyle': '--',
                            'label': lab, 'zorder': -5}
                ax.loglog(f, (10 ** ci) * S, **cikwargs)


def plot_rotary_spectrum(f, S, axs, P,
                         label='__nolabel',
                         color=None,
                         filled=False,
                         cilines=False):
    """Plot the rotary sepctrum with positve frequencies
    on the right hand axis and negative frequencies on the left

    Parameters
    ----------
    f : list
        frequencies (cycles / day)
    S : list
        power spectra density [m ** 2 / s ** 2 days]
    axs : list
        list of axis on which to draw the plot.
    P : int
        The time-bandwith product, which sets the degree of smoothing across
        frequencies. A larger P value leads to smoother spectra.
    label : string
        name identifying the dataset (ie, observations, experiment name, etc)
    color : str
        color that will be used to draw the plot
    filled : boolean
        A value of True fills the area between the upper and lower 95%
        confidence intervals using the same color as the corresponding line.
    cilines : boolean
        A value of True plots upper and lower 95% confidence intervals

    """

    fs = [-f[np.where(f < 0)], f[np.where(f > 0)]]
    ss = [S[np.where(f < 0)], S[np.where(f > 0)]]
    xlabs = ['Negative Frequency (cycles/day)',
             'Positive Frequency (cycles/day)']
    ylabs = ['Power Spectral Density (m$^2$/s$^2$ days)', None]

    for ax, f, S, xlab, ylab in zip(axs, fs, ss, xlabs, ylabs):

        # add the frequency line
        rs = ax.loglog(f, S, color=color, label=label, zorder=100)

        # plot the confidence intervals
        plot_confidence_intervals(
            ax, f, S, P,
            c=rs[0].get_color(),
            cilines=cilines,
            filled=filled
        )

        # label the axis
        ax.set_ylabel(ylab)
        ax.set_xlabel(xlab)


def plot_rotary_spectral_estimate(*, ds, plot_dir, P=4, style='default'):
    """ Calculate and plot the rotary spectral estimate for modelled/
    observed velocities using drift correction factor output. Observed
    velocities represent the drifter tracks and modelled velocities are
    determined by interpolating the model velocity fields to the observed
    drifter positions. It is important to note that observed drifters
    often report every three hours whereas the model data is reported
    every hour. Caution should therefore be used when comparing at
    higher frequencies. Reference material can be found at
    www.jmlilly.net/course/labs/html/SpectralAnalysis-Python.html

    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset containing drift correction factor output for a
        single drifter or a concatenated xarray dataset containing drift
        correction factor output for multiple drifters
    plot_dir : str
        path to the folder where the output plot will be written
    P : int
        Time-bandwidth product, which sets the degree of smoothing across
        frequencies. A larger P value leads to smoother spectra.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    logger.info('...creating rotary spectra plot')

    if 'buoyid' not in list(ds.dims.keys()):
        ds = ds.assign_coords(buoyid=ds.obs_buoyid)
        ds = ds.expand_dims('buoyid')

    for buoy in ds.buoyid.values:
        d = ds.sel(buoyid=buoy, drop=True)
        d = d.dropna(dim='time')

        if 'setname' not in list(ds.dims.keys()):
            sname = [d.setname if 'setname' in d.keys()
                     else d.ocean_model_name][0]
            d = d.assign_coords(setname=sname)
            d = d.expand_dims('setname')

        d = d.dropna(dim='time')
        dt = (d.time[1] - d.time[0]).values / np.timedelta64(1, 'D')

        # get the complex-valued velocities
        cvdict = create_cvdict(d)
        names = list(cvdict.keys())
        cvs = [cvdict[n] for n in names]

        # to set custom colors:
        colors = ['gold', 'cornflowerblue']
        if len(names) > len(colors):
            colors = colors + [None] * (len(names)-len(colors))

        # apply a style to the upcoming figure
        with plt.style.context(style):

            fig, ax = plt.subplots(1, 2, sharey=True, figsize=(12, 6))
            for setname, cv, col in zip(names, cvs, colors):

                f, S = calc_rotary_spectrum(cv, dt, P)
                plot_rotary_spectrum(
                    f, S, [ax[0], ax[1]], P,
                    label=setname,
                    color=col,
                    cilines=False,  # add upper and lower confidence intervals
                    filled=True,  # fill range between confidence intervals
                )

            for n in range(np.size(ax)):
                ax[n].autoscale(enable=True, tight=True, axis='x')
                add_reference_lines(ax[n], d, cori_lats=['mean'])
                ax[n].grid(linestyle=':')

            handles, labels = ax[0].get_legend_handles_labels()
            lgd = fig.legend(handles, labels,
                             loc='center left',
                             bbox_to_anchor=(1, 0.5),
                             frameon=False,
                             facecolor='white',
                             framealpha=1)

            ax[0].invert_xaxis()
            fig.tight_layout(h_pad=2, w_pad=2)
            stitle = fig.suptitle('Multitaper Rotary Spectral Estimate\n'
                                  + str(buoy) + ' (P = ' + str(P)
                                  + ')', y=1.05)
            plt.savefig(
                os.path.join(plot_dir,
                             ('dcf_rotary_spectral_estimate_' + str(buoy)
                              + '_P' + str(P) + '.png')),
                bbox_extra_artists=(lgd, stitle),
                bbox_inches='tight',
                dpi=300
            )

            plt.close('all')


def main():

    cli.run(plot_rotary_spectral_estimate)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
