"""
hexbin map plot of error in ocean model speed
==============================================
:Author: Jennifer Holden, Nancy soontiens
:Created: 2022-07, 2022
"""

import os
import sys
import numpy as np
import cartopy.crs as ccrs
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import utils
from driftutils import cli
from plotutils import map_functions as mapfuncs

matplotlib.use('Agg')
logger = utils.logger


def plot_dcf_hexbins(
    fig,
    ax,
    ds,
    C=None,
    colormap='RdBu_r',
    norm=None,
    hexsize=None
):
    """
    General plotting function used to create hexbin plots from drift
    correction factor output. This function is called by plot_error_hexes()
    and plot_error_counts().

    Parameters
    ----------
    fig : figure
        current matplotlib figure
    ax : Axes
        Axis to use for plotting
    ds : xr.Dataset
        xarray dataset composed of one or more drifter tracks from
        drift correction factor output.
    C : str
        Name of values that will be accumulated in the hex bins. If None,
        hex bin colors will be base on a count of the number of points
        per bin. If a name is given, C will be taken from the variable of
        the same name in ds.
    colormap : str
        Name of color map to use when creating the hexbin. Some options
        are 'RdBu_r' and 'Purples'
    norm :
        If norm is None, bin values are scaled to the colormap range [0,1]
        and mapped to colors using a linear scaling. A value of
        colors.CenteredNorm() will generate symmetrical color mapped values
        centered on zero. See matplotlib.pyplot.hexbin documentation for
        more details
    hexsize : tuple
        desc
    """

    if 'buoyid' not in list(ds.dims.keys()):
        ds = ds.assign_coords(buoyid=ds.obs_buoyid)
        ds = ds.expand_dims('buoyid')

    # define the variables for convenience
    lons = utils.wrap_to_180(np.concatenate(ds.lon.values))
    lats = np.concatenate(ds.lat.values)
    map_extremes = [np.nanmin(lons),
                    np.nanmax(lons),
                    np.nanmin(lats),
                    np.nanmax(lats)]

    # define the values to use for C if necessary
    cvals = None
    # if plotting counts, only plot if at least 2 counts are present in
    # a bin for consistency with the values plot.
    mcount = 2
    if C is not None:
        cvals = np.concatenate(ds[C].values)
        # if not plotting counts one value in each bin is ok
        mcount = 1

    # calculate the hexbin size
    if hexsize is None:
        hexsize = mapfuncs.calculate_hexbin_size(fig, ax, map_extremes)

    hexplt = ax.hexbin(
        lons, lats,
        C=cvals,
        norm=norm,
        transform=ccrs.PlateCarree(),
        gridsize=hexsize,
        cmap=colormap,
        edgecolor='black',
        linewidth=0.1,
        zorder=100,
        alpha=0.95,
        mincnt=mcount
    )

    # add the colorbar
    cbar = plt.colorbar(hexplt, ax=ax)

    # set the aspect ratio to produce a square map
    ax.set_aspect('equal', adjustable='datalim', anchor='C')
    plt.draw()

    # set the extent to the same as the one used when
    # setting the aspect ratio to square. This ensures
    # that any islands not included in the data extent
    # will still appear in the plot.
    bbox_aspect = list(ax.get_xlim()) + list(ax.get_ylim())
    ax.set_extent(bbox_aspect, crs=ccrs.PlateCarree())

    # add the land
    mapfuncs.plot_land_mask(ax, bbox_aspect)

    # draw parallels and meridians
    ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=['bottom', 'left'],
        linewidth=0.5, color='gray', alpha=0.5, linestyle='--',
        xformatter=LongitudeFormatter(),
        xlabel_style={'rotation': 45, 'ha': 'center'},
        yformatter=LatitudeFormatter(),
        ylabel_style={'rotation': 45, 'ha': 'center'}
    )

    return cbar


def plot_error_hexes(*, ds, plot_dir, style='default'):
    """
    plot error in ocean model speed from drift correction factor output

    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset composed of one or more drifter tracks from
        drift correction factor output.
    plot_dir : str
        Full path to the directory where the plots will be generated.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    logger.info('Creating plot of error in ocean model speed')

    with plt.style.context(style):

        fig = plt.figure(figsize=(8, 7))
        gs = fig.add_gridspec(1, 1)
        ax = fig.add_subplot(gs[0, 0], projection=ccrs.PlateCarree())

        cbar = plot_dcf_hexbins(fig, ax, ds,
                                C='speed_ocean_error',
                                norm=colors.CenteredNorm())

    cbar.set_label('ocean model speed - drifter speed [m/s]')

    # ax.set_title('Error in Ocean Model Speed')
    fig.savefig(
        os.path.join(plot_dir, ('dcf_error_in_ocean_model_speed.png')),
        bbox_inches='tight',
        dpi=300
    )

    plt.close()


def plot_error_counts(*, ds, plot_dir, style='default'):
    """
    plot number of counts per hexbin for drift correction factor output.

    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset composed of one or more drifter tracks from
        drift correction factor output.
    plot_dir : str
        Full path to the directory where the plots will be generated.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    logger.info('Creating plot of counts per hexbin for error '
                + 'in ocean model speed')

    with plt.style.context(style):

        fig = plt.figure(figsize=(8, 7))
        gs = fig.add_gridspec(1, 1)
        ax = fig.add_subplot(gs[0, 0], projection=ccrs.PlateCarree())

        cbar = plot_dcf_hexbins(fig, ax, ds,
                                C=None,
                                colormap='Purples',
                                norm=None)

    cbar.set_label('number of counts per bin')

    # Tidy up the plot then save it.
    fig.savefig(
        os.path.join(plot_dir, ('dcf_counts_per_bin' + '.png')),
        bbox_inches='tight',
        dpi=300
    )
    plt.close()


def dcf_error_hexes_workflow(*, ds, plot_dir, style='default'):
    """
    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset composed of one or more drifter tracks from
        drift correction factor output.
    plot_dir : str
        Full path to the directory where the plots will be generated.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlib's
        built in default parameters. Alternately, users can provide one of
        Matplotlib's other available styles or else provide a path to a
        custom *.mplstyle style sheet created by the user.
    """

    # do not try to create the plots for data with only one measurement.
    if len(np.concatenate(ds.speed_ocean_error.values)) < 2:
        logger.info('...failed to create plot of error in ocean model speed '
                    + ' - drift correction factor output only contains one '
                    + 'measurement.')
        return

    # Assume that the user is ok with the data in plot_dir being overwritten.
    if plot_dir:
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
    else:
        sys.exit('...failed to create plot of error in ocean model speed  '
                 + '- the user must provide plot_dir (full path to the '
                 + 'directory where the plots will be generated)')

    plot_error_hexes(ds=ds, plot_dir=plot_dir, style=style)
    plot_error_counts(ds=ds, plot_dir=plot_dir, style=style)


def main():

    cli.run(dcf_error_hexes_workflow)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
