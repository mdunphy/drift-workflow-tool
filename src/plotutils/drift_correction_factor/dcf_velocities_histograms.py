"""
2d histogram plots of eastward and northward velocities
from drift correction factor output
==============================================
:Author: Jennifer Holden, Nancy Soontiens
:Created: 2022-07-222
"""

import os
import numpy as np
import sys
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from matplotlib.ticker import MaxNLocator

from driftutils import utils
from driftutils import cli

matplotlib.use('Agg')
logger = utils.logger


def plot_velocities_histograms(*, ds, plot_dir, num_bins=50, style='default'):
    """ Scatter plots of the eastward and northward drifter velocity
    versus the ocean velocity from a collection of drift correction
    factor output files

    Parameters
    ----------
    ds : xr.Dataset
        Xarray dataset containing output data from a drift correction
        factors experiment.
    plot_dir : str
        Full path to the directory where the figure will be written
    num_bins : int
        Number of bins to use when creating the 2d histogram plots.
        Default is set to 50
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    # Assume that the user is ok with the data in plot_dir being overwritten.
    if plot_dir:
        if not os.path.exists(plot_dir):
            os.mkdir(plot_dir)
    else:
        sys.exit('...failed to create velocities histograms - user must '
                 + 'provide plot_dir (full path to the directory where '
                 + 'the plots will be generated)')

    logger.info('creating velocities histograms')

    # apply a style for the upcoming plot
    with plt.style.context(style):

        # begin to set up the figure
        fig = plt.figure(figsize=(6, 12), dpi=100)

        grid = ImageGrid(fig, 111,
                         nrows_ncols=(1, 2),
                         axes_pad=0.2,
                         cbar_mode="single",
                         cbar_location="right",
                         cbar_pad=0.2)

        # This is needed in the case where a single ds is used as input
        # instead of a concatenated dataset.
        if 'buoyid' not in list(ds.dims.keys()):
            ds = ds.assign_coords(buoyid=ds.obs_buoyid)
            ds = ds.expand_dims('buoyid')

        velocities = [ds.ueast_drifter.values.ravel(),
                      ds.ueast_ocean.values.ravel(),
                      ds.vnorth_drifter.values.ravel(),
                      ds.vnorth_ocean.values.ravel()]

        # Calculate the maximum data value across both the Eastward and
        # Northward velocities so that the colorbar and axes can be shared.
        # Add a small buffer to keep the data from plotting directly on the
        # edges of the plot.
        allvel = np.concatenate(velocities)
        maxval = np.nanmax([np.abs(np.nanmin(allvel)),
                            np.abs(np.nanmax(allvel))])
        maxval = maxval + 0.05 * maxval

        # create one subplot each for the Eastward and Northward velocities
        ueast_data = [velocities[0], velocities[1]]
        vnorth_data = [velocities[2], velocities[3]]
        titlestrs = ['Eastward Component', 'Northward Component']
        for ax, data, title in zip(grid, [ueast_data, vnorth_data], titlestrs):

            bins = np.linspace(-1 * maxval, maxval, num=num_bins)
            h = ax.hist2d(data[0], data[1], cmin=1, bins=bins)

            # add labels and gridlines
            ax.set_xlabel('Drifter Velocity [m/s]')
            ax.set_ylabel('Ocean Velocity [m/s]')
            ax.set_title(title)
            ax.grid(True, linestyle='--', alpha=0.5)

            # adjust the number of tickmarks being displayed on the axis
            locator = MaxNLocator(prune='both', nbins=6, symmetric=True)
            ax.xaxis.set_major_locator(locator)
            ax.yaxis.set_major_locator(locator)
            ax.tick_params(axis='x', rotation=45)
            ax.tick_params(axis='y', rotation=45)

            # add a red dotted line
            starting_point = (float(-1 * maxval), float(-1 * maxval))
            slope = float((maxval - (-1 * maxval)) / (maxval - (-1 * maxval)))
            ax.axline(starting_point,
                      slope=slope,
                      color='red',
                      linewidth=0.6,
                      linestyle=(0, (5, 5)))

        # add a common colorbar
        cbar = fig.colorbar(h[3], cax=grid.cbar_axes[0],
                            orientation='vertical')
        cbar.set_label('Counts per bin')

        ######################################
        # Tidy up the plot then save:
        ######################################
        fig.savefig(
            os.path.join(plot_dir, ('dcf_velocities_histograms.png')),
            bbox_inches='tight',
            dpi=300
        )
        plt.close()


def main():

    cli.run(plot_velocities_histograms)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
