"""
Drifter track map for correction factor output
==============================================
:Author: Jennifer Holden
:Created: 2022-05-26
"""

import os
import cartopy.crs as ccrs
import matplotlib
import matplotlib.pyplot as plt
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import utils
from driftutils import cli
from plotutils import map_functions as mapfuncs

matplotlib.use('Agg')
logger = utils.logger


def plot_drifter_tracks(
    *,
    ds,
    plot_dir,
    style='default'
):
    """
    Creates a drifter track map from all tracks in a dataset or netcdf
    file. Individual tracks are distinguished by a buoyid attribute in
    the input dataset. If a path to a folder is provided instead, all
    netcdf files in the folder are concatonated into a single dataset
    then plotted.

    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset composed of one or more drifter tracks from
        drift correction factor output.
    plot_dir : str
        Full path to the directory where the plots will be generated.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    # Assume that the user is ok with the data in plot_dir being overwritten.
    if plot_dir:
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
    else:
        raise RuntimeError(
            '...failed to created drifter tracks plot - '
            + 'the user must provide plot_dir (full path to the '
            + 'directory where the plots will be generated)'
        )

    logger.info('\nCreating drifter tracks plot')

    with plt.style.context(style):

        fig = plt.figure(figsize=(8, 8))
        gs = fig.add_gridspec(1, 1)
        ax = fig.add_subplot(gs[0, 0], projection=ccrs.PlateCarree())

        if 'buoyid' not in list(ds.dims.keys()):
            ds = ds.assign_coords(buoyid=ds.obs_buoyid)
            ds = ds.expand_dims('buoyid')

        for buoy in ds.buoyid.values:
            dsel = ds.sel(buoyid=buoy, drop=True)
            dsel = dsel.dropna(dim='time')

            lons = utils.wrap_to_180(dsel.lon.values)
            lats = dsel.lat.values

            # add the drifter track
            ax.plot(lons,
                    lats,
                    linewidth=1,
                    transform=ccrs.PlateCarree(),
                    zorder=3,
                    label=buoy)

        # set the aspect ratio to produce a square map
        ax.set_aspect('equal', adjustable='datalim', anchor='C')
        plt.draw()

        # set the extent to the same as the one used when
        # setting the aspect ratio to square. This ensures
        # that any islands not included in the data extent
        # will still appear in the plot extent.
        bbox_aspect = list(ax.get_xlim()) + list(ax.get_ylim())
        ax.set_extent(bbox_aspect, crs=ccrs.PlateCarree())

        # add the land
        mapfuncs.plot_land_mask(ax, bbox_aspect)

        # draw parallels and meridians
        ax.gridlines(
            crs=ccrs.PlateCarree(),
            draw_labels=['bottom', 'left'],
            xformatter=LongitudeFormatter(),
            xlabel_style={'rotation': 45, 'ha': 'center'},
            yformatter=LatitudeFormatter(),
            ylabel_style={'rotation': 45, 'ha': 'center'}
        )

        outname = 'dcf_drifter_tracks'

        # if there is only one drifter being plotted, add a title to the plot
        # and the buoyid to the filename
        if len(ds.buoyid.values) == 1:
            ax.set_title(ds.buoyid.coords['buoyid'].values[0])
            outname = outname + '_' + str(ds.buoyid.coords['buoyid'].values[0])

        # Tidy up the plot then save it.
        fig.savefig(
            os.path.join(plot_dir, (outname + '.png')),
            bbox_inches='tight',
            dpi=300
        )
        plt.close('all')


def main():

    cli.run(plot_drifter_tracks)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
