"""
Plot workflow script
================
:Author: Jennifer Holden
:Created: 2020-07-07
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


from driftutils.utils import logger
from plotutils import plotting_utils as putils
from plotutils import map_functions as mapfuncs
from plotutils import skillscore_functions as ssfuncs


####################################################################
# plot subtracted hexbins
####################################################################
def plot_subtracted_hexbins(
    datasets_dict,
    dataset_labels,
    plot_durations,
    skills_list,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plot_parameters,
    zonecolors,
    polygons,
    reference_set,
    proj=ccrs.PlateCarree(),
    dataproj=ccrs.PlateCarree(),
    zone='all'
):

    """Create hexbin plots of skill scores

    Parameters
    ----------
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    dataset_labels : list
        list of setnames for the two experiments being subtracted.
    plot_durations: list
        list of integers representing the plotting durations (ie, [48]
        would produce a plot representing skill score at 48h, etc)
    skills_list : list
        list of strings representing the skill scores to be plotted
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from 
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    zonecolors : dict
        dictionary of color values to use with regional analysis (keys
        are the zone names and values are the colors)
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    reference_set : str
        unique identifier for the experiment being analysed. This
        identifier must match one of the setname dimension values
        in the given comparison set output files.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    proj : cartopy.crs.Projection
        Cartopy projection to use when plotting the data.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    logger.info('\nplotting subtracted hexbins')

    if len(list(datasets_dict.keys())) < 2:
        logger.info('Not able to create subtracted plots using only one '
                    'experiment so skipping. The current set of data '
                    'only contains ' + str(list(datasets_dict.keys())))

        return

    # determine if there is a reference set chosen, then create a zipped
    # list of pairs:
    plot_pairs = putils.find_plot_pairs(
        list(dataset_labels),
        reference_set=reference_set
    )

    psavedir = os.path.join(savedir, 'pickled_data')

    # read in the datasets and create a dictionary with skill as keys.
    # values will be dictionaries with setnames as keys and binned
    # dataframes as values
    all_skills_dict = {}
    for skill in skills_list:
        df_res_dict = putils.drifter_to_binned_dataframes_list(
            datasets_dict, skill, pickledir=psavedir
        )
        all_skills_dict[skill] = df_res_dict

    # start calling the plotting here
    for skill in skills_list:

        # subtracted hexbin plots are not realistic for molcard and liu.
        if skill != 'sep':
            logger.info(
                '\nComparison hexbin plot for subtracted values not '
                'realistic for molcard and liu skill scores. '
                'Skipping plot.'
            )
            continue

        for plotdur in plot_durations:

            df_res_combined_dict = all_skills_dict[skill]

            for plot_pair in plot_pairs:

                logger.info('\n....plotting ' + str(' - '.join(plot_pair))
                            + ' for ' + skill + ' at ' + str(plotdur) + 'h')

                df1 = df_res_combined_dict[plot_pair[0]]
                df2 = df_res_combined_dict[plot_pair[1]]

                # only create plot if there are more than one starting point.
                # otherwise the program crashes.
                checklats = df1['initial_lat'].values.tolist()
                checklons = df1['initial_lon'].values.tolist()
                starts = list(zip(checklats, checklons))

                if len(set(starts)) > 1:

                    fig = plt.figure(figsize=(
                        plot_parameters['figure_height'],
                        plot_parameters['figure_width'])
                    )
                    ax = fig.add_subplot(1, 1, 1, projection=proj)

                    # if using a zone other than the default 'all', choose
                    # the extremes by including the polygon area boundry.
                    map_extremes = None
                    mapstr = None

                    if zone != 'all':
                        add_polygon = True
                        inc_poly = polygons
                    else:
                        add_polygon = False
                        inc_poly = None

                    ################################
                    # calculate and set plot extent
                    ################################
                    plot_extremes = mapfuncs.finalize_plot_extremes(
                        data=pd.concat([df1, df2]),
                        proj=proj,
                        dataproj=dataproj,
                        min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
                        lon_buff_percent=None,
                        lat_buff_percent=None,
                        polygons=inc_poly,
                        zone=zone,
                    )
                    map_extremes = plot_extremes['padded_extremes']

                    # use the calculated extents to create the plot
                    ax.set_extent(map_extremes)

                    plot_pairs_label = ' - '.join(plot_pair)

                    subsavestr = mapfuncs.plot_subtracted_hexbins(
                        fig,
                        skill,
                        plotdur,
                        df1,
                        df2,
                        dataset_labels,
                        zone=zone,
                        edge_color='black',
                        opacity_level=1.0,
                        landmask_type=landmask_type,
                        landmask_files=landmask_files,
                        etopo_file=etopo_file,
                        ax=ax,
                        logplot=True,
                        map_extremes=map_extremes,
                        mapstr=mapstr,
                        plot_parameters=plot_parameters,
                        cbar_label=None,
                        dataproj=None,
                        add_gridlines=True,
                        plot_pairs_label=plot_pairs_label
                    )

                    # if adding the polygon boundry
                    if add_polygon:
                        mapfuncs.plot_polygons(
                            polygons,
                            ax=ax,
                            zones = zone,
                            zcolors = zonecolors,
                            polygon_border_only=True,
                        )

                    # if the plot was created (ie, if there was more
                    # than one data point), save the figure.
                    if subsavestr:
                        if zone == 'all':
                            zone_title_str = "full spatial domain"
                        else:
                            zone_title_str = zone
                        ax.set_title(zone_title_str, fontsize=12)
                        subhexstr = (subsavestr + '_' + skill
                                     + '_' + '-'.join(plot_pair))
                        savepath = putils.create_savename(
                            savedir,
                            subhexstr,
                            drifterid=None,
                            zone=zone,
                            extension='png'
                        )

                        plt.savefig(savepath, bbox_inches='tight', dpi=300)
                        logger.info('saving ' + savepath)

                    # close the figure
                    plt.close()

                else:
                    logger.info('\nWARNING! cannot create hexbin plot because'
                                ' there is only one starting point. Skipping ')
                    plt.close()


####################################################################
# plot subtracted of means for each drifter
####################################################################
def plot_subtracted_skillscores_workflow(
    datasets_dict,
    datasets_list,
    skills_list,
    savedir,
    buoyids,
    skill_label_params,
    reference_set,
    zone='all'
):

    """ plot subtracted skill scores

    Parameters
    ----------
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    datasets_list : list
        a list of datasets, one element for each file in the datadir,
        usually corresponds to one element per each drifter.
        ie, [drifter1, drifter2, etc]
    skills_list : list
        list of strings representing the skill scores to be plotted
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    reference_set : str
        unique identifier for the experiment being analysed. This
        identifier must match one of the setname dimension values
        in the given comparison set output files.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    logger.info('\nplotting subtracted skill scores')

    if len(list(datasets_dict.keys())) < 2:
        logger.info(
            'Not able to create subtracted plots using only one experiment '
            'so skipping. The current set of data only contains '
            + str(list(datasets_dict.keys()))
        )
        return

    # determine if there is a reference set chosen, then create a zipped
    # list of pairs:
    plot_pairs = putils.find_plot_pairs(
        list(datasets_dict.keys()),
        reference_set=reference_set
    )

    psavedir = os.path.join(savedir, 'pickled_data')

    for skill in skills_list:

        if skill != 'sep':
            logger.info('\nComparison skill score line plot for subtracted '
                        'values not realistic for molcard, liu, obsratio and '
                        'modratio skill scores. Skipping plot.')

        else:
            for plot_pair in plot_pairs:
                logger.info('....plotting ' + skill + ' for '
                            + str('-'.join(plot_pair)))
                plotlabel = plot_pair[0] + ' - ' + plot_pair[1]
                fig, ax = plt.subplots(figsize=(11, 8.5))

                # plot the subtracted mean for each drifter
                plims = []
                tlims = []
                for drifter in buoyids:
                    for ds in datasets_list:
                        if ds.obs_buoyid == drifter:
                            df1, all_tracks1 =\
                                putils.bin_skills_individual_drifter(
                                    ds.sel(setname=plot_pair[0]),
                                    skill
                                )
                            df2, all_tracks2 =\
                                putils.bin_skills_individual_drifter(
                                    ds.sel(setname=plot_pair[1]),
                                    skill
                                )

                            plim, tlim =\
                                ssfuncs.plot_subtracted_skill(
                                    df1,
                                    df2,
                                    skill,
                                    leg_label=None,
                                    skill_labels=skill_label_params,
                                    ax=ax
                                )
                            plims.append(plim)
                            tlims.append(tlim)

                # plot the subtracted mean for the entire set
                alldf1, all_drifters_all_tracks1 =\
                    putils.bin_skills_all_drifters(
                        datasets_dict[plot_pair[0]],
                        skill,
                        pickledir=psavedir
                    )
                alldf2, all_drifters_all_tracks2 =\
                    putils.bin_skills_all_drifters(
                        datasets_dict[plot_pair[1]],
                        skill,
                        pickledir=psavedir
                    )
                plim, tlim = ssfuncs.plot_subtracted_skill(
                    alldf1,
                    alldf2,
                    skill,
                    leg_label=plotlabel,
                    skill_labels=skill_label_params,
                    lwidth=3,
                    zorder=3,
                    lcolor='k',
                    ax=ax
                )

                # set the x and y limits to refect the entire dataset
                plt.xlim([0, np.max(tlims)])
                plt.ylim(np.max(plims) * -1, np.max(plims))

                # save the figure
                subskillstr = "diff-lineplot_{}_{}".format(
                    str('-'.join(plot_pair)),
                    skill
                )

                if zone != 'all':
                    plt.title(zone)

                savepath = putils.create_savename(
                    savedir,
                    subskillstr,
                    drifterid=None,
                    zone=zone,
                    extension='png'
                )

                plt.savefig(savepath, bbox_inches='tight', dpi=300)
                plt.close()
                logger.info('saving ' + savepath)
