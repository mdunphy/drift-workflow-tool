"""
Workflow module with functions for use when plotting Hexbin plots
=================================================================
:Author: Jennifer Holden
:Created: 2022-02-28
"""

import os
import matplotlib
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import utils
from plotutils import plotting_utils as putils
from plotutils import map_functions as mapfuncs

logger = utils.logger
matplotlib.use('Agg')


def plot_hexbin_skillscores_workflow(
    datasets_dict,
    plot_durations,
    skills_list,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plot_parameters,
    polygons,
    zonecolors,
    zone='all',
    dataproj=None
):

    """Create hexbin plots of skill scores

    Parameters
    ----------
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    plot_durations: list
        list of integers representing the plotting durations (ie, [48]
        would produce a plot representing skill score at 48h, etc)
    skills_list : list
        list of strings representing the skill scores to be plotted
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from 
        the landmask_files dictionary, or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zonecolors : dict
        dictionary with values of color to use for each zone key.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    psavedir = os.path.join(savedir, 'pickled_data')

    if not dataproj:
        dataproj = ccrs.PlateCarree()

    for skill in skills_list:
        logger.info('\ncreating hexbin plot for ' + skill)
        df_all = putils.drifter_to_binned_dataframes_list(
                datasets_dict, skill, pickledir=psavedir
            )

        for setname in datasets_dict.keys():
            for plotdur in plot_durations:
                # only create plot if there are more than one starting point.
                # otherwise the program crashes.
                checklats = df_all[setname]['initial_lat'].values.tolist()
                checklons = df_all[setname]['initial_lon'].values.tolist()
                starts = list(zip(checklats, checklons))

                if len(set(starts)) > 1:
                    # Generate axes using Cartopy (right now, it's best
                    # to use PlateCarree() for both the data projection
                    # and data transform functions. We should revisit
                    # this at a later date.
                    fig = plt.figure(figsize=(
                                plot_parameters['figure_height'],
                                plot_parameters['figure_width']))

                    ax = fig.add_subplot(1, 1, 1,
                                         projection=ccrs.PlateCarree())

                    # if using a zone other than the default 'all', choose
                    # the extremes by including the polygon area boundry.
                    proj = ccrs.PlateCarree()
                    dataproj = ccrs.PlateCarree()

                    if zone != 'all':
                        add_polygon = True
                        inc_poly = polygons
                    else:
                        add_polygon = False
                        mapstr = None
                        inc_poly = None

                    ################################
                    # calculate and set plot extent
                    ################################
                    plot_extremes = mapfuncs.finalize_plot_extremes(
                        data=df_all[setname],
                        proj=proj,
                        dataproj=dataproj,
                        min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
                        lon_buff_percent=None,
                        lat_buff_percent=None,
                        polygons=inc_poly,
                        zone=zone,
                    )
                    map_extremes = plot_extremes['padded_extremes']

                    # use the calculated extents to create the plot
                    ax.set_extent(map_extremes)

                    ################################
                    # plot the bathymetry
                    ################################
                    mapfuncs.plot_bathymetry(
                        ax, map_extremes,
                        plot_parameters=plot_parameters,
                        bathymetry_file=etopo_file,
                        dataproj=dataproj,
                        add_contourf=True,
                        colorbar_visible=False,
                        add_contours=False,
                        fig=fig
                    )

                    ################################
                    # plot the landmask
                    ################################
                    # set the landmask if there are files given.
                    landmask_type = mapfuncs.get_landmask_type(landmask_files, 
                                                               setname)
                    # plot the landmask
                    mapfuncs.plot_land_mask(
                        ax, map_extremes,
                        dataproj=dataproj,
                        landmask_type=landmask_type,
                        landmask_files=landmask_files
                    )

                    ################################
                    # plot the actual hexbins
                    ################################

                    # calculate the best hexbin size to use
                    hexsize = mapfuncs.calculate_hexbin_size(
                        fig, ax, map_extremes
                    )

                    # create the actual plot:
                    mapstr = ''
                    mapstr, skilllabel = mapfuncs.plot_hexbins(
                        skill,
                        plotdur,
                        setname,
                        df_all[setname],
                        plot_parameters,
                        grid_spacing=hexsize,
                        ax=ax,
                        map_extremes=map_extremes,
                        mapstr=mapstr,
                        dataproj=ccrs.PlateCarree(),
                        fig=fig,
                        )

                    # if adding the polygon boundry
                    if add_polygon:
                        mapfuncs.plot_polygons(
                            polygons,
                            ax=ax,
                            zones=zone,
                            zcolors=zonecolors,
                            polygon_border_only=True,
                            )

                    ################################
                    # add gridlines and title
                    ################################
                    ax.gridlines(
                        crs=dataproj,
                        draw_labels=['bottom', 'left'],
                        xformatter=LongitudeFormatter(),
                        xlabel_style={'rotation': 45, 'ha': 'center'},
                        yformatter=LatitudeFormatter(),
                        ylabel_style={'rotation': 45, 'ha': 'center'}
                    )

                    if zone == 'all':
                        zone_title_str = "full spatial domain"
                    else:
                        zone_title_str = zone
                    full_title_str = (
                        skilllabel + ' at ' + str(plotdur).zfill(3) + ' hr'
                        + '\n' + setname + ' (' + zone_title_str + ')'
                    )
                    ax.set_title(full_title_str, fontsize=12)

                    #################################################
                    # assuming a plot was created (ie, there was more
                    # than one point to plot, save the figure.
                    #################################################
                    if mapstr:
                        hexstr = (
                            'hexbins_' + str(plotdur).zfill(3) + 'hr'
                            + '_' + skill + '_' + setname + mapstr
                            )

                        savepath = putils.create_savename(
                            savedir,
                            hexstr,
                            drifterid=None,
                            zone=zone,
                            extension=plot_parameters['output_filetype']
                            )

                        plt.savefig(
                            savepath,
                            bbox_inches='tight',
                            dpi=plot_parameters['dpi']
                            )

                        plt.close()
                        logger.info('saving ' + savepath)

                else:
                    logger.info(
                        'WARNING! cannot create hexbin plot because there '
                        'is only one starting point. Skipping '
                    )
                    plt.close()
