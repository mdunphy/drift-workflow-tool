"""
Workflow module with functions to create lineflow plots
=======================================================
:Author: Jennifer Holden
:Created: 2022-02-28
"""

import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


from driftutils import utils
from plotutils import plotting_utils as putils
from plotutils import skillscore_functions as ssfuncs

logger = utils.logger
####################################################################
# plot skill scores on subplots
####################################################################

def plot_drifter_skillscores_workflow(
    datasets_dict,
    skill_label_params,
    savedir,
    label_list,
    plotcolors,
    buoyids,
    overwrite_savedir,
    zone='all'
):
    """ plot skill scores on subplots

    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    label_list : list
        list of the setnames dimensions included in the input files. If
        the setname dimension is not present in the input file, the
        ocean_model attribute is used instead.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    logger.info('\nplotting drifter skillscores for each drifter')
    skills_subdir = utils.setup_savedir(
        savedir,
        'skills_subplots',
        overwrite_savedir = overwrite_savedir
        )

    # for each common id, plot the skill scores together
    for drifterid in buoyids:

        logger.info('......' + drifterid)

        fig, ax = plt.subplots(
            3, 1, squeeze=False, figsize=(8.5, 11), sharex='all'
            )

        for setname in datasets_dict.keys():
            sub_dict = [
                d for d in datasets_dict[setname]
                    if d.obs_buoyid == drifterid
                ][0]

            ssfuncs.plot_skills(
                drifterid,
                sub_dict,
                setname,
                'sep',
                plotcolors[setname],
                skill_label_params,
                lwidth=1,
                opacity=1,
                labs=True,
                leg=False,
                add_markers=True,
                ax=ax[0][0]
            )

            handles, labels = ax[0][0].get_legend_handles_labels()
            newlabels = list(label_list)

            ssfuncs.plot_skills(
                drifterid,
                sub_dict,
                setname,
                'molcard',
                plotcolors[setname],
                skill_label_params,
                lwidth=1,
                opacity=1,
                labs=True,
                leg=False,
                add_markers=True,
                ax=ax[1][0]
            )

            ssfuncs.plot_skills(
                drifterid,
                sub_dict,
                setname,
                'liu',
                plotcolors[setname],
                skill_label_params,
                lwidth=1,
                opacity=1,
                labs=True,
                leg=False,
                add_markers=True,
                ax=ax[2][0]
            )

        ax[0][0].legend(
            handles,
            newlabels,
            loc='best',
            fontsize=10
        )

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        if zone == 'all':
            suptitlestr = str(drifterid)
        else:
            suptitlestr = str(drifterid) + '\n' + zone
        fig.suptitle(suptitlestr, fontsize=14)
        ax[0][0].set_xlabel('')
        ax[1][0].set_xlabel('')
        plt.subplots_adjust(wspace=0, hspace=0.05)

        multiskillfignamepng =\
                        "{}_skills_{}.png".format(str(drifterid), zone)

        plt.savefig(
            os.path.join(skills_subdir, multiskillfignamepng),
            bbox_inches='tight',
            dpi=300
        )

        plt.close()

####################################################################
# plot average of means for each model
####################################################################

def plot_average_of_mean_skills_workflow(
    datasets_dict,
    skill_label_params,
    savedir,
    plotcolors,
    skills_list,
    zone='all'
):
    """
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skills_list : list
        list of skill scores to calculate. If None or missing, default
        ['sep','molcard'] is used, but this will be overwritten by the
        user_plotting_config.yaml.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    logger.info(
        '\nplotting avg means for ' + str(
            '-'.join(list(datasets_dict.keys()))
            )
        )

    for skill in skills_list:

        fig, ax = plt.subplots(figsize=(11, 8.5))

        for setname in datasets_dict.keys():

            all_mean, all_drifters_all_tracks =\
                putils.bin_skills_all_drifters(
                    datasets_dict[setname],
                    skill,
                    pickledir=os.path.join(savedir,'pickled_data')
                )

            ssfuncs.plot_average_skill(
                all_mean,
                setname,
                skill,
                plotcolors[setname],
                skill_label_params,
                lwidth=1,
                leg=True,
                ax=ax
            )

        if zone != 'all':
            ax.set_title(zone)

        combinedskfignamepng = "mean_{}_{}.png".format(skill,zone)

        plt.savefig(
            os.path.join(savedir, combinedskfignamepng),
            bbox_inches='tight',
            dpi=300,
            papertype='letter',
            orientation='portrait'
        )

        plt.close()

####################################################################
# plot average of means for each model WITH filled
####################################################################
def plot_skills_subplots_with_filled_workflow(
    datasets_dict,
    skill_label_params,
    savedir,
    plotcolors,
    skills_list,
    filled_range_type,
    zone='all'
):

    """
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skills_list : list
        list of skill scores to calculate. If None or missing, default
        ['sep','molcard'] is used, but this will be overwritten by the
        user_plotting_config.yaml.
    filled_range_type : list
        optional argument specifying a method(s) for calculating the
        filled range for use with average skillscore plots.
        If None or missing, Interquartile Range (IQR) is used.
        Can be list with multiple options, Ex ['IQR','boot95, 'extremes','1std']
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    logger.info('\nplotting average skill scores with filled range')

    for skill in skills_list:

        for fill_range_type in filled_range_type:

            fig, ax = plt.subplots(figsize=(11, 8.5))
            logger.info(
                'plotting ' + skill + ' with ' + fill_range_type
                )

            ssfuncs.plot_skill_with_filled(
                datasets_dict,
                skill,
                plotcolors,
                skill_label_params,
                lwidth=2,
                filledrangetype=fill_range_type,
                labs=True,
                leg=True,
                ax=ax,
                bounding_bars_only=True,
                pickledir=os.path.join(savedir,'pickled_data')
            )

            if zone != 'all':
                ax.set_title(zone, fontsize=14)

            combinedskfignamepng =\
                "mean_{}_{}_{}.png".format(skill,fill_range_type, zone)

            plt.savefig(
                os.path.join(savedir, combinedskfignamepng),
                bbox_inches='tight',
                dpi=300,
                papertype='letter',
                orientation='portrait'
            )

            plt.close()


####################################################################
# plot average of means for each model WITH individual means
####################################################################
def plot_skills_subplots_with_indv_workflow(
    datasets_dict,
    skill_label_params,
    savedir,
    plotcolors,
    skills_list,
    buoyids,
    zone='all'
):

    """
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skills_list : list
        list of skill scores to calculate. If None or missing, default
        ['sep','molcard'] is used, but this will be overwritten by the
        user_plotting_config.yaml.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
"""

    for skill in skills_list:
        fig, ax = plt.subplots(figsize=(11, 8.5))
        logger.info('\nplotting ' + skill + ' with individual tracks')

        ssfuncs.plot_skill_with_indv(
            buoyids,
            datasets_dict,
            skill,
            plotcolors,
            skill_label_params,
            lwidth=2,
            labs=True,
            leg=False,
            ax=ax)

        if zone != 'all':
            ax.set_title(zone, fontsize=14)

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

        combinedskfignamepng =\
            "mean_{}_including-indv-tracks_{}.png".format(skill, zone)

        plt.savefig(
            os.path.join(savedir, combinedskfignamepng),
            bbox_inches='tight',
            dpi=300,
            papertype='letter',
            orientation='portrait'
        )
        logger.info(
            'saving figure ' + os.path.join(
                savedir, combinedskfignamepng
                )
            )
        plt.close()

